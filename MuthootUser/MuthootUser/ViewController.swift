//
//  ViewController.swift
//  MuthootUser
//
//  Created by SmartConnect Technologies on 04/05/21.
//  Copyright © 2021 SmartConnect Technologies. All rights reserved.
//

import UIKit
import Muthoot

class ViewController: UIViewController {

   // let sdk = VersionControlSocketHelper()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    fileprivate func setupUI() {

        // Start Button
        let btnStart = UIButton()
        btnStart.layer.cornerRadius = 10
        btnStart.backgroundColor = Colors.theme
        btnStart.setTitle("Start", for: .normal)
        btnStart.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        btnStart.addTarget(self, action: #selector(handleStart(_:)), for: .touchUpInside)
        view.addSubview(btnStart)
        btnStart.enableAutolayout()
        btnStart.centerX()
        btnStart.centerY()
        btnStart.fixWidth(120)
        btnStart.fixHeight(40)
    }
    
    @objc fileprivate func handleStart(_ sender : UIButton) {
        let vc = MobileVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

