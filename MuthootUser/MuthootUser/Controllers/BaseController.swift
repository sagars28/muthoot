//
//  BaseController.swift
//  MuthootUser
//
//  Created by Sagar on 20/07/21.
//  Copyright © 2021 SmartConnect Technologies. All rights reserved.
//

import UIKit

public class BaseController: UIViewController {

    let viewNavigationBarBase = UIView()
    let lblNavTitle = UILabel()
    let lblNavTitleMiddle = UILabel()
    let btnNavBack = UIButton()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override public var prefersStatusBarHidden: Bool {
        return false
    }
    
    public static var getStatusBarHeight : CGFloat {
        var statusBarHeight: CGFloat = 0
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            statusBarHeight = window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        } else {
            statusBarHeight = UIApplication.shared.statusBarFrame.height
        }
        return statusBarHeight
    }
    
    @objc open func handToolBar() {
        view.endEditing(true)
    }
    
    //MARK: NAVIGATION BAR
        open func createNavigationBar(_ navTitle: String) {
            
            viewNavigationBarBase.backgroundColor = Colors.theme
            view.addSubview(viewNavigationBarBase)
            viewNavigationBarBase.enableAutolayout()
            viewNavigationBarBase.leadingMargin(0)
            viewNavigationBarBase.trailingMargin(0)
            viewNavigationBarBase.topMargin(0)
            viewNavigationBarBase.fixHeight(BaseController.getStatusBarHeight + topbarHeight)
            
            let logoBackView = UIImageView()
            logoBackView.clipsToBounds = true
            logoBackView.backgroundColor = .white
            logoBackView.layer.cornerRadius = 3
            logoBackView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
            viewNavigationBarBase.addSubview(logoBackView)
            logoBackView.enableAutolayout()
            logoBackView.bottomMargin(16)
            logoBackView.leadingMargin(16)
            logoBackView.fixWidth(30)
            logoBackView.fixHeight(20)
            
            let ivHeader = UIImageView()
            ivHeader.clipsToBounds = true
            ivHeader.contentMode = .scaleAspectFill
            ivHeader.image = UIImage(named: "logo", in: Bundle(for: type(of: self
                )), compatibleWith: nil)
            ivHeader.backgroundColor = Colors.theme
            logoBackView.addSubview(ivHeader)
            ivHeader.enableAutolayout()
            ivHeader.centerX()
            ivHeader.bottomMargin(0)
            ivHeader.fixWidth(20)
            ivHeader.fixHeight(18)
    
            
            lblNavTitleMiddle.text = navTitle
            lblNavTitleMiddle.textAlignment = .left
            lblNavTitleMiddle.textColor = .white
            lblNavTitleMiddle.numberOfLines = 2
            lblNavTitleMiddle.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title + 6)
          //  lblNavTitleMiddle.setCharacterSpacing(0.15)
            viewNavigationBarBase.addSubview(lblNavTitleMiddle)
            lblNavTitleMiddle.enableAutolayout()
            lblNavTitleMiddle.add(toRight: 16, of: logoBackView)
            lblNavTitleMiddle.centerY(to: ivHeader)
        }
    
    //MARK:- Custom Toast
    func customToast(toastText: String, withStatus:String) {
        if toastText == "" {
            return
        }
        
        let labelToast = PaddingLabel()
        labelToast.text = toastText
        labelToast.textColor = .white
        labelToast.numberOfLines = 0
        labelToast.font = UIFont.systemFont(ofSize: Device.isIpad ? 20 : 16)
        labelToast.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        
        UIApplication.shared.keyWindow?.addSubview(labelToast)
        labelToast.enableAutolayout()
        labelToast.fixWidth(screenWidth)
        labelToast.centerX()
        labelToast.flexibleHeightGreater(40)
        labelToast.topMargin(Constraints.top + self.topHeight + 10)
        
        if withStatus == toastFailure {
            labelToast.backgroundColor = Colors.theme
        }
        
        UIView.animate(withDuration: 5.0, delay: 0.0, options: [], animations: {() -> Void in
            labelToast.alpha = 0
        }, completion: {(_) in
            labelToast.removeFromSuperview()
        })
        //}
    }
    
    open func createNavigationBarWithBack(_ navTitle: String) {
        
        
        viewNavigationBarBase.backgroundColor = Colors.theme
        view.addSubview(viewNavigationBarBase)
        viewNavigationBarBase.enableAutolayout()
        viewNavigationBarBase.leadingMargin(0)
        viewNavigationBarBase.trailingMargin(0)
        viewNavigationBarBase.topMargin(0)
        viewNavigationBarBase.fixHeight(BaseController.getStatusBarHeight + topbarHeight)
        
        // Back Button
        btnNavBack.tintColor = .white
        btnNavBack.setImage( UIImage(named: "arrowLeft", in: Bundle(for: type(of: self
        )), compatibleWith: nil)?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnNavBack.imageView?.tintColor = .white
        btnNavBack.addTarget(self, action: #selector(handleBack(_:)), for: .touchUpInside)
        viewNavigationBarBase.addSubview(btnNavBack)
        btnNavBack.enableAutolayout()
        btnNavBack.leadingMargin(8)
        btnNavBack.bottomMargin(8)
        btnNavBack.fixHeight(35)
        btnNavBack.fixWidth(35)
        
        let logoBackView = UIImageView()
        logoBackView.clipsToBounds = true
        logoBackView.backgroundColor = .white
        logoBackView.layer.cornerRadius = 3
        logoBackView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        viewNavigationBarBase.addSubview(logoBackView)
        logoBackView.enableAutolayout()
        logoBackView.bottomMargin(16)
        logoBackView.add(toRight: 8, of: btnNavBack)
        logoBackView.fixWidth(30)
        logoBackView.fixHeight(20)
        
        let ivHeader = UIImageView()
        ivHeader.clipsToBounds = true
        ivHeader.contentMode = .scaleAspectFill
        ivHeader.image = UIImage(named: "logo", in: Bundle(for: type(of: self
            )), compatibleWith: nil)
        ivHeader.backgroundColor = Colors.theme
        logoBackView.addSubview(ivHeader)
        ivHeader.enableAutolayout()
        ivHeader.centerX()
        ivHeader.bottomMargin(0)
        ivHeader.fixWidth(20)
        ivHeader.fixHeight(18)
        
        lblNavTitleMiddle.text = navTitle
        lblNavTitleMiddle.textAlignment = .left
        lblNavTitleMiddle.textColor = .white
        lblNavTitleMiddle.numberOfLines = 2
        lblNavTitleMiddle.font = UIFont(name: Fonts.latoRegular, size: TextSize.title + 6)
        viewNavigationBarBase.addSubview(lblNavTitleMiddle)
        lblNavTitleMiddle.enableAutolayout()
        lblNavTitleMiddle.add(toRight: 16, of: logoBackView)
        lblNavTitleMiddle.centerY(to: ivHeader)
    }
    
    @objc func handleBack(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    
    func showToast(message : String) {
        
        let toastLabel = PaddingLabel()
        toastLabel.numberOfLines = 0
        toastLabel.clipsToBounds = true
        toastLabel.backgroundColor = UIColor.white
        toastLabel.textColor = UIColor.black
        toastLabel.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.toastShadow()
        //toastLabel.layer.borderWidth = 0.5
        //toastLabel.layer.borderColor = Colors.gray.cgColor
        self.view.addSubview(toastLabel)
        toastLabel.enableAutolayout()
        toastLabel.flexibleWidthSmaller(screenWidth - 40)
        toastLabel.centerX()
        toastLabel.flexibleHeightGreater(40)
        toastLabel.bottomMargin(100)
        
        UIView.animate(withDuration: 4.0, delay: 0.0, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func getAttrString(_ strFirst: String, _ strSecond: String) -> NSAttributedString {
        
        let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 2
        
        let attrStr = NSMutableAttributedString(string: strFirst, attributes: [.font : UIFont(name: Fonts.acuminProRegular, size: TextSize.title - 1)!, .foregroundColor : Colors.textGray,.paragraphStyle : paragraphStyle])
        attrStr.append(NSAttributedString(string: strSecond, attributes: [.font : UIFont(name: Fonts.latoRegular, size: TextSize.title)!,.foregroundColor : UIColor.black, .paragraphStyle : paragraphStyle]))
        return attrStr
    }
    
    func getAttrString2(_ strFirst: String, _ strSecond: String) -> NSAttributedString {
        
        let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 3
        
        let attrStr = NSMutableAttributedString(string: strFirst, attributes: [.font : UIFont(name: Fonts.latoRegular, size: TextSize.title + 1)!, .foregroundColor : Colors.textGray,.paragraphStyle : paragraphStyle])
        attrStr.append(NSAttributedString(string: strSecond, attributes: [.font : UIFont(name: Fonts.latoRegular, size: TextSize.title + 2)!,.foregroundColor : Colors.darkGray, .paragraphStyle : paragraphStyle]))
        return attrStr
    }
}
