//
//  StaffPopup.swift
//  Muthoot Track
//
//  Created by Sagar on 19/07/21.
//


import UIKit
public class StaffPopup: UIView {
    
     var arrData : [String]?
    
     let backView = UIView()
     let iv = UIImageView()
    
    var options = [String]()
    var optionTitle = ""
    
    let tblViewSlider = UITableView()
    var tblViewHeightConstraint : NSLayoutConstraint?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    public override func layoutSubviews() {
        if ((tblViewSlider.contentSize.height + 100) > screenHeight * 0.5) {
            tblViewHeightConstraint?.constant = screenHeight * 0.4
        }
        else {
            tblViewHeightConstraint?.constant = tblViewSlider.contentSize.height
        }
    }

    func createUI() {
        
        //options = arrOption
        //optionTitle = title
        
        self.isUserInteractionEnabled = false

        let btnBack = UIButton()
        btnBack.isUserInteractionEnabled = false
        btnBack.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        addSubview(btnBack)
        btnBack.enableAutolayout()
        btnBack.bottomMargin(0)
        btnBack.leadingMargin(0)
        btnBack.trailingMargin(0)
        
        backView.layer.cornerRadius = 30
        backView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        backView.clipsToBounds = true
        backView.backgroundColor = .red
        btnBack.addSubview(backView)
        backView.enableAutolayout()
        backView.topMargin(0)
        backView.leadingMargin(0)
        backView.trailingMargin(0)
        backView.bottomMargin(0)

        let lblSignature = UILabel()
        lblSignature.text = "Items"
        lblSignature.textColor = .black
        lblSignature.font = UIFont(name: Fonts.HelveticaNeueMedium, size: 18)
        backView.addSubview(lblSignature)
        lblSignature.enableAutolayout()
        lblSignature.topMargin(16)
        lblSignature.leadingMargin(20)
        lblSignature.trailingMargin(20)
        lblSignature.fixHeight(30)
        
        let separator = UIView()
        separator.clipsToBounds = true
        separator.backgroundColor = Colors.separator
        backView.addSubview(separator)
        separator.enableAutolayout()
        separator.belowView(16, to: lblSignature)
        separator.trailingMargin(0)
        separator.leadingMargin(0)
        separator.fixHeight(2)
        
        let separator2 = UIView()
        separator2.clipsToBounds = true
        separator2.backgroundColor = Colors.separator
        backView.addSubview(separator2)
        separator2.enableAutolayout()
        separator2.bottomMargin(Constraints.bottom + 50)
        separator2.trailingMargin(0)
        separator2.leadingMargin(0)
        separator2.fixHeight(2)
        
        tblViewSlider.backgroundColor = .white
        tblViewSlider.delegate = self
        tblViewSlider.dataSource = self
        tblViewSlider.bounces = false
        tblViewSlider.separatorStyle = .singleLine
        tblViewSlider.backgroundColor = UIColor.white
        tblViewSlider.estimatedRowHeight = 50
        tblViewSlider.sectionFooterHeight = .leastNonzeroMagnitude
        tblViewSlider.showsVerticalScrollIndicator = false
        tblViewSlider.isScrollEnabled = true
        tblViewSlider.register(OrnamentDisplayCell.self, forCellReuseIdentifier: "cell")
        backView.addSubview(tblViewSlider)
        tblViewSlider.enableAutolayout()
        tblViewSlider.leadingMargin(0)
        tblViewSlider.trailingMargin(0)
        tblViewSlider.belowView(0, to: separator)
        tblViewSlider.aboveView(0, to: separator2)
        tblViewHeightConstraint = tblViewSlider.heightAnchor.constraint(greaterThanOrEqualToConstant: 10)
        tblViewHeightConstraint?.isActive = true
        
        let btnClose = UIButton()
        btnClose.titleLabel?.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title + 2)
        btnClose.setTitle("CLOSE", for: .normal)
        btnClose.setTitleColor(Colors.red, for: .normal)
        btnClose.addTarget(self, action: #selector(removeSelf), for: .touchUpInside)
        backView.addSubview(btnClose)
        btnClose.enableAutolayout()
        btnClose.belowView(8, to: separator2)
        btnClose.trailingMargin(20)
        btnClose.fixHeight(40)
        
        backView.frame.origin.y = screenHeight
        animate()
    }
    
    
    func animate() {
        UIView.animate(withDuration: 0.3) {
            self.backView.frame.origin.y = screenHeight - (self.tblViewSlider.contentSize.height)
        }
    }

    func animateDissmiss() {
        UIView.animate(withDuration: 0.3, animations: {
            self.backView.frame.origin.y = screenHeight
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
    @objc func removeSelf() {
        animateDissmiss()
    }
}

extension StaffPopup : UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrData?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = arrData?[indexPath.row]
        return cell
    }
}
