//
//  SignaturePopupView.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 6/4/21.
//

import Foundation
import UIKit

protocol SignatureDelegate {
    func SignatureText(sign: String)
}

class Lines  {
    static let shared = Lines()
    var lines = [Line]()
}


public class SignaturePopup: UIView {

    var delegate: SignatureDelegate?
     let backView = UIView()
     let signView = Canvas()
     let iv = UIImageView()
    
    var options = [String]()
    var optionTitle = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func createUI() {
        
        //options = arrOption
        //optionTitle = title
        Lines.shared.lines.removeAll()
        signView.clear()

        let btnBack = UIButton()
        btnBack.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        addSubview(btnBack)
        btnBack.enableAutolayout()
        btnBack.topMargin(0)
        btnBack.bottomMargin(0)
        btnBack.leadingMargin(0)
        btnBack.trailingMargin(0)
        
        backView.layer.cornerRadius = 30
        backView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        backView.clipsToBounds = true
        backView.frame =  CGRect(x: 0, y: screenHeight, width: screenWidth, height: screenHeight * 0.6)
        backView.backgroundColor = .white
        btnBack.addSubview(backView)
        
        let lblSignature = UILabel()
        lblSignature.text = "Signature"
        lblSignature.textColor = .black
        lblSignature.font = UIFont(name: Fonts.HelveticaNeueMedium, size: 18)
        backView.addSubview(lblSignature)
        lblSignature.enableAutolayout()
        lblSignature.topMargin(16)
        lblSignature.leadingMargin(20)
        lblSignature.trailingMargin(20)
        lblSignature.fixHeight(30)
        
        let separator = UIView()
        separator.clipsToBounds = true
        separator.backgroundColor = Colors.gray
        backView.addSubview(separator)
        separator.enableAutolayout()
        separator.belowView(16, to: lblSignature)
        separator.trailingMargin(0)
        separator.leadingMargin(0)
        separator.fixHeight(0.8)
        
        let separator2 = UIView()
        separator2.clipsToBounds = true
        separator2.backgroundColor = Colors.gray
        backView.addSubview(separator2)
        separator2.enableAutolayout()
        separator2.bottomMargin(Constraints.bottom + 50)
        separator2.trailingMargin(0)
        separator2.leadingMargin(0)
        separator2.fixHeight(0.8)
    
        signView.setStrokeColor(color: .black)
        signView.setStrokeWidth(width: 2)
        signView.clipsToBounds = true
        signView.backgroundColor = .white
        backView.addSubview(signView)
        signView.enableAutolayout()
        signView.belowView(0, to: separator)
        signView.trailingMargin(0)
        signView.leadingMargin(0)
        signView.aboveView(0, to: separator2)
        
        let btnCancel = UIButton()
        btnCancel.clipsToBounds = true
        btnCancel.setTitle("CLOSE", for: .normal)
        btnCancel.setTitleColor(.red, for: .normal)
        btnCancel.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: 16)
        btnCancel.addTarget(self, action: #selector(removeSelf), for: UIControl.Event.touchUpInside)
        backView.addSubview(btnCancel)
        btnCancel.enableAutolayout()
        btnCancel.belowView(8, to: separator2)
        btnCancel.trailingMargin(30)
        btnCancel.fixHeight(40)
        
        let btnClear = UIButton()
        btnClear.clipsToBounds = true
        btnClear.setTitle("CLEAR", for: .normal)
        btnClear.setTitleColor(Colors.theme, for: .normal)
        btnClear.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: 16)
        btnClear.addTarget(self, action: #selector(HandleClear(_:)), for: .touchUpInside)
        backView.addSubview(btnClear)
        btnClear.enableAutolayout()
        btnClear.belowView(8, to: separator2)
        btnClear.add(toLeft: 16, of: btnCancel)
        btnClear.fixHeight(40)
        
        let btnSave = UIButton()
        btnSave.clipsToBounds = true
        btnSave.setTitle("SAVE", for: .normal)
        btnSave.setTitleColor(Colors.theme, for: .normal)
        btnSave.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: 16)
        btnSave.addTarget(self, action: #selector(HandleSave(_:)), for: .touchUpInside)
        backView.addSubview(btnSave)
        btnSave.enableAutolayout()
        btnSave.belowView(8, to: separator2)
        btnSave.add(toLeft: 16, of: btnClear)
        btnSave.fixHeight(40)
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFit
        backView.addSubview(iv)
        iv.enableAutolayout()
        iv.fixHeight(200)
        iv.trailingMargin(8)
        iv.belowView(8, to: separator)
        iv.fixHeight(200)
        
        animate()
    }
    
    @objc func HandleClear(_ sender : UIButton) {
        signView.clear()
    }
    
    @objc func HandleSave(_ sender : UIButton) {
        
        if (Lines.shared.lines.isEmpty) {
        }
        else {
            let base64str = convertImageToBase64String(img: signView.asImage())
            delegate?.SignatureText(sign: base64str)
            removeSelf()
        }
    }
    
    
    
   /* public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.SelectedText(tag: tag, text: options[indexPath.row], index: indexPath.row)
        animateDissmiss()
    }*/

    func animate() {
        UIView.animate(withDuration: 0.3) {
            self.backView.frame.origin.y = screenHeight - (screenHeight * 0.6)
        }
    }

    func animateDissmiss() {
        UIView.animate(withDuration: 0.3, animations: {
            self.backView.frame.origin.y = screenHeight
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
    @objc func removeSelf() {
        animateDissmiss()
    }
}


// Signature View
struct Line {
    let strokeWidth: Float
    let color: UIColor
    var points: [CGPoint]
}

class Canvas: UIView {

    // public function
    fileprivate var strokeColor = UIColor.black
    fileprivate var strokeWidth: Float = 1

    func setStrokeWidth(width: Float) {
        self.strokeWidth = width
    }

    func setStrokeColor(color: UIColor) {
        self.strokeColor = color
    }

    func undo() {
        _ = Lines.shared.lines.popLast()
        setNeedsDisplay()
    }

    func clear() {
        Lines.shared.lines.removeAll()
        setNeedsDisplay()
    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)

        guard let context = UIGraphicsGetCurrentContext() else { return }

        Lines.shared.lines.forEach { (line) in
            context.setStrokeColor(line.color.cgColor)
            context.setLineWidth(CGFloat(line.strokeWidth))
            context.setLineCap(.round)
            for (i, p) in line.points.enumerated() {
                if i == 0 {
                    context.move(to: p)
                } else {
                    context.addLine(to: p)
                }
            }
            context.strokePath()
        }
    }
 
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        Lines.shared.lines.append(Line.init(strokeWidth: strokeWidth, color: strokeColor, points: []))
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let point = touches.first?.location(in: self) else { return }
        guard var lastLine = Lines.shared.lines.popLast() else { return }
        lastLine.points.append(point)
        Lines.shared.lines.append(lastLine)
        setNeedsDisplay()
    }
}

extension UIView {
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}

