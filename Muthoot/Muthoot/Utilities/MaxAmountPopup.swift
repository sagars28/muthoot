//
//  MaxAmountPopup.swift
//  Muthoot
//
//  Created by Sagar on 07/01/22.
//

import Foundation
import UIKit

protocol MaxAmountPr {
    func value()
}

class MaxAmountPopup : UIView {
    
    var delegate : MaxAmountPr?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func createUI(maxAmnt : String) {
        
        let btnBack = UIButton()
        btnBack.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        addSubview(btnBack)
        btnBack.enableAutolayout()
        btnBack.topMargin(0)
        btnBack.bottomMargin(0)
        btnBack.leadingMargin(0)
        btnBack.trailingMargin(0)
    
        let viewBack = UIView()
        viewBack.backgroundColor = UIColor.white
        btnBack.addSubview(viewBack)
        viewBack.enableAutolayout()
        viewBack.centerY()
        viewBack.leadingMargin(20)
        viewBack.trailingMargin(20)
        
        let lblMsg = UILabel()
        lblMsg.numberOfLines = 0
        lblMsg.text = "Max loan amount eligible for door-step service is ₹ \(maxAmnt) only.\n\nPress Cancel to modify it again."
        lblMsg.font = UIFont(name: Fonts.latoRegular, size: 17)
        viewBack.addSubview(lblMsg)
        lblMsg.enableAutolayout()
        lblMsg.topMargin(20)
        lblMsg.leadingMargin(20)
        lblMsg.trailingMargin(20)
        
        let btnContinue = UIButton()
        btnContinue.setTitle("CONTINUE", for: .normal)
        btnContinue.titleLabel?.font = UIFont(name: Fonts.latoRegular, size: 17)
        btnContinue.setTitleColor(Colors.theme, for: .normal)
        btnContinue.addTarget(self, action: #selector(handleContinueBtn(_:)), for: .touchUpInside)
        viewBack.addSubview(btnContinue)
        btnContinue.enableAutolayout()
        btnContinue.belowView(20, to: lblMsg)
        btnContinue.trailingMargin(20)
        btnContinue.fixHeight(40)
        btnContinue.bottomMargin(20)
        
        let btnCancel = UIButton()
        btnCancel.setTitle("CANCEL", for: .normal)
        btnCancel.titleLabel?.font = UIFont(name: Fonts.latoRegular, size: 17)
        btnCancel.setTitleColor(Colors.red, for: .normal)
        btnCancel.addTarget(self, action: #selector(removeSelf), for: .touchUpInside)
        viewBack.addSubview(btnCancel)
        btnCancel.enableAutolayout()
        btnCancel.centerY(to: btnContinue)
        btnCancel.add(toLeft: 20, of: btnContinue)
        btnCancel.fixHeight(40)
    }
    
    func animateDissmiss() {
        self.removeFromSuperview()
    }
    
    @objc func removeSelf() {
        animateDissmiss()
    }
    
    @objc func handleContinueBtn(_ sender : UIButton) {
        delegate?.value()
        removeSelf()
    }
}
