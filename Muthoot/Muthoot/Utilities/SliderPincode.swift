//
//  SliderPincode.swift
//  Muthoot Track
//
//  Created by Sagar on 08/12/21.
//

import UIKit

class SliderPincode: UIView, UITableViewDelegate, UITableViewDataSource {

    var delegate: SelectionDelegate?

    let tblViewSlider = UITableView()
    var tblViewHeightConstraint : NSLayoutConstraint?
    
    var options = [String]()
    var optionTitle = ""

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func layoutSubviews() {
        if (tblViewSlider.contentSize.height > screenHeight * 0.4) {
            tblViewHeightConstraint?.constant = screenHeight * 0.4
        }
        else {
            tblViewHeightConstraint?.constant = tblViewSlider.contentSize.height
        }
    }

    func createUI(title: String, arrOption: [String], y : CGFloat) {

        options = arrOption
        optionTitle = title

       /* let btnBack = UIButton()
        btnBack.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        btnBack.addTarget(self, action: #selector(removeSelf), for: UIControl.Event.touchUpInside)
        addSubview(btnBack)
        btnBack.enableAutolayout()
        btnBack.topMargin(0)
        btnBack.bottomMargin(0)
        btnBack.leadingMargin(0)
        btnBack.trailingMargin(0)*/

        tblViewSlider.backgroundColor = .white
        tblViewSlider.delegate = self
        tblViewSlider.dataSource = self
        tblViewSlider.bounces = false
        tblViewSlider.separatorStyle = .none
        tblViewSlider.backgroundColor = UIColor.white
        tblViewSlider.rowHeight = 50
        tblViewSlider.sectionHeaderHeight = 0
        tblViewSlider.sectionFooterHeight = .leastNonzeroMagnitude
        tblViewSlider.showsVerticalScrollIndicator = false
        tblViewSlider.isScrollEnabled = true
        tblViewSlider.layer.borderWidth = 0.8
        tblViewSlider.layer.borderColor = UIColor.lightGray.cgColor
        tblViewSlider.keyboardDismissMode = .onDrag
        if #available(iOS 15.0, *) {
            tblViewSlider.sectionHeaderTopPadding = 0
        }
        self.addSubview(tblViewSlider)
        tblViewSlider.enableAutolayout()
        tblViewSlider.topMargin(y)
        tblViewSlider.leadingMargin(28)
        tblViewSlider.trailingMargin(28)
        tblViewHeightConstraint = tblViewSlider.heightAnchor.constraint(greaterThanOrEqualToConstant: 20)
        tblViewHeightConstraint?.isActive = true
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: "cellID")

        cell.backgroundColor = .clear
        cell.textLabel?.textColor = .black
        cell.textLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        cell.textLabel?.text = options[indexPath.row]
        return cell
    }

    /*func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewBooking = UIView()
       // viewBooking.layer.cornerRadius = 12
       // viewBooking.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        viewBooking.backgroundColor = Colors.theme

        let lblBooking = UILabel()
        lblBooking.textColor = UIColor.white
        lblBooking.textAlignment = .left
        lblBooking.backgroundColor = .clear
        lblBooking.text = optionTitle
        lblBooking.font = UIFont.init(name: Fonts.HelveticaNeueMedium, size: TextSize.title+2)
        viewBooking.addSubview(lblBooking)
        lblBooking.enableAutolayout()
        lblBooking.centerY()
        lblBooking.leadingMargin(16)
        lblBooking.fixHeight(40)
        lblBooking.trailingMargin(20)

        return viewBooking
    }*/

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.SelectedText(tag: tag, text: options[indexPath.row], index: indexPath.row)
        animateDissmiss()
    }

    func animate() {
        UIView.animate(withDuration: 0.3) {
            self.tblViewSlider.frame.origin.y = screenHeight - (screenHeight * 0.4)
        }
    }

    func animateDissmiss() {
        UIView.animate(withDuration: 0.3, animations: {
        }) { (_) in
            self.removeFromSuperview()
        }
    }

    @objc func removeSelf() {
        animateDissmiss()
    }
}
