//
//  BottomBtnBar.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/7/21.
//

import UIKit

class BottomBtnBar : UIView {
    
    let btnBack = UIButton()
    let btnNext = UIButton()
    
    func setView() {
        
        self.backgroundColor = Colors.theme
        btnBack.contentHorizontalAlignment = .left
        btnBack.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 2)
        btnBack.setImage(UIImage(named: "back", in: Bundle(for: type(of: self
        )), compatibleWith: nil), for: .normal)
        btnBack.setTitle("BACK", for: .normal)
        btnBack.setTitleColor(.white, for: .normal)
        btnBack.titleEdgeInsets = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0)
       // btnBack.addTarget(self,action:#selector(handlePincode),for:.touchUpInside)
        addSubview(btnBack)
        btnBack.enableAutolayout()
        btnBack.centerY()
        btnBack.fixHeight(50)
        btnBack.leadingMargin(16)
        btnBack.fixWidth(150)
        
        btnNext.contentHorizontalAlignment = .right
        btnNext.semanticContentAttribute = .forceRightToLeft
        btnNext.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 2)
        btnNext.setImage(UIImage(named: "arrowRight", in: Bundle(for: type(of: self
                )), compatibleWith: nil), for: .normal)
        btnNext.setTitle("NEXT", for: .normal)
        btnNext.setTitleColor(.white, for: .normal)
        btnNext.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 14)
        // btnBack.addTarget(self,action:#selector(handlePincode),for:.touchUpInside)
        addSubview(btnNext)
        btnNext.enableAutolayout()
        btnNext.centerY()
        btnNext.fixHeight(50)
        btnNext.trailingMargin(20)
        btnNext.fixWidth(120)
       // btnNext.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
