//
//  SocketHelper.swift
//  Muthoot Track
//
//  Created by SmartConnect Technologies on 27/02/21.
//
// This Socket is use only for to check version

import Foundation
import SocketIO
import UIKit

public class VersionControlSocketHelper: NSObject {
    
    static let shared = VersionControlSocketHelper()
    public var manager: SocketManager?
    public var socket: SocketIOClient?
    
    
    // old url = http://103.249.134.81:3503/
    
    

    public override init() {
        super.init()
        configureSocketClient()
    }
    
    private func configureSocketClient() {
        
        guard let url = URL(string: MuthootManager.url) else { return  }
        
        manager = SocketManager(socketURL: url, config: [.log(false), .compress,.path("/version")])
        guard let manager = manager else {
            return
        }
        socket = manager.socket(forNamespace: "/**********")
    }
    
    
    public func establishConnection() {
        socket = manager?.defaultSocket
        socket?.on(clientEvent: .connect) { [self] data, ack in
            //print(self.socket!.status as Any)
        }
        
        socket?.on(clientEvent: .disconnect) { [self] data, ack in
            //print(self.socket!.status as Any)
        }
        socket?.connect()
        socketListener()
    }
    
   public func firstEmits() {
    
        // Create Dictionary
       let mainDict : [String:Any] = ["appOS" : "ios", "versionNumber" : MuthootManager.version, "deviceID" :UserDetails.udid, "versionName" : MuthootManager.build]
        //print(mainDict)
       
        guard let data1 = UIApplication.jsonData(from: mainDict) else {
            return
        }
       
        // use version number and build version number as Encryption Key
       DataFile.updateKeyIV(passwordString:"\(MuthootManager.version)\(MuthootManager.build)") //Save key
        let encryptedData = DataFile.encryptedData(Payload: data1, attachePublicKey: false)
    
        //Create paramter using encrypted data as follow
        let dic = ["eventName":"GETCUSTOMERVERSIONS", "data": encryptedData] as [String : Any]
    
        // Emits "GETCUSTOMERVERSIONS" to get public and private key
        emitForSocket(eventName: "req", arg: dic)
   }
    
    func socketListener() {
        
        socket?.on("res") { (data, ack) in
       // print(data)
            
            if data.count > 0 {
                do {
                    if let AuthDataDict = (data[0]) as? NSDictionary {
                      //  print(AuthDataDict)
                        let eventName = AuthDataDict["eventName"] as! String
                        
                        switch eventName {
                        case "config" :
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "Config"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                            break
                            
                        default : break
                        }
                    }
                }
            }
            
            if let str = data as? [String] {
                if str.count > 0 {
                    let separatedStr = str[0].components(separatedBy: ".")
                    if (separatedStr.count >= 2) {
                        DataFile.updateKeyIV(passwordString:UserDetails.udid) //Save key
                        let decryptedStr = DataFile.decryptedData(encrypted: separatedStr[0])
                        let dict = convertToDictionary(text : decryptedStr)
                        //print(dict)
                        if let versionData = dict?["versionData"] as? [String:Any] {
                            if let url = versionData["customerServer"] as? String {
                                // Save Requred Data In UserDefaults
                                UserDetails.privateKey = separatedStr[2]
                                UserDetails.publicKey = separatedStr[1]
                                UserDetails.baseURL = url
                                VersionControlSocketHelper.shared.closeConnection()
                                SocketHelper.shared.establishConnection()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func closeConnection() {
        socket?.removeAllHandlers()
        socket?.disconnect()
    }
    
    func emitForSocket(eventName: String, arg: Any) {
       // print(arg)
        socket?.emit(eventName,with: [arg])
    }
}


