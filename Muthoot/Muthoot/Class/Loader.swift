//
//  Loader.swift
//  Muthoot Track
//
//  Created by Sagar on 16/06/21.
//

import UIKit

class Loader : NSObject {
    
    static let shared = Loader()
    
    var myActivityIndicator:UIActivityIndicatorView!
    let btnBack = UIButton()
    
    func StartActivityIndicator(obj:UIViewController)
    {
        btnBack.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        let window = UIApplication.shared.windows.first { $0.isKeyWindow }
        window?.addSubview(btnBack)
        btnBack.enableAutolayout()
        btnBack.topMargin(0)
        btnBack.bottomMargin(0)
        btnBack.leadingMargin(0)
        btnBack.trailingMargin(0)
        
        self.myActivityIndicator = UIActivityIndicatorView()
        
        if #available(iOS 13.0, *) {
            self.myActivityIndicator.style = .large
        } else {
            self.myActivityIndicator.style = .gray
        }
        self.myActivityIndicator.center = obj.view.center;
        self.myActivityIndicator.color = Colors.theme
        
        btnBack.addSubview(myActivityIndicator);
        self.myActivityIndicator.startAnimating();
    }
    
    func StopActivityIndicator(obj:UIViewController)-> Void
    {
        guard let activity = myActivityIndicator else {
            return
        }
        activity.stopAnimating()
        activity.removeFromSuperview()
        btnBack.removeFromSuperview()
    }
}
