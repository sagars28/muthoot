//
//  SocketHelper.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/14/21.
//

import Foundation
import SocketIO
import UIKit

public class SocketHelper: NSObject {
    
    
    static let shared = SocketHelper()
    public var manager: SocketManager?
    public var socket: SocketIOClient?
    
    
    public override init() {
        super.init()
        configureSocketClient()
    }
    
    // old = UserDetails.baseURL
    
    private func configureSocketClient() {
        
        guard let url = URL(string: MuthootManager.url) else { return  }
        
        manager = SocketManager(socketURL: url, config: [.log(false), .compress, .path("/customer")])
        guard let manager = manager else {
            return
        }
        socket = manager.socket(forNamespace: "/**********")
    }
    
    public func establishConnection() {
        socket = manager?.defaultSocket
        socket?.on(clientEvent: .connect) { [self] data, ack in
            //print("Customer Socket", self.socket!.status as Any)
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ConfigMainSocket"), object: nil, userInfo: nil)
                Loader.shared.StopActivityIndicator(obj: base)
            }
        }
        
        socket?.on(clientEvent: .disconnect) { [self] data, ack in
            //print(self.socket!.status as Any)
        }
        socket?.connect()
        socketListener()
    }
    
    func socketListener() {
        socket?.on("res") { (data, ack) in
       // print(data)
            
            if data.count > 0 {
                do {
                    let AuthDataDict = (data[0]) as! NSDictionary
                    let eventName = AuthDataDict["eventName"] as! String
                    
                    switch eventName {
                    
                    case "config" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ConfigMainSocket"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "pincodeSearchResponse" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PincodeSearchResponse"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                    
                    case "moveToLoanStart" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GetPincode"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "moveToLoanSummary" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MoveToLoanSummary"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "sendPinCodeResponse" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SendPinCodeResponse"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "sendLoanAmountResponse" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SendLoanAmountResponse"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "deleteAddressResponse" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DeleteAddressResponse"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "updateDefaultAddressReponse" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateDefaultAddressReponse"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "sendLoanAddressResponse" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SendLoanAddressResponse"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        
                        break
                        
                    case "sendLoanDateTimeResponse" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SendLoanDateTime"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "successStartLoanCancel" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuccessStartLoanCancel"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "successSubmitReschedule" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuccessSubmitReschedule"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "successSubmitLoanCancel" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuccessSubmitLoanCancel"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "successGetKycForApproval" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuccessGetKycForApproval"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "successApproveKyc" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuccessApproveKyc"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "successCONFIRMSTARTPROCESS" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuccessCONFIRMSTARTPROCESS"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "successCONFIRMLOANDETAILSCUSTOMER" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuccessCONFIRMLOANDETAILSCUSTOMER"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "successStartAgreementCreation" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuccessStartAgreementCreation"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "successGETSIGNEDAGREEMENT" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuccessGETSIGNEDAGREEMENT"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "successSubmitOwnershipAgreement" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuccessSubmitOwnershipAgreement"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "successStartPledgeForm" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuccessStartPledgeForm"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "successSubmitAgreement" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuccessSubmitAgreement"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "moveToRRDateSelector" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MoveToRRDateSelector"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "sendLoanRRDateTimeResponse" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SendLoanRRDateTimeResponse"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "successConfirmGoldHandover" :
                       NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuccessConfirmGoldHandover"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "successSTARTGOLDHANDOVER" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuccessSTARTGOLDHANDOVER"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "sucessConfirmedRepledgeProcess" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SucessConfirmedRepledgeProcess"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                    case "successRejectKyc" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuccessRejectKyc"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        break
                        
                        
                    case "errorResponse" :
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SendLoanDateTime"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SendLoanAddressResponse"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "StartProcess"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SuccessStartLoanCancel"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "successApproveKyc"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SendLoanAmountError"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LoanReleaseRepledgeError"), object: nil, userInfo: data[0] as? [AnyHashable : Any])
                        
                        break
                        
                    default :
                        break
                    }
                }
            }
        }
    }
    
    func closeConnection() {
        socket?.removeAllHandlers()
        socket?.disconnect()
    }
    
    func emitForSocket(eventName: String, arg: Any) {
        
       // print(arg)
        
        if (Internet.isConnected()) {
            socket?.emit(eventName,with: [arg])
        }
        else {
            let base = BaseController()
            base.customToast(toastText: "No Internet", withStatus: toastFailure)
            Loader.shared.StopActivityIndicator(obj: base)
        }
    }
}


