//
//  Extension.swift
//  MuthootUser
//
//  Created by SmartConnect Technologies on 04/05/21.
//  Copyright © 2021 SmartConnect Technologies. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
 
    convenience init?(hexString: String) {
        var chars = Array(hexString.hasPrefix("#") ? hexString.dropFirst() : hexString[...])
        let red, green, blue, alpha: CGFloat
        switch chars.count {
        case 3:
            chars = chars.flatMap { [$0, $0] }
            fallthrough
        case 6:
            chars = ["F","F"] + chars
            fallthrough
        case 8:
            alpha = CGFloat(strtoul(String(chars[0...1]), nil, 16)) / 255
            red   = CGFloat(strtoul(String(chars[2...3]), nil, 16)) / 255
            green = CGFloat(strtoul(String(chars[4...5]), nil, 16)) / 255
            blue  = CGFloat(strtoul(String(chars[6...7]), nil, 16)) / 255
        default:
            return nil
        }
        self.init(red: red, green: green, blue:  blue, alpha: alpha)
    }
}

extension UIToolbar {
    
    open func ToolbarPiker(mySelect : Selector, title:String) -> UIToolbar {
        let toolBar = UIToolbar()
        
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: title, style: UIBarButtonItem.Style.plain, target: self, action: mySelect)
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([ spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
}

func isIphoneX() -> Bool {
    if( UIDevice.current.userInterfaceIdiom == .phone) {
        if(UIScreen.main.bounds.size.height >= 812) {
            return true
        }
    }
    return false
}

extension UIViewController {
    var topbarHeight: CGFloat {
        return (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
}

extension UIViewController {
    var topHeight: CGFloat {
        if #available(iOS 13.0, *) {
            return (view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0.0) +
                (self.navigationController?.navigationBar.frame.height ?? 0.0) + 10
        } else {
            return UIApplication.shared.statusBarFrame.size.height +
                (self.navigationController?.navigationBar.frame.height ?? 0.0) + 10
        }
    }
}

extension UIView {
    func shadow(shadowColor: UIColor, shadowOffset: CGSize, shadowOpacity: Float, shadowRadius: CGFloat) {
        layer.shadowOffset = shadowOffset
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
        layer.masksToBounds = false
        clipsToBounds = false
    }
    
    func addShadow() {
        layer.shadowOffset = CGSize(width: 0, height: 1.5)
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 3
        layer.masksToBounds = false
        clipsToBounds = false
    }
    
    func toastShadow() {
        layer.cornerRadius = 10
        layer.shadowOffset = CGSize(width: 0.5, height: 4.0)
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 5
        layer.masksToBounds = false
        clipsToBounds = false
    }
}

extension UITextField {
    @objc func setProperties() {
        layer.cornerRadius = 5
        layer.borderColor = Colors.textGray.cgColor
        layer.borderWidth = 1
        autocapitalizationType = .none
        autocorrectionType = .no
        returnKeyType = .next
        keyboardType = .default
        textColor = .black
        font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        setLeftPaddingPoints(15)
        setRightPaddingPoints(15)
    }

    func setLeftPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 5, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: bounds.width - (amount + 5), y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension UITextField {
    func setBorderLine() {
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: self.frame.height - 1, width: self.frame.width, height: 1.0)
        bottomLine.backgroundColor = UIColor.black.cgColor
    }
}


public extension UIDevice {
    
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod touch (5th generation)"
            case "iPod7,1":                                 return "iPod touch (6th generation)"
            case "iPod9,1":                                 return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPhone12,1":                              return "iPhone 11"
            case "iPhone12,3":                              return "iPhone 11 Pro"
            case "iPhone12,5":                              return "iPhone 11 Pro Max"
            case "iPhone12,8":                              return "iPhone SE (2nd generation)"
            case "iPhone13,1":                              return "iPhone 12 mini"
            case "iPhone13,2":                              return "iPhone 12"
            case "iPhone13,3":                              return "iPhone 12 Pro"
            case "iPhone13,4":                              return "iPhone 12 Pro Max"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                    return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                      return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                    return "iPad (7th generation)"
            case "iPad11,6", "iPad11,7":                    return "iPad (8th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad11,3", "iPad11,4":                    return "iPad Air (3rd generation)"
            case "iPad13,1", "iPad13,2":                    return "iPad Air (4th generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                    return "iPad mini (5th generation)"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch) (1st generation)"
            case "iPad8,9", "iPad8,10":                     return "iPad Pro (11-inch) (2nd generation)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch) (1st generation)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "iPad8,11", "iPad8,12":                    return "iPad Pro (12.9-inch) (4th generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "AudioAccessory5,1":                       return "HomePod mini"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
}

extension UIApplication {
    
    static func `jsonString`(from object:Any) -> String? {
        let encoder = JSONEncoder()
        var stri = String()
        guard let data = jsonData(from: object) else {
            return nil
        }
        
        do {
            let data1 = try encoder.encode(data)
            stri = String(data: data1, encoding: .utf8)!
        } catch {
        }
        
        return stri
    }
    
    static func jsonData(from object:Any) -> Data? {
        
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        
        return data
    }
}

extension String {
    func convertToDictionary() -> [String: Any]? {
        if let data = data(using: .utf8) {
            return try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        }
        return nil
    }
}




class TopPgrogess : UIView {
    
    let ivFirst = UIImageView()
    let ivSecond = UIImageView()
    let ivThird = UIImageView()
    let ivFourth = UIImageView()
    
    let lblOne = UILabel()
    let lblTwo = UILabel()
    let lblThree = UILabel()
    let lblFour = UILabel()
    
    let btnOne = UIButton()
    let btnTwo = UIButton()
    let btnThree = UIButton()
    let btnFour = UIButton()
    
    func setView() {
        
        // Middle Line
        let lineMiddle = UIView()
        lineMiddle.backgroundColor = Colors.gray
        lineMiddle.clipsToBounds = true
        addSubview(lineMiddle)
        lineMiddle.enableAutolayout()
        lineMiddle.centerY()
        lineMiddle.centerX()
        lineMiddle.fixHeight(1)
        lineMiddle.fixWidth(50)
         
        // Third / Right Image View
        ivThird.isUserInteractionEnabled = false
        ivThird.clipsToBounds = true
        ivThird.backgroundColor = Colors.gray
        ivThird.layer.cornerRadius = 15
        addSubview(ivThird)
        ivThird.enableAutolayout()
        ivThird.centerY(to: lineMiddle)
        ivThird.add(toRight: 8, of: lineMiddle)
        ivThird.fixHeight(30)
        ivThird.fixWidth(30)
        
        lblThree.isUserInteractionEnabled = false
        lblThree.text = "3"
        lblThree.textColor = .white
        lblThree.textAlignment = .center
        ivThird.addSubview(lblThree)
        lblThree.layer.cornerRadius = 18
        lblThree.enableAutolayout()
        lblThree.leadingMargin(0)
        lblThree.topMargin(0)
        lblThree.trailingMargin(0)
        lblThree.bottomMargin(0)
        
        btnThree.backgroundColor = .red
        btnThree.isUserInteractionEnabled = true
        btnThree.clipsToBounds = true
        btnThree.backgroundColor = .clear
        addSubview(btnThree)
        btnThree.enableAutolayout()
        btnThree.centerY(to: lineMiddle)
        btnThree.add(toRight: 8, of: lineMiddle)
        btnThree.fixHeight(30)
        btnThree.fixWidth(30)
        
        
        //Right line after third image
        let lineRight = UIView()
        lineRight.backgroundColor = Colors.gray
        lineRight.clipsToBounds = true
        addSubview(lineRight)
        lineRight.enableAutolayout()
        lineRight.centerY()
        lineRight.add(toRight: 8, of : ivThird)
        lineRight.fixHeight(1)
        lineRight.fixWidth(50)
        
        ivFourth.isUserInteractionEnabled = false
        ivFourth.clipsToBounds = true
        ivFourth.backgroundColor = Colors.gray
        ivFourth.layer.cornerRadius = 15
        addSubview(ivFourth)
        ivFourth.enableAutolayout()
        ivFourth.centerY(to: lineMiddle)
        ivFourth.add(toRight: 8, of: lineRight)
        ivFourth.fixHeight(30)
        ivFourth.fixWidth(30)
        
        lblFour.isUserInteractionEnabled = false
        lblFour.text = "4"
        lblFour.textColor = .white
        lblFour.textAlignment = .center
        ivFourth.addSubview(lblFour)
        lblFour.layer.cornerRadius = 18
        lblFour.enableAutolayout()
        lblFour.leadingMargin(0)
        lblFour.topMargin(0)
        lblFour.trailingMargin(0)
        lblFour.bottomMargin(0)
        

        btnFour.isUserInteractionEnabled = true
        btnFour.clipsToBounds = true
        btnFour.backgroundColor = .clear
        addSubview(btnFour)
        btnFour.enableAutolayout()
        btnFour.centerY(to: lineMiddle)
        btnFour.add(toRight: 8, of: lineRight)
        btnFour.fixHeight(30)
        btnFour.fixWidth(30)
        
        //Left Side
        ivSecond.isUserInteractionEnabled = false
        ivSecond.clipsToBounds = true
        ivSecond.backgroundColor = Colors.gray
        ivSecond.layer.cornerRadius = 15
        addSubview(ivSecond)
        ivSecond.enableAutolayout()
        ivSecond.centerY(to: lineMiddle)
        ivSecond.add(toLeft: 8, of: lineMiddle)
        ivSecond.fixHeight(30)
        ivSecond.fixWidth(30)
    
        lblTwo.isUserInteractionEnabled = false
        lblTwo.text = "2"
        lblTwo.textColor = .white
        lblTwo.textAlignment = .center
        ivSecond.addSubview(lblTwo)
        lblTwo.layer.cornerRadius = 18
        lblTwo.enableAutolayout()
        lblTwo.leadingMargin(0)
        lblTwo.topMargin(0)
        lblTwo.trailingMargin(0)
        lblTwo.bottomMargin(0)
        
        btnTwo.backgroundColor = .red
        btnTwo.isUserInteractionEnabled = true
        btnTwo.clipsToBounds = true
        btnTwo.backgroundColor = .clear
        addSubview(btnTwo)
        btnTwo.enableAutolayout()
        btnTwo.centerY(to: lineMiddle)
        btnTwo.add(toLeft: 8, of: lineMiddle)
        btnTwo.fixHeight(30)
        btnTwo.fixWidth(30)
        
        
        let lineLeft = UIView()
        lineLeft.backgroundColor = Colors.gray
        lineLeft.clipsToBounds = true
        addSubview(lineLeft)
        lineLeft.enableAutolayout()
        lineLeft.centerY()
        lineLeft.add(toLeft: 8, of : ivSecond)
        lineLeft.fixHeight(1)
        lineLeft.fixWidth(50)
        
        ivFirst.isUserInteractionEnabled = false
        ivFirst.clipsToBounds = true
        ivFirst.contentMode = .scaleAspectFit
        ivFirst.backgroundColor = Colors.gray
        ivFirst.layer.cornerRadius = 15
        addSubview(ivFirst)
        ivFirst.enableAutolayout()
        ivFirst.centerY(to: lineMiddle)
        ivFirst.add(toLeft: 8, of: lineLeft)
        ivFirst.fixHeight(30)
        ivFirst.fixWidth(30)
        
        lblOne.text = "1"
        lblOne.textColor = .white
        lblOne.textAlignment = .center
        ivFirst.addSubview(lblOne)
        lblOne.layer.cornerRadius = 18
        lblOne.enableAutolayout()
        lblOne.leadingMargin(0)
        lblOne.topMargin(0)
        lblOne.trailingMargin(0)
        lblOne.bottomMargin(0)
        
        btnOne.backgroundColor = .red
        btnOne.isUserInteractionEnabled = true
        btnOne.clipsToBounds = true
        btnOne.backgroundColor = .clear
        addSubview(btnOne)
        btnOne.enableAutolayout()
        btnOne.centerY(to: lineMiddle)
        btnOne.add(toLeft: 8, of: lineLeft)
        btnOne.fixHeight(30)
        btnOne.fixWidth(30)
        
        let separator = UIView()
        separator.backgroundColor = Colors.gray
        separator.clipsToBounds = true
        addSubview(separator)
        separator.enableAutolayout()
        separator.bottomMargin(1)
        separator.leadingMargin(0)
        separator.fixHeight(1)
        separator.trailingMargin(0)
        
    }
}

class CustomSlider: UISlider {
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        let point = CGPoint(x: bounds.minX, y: bounds.midY)
        return CGRect(origin: point, size: CGSize(width: bounds.width, height: 20))
    }

    /*override func thumbRect(forBounds bounds: CGRect, trackRect rect: CGRect, value: Float) -> CGRect {
        let point = CGPoint(x: bounds.minX, y: bounds.midY)
        return CGRect(origin: point, size: CGSize(width: bounds.width, height: 10))
    }*/
}

func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
           // print(error.localizedDescription)
        }
    }
    return nil
}

extension Array {
    public func toDictionary<Key: Hashable>(with selectKey: (Element) -> Key) -> [Key:Element] {
        var dict = [Key:Element]()
        for element in self {
            dict[selectKey(element)] = element
        }
        return dict
    }
}

class PaddingLabel: UILabel {
    
    var topInset: CGFloat = 10
    var bottomInset: CGFloat = 10
    var leftInset: CGFloat = 10
    var rightInset: CGFloat = 10
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset, height: size.height + topInset + bottomInset)
    }
}

extension Int {
    private static var numberFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        numberFormatter.currencyGroupingSeparator = ","
        numberFormatter.currencySymbol = ""
        //numberFormatter.maximumFractionDigits = 2
        return numberFormatter
    }()
    
    var delimiter: String {
        return Int.numberFormatter.string(from: NSNumber(value: self)) ?? ""
    }
}

extension Float {
    private static var numberFormatter: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.currencySymbol = ""
        numberFormatter.currencyGroupingSeparator = ","
       // numberFormatter.maximumFractionDigits = 2
        return numberFormatter
    }()
    var delimiter: String {
        return Float.numberFormatter.string(from: NSNumber(value: self)) ?? ""
    }
}

extension String {
    func removeFormatAmount() -> String {
        return self.replacingOccurrences(of: ",", with: "", options:
            NSString.CompareOptions.literal, range: nil)
     }
}


extension String {
    func convertToMonth(in formate : String, out outFormate : String) -> String {
         let olDateFormatter = DateFormatter()
         olDateFormatter.locale = .current
         olDateFormatter.dateFormat = formate

         let oldDate = olDateFormatter.date(from: self)

         let convertDateFormatter = DateFormatter()
         convertDateFormatter.locale = .current
         convertDateFormatter.dateFormat = outFormate
        
         return convertDateFormatter.string(from: oldDate!)
    }
}

extension String {
    func base64ToImage() -> UIImage? {
        if let url = URL(string: self), let data = try? Data(contentsOf: url),let image = UIImage(data: data) {
            return image
        }
        return nil
    }
}

func convertImageToBase64String (img: UIImage) -> String {
    return img.jpegData(compressionQuality: 3)?.base64EncodedString() ?? ""
}

extension String {
    func matches(for regex: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex)
            let results = regex.matches(in: self, range:  NSRange(self.startIndex..., in: self))
            return results.map {
               // self.substring(with: Range($0.range, in: self)!)
                String(self[Range($0.range, in: self)!])
            }
        } catch let error {
            return []
        }
    }
}

public extension UIFont {
    static func register(from url: URL) throws {
        guard let fontDataProvider = CGDataProvider(url: url as CFURL) else {
           // throw fatalError()
            return
        }
        let font = CGFont(fontDataProvider)!
        var error: Unmanaged<CFError>?
        guard CTFontManagerRegisterGraphicsFont(font, &error) else {
            throw error!.takeUnretainedValue()
        }
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector(("statusBar"))) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}


internal class CustomeScrollView: UIScrollView, UITextFieldDelegate, UITextViewDelegate {
    override public var contentSize: CGSize {
        didSet {
            self.UpdateFromContentSizeChange()
        }
    }
    
    override public var frame: CGRect {
        didSet {
            self.UpdateContentInset()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    override public func awakeFromNib() {
        setup()
    }
    
    func contentSizeToFit() {
        self.contentSize = self.CalculatedContentSizeFromSubviewFrames()
    }
    
    func focusNextTextField() -> Bool {
        return self.FocusNextTextField()
    }
    
    @objc func scrollToActiveTextField() {
        return self.ScrollToActiveTextField()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override public func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        
        guard newSuperview != nil else { return }
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AssignTextDelegateForViewsBeneathView(_:)), object: self)
    }
    
    override public func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.FindFirstResponderBeneathView(self)?.resignFirstResponder()
        super.touchesEnded(touches, with: event)
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if !self.focusNextTextField() {
            textField.resignFirstResponder()
        }
        return true
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(AssignTextDelegateForViewsBeneathView(_:)), object: self)
        
        Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(AssignTextDelegateForViewsBeneathView(_:)), userInfo: nil, repeats: false)
    }
}

private extension CustomeScrollView {
    func setup() {
        NotificationCenter.default.addObserver(self, selector: #selector(KeyboardWillShow(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(KeyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(scrollToActiveTextField), name: UITextView.textDidBeginEditingNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(scrollToActiveTextField), name: UITextField.textDidBeginEditingNotification, object: nil)
    }
}

let kContentPadding: CGFloat = 10
let kMinimumScrollOffsetPadding: CGFloat = 20

extension UIScrollView {
    @objc func KeyboardWillShow(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        guard let rectNotification = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        
        let keyboardRect = self.convert(rectNotification.cgRectValue, from: nil)
        guard !keyboardRect.isEmpty else { return }
        
        let state = self.keyboardAvoidingState()
        
        guard let firstResponder = self.FindFirstResponderBeneathView(self) else { return }
        
        state.keyboardRect = keyboardRect
        if !state.keyboardVisible {
            state.priorInset = self.contentInset
            state.priorScrollIndicatorInsets = self.scrollIndicatorInsets
            state.priorPagingEnabled = self.isPagingEnabled
        }
        
        state.keyboardVisible = true
        self.isPagingEnabled = false
        
        if self is CustomeScrollView {
            state.priorContentSize = self.contentSize
            if self.contentSize.equalTo(CGSize.zero) {
                self.contentSize = self.CalculatedContentSizeFromSubviewFrames()
            }
        }
        
        let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Float ?? 0.0
        let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? Int ?? 0
        let options = UIView.AnimationOptions(rawValue: UInt(curve))
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: options, animations: { [weak self] in
            guard let self = self else { return }
            
            self.contentInset = self.ContentInsetForKeyboard()
            let viewableHeight = self.bounds.size.height - self.contentInset.top - self.contentInset.bottom
            let point = CGPoint(x: self.contentOffset.x, y: self.IdealOffsetForView(firstResponder, viewAreaHeight: viewableHeight))
            self.setContentOffset(point, animated: false)
            
            self.scrollIndicatorInsets = self.contentInset
            self.layoutIfNeeded()
        })
    }
    
    @objc func KeyboardWillHide(_ notification: Notification) {
        guard let userInfo = notification.userInfo else { return }
        
        guard let rectNotification = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        let keyboardRect = self.convert(rectNotification.cgRectValue, from: nil)
        guard !keyboardRect.isEmpty else { return }
        
        let state = self.keyboardAvoidingState()
        guard state.keyboardVisible else { return }
        state.keyboardRect = CGRect.zero
        state.keyboardVisible = false
        
        let duration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? Float ?? 0.0
        let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? Int ?? 0
        let options = UIView.AnimationOptions(rawValue: UInt(curve))
        
        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: options, animations: { [weak self] in
            guard let self = self, self is CustomeScrollView else { return }
            
            self.contentSize = state.priorContentSize
            self.contentInset = state.priorInset
            self.scrollIndicatorInsets = state.priorScrollIndicatorInsets
            self.isPagingEnabled = state.priorPagingEnabled
            self.layoutIfNeeded()
        })
    }
    
    func UpdateFromContentSizeChange() {
        let state = self.keyboardAvoidingState()
        if state.keyboardVisible {
            state.priorContentSize = self.contentSize
        }
    }
    
    func FocusNextTextField() -> Bool {
        guard let firstResponder = self.FindFirstResponderBeneathView(self) else { return false }
        guard let view = self.FindNextInputViewAfterView(firstResponder, beneathView: self) else { return false }
        
        Timer.scheduledTimer(timeInterval: 0.1, target: view, selector: #selector(becomeFirstResponder), userInfo: nil, repeats: false)
        
        return true
    }
    
    func ScrollToActiveTextField() {
        let state = self.keyboardAvoidingState()
        guard state.keyboardVisible else { return }
        
        let visibleSpace = self.bounds.size.height - self.contentInset.top - self.contentInset.bottom
        let idealOffset = CGPoint(x: 0, y: self.IdealOffsetForView(self.FindFirstResponderBeneathView(self), viewAreaHeight: visibleSpace))
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double((Int64)(0 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) { [weak self] in
            self?.setContentOffset(idealOffset, animated: true)
        }
    }
    
    func FindFirstResponderBeneathView(_ view: UIView) -> UIView? {
        for childView in view.subviews {
            if childView.responds(to: #selector(getter: isFirstResponder)) && childView.isFirstResponder {
                return childView
            }
            let result = FindFirstResponderBeneathView(childView)
            if result != nil {
                return result
            }
        }
        return nil
    }
    
    func UpdateContentInset() {
        let state = self.keyboardAvoidingState()
        if state.keyboardVisible {
            self.contentInset = self.ContentInsetForKeyboard()
        }
    }
    
    func CalculatedContentSizeFromSubviewFrames() -> CGSize {
        let wasShowingVerticalScrollIndicator = self.showsVerticalScrollIndicator
        let wasShowingHorizontalScrollIndicator = self.showsHorizontalScrollIndicator
        
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
        
        var rect = CGRect.zero
        
        self.subviews.forEach {
            rect = rect.union($0.frame)
        }
        
        for view in self.subviews {
            rect = rect.union(view.frame)
        }
        
        rect.size.height += kContentPadding
        self.showsVerticalScrollIndicator = wasShowingVerticalScrollIndicator
        self.showsHorizontalScrollIndicator = wasShowingHorizontalScrollIndicator
        
        return rect.size
    }
    
    func IdealOffsetForView(_ view: UIView?, viewAreaHeight: CGFloat) -> CGFloat {
        let contentSize = self.contentSize
        
        var offset: CGFloat = 0.0
        let subviewRect = view != nil ? view!.convert(view!.bounds, to: self) : CGRect.zero
        
        var padding = (viewAreaHeight - subviewRect.height) / 2
        if padding < kMinimumScrollOffsetPadding {
            padding = kMinimumScrollOffsetPadding
        }
        
        offset = subviewRect.origin.y - padding - self.contentInset.top
        
        if offset > (contentSize.height - viewAreaHeight) {
            offset = contentSize.height - viewAreaHeight
        }
        
        if offset < -self.contentInset.top {
            offset = -self.contentInset.top
        }
        
        return offset
    }
    
    func ContentInsetForKeyboard() -> UIEdgeInsets {
        let state = self.keyboardAvoidingState()
        var newInset = self.contentInset
        
        let keyboardRect = state.keyboardRect
        newInset.bottom = keyboardRect.size.height - max(keyboardRect.maxY - self.bounds.maxY, 0)
        
        return newInset
    }
    
    func ViewIsValidKeyViewCandidate(_ view: UIView) -> Bool {
        if view.isHidden || !view.isUserInteractionEnabled { return false}
        
        if let textField = view as? UITextField, textField.isEnabled {
            return true
        }
        
        if let textView = view as? UITextView, textView.isEditable {
            return true
        }
        
        return false
    }
    
    func FindNextInputViewAfterView(_ priorView: UIView, beneathView view: UIView, candidateView bestCandidate: inout UIView?) {
        let priorFrame = self.convert(priorView.frame, to: priorView.superview)
        let candidateFrame = bestCandidate == nil ? CGRect.zero : self.convert(bestCandidate!.frame, to: bestCandidate!.superview)
        
        var bestCandidateHeuristic = -sqrt(candidateFrame.origin.x * candidateFrame.origin.x + candidateFrame.origin.y * candidateFrame.origin.y) + ( Float(abs(candidateFrame.minY - priorFrame.minY)) < Float.ulpOfOne ? 1e6 : 0)
        
        for childView in view.subviews {
            if ViewIsValidKeyViewCandidate(childView) {
                let frame = self.convert(childView.frame, to: view)
                let heuristic = -sqrt(frame.origin.x * frame.origin.x + frame.origin.y * frame.origin.y)
                    + (Float(abs(frame.minY - priorFrame.minY)) < Float.ulpOfOne ? 1e6 : 0)
                
                if childView != priorView && (Float(abs(frame.minY - priorFrame.minY)) < Float.ulpOfOne
                    && frame.minX > priorFrame.minX
                    || frame.minY > priorFrame.minY)
                    && (bestCandidate == nil || heuristic > bestCandidateHeuristic) {
                    bestCandidate = childView
                    bestCandidateHeuristic = heuristic
                }
            } else {
                self.FindNextInputViewAfterView(priorView, beneathView: view, candidateView: &bestCandidate)
            }
        }
    }
    
    func FindNextInputViewAfterView(_ priorView: UIView, beneathView view: UIView) -> UIView? {
        var candidate: UIView?
        self.FindNextInputViewAfterView(priorView, beneathView: view, candidateView: &candidate)
        return candidate
    }
    
    @objc func AssignTextDelegateForViewsBeneathView(_ obj: AnyObject) {
        func processWithView(_ view: UIView) {
            for childView in view.subviews {
                if childView is UITextField || childView is UITextView {
                    self.initView(childView)
                } else {
                    self.AssignTextDelegateForViewsBeneathView(childView)
                }
            }
        }
        
        if let timer = obj as? Timer, let view = timer.userInfo as? UIView {
            processWithView(view)
        } else if let view = obj as? UIView {
            processWithView(view)
        }
    }
    
    func initView(_ view: UIView) {
        if let textField = view as? UITextField,
            let delegate = self as? UITextFieldDelegate, textField.returnKeyType == UIReturnKeyType.default &&
            textField.delegate !== delegate {
            textField.delegate = delegate
            let otherView = self.FindNextInputViewAfterView(view, beneathView: self)
            textField.returnKeyType = otherView != nil ? .next : .done
        }
    }
    
    func keyboardAvoidingState() -> AvoidState {
        var state = objc_getAssociatedObject(self, &KeyboardKeysPrefrences.DescriptiveName) as? AvoidState
        if state == nil {
            state = AvoidState()
            self.state = state
        }
        
        return self.state!
    }
}

internal class AvoidState: NSObject {
    var priorInset = UIEdgeInsets.zero
    var priorScrollIndicatorInsets = UIEdgeInsets.zero
    
    var keyboardVisible = false
    var keyboardRect = CGRect.zero
    var priorContentSize = CGSize.zero
    
    var priorPagingEnabled = false
}

internal extension UIScrollView {
    fileprivate struct KeyboardKeysPrefrences {
        static var DescriptiveName = "KeyBoard_DescriptiveName"
    }
    
    var state: AvoidState? {
        get {
            let optionalObject: AnyObject? = objc_getAssociatedObject(self, &KeyboardKeysPrefrences.DescriptiveName) as AnyObject?
            if let object: AnyObject = optionalObject {
                return object as? AvoidState
            } else {
                return nil
            }
        }
        set {
            objc_setAssociatedObject(self, &KeyboardKeysPrefrences.DescriptiveName, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}






