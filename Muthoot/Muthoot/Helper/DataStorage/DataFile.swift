//
//  DataFile.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/10/21.
//

class DataFile: NSObject {
    
    struct configResponse {
        static var configDictionary : NSDictionary?
    }
    
    class func getconfigDict(data: NSDictionary){
        configResponse.configDictionary = data
    }
    
    class func setConfigResponse() -> NSDictionary{
        return configResponse.configDictionary ?? NSDictionary()
    }
    
    class func getAurthDetails() -> NSDictionary {
        let credDic : [String: Any] = ["ChannelSource":"IMOBILEAPP","chatInteractionId":UserDetails.chatInteractionID, "token" :UserDetails.token]
        return  credDic as NSDictionary
    }
    
    class func updateKeyIV(passwordString : String){
        let sha256Str = Crypto.shared.sha256(str: passwordString)
        let data = Data( [0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08])
        let stringFromByteArray = String(data: Data(_: data), encoding: .utf8)
        
        let keyIvData = Crypto.shared.pbkdf2sha1(password: sha256Str, salt: stringFromByteArray ?? "String", keyByteCount: 48, rounds: 1000)
        let strByte = stringToBytes(keyIvData!)
        let keyData = Data(strByte!.prefix(upTo: 32))
        let IVData =  Data(strByte!.suffix(16))
        UserDetails.IV = IVData
        UserDetails.keyData = keyData
    }
    
    class func encryptedData(Payload : Data, attachePublicKey: Bool) -> String{
        
        var encryptedString = String()
        do {
            let aes = try AES256(key: UserDetails.keyData, iv: UserDetails.IV)
            let encrypted = try aes.encrypt(Payload)
            
            if attachePublicKey == true {
                encryptedString = encrypted.base64EncodedString()+"."+(UserDetails.publicKey)
            }
            else{
                encryptedString = encrypted.base64EncodedString()
            }
        }
        catch {
            //print(error)
        }
        return encryptedString
        
    }
    
    class func decryptedData(encrypted: String) -> String{
        var stringValue = String()
        let decodedData = Data(base64Encoded: encrypted)!
        do {
            let aes = try AES256(key: UserDetails.keyData, iv: UserDetails.IV)
            let decrypted = try aes.decrypt(decodedData)
            stringValue = String(decoding: decrypted, as: UTF8.self)
            //          print("Decrypted String : \(stringValue)")
        }
        catch {
            //print(error)
        }
        return stringValue
    }
    
    //MARK String To byte
    class func toPairsOfChars(pairs: [String], string: String) -> [String] {
        if string.count == 0 {
            return pairs
        }
        var pairsMod = pairs
        pairsMod.append(String(string.prefix(2)))
        return toPairsOfChars(pairs: pairsMod, string: String(string.dropFirst(2)))
    }
    
    class func stringToBytes(_ string: String) -> [UInt8]? {
        let pairs = toPairsOfChars(pairs: [], string: string)
        return pairs.map { UInt8($0, radix: 16)! }
    }
    
    class func StringToDateAgo(Date : String) -> Date{
        let string = Date
        let formatter4 = DateFormatter()
        formatter4.dateFormat = "yyyy-MM-dd,hh:mm:ss a"
        let datePassed = formatter4.date(from: string)
        return datePassed!
    }
    
    class func timeAgoStringFromDate(date: Date) -> String? {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full

        let now = Date()

        let calendar = NSCalendar.current
        let components1: Set<Calendar.Component> = [.year, .month, .weekOfMonth, .day, .hour, .minute, .second]
        let components = calendar.dateComponents(components1, from: date, to: now)

        if components.year ?? 0 > 0 {
            formatter.allowedUnits = .year
        } else if components.month ?? 0 > 0 {
            formatter.allowedUnits = .month
        } else if components.weekOfMonth ?? 0 > 0 {
            formatter.allowedUnits = .weekOfMonth
        } else if components.day ?? 0 > 0 {
            formatter.allowedUnits = .day
        } else if components.hour ?? 0 > 0 {
            formatter.allowedUnits = [.hour]
        } else if components.minute ?? 0 > 0 {
            formatter.allowedUnits = .minute
        } else {
            formatter.allowedUnits = .second
        }

        let formatString = NSLocalizedString("%@ ago", comment: "Used to say how much time has passed. e.g. '2 hours ago'")

        guard let timeString = formatter.string(for: components) else {
            return nil
        }
        return String(format: formatString, timeString)
    }
}
