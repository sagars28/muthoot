//
//  GoldRateModel.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/13/21.
//

import Foundation


class GoldRateModel: Codable {
    let eventName: String?
    let data: DataClass?
    let status: Bool?
    let message, alertType: String?

    init(eventName: String?, data: DataClass?, status: Bool?, message: String?, alertType: String?) {
        self.eventName = eventName
        self.data = data
        self.status = status
        self.message = message
        self.alertType = alertType
    }
}
 

class DataClass: Codable {
    let loanRequest: LoanRequest?
    let goldRate: GoldRate?
    let disclaimer, header: String?

    init(loanRequest: LoanRequest?, goldRate: GoldRate?, disclaimer: String?, header: String?) {
        self.loanRequest = loanRequest
        self.goldRate = goldRate
        self.disclaimer = disclaimer
        self.header = header
    }
}


class GoldRate: Codable {
    let solidRate, nonSolidRate, stateName, purity: String?

    enum CodingKeys: String, CodingKey {
        case solidRate = "SolidRate"
        case nonSolidRate = "NonSolidRate"
        case stateName = "StateName"
        case purity = "Purity"
    }

    init(solidRate: String?, nonSolidRate: String?, stateName: String?, purity: String?) {
        self.solidRate = solidRate
        self.nonSolidRate = nonSolidRate
        self.stateName = stateName
        self.purity = purity
    }
}

class LoanRequest: Codable {
    let minAmount, maxAmount, requestDays: Int?
    let startTime, endTime: String?
    let interval, bufferTime: Int?

    init(minAmount: Int?, maxAmount: Int?, requestDays: Int?, startTime: String?, endTime: String?, interval: Int?, bufferTime: Int?) {
        self.minAmount = minAmount
        self.maxAmount = maxAmount
        self.requestDays = requestDays
        self.startTime = startTime
        self.endTime = endTime
        self.interval = interval
        self.bufferTime = bufferTime
    }
}
