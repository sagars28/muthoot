//
//  CommonModel.swift
//  Muthoot Track
//
//  Created by Sagar on 11/06/21.
//

import Foundation

class CommonModel {
    static let shared = CommonModel()
    
    var lat = 0.0
    var long = 0.0
    var mobileNumber = ""
}

