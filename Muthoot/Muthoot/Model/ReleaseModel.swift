//
//  ReleaseModel.swift
//  Muthoot Track
//
//  Created by Sagar on 30/06/21.
//

import Foundation

class ReleaseModel: Codable {
    let eventName: String?
    let data: ReleaseData?
    let status: Bool?
    let message: String?

    init(eventName: String?, data: ReleaseData?, status: Bool?, message: String?) {
        self.eventName = eventName
        self.data = data
        self.status = status
        self.message = message
    }
}

// MARK: - DataClass
class ReleaseData: Codable {
    let enquiryID: String?
    let availableSlots: [AvailableSlot3]?
    let availableDates: [String]?
    let noOfDays: Int?
    let loanRequest: LoanRequest?
    let header: String?

    enum CodingKeys: String, CodingKey {
        case enquiryID = "enquiryId"
        case availableSlots, availableDates, noOfDays, loanRequest, header
    }

    init(enquiryID: String?, availableSlots: [AvailableSlot3]?, availableDates: [String]?, noOfDays: Int?, loanRequest: LoanRequest?, header: String?) {
        self.enquiryID = enquiryID
        self.availableSlots = availableSlots
        self.availableDates = availableDates
        self.noOfDays = noOfDays
        self.loanRequest = loanRequest
        self.header = header
    }
}


// MARK: - AvailableSlot
class AvailableSlot3: Codable {
    let date: String?
    let time: String?

    init(date: String?, time: String?) {
        self.date = date
        self.time = time
    }
}

// MARK: - LoanRequest
class LoanRequest3: Codable {
    let minAmount, maxAmount, requestDays: Int?
    let startTime, endTime: String?
    let interval, bufferTime: Int?

    init(minAmount: Int?, maxAmount: Int?, requestDays: Int?, startTime: String?, endTime: String?, interval: Int?, bufferTime: Int?) {
        self.minAmount = minAmount
        self.maxAmount = maxAmount
        self.requestDays = requestDays
        self.startTime = startTime
        self.endTime = endTime
        self.interval = interval
        self.bufferTime = bufferTime
    }
}
