//
//  KYCApproveModel.swift
//  Muthoot Track
//
//  Created by Sagar on 22/06/21.
//

import Foundation

class KYCApproveModel: Codable {
    var eventName : String?
    var data : KYCApprove?
    var message : String?
    var alertType : String?
    var status : Bool?
}

class KYCApprove : Codable {
    let UCICCode : String?
}
