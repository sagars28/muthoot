//
//  PledgeFormModel.swift
//  Muthoot Track
//
//  Created by Sagar on 26/06/21.
//

import Foundation

class PledgeFormModel: Codable {
    let eventName: String?
    let data: PledgeForm?
    let message: String?
    let buttons: [Button1]?
    let status: Bool?

    init(eventName: String?, data: PledgeForm?, message: String?, buttons: [Button1]?, status: Bool?) {
        self.eventName = eventName
        self.data = data
        self.message = message
        self.buttons = buttons
        self.status = status
    }
}

// MARK: - Button
class Button1: Codable {
    let display, value: String?
    let enabled, status: Bool?

    init(display: String?, value: String?, enabled: Bool?, status: Bool?) {
        self.display = display
        self.value = value
        self.enabled = enabled
        self.status = status
    }
}

// MARK: - DataClass
class PledgeForm: Codable {
    let pledgeform: String?

    init(pledgeform: String?) {
        self.pledgeform = pledgeform
    }
}

