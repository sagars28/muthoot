//
//  RepledgeSummaryModel.swift
//  Muthoot Track
//
//  Created by Sagar on 28/06/21.
//

import Foundation

class RepledgeSummaryModel: Codable {
    let eventName: String?
    let data: RepledgeSummary?
    let status: Bool?
    let message: String?

    init(eventName: String?, data: RepledgeSummary?, status: Bool?, message: String?) {
        self.eventName = eventName
        self.data = data
        self.status = status
        self.message = message
    }
}

// MARK: - DataClass
class RepledgeSummary: Codable {
    let loanData: LoanData2?
    let message: String?

    init(loanData: LoanData2?, message: String?) {
        self.loanData = loanData
        self.message = message
    }
}

// MARK: - LoanData
class LoanData2: Codable {
    let id, loanDataID, lrid, enquiryType: String?
    let mobileNo: Int?
    let latitude, longitude, loanAmount, selectedDate: String?
    let selectedTime, name, address, address1: String?
    let cityname, statename, pincode, addressid: String?
    let oppurtunityid, selectedPincode, selectedLatitude, selectedLongitude: String?
    let status: String?
    let createdOn, updatedAt: Int?
    let ucicDetails: [UCICDetail2]?
    let staff: [Staff2]?
    let kycStatus, updatedBy: String?
    let ornaments: [Ornament2]?
    let product: [Product2]?
    let scheme: [Scheme2]?
    let productcode: [Productcode2]?
    let eligibleamount, selectedamount, selectedtenure: String?
    let loandetails: [Loandetail2]?
    let selecteducicno, customerloanno: String?
    let remark, reason, packetNo: String?
    let packetno, finalloanamount: String?
    let reshedulecount: Int?
    let ornamentsummary: [Ornamentsummary2]?
    let selectedbankDetails: SelectedbankDetails2?
    let selectornamentimage, customerimage, signature, cancellationphoto: String?
    let managersignature, pledgesignature: String?
    let resealpacketno, loanstate: String?
    let releasesignature: String?
    let loanpurpose: String?
    let ownershipagreement, pledgeform, releaseagreement: Bool?
    let slabrate: String?
    let finalLoanDetails: FinalLoanDetails2?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case loanDataID = "id"
        case lrid = "LRID"
        case enquiryType, mobileNo, latitude, longitude, loanAmount, selectedDate, selectedTime, name, address, address1, cityname, statename, pincode, addressid, oppurtunityid, selectedPincode, selectedLatitude, selectedLongitude, status, createdOn, updatedAt
        case ucicDetails = "UCICDetails"
        case staff, kycStatus, updatedBy, ornaments, product, scheme, productcode, eligibleamount, selectedamount, selectedtenure, loandetails, selecteducicno, customerloanno, remark, reason
        case packetNo = "PacketNo"
        case packetno, finalloanamount, reshedulecount, ornamentsummary, selectedbankDetails, selectornamentimage, customerimage, signature, cancellationphoto, managersignature, pledgesignature, resealpacketno, loanstate, releasesignature, loanpurpose, ownershipagreement, pledgeform, releaseagreement, slabrate, finalLoanDetails
    }
}

// MARK: - FinalLoanDetails
class FinalLoanDetails2: Codable {
    let customerloanno, productcategory, productname, productdescription: String?
    let productcode, productid, fdgl, scheme: String?
    let schemecode, rate, ltv, repaytype: String?
    let selectedTenure, totalinterest, totalpayable, emi: String?
    let finalloanamount, eligibleamount, selectedamount: String?
}

// MARK: - Loandetail
class Loandetail2: Codable {
    let loanamount, finalloanamount, maximumamountlimit, schememaximumamountlimit: String?
    let roi, tenure, emi, totalinterest: String?
    let totalpayable: String?

    init(loanamount: String?, finalloanamount: String?, maximumamountlimit: String?, schememaximumamountlimit: String?, roi: String?, tenure: String?, emi: String?, totalinterest: String?, totalpayable: String?) {
        self.loanamount = loanamount
        self.finalloanamount = finalloanamount
        self.maximumamountlimit = maximumamountlimit
        self.schememaximumamountlimit = schememaximumamountlimit
        self.roi = roi
        self.tenure = tenure
        self.emi = emi
        self.totalinterest = totalinterest
        self.totalpayable = totalpayable
    }
}

// MARK: - Ornament
class Ornament2: Codable {
    let id, name, karat: String?
    let weight: Int?
    let createdOn, updatedOn: String?
    let status: Bool?
    let lrid, ornamentphoto: String?
    let stoneweight, netweight, purity: Int?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name, karat, weight
        case createdOn = "CreatedOn"
        case updatedOn = "UpdatedOn"
        case status
        case lrid = "LRID"
        case ornamentphoto, stoneweight, netweight, purity
    }
}

// MARK: - Ornamentsummary
class Ornamentsummary2: Codable {
    let items: Int?
    let karats: String?
    let weight, stoneweight, grossweight: Int?

    init(items: Int?, karats: String?, weight: Int?, stoneweight: Int?, grossweight: Int?) {
        self.items = items
        self.karats = karats
        self.weight = weight
        self.stoneweight = stoneweight
        self.grossweight = grossweight
    }
}

// MARK: - Product
class Product2: Codable {
    let id, productid, productname, productcode: String?
   // let productdescription: JSONNull?
    let createdon, updatedon: String?
    let v: Int?
    let productcategory: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case productid, productname, productcode, createdon, updatedon
        case v = "__v"
        case productcategory
    }
}

// MARK: - Productcode
class Productcode2: Codable {
    let id, productcode, productid, createdon: String?
    let fdgl: String?
    let v: Int?
    let updatedat: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case productcode, productid, createdon, fdgl
        case v = "__v"
        case updatedat
    }
}

// MARK: - Scheme
class Scheme2: Codable {
    let scheme, schemecode: String?
    let minamt, maxamt: Int?
    let rate, ltv, repaytype, productid: String?
    let selectedTenure, totalinterest, totalpayable, emi: String?
    let finalloanamount: String?
}

// MARK: - SelectedbankDetails
class SelectedbankDetails2: Codable {
    let id, ucic, masterBankID, bankName: String?
    let branchname, mflBranchName, accountNo, beneficaryName: String?
    let beneficaryBank, beneficaryBankLocation, beneficaryAccountNo, ifsc: String?
    let deviceID, lrid, beneficiary, statusName: String?
    let approvalStatusID, approvalStatus, approvalRemarks: String?
    let isPrefferd, isActive: Bool?
    let fdgl, pledgeDate, glLoyalty, empNo: String?
    let custMobileNo, createdOn, updatedOn, createdBy: String?
    let v, nomineeAddress1, nomineeAddress2, nomineeCity: String?
    let nomineeDOB, nomineeEmail, nomineeGuardian, nomineeName: String?
    let nomineePhoneNo, nomineePincode, nomineeRelation, nomineeState: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case ucic = "UCIC"
        case masterBankID = "MasterBankId"
        case bankName = "BankName"
        case branchname = "Branchname"
        case mflBranchName = "MFLBranchName"
        case accountNo = "AccountNo"
        case beneficaryName = "BeneficaryName"
        case beneficaryBank = "BeneficaryBank"
        case beneficaryBankLocation = "BeneficaryBankLocation"
        case beneficaryAccountNo = "BeneficaryAccountNo"
        case ifsc = "IFSC"
        case deviceID
        case lrid = "LRID"
        case beneficiary = "Beneficiary"
        case statusName = "StatusName"
        case approvalStatusID = "ApprovalStatusId"
        case approvalStatus = "ApprovalStatus"
        case approvalRemarks = "ApprovalRemarks"
        case isPrefferd = "IsPrefferd"
        case isActive = "IsActive"
        case fdgl, pledgeDate
        case glLoyalty = "GL_Loyalty"
        case empNo = "EmpNo"
        case custMobileNo, createdOn, updatedOn, createdBy
        case v = "__v"
        case nomineeAddress1 = "NomineeAddress1"
        case nomineeAddress2 = "NomineeAddress2"
        case nomineeCity = "NomineeCity"
        case nomineeDOB = "NomineeDOB"
        case nomineeEmail = "NomineeEmail"
        case nomineeGuardian = "NomineeGuardian"
        case nomineeName = "NomineeName"
        case nomineePhoneNo = "NomineePhoneNo"
        case nomineePincode = "NomineePincode"
        case nomineeRelation = "NomineeRelation"
        case nomineeState = "NomineeState"
    }
}

// MARK: - Staff
class Staff2: Codable {
    let empID: String?
    let branch: Branch2?
    let designation, profilephoto, name, phoneNo: String?
    let photo: String?

    enum CodingKeys: String, CodingKey {
        case empID = "empId"
        case branch, designation, profilephoto, name, phoneNo, photo
    }
}

// MARK: - Branch
class Branch2: Codable {
    let name, address, city, state: String?
    let pincode, phoneNo, longitude, latitude: String?
}

// MARK: - UCICDetail
class UCICDetail2: Codable {
    let name, dob, mobileNumber, idNo: String?
    let ucic, rank: String?
    let isActive: Bool?

    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case dob = "DOB"
        case mobileNumber = "MobileNumber"
        case idNo = "IDNo"
        case ucic = "UCIC"
        case rank = "Rank"
        case isActive = "IsActive"
    }
}

