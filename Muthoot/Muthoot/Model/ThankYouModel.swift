//
//  ThankYouModel.swift
//  Muthoot Track
//
//  Created by Sagar on 14/06/21.
//

import Foundation
class ThankYouModel: Codable {
    let eventName: String?
    let data: ThankYouData?
    let message: String?
    let status: Bool?
    let alertType: String?

    init(eventName: String?, data: ThankYouData?, message: String?, status: Bool?, alertType: String?) {
        self.eventName = eventName
        self.data = data
        self.message = message
        self.status = status
        self.alertType = alertType
    }
}

class ThankYouData: Codable {
    let loanData: LoanData?

    init(loanData: LoanData?) {
        self.loanData = loanData
    }
}


class LoanData: Codable {
    
    let id, lrid: String?
    let mobileNo: Int?
    let enquiryType, loanAmount, selectedDate, selectedTime: String?
    let name, address, selectedPincode, loanstate: String?

    enum CodingKeys: String, CodingKey {
        case id
        case lrid = "LRID"
        case mobileNo, enquiryType, loanAmount, selectedDate, selectedTime, name, address, selectedPincode, loanstate
    }

    init(id: String?, lrid: String?, mobileNo: Int?, enquiryType: String?, loanAmount: String?, selectedDate: String?, selectedTime: String?, name: String?, address: String?, selectedPincode: String?, loanstate: String?) {
        self.id = id
        self.lrid = lrid
        self.mobileNo = mobileNo
        self.enquiryType = enquiryType
        self.loanAmount = loanAmount
        self.selectedDate = selectedDate
        self.selectedTime = selectedTime
        self.name = name
        self.address = address
        self.selectedPincode = selectedPincode
        self.loanstate = loanstate
    }
}
