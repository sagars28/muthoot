//
//  AddressListModel.swift
//  Muthoot Track
//
//  Created by Sagar on 10/06/21.
//

import Foundation

// MARK: - AddressModel
struct AddressListModel: Codable {
    let eventName: String?
    var data: AddressList?
    let status: Bool?
    let message, alertType: String?
}

// MARK: - DataClass
struct AddressList: Codable {
    let header: String?
    var addresses: [Address]?
    let cityname, statename, countryname: String?
    let addressLimit: Int?
}

// MARK: - Address
struct Address: Codable {
    let name, address, pincode, addresstype: String?
    let addressid, latitude, longitude: String?
    let isDefault: Bool?
}
