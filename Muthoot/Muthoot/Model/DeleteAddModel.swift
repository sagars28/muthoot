//
//  DeleteAddModel.swift
//  Muthoot Track
//
//  Created by Sagar on 11/06/21.
//

import Foundation

struct DeleteAddModel: Codable {
    var eventName : String?
    var data : String?
    var message : String?
    var alertType : String?
    var status : Bool?
}
