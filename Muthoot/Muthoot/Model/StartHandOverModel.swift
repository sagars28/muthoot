//
//  StartHandOverModel.swift
//  Muthoot Track
//
//  Created by Sagar on 29/06/21.
//

import Foundation

class StartHandOverModel: Codable {
    let eventName: String?
    let data: StartHandOver?
    let message: String?
    let buttons: [Button]?
    let status: Bool?

    init(eventName: String?, data: StartHandOver?, message: String?, buttons: [Button]?, status: Bool?) {
        self.eventName = eventName
        self.data = data
        self.message = message
        self.buttons = buttons
        self.status = status
    }
}

// MARK: - DataClass
class StartHandOver: Codable {
    let pledgeform: String?

    init(pledgeform: String?) {
        self.pledgeform = pledgeform
    }
}
