//
//  RescheduleModel.swift
//  Muthoot Track
//
//  Created by Sagar on 19/06/21.
//

import Foundation

class RescheduleModel: Codable {
    let eventName: String?
    let data: RescheduleClass?
    let message: String?
    let buttons: [Button]?
    let status: Bool?

    init(eventName: String?, data: RescheduleClass?, message: String?, buttons: [Button]?, status: Bool?) {
        self.eventName = eventName
        self.data = data
        self.message = message
        self.buttons = buttons
        self.status = status
    }
}

// MARK: - Button
class Button: Codable {
    let display, value: String?
    let enabled, status: Bool?

    init(display: String?, value: String?, enabled: Bool?, status: Bool?) {
        self.display = display
        self.value = value
        self.enabled = enabled
        self.status = status
    }
}

// MARK: - DataClass
class RescheduleClass: Codable {
    let cancelreasons: [Reason]?
    let cancelheader: String?
    let reschedulereasons: [Reason]?
    let rescheduleheader: String?
    let availableslots: [Availableslot]?
    let availabledates: [String]?
    let noofdays: Int?

    init(cancelreasons: [Reason]?, cancelheader: String?, reschedulereasons: [Reason]?, rescheduleheader: String?, availableslots: [Availableslot]?, availabledates: [String]?, noofdays: Int?) {
        self.cancelreasons = cancelreasons
        self.cancelheader = cancelheader
        self.reschedulereasons = reschedulereasons
        self.rescheduleheader = rescheduleheader
        self.availableslots = availableslots
        self.availabledates = availabledates
        self.noofdays = noofdays
    }
}

// MARK: - Availableslot
class Availableslot: Codable {
    let date: String?
    let time: String?
    let period: String?
    
    init(date: String?, time: String?, period : String?) {
        self.date = date
        self.time = time
        self.period = period
    }
}

// MARK: - Reason
class Reason: Codable {
    let id, reason: String?
    let photo: Bool?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case reason, photo
    }
    
    init(id: String?, reason: String?, photo: Bool?) {
        self.id = id
        self.reason = reason
        self.photo = photo
    }
}
