//
//  LoanSummaryModel.swift
//  Muthoot Track
//
//  Created by Sagar on 14/06/21.
//

import Foundation

class LoanSummaryModel: Codable {
    let eventName: String?
    var data: [LoanSummaryData]?
    let header, loanType, confirmationText, message: String?
    let alertType: String?
    let status: Bool?
}

// MARK: - Datum
class LoanSummaryData: Codable {
    let id, datumID, lrid, enquiryType: String?
    let mobileNo: Int?
    let latitude, longitude, loanAmount, selectedDate: String?
    let selectedTime, name, address, address1: String?
    let cityname, statename, pincode, addressid: String?
    let oppurtunityid, selectedPincode, selectedLatitude, selectedLongitude: String?
    let status, createdOn, updatedAt: String?
    let ucicDetails: [UCICDetails]?
    var staff: [Staff]?
    let kycStatus: String?
   // let ornaments, product, scheme, productcode: [JSONAny]?
    
    let ornaments: [Ornament]?
    let product: [Product]?
    let scheme: [Scheme]?
    let productcode: [Productcode]?
    let selectedamount, selectedtenure: String?
    let loandetails: [LoanDetails]?
    let selecteducicno, customerloanno: String?
    let reason, packetNo: String?
    let remark : String?
    let packetno: String?
    let finalloanamount: String?
    let reshedulecount: Int?
    let ornamentsummary: [OrnamentSummary]?
    let selectedbankDetails: SelectedbankDetails?
    let selectornamentimage, customerimage, signature, cancellationphoto: JSONNull?
    let managersignature, pledgesignature : JSONNull?
    let resealpacketno : String?
    let loanstate: String?
    let releasesignature: JSONNull?
    let loanpurpose: String?
    let ownershipagreement, pledgeform, releaseagreement: Bool?
    let slabrate: [JSONAny]?
    let finalLoanDetails: FinalLoanDetails?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case datumID = "id"
        case lrid = "LRID"
        case enquiryType, mobileNo, latitude, longitude, loanAmount, selectedDate, selectedTime, name, address, address1, cityname, statename, pincode, addressid, oppurtunityid, selectedPincode, selectedLatitude, selectedLongitude, status, createdOn, updatedAt
        case ucicDetails = "UCICDetails"
        case staff, kycStatus, ornaments, product, scheme, productcode, selectedamount, selectedtenure, loandetails, selecteducicno, customerloanno, remark, reason
        case packetNo = "PacketNo"
        case packetno, finalloanamount, reshedulecount, ornamentsummary, selectedbankDetails, selectornamentimage, customerimage, signature, cancellationphoto, managersignature, pledgesignature, resealpacketno, loanstate, releasesignature, loanpurpose, ownershipagreement, pledgeform, releaseagreement, slabrate, finalLoanDetails
    }
}

class OrnamentSummary : Codable {
    let grossweight : Int?
    let items : Int?
    let karats : String?
    let stoneweight : Int?
    let weight : Int?
}


class UCICDetails : Codable {
    
    let  DOB : String;
    let  IDNo : String;
    let  IsActive : Bool;
    let  MobileNumber : String;
    let  Name : String;
    let  Rank : String
    let  UCIC : String
}

class Product: Codable {
    let id, productid, productname, productcode: String?
    let productdescription: JSONNull?
    let createdon, updatedon: String?
    let v: Int?
    let productcategory: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case productid, productname, productcode, productdescription, createdon, updatedon
        case v = "__v"
        case productcategory
    }

    init(id: String?, productid: String?, productname: String?, productcode: String?, productdescription: JSONNull?, createdon: String?, updatedon: String?, v: Int?, productcategory: String?) {
        self.id = id
        self.productid = productid
        self.productname = productname
        self.productcode = productcode
        self.productdescription = productdescription
        self.createdon = createdon
        self.updatedon = updatedon
        self.v = v
        self.productcategory = productcategory
    }
}

// MARK: - Productcode
class Productcode: Codable {
    let id, productcode, productid, createdon: String?
    let fdgl: String?
    let v: Int?
    let updatedat: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case productcode, productid, createdon, fdgl
        case v = "__v"
        case updatedat
    }

    init(id: String?, productcode: String?, productid: String?, createdon: String?, fdgl: String?, v: Int?, updatedat: String?) {
        self.id = id
        self.productcode = productcode
        self.productid = productid
        self.createdon = createdon
        self.fdgl = fdgl
        self.v = v
        self.updatedat = updatedat
    }
}

class Scheme: Codable {
    let scheme, schemecode: String?
    let minamt, maxamt: Int?
    let rate, ltv, repaytype, productid: String?
    let selectedTenure, totalinterest, totalpayable, emi: String?
    let finalloanamount: String?

    init(scheme: String?, schemecode: String?, minamt: Int?, maxamt: Int?, rate: String?, ltv: String?, repaytype: String?, productid: String?, selectedTenure: String?, totalinterest: String?, totalpayable: String?, emi: String?, finalloanamount: String?) {
        self.scheme = scheme
        self.schemecode = schemecode
        self.minamt = minamt
        self.maxamt = maxamt
        self.rate = rate
        self.ltv = ltv
        self.repaytype = repaytype
        self.productid = productid
        self.selectedTenure = selectedTenure
        self.totalinterest = totalinterest
        self.totalpayable = totalpayable
        self.emi = emi
        self.finalloanamount = finalloanamount
    }
}


// MARK: - FinalLoanDetails
class FinalLoanDetails: Codable {
    let customerloanno, productcategory, productname, productdescription: String?
    let productcode, productid, fdgl, scheme: String?
    let schemecode, rate, ltv, repaytype: String?
    let selectedTenure, totalinterest, totalpayable, emi: String?
    let finalloanamount, eligibleamount, selectedamount: String?
}

// MARK: - SelectedbankDetails
class SelectedbankDetails: Codable {
    let id, ucic, masterBankID, bankName: String?
        let branchname, mflBranchName, accountNo, beneficaryName: String?
        let beneficaryBank, beneficaryBankLocation, beneficaryAccountNo, ifsc: String?
        let deviceID, lrid, beneficiary, statusName: String?
        let approvalStatusID, approvalStatus, approvalRemarks: String?
        let isPrefferd, isActive: Bool?
        let fdgl, pledgeDate, glLoyalty, empNo: String?
        let custMobileNo, createdOn, updatedOn, createdBy: String?
        let v, nomineeAddress1, nomineeAddress2, nomineeCity: String?
        let nomineeDOB, nomineeEmail, nomineeGuardian, nomineeName: String?
        let nomineePhoneNo, nomineePincode, nomineeRelation, nomineeState: String?

        enum CodingKeys: String, CodingKey {
            case id = "_id"
            case ucic = "UCIC"
            case masterBankID = "MasterBankId"
            case bankName = "BankName"
            case branchname = "Branchname"
            case mflBranchName = "MFLBranchName"
            case accountNo = "AccountNo"
            case beneficaryName = "BeneficaryName"
            case beneficaryBank = "BeneficaryBank"
            case beneficaryBankLocation = "BeneficaryBankLocation"
            case beneficaryAccountNo = "BeneficaryAccountNo"
            case ifsc = "IFSC"
            case deviceID
            case lrid = "LRID"
            case beneficiary = "Beneficiary"
            case statusName = "StatusName"
            case approvalStatusID = "ApprovalStatusId"
            case approvalStatus = "ApprovalStatus"
            case approvalRemarks = "ApprovalRemarks"
            case isPrefferd = "IsPrefferd"
            case isActive = "IsActive"
            case fdgl, pledgeDate
            case glLoyalty = "GL_Loyalty"
            case empNo = "EmpNo"
            case custMobileNo, createdOn, updatedOn, createdBy
            case v = "__v"
            case nomineeAddress1 = "NomineeAddress1"
            case nomineeAddress2 = "NomineeAddress2"
            case nomineeCity = "NomineeCity"
            case nomineeDOB = "NomineeDOB"
            case nomineeEmail = "NomineeEmail"
            case nomineeGuardian = "NomineeGuardian"
            case nomineeName = "NomineeName"
            case nomineePhoneNo = "NomineePhoneNo"
            case nomineePincode = "NomineePincode"
            case nomineeRelation = "NomineeRelation"
            case nomineeState = "NomineeState"
        }

        init(id: String?, ucic: String?, masterBankID: String?, bankName: String?, branchname: String?, mflBranchName: String?, accountNo: String?, beneficaryName: String?, beneficaryBank: String?, beneficaryBankLocation: String?, beneficaryAccountNo: String?, ifsc: String?, deviceID: String?, lrid: String?, beneficiary: String?, statusName: String?, approvalStatusID: String?, approvalStatus: String?, approvalRemarks: String?, isPrefferd: Bool?, isActive: Bool?, fdgl: String?, pledgeDate: String?, glLoyalty: String?, empNo: String?, custMobileNo: String?, createdOn: String?, updatedOn: String?, createdBy: String?, v: String?, nomineeAddress1: String?, nomineeAddress2: String?, nomineeCity: String?, nomineeDOB: String?, nomineeEmail: String?, nomineeGuardian: String?, nomineeName: String?, nomineePhoneNo: String?, nomineePincode: String?, nomineeRelation: String?, nomineeState: String?) {
            self.id = id
            self.ucic = ucic
            self.masterBankID = masterBankID
            self.bankName = bankName
            self.branchname = branchname
            self.mflBranchName = mflBranchName
            self.accountNo = accountNo
            self.beneficaryName = beneficaryName
            self.beneficaryBank = beneficaryBank
            self.beneficaryBankLocation = beneficaryBankLocation
            self.beneficaryAccountNo = beneficaryAccountNo
            self.ifsc = ifsc
            self.deviceID = deviceID
            self.lrid = lrid
            self.beneficiary = beneficiary
            self.statusName = statusName
            self.approvalStatusID = approvalStatusID
            self.approvalStatus = approvalStatus
            self.approvalRemarks = approvalRemarks
            self.isPrefferd = isPrefferd
            self.isActive = isActive
            self.fdgl = fdgl
            self.pledgeDate = pledgeDate
            self.glLoyalty = glLoyalty
            self.empNo = empNo
            self.custMobileNo = custMobileNo
            self.createdOn = createdOn
            self.updatedOn = updatedOn
            self.createdBy = createdBy
            self.v = v
            self.nomineeAddress1 = nomineeAddress1
            self.nomineeAddress2 = nomineeAddress2
            self.nomineeCity = nomineeCity
            self.nomineeDOB = nomineeDOB
            self.nomineeEmail = nomineeEmail
            self.nomineeGuardian = nomineeGuardian
            self.nomineeName = nomineeName
            self.nomineePhoneNo = nomineePhoneNo
            self.nomineePincode = nomineePincode
            self.nomineeRelation = nomineeRelation
            self.nomineeState = nomineeState
        }
}

// MARK: - Staff
class Staff: Codable {
    let empID: String?
    let branch: BranchNew?
    let designation, profilephoto, name, phoneNo: String?
    let photo: String?

    enum CodingKeys: String, CodingKey {
        case empID = "empId"
        case branch, designation, profilephoto, name, phoneNo, photo
    }
}

// MARK: - Branch
struct BranchNew: Codable {
    let name, address : String?
    let city, state: String?
    let pincode, phoneNo: String?
    let longitude, latitude: String?
    
    enum CodingKeys: String, CodingKey {
        case address, city, state, name, pincode, phoneNo
        case longitude, latitude
    }
    
    init(name: String?, address: String?, city: String?, state: String?, pincode: String?, phoneNo: String?, longitude: String?, latitude: String?) {
        self.name = name
        self.address = address
        self.city = city
        self.state = state
        self.pincode = pincode
        self.phoneNo = phoneNo
        self.longitude = longitude
        self.latitude = latitude
    }
}

class Ornament: Codable {
    let id, name, karat: String?
    let weight: Int?
    let createdOn, updatedOn: String?
    let status: Bool?
    let lrid, ornamentphoto: String?
    let stoneweight, netweight, purity: Int?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name, karat, weight
        case createdOn = "CreatedOn"
        case updatedOn = "UpdatedOn"
        case status
        case lrid = "LRID"
        case ornamentphoto, stoneweight, netweight, purity
    }

    init(id: String?, name: String?, karat: String?, weight: Int?, createdOn: String?, updatedOn: String?, status: Bool?, lrid: String?, ornamentphoto: String?, stoneweight: Int?, netweight: Int?, purity: Int?) {
        self.id = id
        self.name = name
        self.karat = karat
        self.weight = weight
        self.createdOn = createdOn
        self.updatedOn = updatedOn
        self.status = status
        self.lrid = lrid
        self.ornamentphoto = ornamentphoto
        self.stoneweight = stoneweight
        self.netweight = netweight
        self.purity = purity
    }
}

class LoanDetails: Codable {
    let loanamount, finalloanamount, maximumamountlimit, schememaximumamountlimit: String?
        let roi, tenure, emi, totalinterest: String?
        let totalpayable: String?

        init(loanamount: String?, finalloanamount: String?, maximumamountlimit: String?, schememaximumamountlimit: String?, roi: String?, tenure: String?, emi: String?, totalinterest: String?, totalpayable: String?) {
            self.loanamount = loanamount
            self.finalloanamount = finalloanamount
            self.maximumamountlimit = maximumamountlimit
            self.schememaximumamountlimit = schememaximumamountlimit
            self.roi = roi
            self.tenure = tenure
            self.emi = emi
            self.totalinterest = totalinterest
            self.totalpayable = totalpayable
        }
}

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}


