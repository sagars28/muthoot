//
//  PincodeModel.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/13/21.
//

import Foundation

class PincodeModel: Codable {
    let eventName: String?
    let data: PincodeData?
    let alertType, message: String?
    let status: Bool?

    init(eventName: String?, data: PincodeData?, alertType: String?, message: String?, status: Bool?) {
        self.eventName = eventName
        self.data = data
        self.alertType = alertType
        self.message = message
        self.status = status
    }
}

// MARK: - DataClass
class PincodeData: Codable {
    let pincodes: [String]?
    let enquiryID, header: String?

    enum CodingKeys: String, CodingKey {
        case pincodes
        case enquiryID = "enquiryId"
        case header
    }

    init(pincodes: [String]?, enquiryID: String?, header: String?) {
        self.pincodes = pincodes
        self.enquiryID = enquiryID
        self.header = header
    }
}

