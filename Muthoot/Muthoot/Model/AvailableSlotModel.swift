//
//  AvailableSlotModel.swift
//  Muthoot Track
//
//  Created by Sagar on 12/06/21.
//

import Foundation

// MARK: - Address
class AvailableSlotModel: Codable {
    let alertType, eventName, message: String?
    let status: Bool?
    let data: AvailableData?
}

class AvailableData: Codable {
    let availableDates: [String]?
    let availableSlots: [AvailableSlot]?
    let noOfDays: Int?
    let header: String?
    let loanRequest: LoanRequestData?

    enum CodingKeys: String, CodingKey {
        case availableDates = "AvailableDates"
        case availableSlots = "AvailableSlots"
        case noOfDays = "NoOfDays"
        case header, loanRequest
    }
}

class AvailableSlot: Codable {
    let date, time, period : String?
}


class LoanRequestData: Codable {
    let bufferTime: Int?
    let endTime: String?
    let interval, maxAmount, minAmount, requestDays: Int?
    let startTime: String?
}


// For Available Time
// Separate Time From


//class SeparateTimeModel {
//    static let shared = SeparateTimeModel()
//    var data : [TimeData]?
//}
//

extension Sequence {
    func grouped<T: Equatable>(by block: (Element) throws -> T) rethrows -> [[Element]] {
        var results: [[Element]] = []
        
        var lastValue: T?
        var index = results.endIndex
        for element in self {
            let value = try block(element)
            if let lastValue = lastValue, lastValue == value {
                results[index].append(element)
            } else {
                results.append([element])
                index = results.index(before: results.endIndex)
                lastValue = value
            }
        }
        return results
    }
}

class SeparateTimeModel {
    static let shared = SeparateTimeModel()
    var availableDates : [TimeData]?
}

class TimeData {
    var selectedDate : String?
    var dataTime : [Time]?
    
    init(dataTime : Time, selectedDate : String) {
        self.dataTime?.append(dataTime)
        self.selectedDate = selectedDate
    }
}

class Time {
    var time : String?
    init(time : String) {
        self.time = time
    }
}

