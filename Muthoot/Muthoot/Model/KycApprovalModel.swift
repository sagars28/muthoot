//
//  KYCDetailsModel.swift
//  Muthoot Track
//
//  Created by Sagar on 21/06/21.
//

import Foundation

class KycApprovalModel: Codable {
    let eventName: String?
    let data: KycApproval?
    let message: String?
    //let buttons: JSONNull?
    let status: Bool?

    init(eventName: String?, data: KycApproval?, message: String?, status: Bool?) {
        self.eventName = eventName
        self.data = data
        self.message = message
        self.status = status
    }
}

class KycApproval: Codable {
    let id, custPrefix, firstName, middleName: String?
    let lastName, idType: String?
    let idDocUploaded: Int?
    let uciCode, genderID, careOfPrefix, careOfRelation: String?
    let careOfName, dob, emailID, motherPrefix: String?
    let motherName, maritialStatus, residentialStatus, nationality: String?
    let mobileNumber, occupationID, incomeID, presentAddressLine1: String?
    let presentAddressLine2, presentTown, presentStateID, presentCountry: String?
    let presentPinCode, riskCategory, permanentAddressLine1, permanentAddressLine2: String?
    let permanentTown, permanentStateID, permanentStateName, permanentCountry: String?
    let permanentPinCode, ucicType: String?
    let uploadedDocuments: [UploadedDocument]?
    let lrid: String?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case custPrefix = "CustPrefix"
        case firstName = "FirstName"
        case middleName = "MiddleName"
        case lastName = "LastName"
        case idType = "IDType"
        case idDocUploaded = "IDDocUploaded"
        case uciCode = "UCICode"
        case genderID = "GenderId"
        case careOfPrefix = "CareOfPrefix"
        case careOfRelation = "CareOfRelation"
        case careOfName = "CareOfName"
        case dob = "DOB"
        case emailID = "EmailId"
        case motherPrefix = "MotherPrefix"
        case motherName = "MotherName"
        case maritialStatus = "MaritialStatus"
        case residentialStatus = "ResidentialStatus"
        case nationality = "Nationality"
        case mobileNumber = "MobileNumber"
        case occupationID = "OccupationId"
        case incomeID = "IncomeId"
        case presentAddressLine1 = "PresentAddressLine1"
        case presentAddressLine2 = "PresentAddressLine2"
        case presentTown = "PresentTown"
        case presentStateID = "PresentStateId"
        case presentCountry = "PresentCountry"
        case presentPinCode = "PresentPinCode"
        case riskCategory = "RiskCategory"
        case permanentAddressLine1 = "PermanentAddressLine1"
        case permanentAddressLine2 = "PermanentAddressLine2"
        case permanentTown = "PermanentTown"
        case permanentStateID = "PermanentStateId"
        case permanentStateName = "PermanentStateName"
        case permanentCountry = "PermanentCountry"
        case permanentPinCode = "PermanentPinCode"
        case ucicType = "UCICType"
        case uploadedDocuments = "UploadedDocuments"
        case lrid = "LRID"
    }

    init(id: String?, custPrefix: String?, firstName: String?, middleName: String?, lastName: String?, idType: String?, idDocUploaded: Int?, uciCode: String?, genderID: String?, careOfPrefix: String?, careOfRelation: String?, careOfName: String?, dob: String?, emailID: String?, motherPrefix: String?, motherName: String?, maritialStatus: String?, residentialStatus: String?, nationality: String?, mobileNumber: String?, occupationID: String?, incomeID: String?, presentAddressLine1: String?, presentAddressLine2: String?, presentTown: String?, presentStateID: String?, presentCountry: String?, presentPinCode: String?, riskCategory: String?, permanentAddressLine1: String?, permanentAddressLine2: String?, permanentTown: String?, permanentStateID: String?, permanentStateName: String?, permanentCountry: String?, permanentPinCode: String?, ucicType: String?, uploadedDocuments: [UploadedDocument]?, lrid: String?) {
        self.id = id
        self.custPrefix = custPrefix
        self.firstName = firstName
        self.middleName = middleName
        self.lastName = lastName
        self.idType = idType
        self.idDocUploaded = idDocUploaded
        self.uciCode = uciCode
        self.genderID = genderID
        self.careOfPrefix = careOfPrefix
        self.careOfRelation = careOfRelation
        self.careOfName = careOfName
        self.dob = dob
        self.emailID = emailID
        self.motherPrefix = motherPrefix
        self.motherName = motherName
        self.maritialStatus = maritialStatus
        self.residentialStatus = residentialStatus
        self.nationality = nationality
        self.mobileNumber = mobileNumber
        self.occupationID = occupationID
        self.incomeID = incomeID
        self.presentAddressLine1 = presentAddressLine1
        self.presentAddressLine2 = presentAddressLine2
        self.presentTown = presentTown
        self.presentStateID = presentStateID
        self.presentCountry = presentCountry
        self.presentPinCode = presentPinCode
        self.riskCategory = riskCategory
        self.permanentAddressLine1 = permanentAddressLine1
        self.permanentAddressLine2 = permanentAddressLine2
        self.permanentTown = permanentTown
        self.permanentStateID = permanentStateID
        self.permanentStateName = permanentStateName
        self.permanentCountry = permanentCountry
        self.permanentPinCode = permanentPinCode
        self.ucicType = ucicType
        self.uploadedDocuments = uploadedDocuments
        self.lrid = lrid
    }
}

class UploadedDocument: Codable {
    let idNo, base64Image: String?
    let uploadtypeid: Int?
    let documenttypeid, fileNameArg, expiryDate: String?

    enum CodingKeys: String, CodingKey {
        case idNo = "IDNo"
        case base64Image, uploadtypeid, documenttypeid
        case fileNameArg = "FileNameArg"
        case expiryDate = "ExpiryDate"
    }

    init(idNo: String?, base64Image: String?, uploadtypeid: Int?, documenttypeid: String?, fileNameArg: String?, expiryDate: String?) {
        self.idNo = idNo
        self.base64Image = base64Image
        self.uploadtypeid = uploadtypeid
        self.documenttypeid = documenttypeid
        self.fileNameArg = fileNameArg
        self.expiryDate = expiryDate
    }
}

