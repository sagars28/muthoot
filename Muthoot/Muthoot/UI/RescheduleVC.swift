//
//  RescheduleVC.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/31/21.
//

import UIKit

public class RescheduleVC: BaseController {
    
    let viewCat = UIView()
    
    var scrollViewMain = UIScrollView()
    var viewBack = UIView() // Reschedule View
    let cancelView = UIView() // Cancel View
    
    var cvDate : UICollectionView!
    var cvTime : UICollectionView!
    
    let txtRemark = UITextView()
    let txtCancelReason = UITextView()
    let btnCancelReason = UIButton()
    let btnCancelLoan = UIButton()
    var btnSegment : UISegmentedControl!
    let btnReason = UIButton()
    
    let lblCancelReason = UILabel()
    
    let lblService = UILabel()
    let lblMonth = UILabel()
    
    var heightConstraint : NSLayoutConstraint?
    
    var horizontalBarWidthAnchor: NSLayoutConstraint?
    var horizontalBarLeftAnchor: NSLayoutConstraint?
    
    var selectedDate : Int?
    var selectedTime : Int?
    
    var arrAvailableTimeSlot = [String]()
    var arrTimeSlot = [String]()
    
    var lastTag = 200
    var arrCat = [String]()
    
    var objReschedule : RescheduleModel?
    var objLoanSummary : LoanSummaryModel? // Coming from Loan Summary View Controller
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.viewBackground
        setupData()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        createNavigationBarWithBack("Reschedule Appointments")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.HandleRescheduleResponce(notification:)), name: Notification.Name("SuccessSubmitReschedule"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.HandleCancelResponce(notification:)), name: Notification.Name("SuccessSubmitLoanCancel"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    public override func viewDidLayoutSubviews() {
        heightConstraint?.constant = cvTime.contentSize.height
    }
    
    
    fileprivate func setupUI() {
        
        viewCat.backgroundColor = .white
        viewCat.layer.cornerRadius = 22.5
        self.view.addSubview(viewCat)
        viewCat.enableAutolayout()
        viewCat.topMargin(Constraints.top + self.topHeight + 10)
        viewCat.leadingMargin(30)
        viewCat.trailingMargin(30)
        viewCat.fixHeight(45)
        
        
        var btnGlobal = UIButton()
        let width = ((Int(screenWidth - 60))/arrCat.count)
        
        
        let horizontalBarView = UIView()
        horizontalBarView.clipsToBounds = true
        horizontalBarView.backgroundColor = Colors.theme
        horizontalBarView.layer.cornerRadius = 22.5
        horizontalBarView.addShadow()
        viewCat.addSubview(horizontalBarView)
        horizontalBarView.enableAutolayout()
        horizontalBarView.fixHeight(45)
        horizontalBarView.bottomMargin(0)
        
        horizontalBarLeftAnchor = horizontalBarView.leadingAnchor.constraint(equalTo: viewCat.leadingAnchor, constant: 0)
        horizontalBarWidthAnchor = horizontalBarView.widthAnchor.constraint(equalToConstant: CGFloat(width))
        
        horizontalBarLeftAnchor?.isActive = true
        horizontalBarWidthAnchor?.isActive = true
        
        for idx in 0..<arrCat.count {
            let btnCategory = UIButton()
            btnCategory.tag = 200 + idx
            btnCategory.setTitle(arrCat[idx], for: .normal)
            btnCategory.titleLabel?.font = UIFont(name: Fonts.HelveticaNeueMedium, size: TextSize.title)
            btnCategory.setTitleColor(idx == 0 ? .white:Colors.gray, for: .normal)
            btnCategory.backgroundColor  = .clear // Colors.theme
            btnCategory.addTarget(self, action: #selector(slideAction), for: .touchUpInside)
            viewCat.addSubview(btnCategory)
            btnCategory.enableAutolayout()
            btnCategory.fixWidth(CGFloat(width))
            btnCategory.topMargin(0)
            btnCategory.bottomMargin(0)
            btnCategory.fixHeight(45)
            
            idx == 0 ? btnCategory.leadingMargin(0) : btnCategory.add(toRight: 0, of: btnGlobal)
            btnGlobal = btnCategory
        }
        
        let bottomView = UIView()
        bottomView.backgroundColor = .white
        bottomView.clipsToBounds = true
        bottomView.addShadow()
        view.addSubview(bottomView)
        bottomView.enableAutolayout()
        bottomView.centerX()
        bottomView.fixWidth(screenWidth)
        bottomView.bottomMargin(Constraints.bottom)
        bottomView.fixHeight(50)
        
        let separatorBottom = UIView()
        separatorBottom.backgroundColor = Colors.textGray.withAlphaComponent(0.5)
        self.view.addSubview(separatorBottom)
        separatorBottom.enableAutolayout()
        separatorBottom.centerX()
        separatorBottom.fixWidth(screenWidth)
        separatorBottom.aboveView(0, to: bottomView)
        separatorBottom.fixHeight(0.5)
        
        btnCancelLoan.setTitle("Reschedule loan request".uppercased(), for: .normal)
        btnCancelLoan.setTitleColor(Colors.theme, for: .normal)
        btnCancelLoan.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        btnCancelLoan.addTarget(self, action: #selector(handleRescheduleOrCancelBtn(_:)), for: .touchUpInside)
        bottomView.addSubview(btnCancelLoan)
        btnCancelLoan.enableAutolayout()
        btnCancelLoan.trailingMargin(20)
        btnCancelLoan.bottomMargin(0)
        btnCancelLoan.topMargin(0)
        
        scrollViewMain.contentInsetAdjustmentBehavior = .never
        scrollViewMain.backgroundColor = Colors.viewBackground
        scrollViewMain.bounces = false
        scrollViewMain.isScrollEnabled = true
        scrollViewMain.showsVerticalScrollIndicator = true
        view.addSubview(scrollViewMain)
        scrollViewMain.enableAutolayout()
        scrollViewMain.leadingMargin(0)
        scrollViewMain.fixWidth(screenWidth)
        scrollViewMain.belowView(8, to: viewCat)
        scrollViewMain.aboveView(0, to: separatorBottom)
        
        cancelView.isHidden = true
        cancelView.clipsToBounds = true
        cancelView.backgroundColor = Colors.viewBackground
        
        viewBack.clipsToBounds = true
        viewBack.backgroundColor = Colors.viewBackground
        
        
        let stackView   = UIStackView()
        stackView.axis  = NSLayoutConstraint.Axis.vertical
        stackView.distribution  = .fill
        stackView.spacing   = 16.0
        scrollViewMain.addSubview(stackView)
        stackView.enableAutolayout()
        stackView.leadingMargin(0)
        stackView.trailingMargin(0)
        stackView.topMargin(0)
        stackView.bottomMargin(0)
        stackView.flexibleHeightGreater(10)
        
        viewBack.clipsToBounds = true
        viewBack.backgroundColor = Colors.viewBackground
        stackView.addArrangedSubview(viewBack)
        stackView.addArrangedSubview(cancelView)
        
        lblService.numberOfLines = 0
        lblService.textAlignment = .center
        lblService.font = UIFont(name: Fonts.latoRegular, size: TextSize.title + 2)
        viewBack.addSubview(lblService)
        lblService.enableAutolayout()
        lblService.topMargin(8)
        lblService.leadingMargin(20)
        lblService.trailingMargin(8)
        lblService.flexibleHeightGreater(25)
        
        
        let lblSelectDate = UILabel()
        lblSelectDate.text = "Select Date"
        lblSelectDate.textAlignment = .left
        lblSelectDate.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        lblSelectDate.textColor = Colors.theme
        viewBack.addSubview(lblSelectDate)
        lblSelectDate.enableAutolayout()
        lblSelectDate.belowView(20, to: lblService)
        lblSelectDate.leadingMargin(30)
        lblSelectDate.trailingMargin(16)
        lblSelectDate.fixHeight(30)
        
        
        let viewTime = UIView()
        viewTime.backgroundColor = Colors.viewBackground
        viewTime.clipsToBounds = true
        viewBack.addSubview(viewTime)
        viewTime.enableAutolayout()
        viewTime.belowView(8, to: lblSelectDate)
        viewTime.leadingMargin(8)
        viewTime.trailingMargin(8)
        viewTime.fixHeight(100)
        
        lblMonth.text = ""
        lblMonth.textAlignment = .left
        lblMonth.font = UIFont(name: Fonts.HelveticaNeueMedium, size: TextSize.subTitle + 2)
        lblMonth.textColor = .black
        viewTime.addSubview(lblMonth)
        lblMonth.enableAutolayout()
        lblMonth.bottomMargin(16)
        lblMonth.leadingMargin(22)
        //lblMonth.fixWidth(50)
        lblMonth.fixHeight(30)
        
        // let width = (screenWidth-100)/3
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 62, height: 90)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 16
        layout.minimumLineSpacing = 16
        
        
        cvDate = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvDate.clipsToBounds = true
        cvDate.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 16)
        cvDate.tag = 500
        cvDate.showsHorizontalScrollIndicator = false
        cvDate.delegate = self
        cvDate.dataSource = self
        cvDate.backgroundColor = Colors.viewBackground
        cvDate.showsVerticalScrollIndicator = false
        cvDate.isScrollEnabled = true
        cvDate.bounces = false
        cvDate.register(DayCell.self, forCellWithReuseIdentifier: "cell")
        viewTime.addSubview(cvDate)
        cvDate.enableAutolayout()
        cvDate.topMargin(0)
        cvDate.leadingMargin(70)
        cvDate.trailingMargin(0)
        cvDate.bottomMargin(0)
        
        
        let lblSelectTime = UILabel()
        lblSelectTime.text = "Select Time"
        lblSelectTime.textAlignment = .left
        lblSelectTime.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        lblSelectTime.textColor = Colors.theme
        viewBack.addSubview(lblSelectTime)
        lblSelectTime.enableAutolayout()
        lblSelectTime.belowView(20, to: viewTime)
        lblSelectTime.leadingMargin(30)
        lblSelectTime.trailingMargin(8)
        lblSelectTime.fixHeight(30)
        
        let width1 = (screenWidth-120)/3
        let layout1 = UICollectionViewFlowLayout()
        layout1.itemSize = CGSize(width: width1, height: 40)
        layout1.scrollDirection = .vertical
        layout1.minimumInteritemSpacing = 16
        layout1.minimumLineSpacing = 16
        
        cvTime = UICollectionView(frame: .zero, collectionViewLayout: layout1)
        cvTime.clipsToBounds = true
        cvTime.contentInset = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30)
        cvTime.tag = 501
        cvTime.showsHorizontalScrollIndicator = false
        cvTime.delegate = self
        cvTime.dataSource = self
        cvTime.backgroundColor = Colors.viewBackground
        cvTime.showsVerticalScrollIndicator = false
        cvTime.isScrollEnabled = true
        cvTime.bounces = false
        cvTime.register(TimeCell.self, forCellWithReuseIdentifier: "cell")
        viewBack.addSubview(cvTime)
        cvTime.enableAutolayout()
        cvTime.fixWidth(screenWidth)
        cvTime.belowView(15, to: lblSelectTime)
        cvTime.leadingMargin(0)
        cvTime.trailingMargin(0)
        heightConstraint = cvTime.heightAnchor.constraint(equalToConstant: 20)
        heightConstraint?.isActive = true
        
        let lblReschedule = UILabel()
        lblReschedule.text = "Select Reschedule Reason"
        lblReschedule.textAlignment = .left
        lblReschedule.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle + 3)
        lblReschedule.textColor = Colors.gray
        viewBack.addSubview(lblReschedule)
        lblReschedule.enableAutolayout()
        lblReschedule.belowView(20, to: cvTime)
        lblReschedule.leadingMargin(30)
        lblReschedule.trailingMargin(16)
        lblReschedule.fixHeight(30)
        
        btnReason.titleLabel?.numberOfLines = 0
        btnReason.titleLabel?.lineBreakMode = .byTruncatingTail
        btnReason.backgroundColor = .white
        btnReason.contentHorizontalAlignment = .left
        btnReason.titleLabel?.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title + 1)
        btnReason.setTitle("Select Reason", for: .normal)
        btnReason.setTitleColor(.black, for: .normal)
        btnReason.layer.cornerRadius = 5
        btnReason.layer.borderWidth = 0.5
        btnReason.layer.borderColor = Colors.darkGray.cgColor
        btnReason.titleEdgeInsets = UIEdgeInsets(top: 4, left: 20, bottom: 4, right: 30)
        btnReason.addTarget(self,action:#selector(handleRescheduleBtn),
                            for:.touchUpInside)
        viewBack.addSubview(btnReason)
        btnReason.enableAutolayout()
        btnReason.belowView(8, to: lblReschedule)
        btnReason.fixHeight(50)
        btnReason.leadingMargin(30)
        btnReason.trailingMargin(30)
        
        
        let downArrow = UIImageView()
        downArrow.clipsToBounds = true
        downArrow.contentMode = .scaleAspectFit
        downArrow.image = UIImage(named: "shape", in: Bundle(for: type(of: self
        )), compatibleWith: nil)
        btnReason.addSubview(downArrow)
        downArrow.enableAutolayout()
        downArrow.fixHeight(12)
        downArrow.fixWidth(12)
        downArrow.trailingMargin(16)
        downArrow.centerY()
        
        let lblRemark = UILabel()
        lblRemark.text = "Remark"
        lblRemark.textAlignment = .left
        lblRemark.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle + 3)
        lblRemark.textColor = Colors.gray
        viewBack.addSubview(lblRemark)
        lblRemark.enableAutolayout()
        lblRemark.belowView(20, to: btnReason)
        lblRemark.leadingMargin(30)
        lblRemark.trailingMargin(16)
        lblRemark.fixHeight(30)
        
        
        txtRemark.clipsToBounds = true
        txtRemark.isSelectable = true
        txtRemark.isScrollEnabled = true
        txtRemark.isEditable = true
        let toolbar = UIToolbar().ToolbarPiker(mySelect: #selector(handToolBar), title: "Done")
        txtRemark.inputAccessoryView = toolbar
        txtRemark.backgroundColor = .white
        txtRemark.layer.cornerRadius = 8
        txtRemark.layer.borderColor = Colors.darkGray.cgColor
        txtRemark.layer.borderWidth = 0.5
        txtRemark.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        txtRemark.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle + 1)
        txtRemark.addShadow()
        txtRemark.clipsToBounds = true
        viewBack.addSubview(txtRemark)
        txtRemark.enableAutolayout()
        txtRemark.belowView(8, to: lblRemark)
        txtRemark.leadingMargin(30)
        txtRemark.trailingMargin(30)
        txtRemark.fixHeight(120)
        txtRemark.bottomMargin(20)
        
        //MARK:-  End Of Reschedule View
        // Start Cancel View
        
        lblCancelReason.numberOfLines = 0
        lblCancelReason.text = "Please let us know why you're cancelling.."
        lblCancelReason.textAlignment = .center
        lblCancelReason.font = UIFont(name: Fonts.latoRegular, size: TextSize.title + 2)
        lblCancelReason.textColor = .black
        cancelView.addSubview(lblCancelReason)
        lblCancelReason.enableAutolayout()
        lblCancelReason.topMargin(20)
        lblCancelReason.leadingMargin(20)
        lblCancelReason.trailingMargin(16)
        lblCancelReason.flexibleHeightGreater(30)
        
        let lblSelectCancelReason = UILabel()
        lblSelectCancelReason.text = "Select Cancel Reason"
        lblSelectCancelReason.textAlignment = .left
        lblSelectCancelReason.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        lblSelectCancelReason.textColor = Colors.gray
        cancelView.addSubview(lblSelectCancelReason)
        lblSelectCancelReason.enableAutolayout()
        lblSelectCancelReason.belowView(20, to: lblCancelReason)
        lblSelectCancelReason.leadingMargin(30)
        lblSelectCancelReason.trailingMargin(16)
        lblSelectCancelReason.fixHeight(30)
        
        btnCancelReason.titleLabel?.numberOfLines = 0
        btnCancelReason.titleLabel?.lineBreakMode = .byTruncatingTail
        btnCancelReason.backgroundColor = .white
        btnCancelReason.contentHorizontalAlignment = .left
        btnCancelReason.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 1)
        btnCancelReason.setTitle("Select Reason", for: .normal)
        btnCancelReason.setTitleColor(.black, for: .normal)
        btnCancelReason.layer.cornerRadius = 5
        btnCancelReason.layer.borderWidth = 0.5
        btnCancelReason.layer.borderColor = Colors.textGray.cgColor
        btnCancelReason.titleEdgeInsets = UIEdgeInsets(top: 4, left: 20, bottom: 4, right: 30)
        btnCancelReason.addTarget(self,action:#selector(handleCancelReason(_:)),
                                  for:.touchUpInside)
        cancelView.addSubview(btnCancelReason)
        btnCancelReason.enableAutolayout()
        btnCancelReason.belowView(8, to: lblSelectCancelReason)
        btnCancelReason.flexibleHeightGreater(50)
        btnCancelReason.leadingMargin(30)
        btnCancelReason.trailingMargin(30)
        
        let downArrow2 = UIImageView()
        downArrow2.clipsToBounds = true
        downArrow2.contentMode = .scaleAspectFit
        downArrow2.image = UIImage(named: "shape", in: Bundle(for: type(of: self
        )), compatibleWith: nil)
        btnCancelReason.addSubview(downArrow2)
        downArrow2.enableAutolayout()
        downArrow2.fixHeight(12)
        downArrow2.fixWidth(12)
        downArrow2.trailingMargin(16)
        downArrow2.centerY()
        
        let lblRemark2 = UILabel()
        lblRemark2.text = "Remark"
        lblRemark2.textAlignment = .left
        lblRemark2.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        lblRemark2.textColor = Colors.gray
        cancelView.addSubview(lblRemark2)
        lblRemark2.enableAutolayout()
        lblRemark2.belowView(20, to: btnCancelReason)
        lblRemark2.leadingMargin(30)
        lblRemark2.trailingMargin(16)
        lblRemark2.fixHeight(30)
        
        
        txtCancelReason.clipsToBounds = true
        txtCancelReason.isSelectable = true
        txtCancelReason.isScrollEnabled = true
        txtCancelReason.isEditable = true
        txtCancelReason.inputAccessoryView = toolbar
        txtCancelReason.backgroundColor = .white
        txtCancelReason.layer.cornerRadius = 8
        txtCancelReason.layer.borderColor = Colors.textGray.cgColor
        txtCancelReason.layer.borderWidth = 0.5
        txtCancelReason.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        txtCancelReason.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle + 1)
        txtCancelReason.addShadow()
        txtCancelReason.clipsToBounds = true
        cancelView.addSubview(txtCancelReason)
        txtCancelReason.enableAutolayout()
        txtCancelReason.belowView(8, to: lblRemark2)
        txtCancelReason.leadingMargin(30)
        txtCancelReason.trailingMargin(30)
        txtCancelReason.fixHeight(120)
        txtCancelReason.bottomMargin(20)
        
    }
    
    
    func setupData() {
        
        for v in view.subviews {
            v.removeFromSuperview()
        }
        
        createNavigationBarWithBack("Reschedule Appointments")
        arrCat = ["Reschedule", "Cancel"]
        setupUI()
        
        //Reschedule Tab Setup
        
        let header = self.objReschedule?.data?.rescheduleheader ?? ""
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1
        lblService.attributedText = NSMutableAttributedString(string: header, attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        lblService.textAlignment = .center
        
        if ((self.objReschedule?.data?.availabledates?.count ?? 0) > 0) {
            self.lblMonth.text = self.objReschedule?.data?.availabledates?[0].convertToMonth(in: "dd/MM/yyyy", out: "MMM")
        }
        
        arrAvailableTimeSlot.removeAll()
        arrTimeSlot.removeAll()
        if ((self.objReschedule?.data?.availableslots?.count ?? 0) > 0) {
            for index in 0..<(self.objReschedule?.data?.availableslots?.count ?? 0) {
                if ((self.objReschedule?.data?.availableslots?[index].date ?? "") == (self.objReschedule?.data?.availabledates?[0] ?? "")) {
                    self.arrAvailableTimeSlot.append((self.objReschedule?.data?.availableslots?[index].period ?? ""))
                    self.arrTimeSlot.append((self.objReschedule?.data?.availableslots?[index].time ?? ""))
                }
            }
            selectedDate = 0
        }
        
        let header2 = self.objReschedule?.data?.cancelheader ?? ""
        let paragraphStyle2 = NSMutableParagraphStyle()
        paragraphStyle2.lineHeightMultiple = 1
        
        lblCancelReason.attributedText = NSMutableAttributedString(string: header2, attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle2])
        lblCancelReason.textAlignment = .center
        
        cvDate.reloadData()
        cvTime.reloadData()
        heightConstraint?.constant = cvTime.contentSize.height
        
        if ((self.objReschedule?.data?.availabledates?.count ?? 0) >= 0) {
            let indexPath = IndexPath(item: 0, section: 0)
            cvDate.selectItem(at: indexPath, animated: true, scrollPosition: .top)
        }
    }
    
    
    
    //MARK: Slide Button Action
    @objc func slideAction(_ sender: UIButton) {
        if sender.tag != lastTag {
            for idx in 0..<arrCat.count {
                let btn = self.view.viewWithTag(200+idx) as! UIButton
                btn.setTitleColor(Colors.gray, for: .normal)
            }
            sender.setTitleColor(.white, for: .normal)
            horizontalBarLeftAnchor?.constant = sender.frame.minX
            
            UIView.animate(withDuration: 0.25, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.view.layoutIfNeeded()
            })
            
            lastTag = sender.tag
            self.view.endEditing(true)
            
            switch sender.tag {
            case 200:
                self.cancelView.isHidden = true
                self.viewBack.isHidden = false
                btnCancelLoan.setTitle("Reschedule loan request".uppercased(), for: .normal)
                
            case 201:
                self.cancelView.isHidden = false
                self.viewBack.isHidden = true
                btnCancelLoan.setTitle("Cancel loan request".uppercased(), for: .normal)
                
            default:
                break
            }
        }
    }
    
    @objc fileprivate func setOffset() {
        let btn = view.viewWithTag(200) as? UIButton
        slideAction(btn ?? UIButton())
    }
    
    @objc func handleRescheduleBtn(_ sender : UIButton) {
        let popup = SliderPopup()
        popup.delegate = self
        popup.frame = view.bounds
        let arr = objReschedule?.data?.reschedulereasons?.map { ($0.reason ?? "") } ?? []
        popup.createUI(title: "Select Reason", arrOption: arr)
        view.window?.addSubview(popup)
    }
    
    
    @objc func handleCancelReason(_ sender : UIButton) {
        let popup = SliderPopup()
        popup.delegate = self
        popup.frame = view.bounds
        let arr = objReschedule?.data?.cancelreasons?.map { ($0.reason ?? "") } ?? []
        popup.createUI(title: "Select Reason", arrOption: arr)
        view.window?.addSubview(popup)
    }
    
    @objc func handleHeight() {
        heightConstraint?.constant = cvTime.contentSize.height
        self.view.layoutIfNeeded()
    }
    
    @objc func HandleRescheduleResponce(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(DeleteAddModel.self, from: jsonData!)
            if (data.status ?? false) {
                
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "", message: data.message ?? "", preferredStyle: .alert)
                    let action = UIAlertAction(title: "Ok", style: .cancel) { (action) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            else {
                self.showToast(message: data.message ?? "")
            }
        }
        catch let error {
            //print(error)
        }
    }
    
    @objc func HandleCancelResponce(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(DeleteAddModel.self, from: jsonData!)
            if (data.status ?? false) {
                
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: "", message: data.message ?? "", preferredStyle: .alert)
                    let action = UIAlertAction(title: "Ok", style: .cancel) { (action) in
                        self.navigationController?.popToRootViewController(animated: true)
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            else {
                self.showToast(message: data.message ?? "")
            }
        }
        catch let error {
           // print(error)
        }
    }
    
    
    @objc func keyboardWillShow(notification:NSNotification) {
        
        guard let userInfo = notification.userInfo else {return}
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        let keyboardFrame = keyboardSize.cgRectValue
        
        if (lastTag == 201) {
            if (isIphoneX()) {
                if self.scrollViewMain.bounds.origin.y == 0 ||  self.scrollViewMain.bounds.origin.y == 0.0 {
                    self.scrollViewMain.bounds.origin.y += 50
                }
            }
            else {
                if self.scrollViewMain.bounds.origin.y == 0 ||  self.scrollViewMain.bounds.origin.y == 0.0 {
                    self.scrollViewMain.bounds.origin.y += 140
                }
            }
        }
        else {
            if self.viewBack.bounds.origin.y == 0{
                self.viewBack.bounds.origin.y += keyboardFrame.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        if (lastTag == 201) {
            if (isIphoneX()) {
                self.scrollViewMain.bounds.origin.y = 0
            }
            else {
                self.scrollViewMain.bounds.origin.y = 0
            }
        }
        else {
            self.viewBack.bounds.origin.y = 0
        }
    }
    
    @objc func handleRescheduleOrCancelBtn(_ sender : UIButton) {
        if (lastTag == 200) {
            
            if (self.selectedDate == nil) {
                self.showToast(message: "Please Select Date")
                return
            }
            
            if (self.selectedTime == nil) {
                self.showToast(message: "Please Select Time")
                return
            }
            
            if (btnReason.titleLabel?.text == "" || btnReason.titleLabel?.text == "Select Reason" ) {
                self.showToast(message: "Please Select Reason")
                return
            }
            
            if (btnReason.titleLabel?.text == "Other" || btnReason.titleLabel?.text == "Others") {
                if (self.txtRemark.text ?? "" == "") {
                    self.showToast(message: "Please Enter Remark")
                    return
                }
            }
            
            Loader.shared.StartActivityIndicator(obj: self)
            let mobileNo = self.objLoanSummary?.data?[0].mobileNo ?? 0
            let LRID = self.objLoanSummary?.data?[0].lrid ?? ""
            let enquiryId = self.objLoanSummary?.data?[0].datumID ?? ""
            let remark = self.txtRemark.text ?? ""
            let reason = self.btnReason.titleLabel?.text ?? ""
            let selecteddate = self.objReschedule?.data?.availabledates?[selectedDate!] ?? ""
            let selectedtime = self.arrTimeSlot[selectedTime!]+":00"
            
            let locationDict = ["mobileNo": mobileNo,
                                "LRID":LRID ,
                                "enquiryId": enquiryId,
                                "remark": remark,
                                "reason": reason,
                                "selecteddate": selecteddate,
                                "selectedtime": selectedtime] as [String : Any]
            
            let mainDict = ["eventName": "SUBMITRESCHEDULEAPPOINTMENT", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
        }
        
        else {
            if (btnCancelReason.titleLabel?.text == "" || btnCancelReason.titleLabel?.text == "Select Reason" ) {
                self.showToast(message: "Please Select Reason")
                return
            }
            
            if (btnCancelReason.titleLabel?.text == "Other" || btnCancelReason.titleLabel?.text == "Others") {
                if (self.txtCancelReason.text ?? "" == "") {
                    self.showToast(message: "Please Enter Remark")
                    return
                }
            }
            
            Loader.shared.StartActivityIndicator(obj: self)
            // Emits StartProcess
            
            let mobileNo = self.objLoanSummary?.data?[0].mobileNo ?? 0
            let LRID = self.objLoanSummary?.data?[0].lrid ?? ""
            let enquiryId = self.objLoanSummary?.data?[0].datumID ?? ""
            let remark = self.txtCancelReason.text ?? ""
            let reason = self.btnCancelReason.titleLabel?.text ?? ""
            
            let locationDict = ["mobileNo": mobileNo,
                                "LRID": LRID,
                                "enquiryId": enquiryId,
                                "remark": remark,
                                "reason": reason] as [String : Any]
            
            let mainDict = ["eventName": "CANCELLED", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
        }
    }
}


extension RescheduleVC : UICollectionViewDataSource, UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView.tag == 500) {
            return self.objReschedule?.data?.availabledates?.count ?? 0
        }
        else {
            return self.arrAvailableTimeSlot.count
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (collectionView.tag == 500) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DayCell
            cell.objSlot2 = self.objReschedule
            cell.setupData2(index : indexPath.row)
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TimeCell
            cell.lblTime.text = self.arrAvailableTimeSlot[indexPath.row]
            return cell
        }
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView.tag == 500) {
            selectedDate = indexPath.row
            
            arrTimeSlot.removeAll()
            arrAvailableTimeSlot.removeAll()
            if ((self.objReschedule?.data?.availabledates?.count ?? 0) > 0) {
                for index in 0..<(self.objReschedule?.data?.availableslots?.count ?? 0) {
                    if ((self.objReschedule?.data?.availableslots?[index].date ?? "") == (self.objReschedule?.data?.availabledates?[selectedDate!] ?? "")) {
                        self.arrAvailableTimeSlot.append((self.objReschedule?.data?.availableslots?[index].period ?? ""))
                        self.arrTimeSlot.append((self.objReschedule?.data?.availableslots?[index].time ?? ""))
                    }
                }
            }
            self.selectedTime = nil
            cvTime.reloadData()
            perform(#selector(handleHeight), with: self, with: 0.2)
            
            cvTime.reloadData()
            perform(#selector(handleHeight), with: self, with: 0.2)
        }
        else {
            selectedTime = indexPath.row
        }
    }
}

extension RescheduleVC : UITextViewDelegate {
    public func textViewDidBeginEditing(_ textView: UITextView) {
        //textView.contentInset = UIEdgeInsets.zero;
    }
}

extension RescheduleVC : SelectionDelegate {
    func SelectedText(tag: Int, text: String, index: Int) {
        if (lastTag == 200) {
            btnReason.setTitle(text, for: .normal)
        }
        else {
            btnCancelReason.setTitle(text, for: .normal)
        }
    }
}

