//
//  RepledgeRequestVC.swift
//  Muthoot Track
//
//  Created by Sagar on 28/06/21.
//

import UIKit

public class RepledgeRequestVC: BaseController {
    
    let scrollViewMain = UIScrollView()
    let viewBack = UIView()
    
    let lblService = UILabel()
    let lblMonth = UILabel()
    
    var cvDate : UICollectionView!
    var cvTime : UICollectionView!
    var heightConstraint : NSLayoutConstraint?
    
    var selectedDate : Int?
    var selectedTime : Int?
    var objSlot : RepledgeModel?
    var arrAvailableTimeSlot = [String]()
    
    let enqID = ""
    
    public var mobileNo = ""
    public var loanNo = ""
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        SocketHelper.shared.closeConnection()
        VersionControlSocketHelper.shared.establishConnection()
        
        self.view.backgroundColor = .white
        setupUI()
        Loader.shared.StartActivityIndicator(obj: self)
        CommonModel.shared.mobileNumber = self.mobileNo
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        
        // For Version Control
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleConfig(notification:)), name: Notification.Name("Config"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleConfigMainSocket(notification:)), name: Notification.Name("ConfigMainSocket"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleReplegeRequest(notification:)), name: Notification.Name("MoveToRRDateSelector"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleSendDateTime(notification:)), name: Notification.Name("SendLoanRRDateTimeResponse"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleError(notification:)), name: Notification.Name("LoanReleaseRepledgeError"), object: nil)
        
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    fileprivate func setupUI() {
        
        let bottomView = UIView()
        bottomView.clipsToBounds = true
        bottomView.backgroundColor = Colors.theme
        view.addSubview(bottomView)
        bottomView.enableAutolayout()
        bottomView.centerX()
        bottomView.fixWidth(screenWidth)
        bottomView.bottomMargin(Constraints.bottom)
        bottomView.fixHeight(50)
        
        let btnComplete = UIButton()
        btnComplete.backgroundColor = Colors.theme
        btnComplete.clipsToBounds = true
        btnComplete.setTitle("COMPLETE", for: .normal)
        btnComplete.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        btnComplete.setTitleColor(.white, for: .normal)
        btnComplete.layer.cornerRadius = 6
        btnComplete.addShadow()
        btnComplete.addTarget(self, action: #selector(handleComplete(_:)), for: .touchUpInside)
        bottomView.addSubview(btnComplete)
        btnComplete.enableAutolayout()
        btnComplete.centerY()
        btnComplete.trailingMargin(20)
        btnComplete.fixHeight(50)
        
        
        
        scrollViewMain.contentInsetAdjustmentBehavior = .never
        scrollViewMain.backgroundColor = UIColor.white
        scrollViewMain.bounces = false
        scrollViewMain.isScrollEnabled = true
        scrollViewMain.showsVerticalScrollIndicator = true
        view.addSubview(scrollViewMain)
        scrollViewMain.enableAutolayout()
        scrollViewMain.leadingMargin(0)
        scrollViewMain.fixWidth(screenWidth)
        scrollViewMain.topMargin(self.topbarHeight)
        scrollViewMain.aboveView(0, to: bottomView)
        
        // Main View Under Scroll View
        
        viewBack.backgroundColor = .white
        scrollViewMain.addSubview(viewBack)
        viewBack.enableAutolayout()
        viewBack.centerX()
        viewBack.fixWidth(screenWidth)
        viewBack.topMargin(0)
        viewBack.bottomMargin(0)
        viewBack.flexibleHeightGreater(10)
        
        
        lblService.font = UIFont(name: Fonts.HelveticaNeueMedium, size: 18)
        viewBack.addSubview(lblService)
        lblService.enableAutolayout()
        lblService.topMargin(8)
        lblService.leadingMargin(8)
        lblService.trailingMargin(8)
        lblService.fixHeight(30)
        
        
        let lblSelectDate = UILabel()
        lblSelectDate.text = "Select Date"
        lblSelectDate.textAlignment = .left
        lblSelectDate.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        lblSelectDate.textColor = Colors.theme
        viewBack.addSubview(lblSelectDate)
        lblSelectDate.enableAutolayout()
        lblSelectDate.belowView(20, to: lblService)
        lblSelectDate.leadingMargin(16)
        lblSelectDate.trailingMargin(16)
        lblSelectDate.fixHeight(30)
        
        
        let viewTime = UIView()
        viewTime.backgroundColor = .white
        viewTime.clipsToBounds = true
        viewBack.addSubview(viewTime)
        viewTime.enableAutolayout()
        viewTime.belowView(8, to: lblSelectDate)
        viewTime.leadingMargin(8)
        viewTime.trailingMargin(8)
        viewTime.fixHeight(100)
        
        
        lblMonth.text = ""
        lblMonth.textAlignment = .left
        lblMonth.font = UIFont(name: Fonts.HelveticaNeueMedium, size: TextSize.title)
        lblMonth.textColor = .black
        viewTime.addSubview(lblMonth)
        lblMonth.enableAutolayout()
        lblMonth.bottomMargin(16)
        lblMonth.leadingMargin(8)
        //lblMonth.fixWidth(50)
        lblMonth.fixHeight(30)
        
        // let width = (screenWidth-100)/3
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 62, height: 90)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 16
        layout.minimumLineSpacing = 16
        
        
        cvDate = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvDate.isUserInteractionEnabled = true
        cvDate.clipsToBounds = true
        cvDate.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 16)
        cvDate.tag = 500
        cvDate.showsHorizontalScrollIndicator = false
        cvDate.delegate = self
        cvDate.dataSource = self
        cvDate.backgroundColor = .white
        cvDate.showsVerticalScrollIndicator = false
        cvDate.isScrollEnabled = true
        cvDate.bounces = false
        cvDate.register(DayCell.self, forCellWithReuseIdentifier: "cell")
        viewTime.addSubview(cvDate)
        cvDate.enableAutolayout()
        cvDate.fixWidth(screenWidth)
        cvDate.topMargin(0)
        cvDate.leadingMargin(56)
        cvDate.trailingMargin(0)
        cvDate.bottomMargin(0)
        
        
        let lblSelectTime = UILabel()
        lblSelectTime.text = "Select Time"
        lblSelectTime.textAlignment = .left
        lblSelectTime.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        lblSelectTime.textColor = Colors.theme
        viewBack.addSubview(lblSelectTime)
        lblSelectTime.enableAutolayout()
        lblSelectTime.belowView(20, to: viewTime)
        lblSelectTime.leadingMargin(16)
        lblSelectTime.trailingMargin(8)
        lblSelectTime.fixHeight(30)
        
        let width1 = (screenWidth-120)/3
        let layout1 = UICollectionViewFlowLayout()
        layout1.itemSize = CGSize(width: width1, height: 40)
        layout1.scrollDirection = .vertical
        layout1.minimumInteritemSpacing = 16
        layout1.minimumLineSpacing = 16
        
        cvTime = UICollectionView(frame: .zero, collectionViewLayout: layout1)
        cvTime.clipsToBounds = true
        cvTime.contentInset = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30)
        cvTime.tag = 501
        cvTime.showsHorizontalScrollIndicator = false
        cvTime.delegate = self
        cvTime.dataSource = self
        cvTime.backgroundColor = .white
        cvTime.showsVerticalScrollIndicator = false
        cvTime.isScrollEnabled = true
        cvTime.bounces = false
        cvTime.register(TimeCell.self, forCellWithReuseIdentifier: "cell")
        viewBack.addSubview(cvTime)
        cvTime.enableAutolayout()
        cvTime.fixWidth(screenWidth)
        cvTime.belowView(15, to: lblSelectTime)
        cvTime.leadingMargin(0)
        cvTime.trailingMargin(0)
        cvTime.bottomMargin(20)
        heightConstraint = cvTime.heightAnchor.constraint(equalToConstant: 20)
        heightConstraint?.isActive = true
    }
    
    @objc func handleHeight() {
        heightConstraint?.constant = cvTime.contentSize.height
        self.view.layoutIfNeeded()
    }
    
    // MARK:- SetupData
    
    func setupData() {
        if ((self.objSlot?.data?.availableDates?.count ?? 0) > 0) {
            let date = self.objSlot?.data?.availableDates?[0].convertToMonth(in: "dd/MM/yyyy", out: "MMM")
            lblMonth.text = date
            
            // Fetch Particulate Date
            // And Stored On arrAvailableTimeSlot Array
            
            arrAvailableTimeSlot.removeAll()
            if ((self.objSlot?.data?.availableDates?.count ?? 0) > 0) {
                for index in 0..<(self.objSlot?.data?.availableSlots?.count ?? 0) {
                    if ((self.objSlot?.data?.availableSlots?[index].date ?? "") == (self.objSlot?.data?.availableDates?[0] ?? "")) {
                        
                        self.arrAvailableTimeSlot.append((self.objSlot?.data?.availableSlots?[index].time ?? ""))
                    }
                }
                selectedDate = 0
            }
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 1
            lblService.attributedText = NSMutableAttributedString(string: self.objSlot?.data?.header ?? "", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
            lblService.textAlignment = .center
            
            cvDate.reloadData()
            cvTime.reloadData()
            
            if ((self.objSlot?.data?.availableDates?.count ?? 0) >= 0) {
                let indexPath = IndexPath(item: 0, section: 0)
                cvDate.selectItem(at: indexPath, animated: true, scrollPosition: .top)
            }
            
            perform(#selector(handleHeight), with: self, with: 0.2)
            perform(#selector(handleHeight), with: self, with: 0.2)
        }
    }
    
    //MARK:- Handle Button
    @objc func handleComplete(_ sender: UIButton) {
        if (selectedDate == nil) {
            self.showToast(message: "Please select date")
            return
        }
        
        if (selectedTime == nil) {
            self.showToast(message: "Please select time slot")
            return
        }
        
        Loader.shared.StartActivityIndicator(obj: self)
        
        let locationDict = ["enquiryId": self.objSlot?.data?.enquiryID ?? "",
                            "selectedDate": self.objSlot?.data?.availableDates?[selectedDate!] ?? "",
                            "selectedTime": self.arrAvailableTimeSlot[selectedTime!]+":00"
        ] as [String : Any]
        
        let mainDict = ["eventName": "RRComplete", "encryption": false,"data":locationDict,
                        "tag": "loanRepledge"]
            as [String : Any]
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
    
    
    
    //MARK:- Handle Response
    
    @objc func handleReplegeRequest(notification: Notification) {
        
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(RepledgeModel.self, from: jsonData!)
            self.objSlot = data
            if (data.status ?? false) {
                setupData()
            }
            else {
                self.showToast(message: data.message ?? "")
            }
        }
        catch let error {
            // print(error)
        }
    }
    
    
    @objc func handleSendDateTime(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(RepledgeSummaryModel.self, from: jsonData!)
            if (data.status ?? false) {
                let vc = LoanSummaryVC()
                //  print("\(data.data?.loanData?.mobileNo ?? 0)")
                CommonModel.shared.mobileNumber = "\(data.data?.loanData?.mobileNo ?? 0)"
                vc.objLoanSummary2 = data
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else {
                self.showToast(message: data.message ?? "")
            }
        }
        catch let error {
            //  print(error)
        }
    }
    
    
    @objc func handleError(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        if let dict = notification.userInfo as NSDictionary? {
            if let status = dict["status"] as? Bool {
                if status == false {
                    let msg = dict["message"] as? String ?? ""
                    let alertController = UIAlertController(title: "", message: msg, preferredStyle: .alert)
                    let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
}

extension RepledgeRequestVC : UICollectionViewDataSource, UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView.tag == 500) {
            return self.objSlot?.data?.availableDates?.count ?? 0
        }
        else {
            return self.arrAvailableTimeSlot.count
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (collectionView.tag == 500) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DayCell
            cell.objSlot3 = self.objSlot
            cell.setupData3(index : indexPath.row)
            return cell
        }
        
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TimeCell
            cell.lblTime.text = self.arrAvailableTimeSlot[indexPath.row]
            return cell
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView.tag == 500) {
            selectedDate = indexPath.row
            
            arrAvailableTimeSlot.removeAll()
            if ((self.objSlot?.data?.availableDates?.count ?? 0) > 0) {
                for index in 0..<(self.objSlot?.data?.availableSlots?.count ?? 0) {
                    if ((self.objSlot?.data?.availableSlots?[index].date ?? "") == (self.objSlot?.data?.availableDates?[selectedDate!] ?? "")) {
                        self.arrAvailableTimeSlot.append((self.objSlot?.data?.availableSlots?[index].time ?? ""))
                    }
                }
            }
            self.selectedTime = nil
            cvTime.reloadData()
            perform(#selector(handleHeight), with: self, with: 0.2)
            
            cvTime.reloadData()
            perform(#selector(handleHeight), with: self, with: 0.2)
        }
        else {
            selectedTime = indexPath.row
        }
    }
}

//MARK:- API Calling OR Socket Events
extension RepledgeRequestVC {
    
    @objc func handleConfig(notification: Notification) {
        VersionControlSocketHelper.shared.firstEmits()
    }
    
    @objc func handleConfigMainSocket(notification: Notification) {
        
        Loader.shared.StartActivityIndicator(obj: self)
        let locationDict = ["requestType": "loanRepledge",
                            "mobileNo": mobileNo,
                            "LoanNo": loanNo
                            
        ] as [String : Any]
        
        let mainDict = ["eventName": "loanReleaseRepledge", "encryption": false,"data":locationDict ] as [String : Any]
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
}
