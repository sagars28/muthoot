//
//  ViewAgreementVC.swift
//  Muthoot Track
//
//  Created by Sagar on 24/06/21.
//

import UIKit
import WebKit

public class ViewAgreementVC:  BaseController, WKUIDelegate, WKNavigationDelegate {
    
    var webView: WKWebView!
    var agreementStr = ""
    var navTitle = "Agreement"
    var status = ""
    
    var dict : NSDictionary?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Colors.viewBackground
        setupUI()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        createNavigationBarWithBack(navTitle)
        btnNavBack.removeTarget(self, action: #selector(handleBack(_:)), for: .allEvents)
        btnNavBack.addTarget(self, action: #selector(handlebtnOK(_:)), for: .touchUpInside)
        setupData()
    }
    
    fileprivate func setupUI() {
        
        let btnDownload = UIButton()
        
        if #available(iOS 13.0, *) {
            btnDownload.setImage(UIImage(named: "download", in: Bundle(for: type(of: self)), with: nil), for: .normal)
        } else {
            btnDownload.setImage(UIImage(named: "download", in: Bundle(for: type(of: self)), compatibleWith : nil), for: .normal)
        }
        
        btnDownload.setTitleColor(.white, for: .normal)
        btnDownload.backgroundColor = Colors.theme
        btnDownload.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle)
        btnDownload.addTarget(self, action: #selector(handleDownload(_:)), for: .touchUpInside)
        viewNavigationBarBase.addSubview(btnDownload)
        btnDownload.enableAutolayout()
        btnDownload.trailingMargin(16)
        btnDownload.fixHeight(40)
        btnDownload.fixWidth(40)
        btnDownload.bottomMargin(6)
        
        
        let bottomView = UIView()
        bottomView.clipsToBounds = true
        bottomView.backgroundColor = Colors.theme
        view.addSubview(bottomView)
        bottomView.enableAutolayout()
        bottomView.centerX()
        bottomView.fixWidth(screenWidth)
        bottomView.bottomMargin(Constraints.bottom)
        bottomView.fixHeight(50)
        
        let btnOK = UIButton()
        btnOK.setTitle("OK", for: .normal)
        btnOK.setTitleColor(.white, for: .normal)
        btnOK.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        btnOK.addTarget(self, action: #selector(handlebtnOK(_:)), for: .touchUpInside)
        bottomView.addSubview(btnOK)
        btnOK.enableAutolayout()
        btnOK.centerX()
        btnOK.fixHeight(40)
        btnOK.centerY()
        
        
        webView = WKWebView(frame: CGRect(x: 0,y: Constraints.top+self.topHeight, width: screenWidth, height:screenHeight-(Constraints.top+self.topHeight+Constraints.bottom + 50)))
        webView.scrollView.showsVerticalScrollIndicator = false
        webView.clipsToBounds = true
        webView.scrollView.bounces = false
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view.addSubview(webView)
    }
    
    func setupData() {
        if let data = Data(base64Encoded: self.agreementStr, options: .ignoreUnknownCharacters) {
            webView.load(data, mimeType: "application/pdf", characterEncodingName: "utf-8", baseURL: URL(fileURLWithPath: ""))
        }
    }
    
    @objc func handlebtnOK(_ sender : UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: LoanSummaryVC.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @objc func handleDownload(_ sender : UIButton) {
        if let data = Data(base64Encoded: agreementStr, options: .ignoreUnknownCharacters) {
            let activityViewController = UIActivityViewController(activityItems: [data], applicationActivities: nil)
            
            activityViewController.popoverPresentationController?.sourceView = self.view
            
            activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
}

extension ViewAgreementVC : SignatureDelegate {
    func SignatureText(sign: String) {
        DispatchQueue.main.async {
            
        }
    }
}
