//
//  LoanDateTimeVC.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/22/21.
//

import UIKit

public class LoanDateTimeVC: BaseController {
    
    var  scrollViewMain = UIScrollView()
    var viewBack = UIView()
    
    let lblService = UILabel()
    let lblMonth = UILabel()
    
    
    var cvDate : UICollectionView!
    var cvTime : UICollectionView!
    
    var heightConstraint : NSLayoutConstraint?
    
    var objSlot : AvailableSlotModel? // Coming From Add Address Controller
    
    var selectedDate : Int?
    var selectedTime : Int?
    
    var enquiryId = ""
    var arrAvailableTimeSlot = [String]()
    var arrTimeSlot = [String]()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoThankYou(notification:)), name: Notification.Name("SendLoanDateTime"), object: nil)
        
        self.view.backgroundColor = .white
        setupUI()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        //  createNavigationBar("Date and Time")
        setupData()
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    fileprivate func setupUI() {
        
        let headerView = TopPgrogess()
        headerView.backgroundColor = .white
        headerView.setView()
        headerView.lblOne.isHidden = true
        headerView.lblTwo.isHidden = true
        headerView.lblThree.isHidden = true
        headerView.ivFirst.backgroundColor = .white
        headerView.ivSecond.backgroundColor = .white
        headerView.ivThird.backgroundColor = .white
        
        if #available(iOS 13.0, *) {
            headerView.ivFirst.image = UIImage(named: "rightWhite", in: Bundle(for: type(of: self)), with: nil)
            headerView.ivSecond.image = UIImage(named: "rightWhite", in: Bundle(for: type(of: self)), with: nil)
            headerView.ivThird.image = UIImage(named: "rightWhite", in: Bundle(for: type(of: self)), with: nil)
        } else {
            headerView.ivFirst.image = UIImage(named: "rightWhite", in: Bundle(for: type(of: self)), compatibleWith : nil)
            headerView.ivSecond.image = UIImage(named: "rightWhite", in: Bundle(for: type(of: self)), compatibleWith: nil)
            headerView.ivThird.image = UIImage(named: "rightWhite", in: Bundle(for: type(of: self)), compatibleWith: nil)
        }
        
        headerView.ivFourth.backgroundColor = Colors.theme
        
        headerView.btnThree.addTarget(self, action: #selector(backBtnTapped(_:)), for: .touchUpInside)
        headerView.btnOne.addTarget(self, action: #selector(backOneTapped(_:)), for: .touchUpInside)
        headerView.btnTwo.addTarget(self, action: #selector(backTwoTapped(_:)), for: .touchUpInside)
        
        view.addSubview(headerView)
        headerView.enableAutolayout()
        headerView.fixHeight(80)
        headerView.leadingMargin(0)
        headerView.trailingMargin(0)
        headerView.topMargin(self.topbarHeight)
        
        let bottomView = BottomBtnBar()
        bottomView.setView()
        bottomView.btnNext.setTitle("COMPLETE", for: .normal)
        bottomView.btnNext.setImage(UIImage(), for: .normal)
        bottomView.btnBack.addTarget(self, action: #selector(backBtnTapped(_:)), for: .touchUpInside)
        bottomView.btnNext.addTarget(self, action: #selector(nextBtnTapped(_:)), for: .touchUpInside)
        view.addSubview(bottomView)
        bottomView.enableAutolayout()
        bottomView.centerX()
        bottomView.fixWidth(screenWidth)
        bottomView.bottomMargin(Constraints.bottom)
        bottomView.fixHeight(50)
        
        scrollViewMain.contentInsetAdjustmentBehavior = .never
        scrollViewMain.backgroundColor = UIColor.white
        scrollViewMain.bounces = false
        scrollViewMain.isScrollEnabled = true
        scrollViewMain.showsVerticalScrollIndicator = true
        view.addSubview(scrollViewMain)
        scrollViewMain.enableAutolayout()
        scrollViewMain.leadingMargin(0)
        scrollViewMain.fixWidth(screenWidth)
        scrollViewMain.belowView(0, to: headerView)
        scrollViewMain.aboveView(0, to: bottomView)
        
        // Main View Under Scroll View
        
        viewBack.backgroundColor = .white
        scrollViewMain.addSubview(viewBack)
        viewBack.enableAutolayout()
        viewBack.centerX()
        viewBack.fixWidth(screenWidth)
        viewBack.topMargin(0)
        viewBack.bottomMargin(0)
        viewBack.flexibleHeightGreater(10)
        
        
        lblService.font = UIFont(name: Fonts.latoRegular, size: TextSize.title + 6)
        viewBack.addSubview(lblService)
        lblService.enableAutolayout()
        lblService.topMargin(8)
        lblService.leadingMargin(8)
        lblService.trailingMargin(8)
        lblService.fixHeight(30)
        
        
        let lblSelectDate = UILabel()
        lblSelectDate.text = "Select Date"
        lblSelectDate.textAlignment = .left
        lblSelectDate.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        lblSelectDate.textColor = Colors.theme
        viewBack.addSubview(lblSelectDate)
        lblSelectDate.enableAutolayout()
        lblSelectDate.belowView(20, to: lblService)
        lblSelectDate.leadingMargin(16)
        lblSelectDate.trailingMargin(16)
        lblSelectDate.fixHeight(30)
        
        
        let viewTime = UIView()
        viewTime.backgroundColor = .white
        viewTime.clipsToBounds = true
        viewBack.addSubview(viewTime)
        viewTime.enableAutolayout()
        viewTime.belowView(8, to: lblSelectDate)
        viewTime.leadingMargin(8)
        viewTime.trailingMargin(8)
        viewTime.fixHeight(100)
        
        
        
        lblMonth.text = ""
        lblMonth.textAlignment = .left
        lblMonth.font = UIFont(name: Fonts.HelveticaNeueMedium, size: TextSize.title)
        lblMonth.textColor = .black
        viewTime.addSubview(lblMonth)
        lblMonth.enableAutolayout()
        lblMonth.bottomMargin(16)
        lblMonth.leadingMargin(8)
        //lblMonth.fixWidth(50)
        lblMonth.fixHeight(30)
        
        // let width = (screenWidth-100)/3
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 62, height: 90)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 16
        layout.minimumLineSpacing = 16
        
        
        cvDate = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvDate.isUserInteractionEnabled = true
        cvDate.clipsToBounds = true
        cvDate.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 16)
        cvDate.tag = 500
        cvDate.showsHorizontalScrollIndicator = false
        cvDate.delegate = self
        cvDate.dataSource = self
        cvDate.backgroundColor = .white
        cvDate.showsVerticalScrollIndicator = false
        cvDate.isScrollEnabled = true
        cvDate.bounces = false
        cvDate.register(DayCell.self, forCellWithReuseIdentifier: "cell")
        viewTime.addSubview(cvDate)
        cvDate.enableAutolayout()
        cvDate.fixWidth(screenWidth)
        cvDate.topMargin(0)
        cvDate.leadingMargin(56)
        cvDate.trailingMargin(0)
        cvDate.bottomMargin(0)
        
        
        let lblSelectTime = UILabel()
        lblSelectTime.text = "Select Time"
        lblSelectTime.textAlignment = .left
        lblSelectTime.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        lblSelectTime.textColor = Colors.theme
        viewBack.addSubview(lblSelectTime)
        lblSelectTime.enableAutolayout()
        lblSelectTime.belowView(20, to: viewTime)
        lblSelectTime.leadingMargin(16)
        lblSelectTime.trailingMargin(8)
        lblSelectTime.fixHeight(30)
        
        let width1 = (screenWidth-120)/3
        let layout1 = UICollectionViewFlowLayout()
        layout1.itemSize = CGSize(width: width1, height: 40)
        layout1.scrollDirection = .vertical
        layout1.minimumInteritemSpacing = 16
        layout1.minimumLineSpacing = 16
        
        cvTime = UICollectionView(frame: .zero, collectionViewLayout: layout1)
        cvTime.clipsToBounds = true
        cvTime.contentInset = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 30)
        cvTime.tag = 501
        cvTime.showsHorizontalScrollIndicator = false
        cvTime.delegate = self
        cvTime.dataSource = self
        cvTime.backgroundColor = .white
        cvTime.showsVerticalScrollIndicator = false
        cvTime.isScrollEnabled = true
        cvTime.bounces = false
        cvTime.register(TimeCell.self, forCellWithReuseIdentifier: "cell")
        viewBack.addSubview(cvTime)
        cvTime.enableAutolayout()
        cvTime.belowView(15, to: lblSelectTime)
        cvTime.leadingMargin(0)
        cvTime.trailingMargin(0)
        cvTime.bottomMargin(20)
        heightConstraint = cvTime.heightAnchor.constraint(equalToConstant: 20)
        heightConstraint?.isActive = true
    }
    
    @objc func handleHeight() {
        heightConstraint?.constant = cvTime.contentSize.height
        self.view.layoutIfNeeded()
    }
    
    
    func setupData() {
        if ((self.objSlot?.data?.availableDates?.count ?? 0) > 0) {
            let date = self.objSlot?.data?.availableDates?[0].convertToMonth(in: "dd/MM/yyyy", out: "MMM")
            
            // Fetch Particulate Date
            // And Stored On arrAvailableTimeSlot Array
            
            arrTimeSlot.removeAll()
            arrAvailableTimeSlot.removeAll()
            if ((self.objSlot?.data?.availableDates?.count ?? 0) > 0) {
                for index in 0..<(self.objSlot?.data?.availableSlots?.count ?? 0) {
                    if ((self.objSlot?.data?.availableSlots?[index].date ?? "") == (self.objSlot?.data?.availableDates?[0] ?? "")) {
                        self.arrAvailableTimeSlot.append((self.objSlot?.data?.availableSlots?[index].period ?? ""))
                        self.arrTimeSlot.append((self.objSlot?.data?.availableSlots?[index].time ?? ""))
                    }
                }
                selectedDate = 0
            }
            
            lblMonth.text = date
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 1
            lblService.attributedText = NSMutableAttributedString(string: self.objSlot?.data?.header ?? "", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
            lblService.textAlignment = .center
            
            cvDate.reloadData()
            cvTime.reloadData()
            
            if ((self.objSlot?.data?.availableDates?.count ?? 0) >= 0) {
                let indexPath = IndexPath(item: 0, section: 0)
                cvDate.selectItem(at: indexPath, animated: true, scrollPosition: .top)
            }
            perform(#selector(handleHeight), with: self, with: 0.2)
            perform(#selector(handleHeight), with: self, with: 0.2)
        }
    }
    
    @objc func gotoThankYou(notification: Notification) {
        
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(ThankYouModel.self, from: jsonData!)
            if (data.status ?? false) {
                let vc = ThankYouVC()
                vc.objLoanSummary = data
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                self.showToast(message: data.message ?? "")
            }
        }
        catch let error {
            //print(error)
        }
    }
    
    @objc func keyboardWillShow(notification:NSNotification) {
        
        guard let userInfo = notification.userInfo else {return}
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        let keyboardFrame = keyboardSize.cgRectValue
        
        if self.view.bounds.origin.y == 0{
            self.view.bounds.origin.y += keyboardFrame.height
        }
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        if self.view.bounds.origin.y != 0 {
            self.view.bounds.origin.y = 0
        }
    }
    
    @objc func backBtnTapped(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func backOneTapped(_ sender : UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: PincodeVC.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
    }
    
    @objc func backTwoTapped(_ sender : UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: CalculateLoanVC.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
    }
    
    @objc func nextBtnTapped(_ sender : UIButton) {
        
        if (selectedDate == nil) {
            self.showToast(message: "Please select date")
            return
        }
        
        if (selectedTime == nil) {
            self.showToast(message: "Please select time slot")
            return
        }
        
        Loader.shared.StartActivityIndicator(obj: self)
        
        let locationDict = ["selectedDate":  self.objSlot?.data?.availableDates?[selectedDate!] ?? "",
                            "selectedTime": self.arrTimeSlot[selectedTime!]+":00",
                            "enquiryId":enquiryId] as [String : Any]
        
        let mainDict = ["eventName": "sendLoanDateTime", "encryption": false,"data":locationDict ] as [String : Any]
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
}


extension LoanDateTimeVC : UICollectionViewDataSource, UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView.tag == 500) {
            return self.objSlot?.data?.availableDates?.count ?? 0
        }
        else {
            return self.arrAvailableTimeSlot.count
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (collectionView.tag == 500) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DayCell
            cell.objSlot = self.objSlot
            cell.setupData(index : indexPath.row)
            return cell
        }
        
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TimeCell
            cell.lblTime.text = self.arrAvailableTimeSlot[indexPath.row]
            return cell
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView.tag == 500) {
            selectedDate = indexPath.row
            
            arrTimeSlot.removeAll()
            arrAvailableTimeSlot.removeAll()
            if ((self.objSlot?.data?.availableDates?.count ?? 0) > 0) {
                for index in 0..<(self.objSlot?.data?.availableSlots?.count ?? 0) {
                    if ((self.objSlot?.data?.availableSlots?[index].date ?? "") == (self.objSlot?.data?.availableDates?[selectedDate!] ?? "")) {
                        self.arrAvailableTimeSlot.append((self.objSlot?.data?.availableSlots?[index].period ?? ""))
                        self.arrTimeSlot.append((self.objSlot?.data?.availableSlots?[index].time ?? ""))
                    }
                }
            }
            self.selectedTime = nil
            cvTime.reloadData()
            perform(#selector(handleHeight), with: self, with: 0.2)
            
            cvTime.reloadData()
            perform(#selector(handleHeight), with: self, with: 0.2)
        }
        else {
            selectedTime = indexPath.row
        }
    }
}
