//
//  MapVC.swift
//  Muthoot Track
//
//  Created by Sagar on 19/07/21.
//

import UIKit
import MapKit

class MapVC: BaseController, UIGestureRecognizerDelegate {
    
    var mapView: MKMapView!
    var locationManager = CLLocationManager()
    
    let tblViewSlider = UITableView()
    var tblViewHeightConstraint : NSLayoutConstraint?
    
    let backView = UIView()
    
    var customerLat = ""
    var customerLong = ""
    
    var branchLat = ""
    var branchLong = ""
    
    var riderLat = ""
    var riderLong = ""
    
    let arrData = ["1", "2"]
    
    var staff: [Staff]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        createNavigationBarWithBack("Tracking")
        setupData()
        getLocation()
    }
    
    override func viewDidLayoutSubviews() {
        if ((tblViewSlider.contentSize.height + 100) > screenHeight * 0.5) {
            tblViewHeightConstraint?.constant = screenHeight * 0.4
        }
        else {
            tblViewHeightConstraint?.constant = tblViewSlider.contentSize.height
        }
    }
    
    func setupUI() {
        
        mapView = MKMapView()
        mapView.showsUserLocation = true
        mapView.delegate = self
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.showsCompass = true
        mapView.showsScale = true
        mapView.showsUserLocation = false
        self.view.addSubview(mapView)
        mapView.enableAutolayout()
        mapView.leadingMargin(0)
        mapView.trailingMargin(0)
        mapView.topMargin(Constraints.top + self.topHeight - 20)
        mapView.bottomMargin(200)
        
        
        let btnLocate = UIButton()
        btnLocate.clipsToBounds = true
        btnLocate.backgroundColor = .white
        btnLocate.setImage( UIImage(named: "locate", in: Bundle(for: type(of: self
        )), compatibleWith: nil), for: .normal)
        btnLocate.addTarget(self, action: #selector(goToCurrentLocation(_:)), for: .touchUpInside)
        mapView.addSubview(btnLocate)
        btnLocate.enableAutolayout()
        btnLocate.trailingMargin(8)
        btnLocate.topMargin(8)
        btnLocate.fixWidth(30)
        btnLocate.fixHeight(30)
        
        
        
        /* popup.frame = view.bounds
         popup.arrData = ["1", "2"]
         popup.createUI()
         popup.backView.isUserInteractionEnabled = true
         let gesture = UIPanGestureRecognizer(target: self, action: #selector(wasDragged(gestureRecognizer:)))
         popup.backView.addGestureRecognizer(gesture)
         popup.backView.isUserInteractionEnabled = true
         gesture.delegate = self
         self.view.addSubview(popup)*/
        backView.isUserInteractionEnabled = true
        backView.clipsToBounds = true
        backView.layer.cornerRadius = 30
        backView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        backView.clipsToBounds = true
        backView.backgroundColor = .white
        self.view.addSubview(backView)
        backView.enableAutolayout()
        backView.leadingMargin(0)
        backView.trailingMargin(0)
        backView.bottomMargin(Constraints.bottom)
        
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(wasDragged(gestureRecognizer:)))
        backView.addGestureRecognizer(gesture)
        backView.isUserInteractionEnabled = true
        gesture.delegate = self
        
        let lblSignature = UIView()
        lblSignature.backgroundColor = Colors.darkGray
        backView.addSubview(lblSignature)
        lblSignature.enableAutolayout()
        lblSignature.topMargin(16)
        lblSignature.centerX()
        lblSignature.fixWidth(30)
        lblSignature.fixHeight(2)
        
        let separator2 = UIView()
        separator2.clipsToBounds = true
        separator2.backgroundColor = Colors.separator
        backView.addSubview(separator2)
        separator2.enableAutolayout()
        separator2.bottomMargin(0)
        separator2.trailingMargin(0)
        separator2.leadingMargin(0)
        separator2.fixHeight(2)
        
        tblViewSlider.backgroundColor = .white
        tblViewSlider.delegate = self
        tblViewSlider.dataSource = self
        tblViewSlider.bounces = false
        tblViewSlider.separatorStyle = .singleLine
        tblViewSlider.backgroundColor = UIColor.white
        tblViewSlider.estimatedRowHeight = 50
        tblViewSlider.sectionFooterHeight = .leastNonzeroMagnitude
        tblViewSlider.showsVerticalScrollIndicator = false
        tblViewSlider.isScrollEnabled = false
        tblViewSlider.register(OrnamentDisplayCell.self, forCellReuseIdentifier: "cell")
        backView.addSubview(tblViewSlider)
        tblViewSlider.enableAutolayout()
        tblViewSlider.leadingMargin(0)
        tblViewSlider.trailingMargin(0)
        tblViewSlider.belowView(0, to: lblSignature)
        tblViewSlider.aboveView(0, to: separator2)
        tblViewSlider.fixHeight(200)
        // tblViewHeightConstraint = tblViewSlider.heightAnchor.constraint(greaterThanOrEqualToConstant: 200)
        //tblViewHeightConstraint?.isActive = true
        tblViewSlider.bottomMargin(0)
        
    }
    
    @objc func wasDragged(gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == UIGestureRecognizer.State.began || gestureRecognizer.state == UIGestureRecognizer.State.changed {
            let translation = gestureRecognizer.translation(in: self.view)
            
            /*print(gestureRecognizer.view!.center.y)
            print("screen Height", self.view.frame.height)
            print("Main", self.view.frame.height - backView.center.y)
            print("backView", backView.center.y)
            print("Back View Height", backView.frame.height)
            print("center", (self.view.frame.height - backView.center.y) + Constraints.bottom + 33.5)
            print(gestureRecognizer.state)*/
            
            switch gestureRecognizer.state {
            case .changed, .began:
                if (((self.view.frame.height - backView.center.y) + (Constraints.bottom + 33.5)
                        <= 227)) {
                    gestureRecognizer.view!.center = CGPoint(x: backView.center.x, y: backView.center.y)
                    gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
                }
                
                else if (((self.view.frame.height - backView.center.y) + (Constraints.bottom + 33.5) + 50 >=  backView.frame.height)){
                    gestureRecognizer.view!.center = CGPoint(x: backView.center.x, y: backView.center.y + translation.y)
                    gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
                }
                
                else {
                    gestureRecognizer.view!.center = CGPoint(x: backView.center.x, y: backView.center.y + translation.y)
                    gestureRecognizer.setTranslation(CGPoint(x: 0,y: 0), in: self.view)
                }
                break
            default:
                break
            }
            
            
            /*else if (gestureRecognizer.state == UIPanGestureRecognizer.State.changed) {
             
             }
             else if (gestureRecognizer.state ==  UIPanGestureRecognizer.State.ended) {
             
             }*/
        }
    }
    
    //MARK:- SetupData
    func setupData() {
        
        addCustomAnnotation()
        addCustomAnnotation2()
        mapView.showAnnotations(mapView.annotations, animated: true)
        self.tblViewSlider.reloadData()
    }
    
    func addCustomAnnotation() {
        let missionDoloresCoor = CLLocationCoordinate2DMake(Double(branchLat) ?? 0.0, Double(branchLong) ?? 0.0)
        let pin = CustomAnnotation(coordinate: missionDoloresCoor, tag: 1)
        self.mapView.addAnnotation(pin)
    }
    
    func addCustomAnnotation2() {
        let missionDoloresCoor = CLLocationCoordinate2DMake(Double(customerLat) ?? 0.0, Double(customerLong) ?? 0.0)
        let pin = CustomAnnotation(coordinate: missionDoloresCoor, tag: 2)
        self.mapView.addAnnotation(pin)
    }
    
    
    fileprivate func getLocation() {
        locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    
    @objc func goToCurrentLocation(_ sender : UIButton) {
        
        if !hasLocationPermission() {
            let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
                UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            getLocation()
        }
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            @unknown default:
                break
            }
        } else {
            hasPermission = false
        }
        return hasPermission
    }
}

extension MapVC : CLLocationManagerDelegate, MKMapViewDelegate {
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let location = locations.last else {//print("Location Not Found")
            return
        }
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        locationManager.stopUpdatingLocation()
        mapView.setRegion(region, animated: true)
        
        let missionDoloresCoor = CLLocationCoordinate2DMake(center.latitude, center.longitude)
        let pin = CustomAnnotation(coordinate: missionDoloresCoor, tag: 3)
        self.mapView.addAnnotation(pin)
        mapView.showAnnotations(mapView.annotations, animated: true)
        
        CommonModel.shared.lat = center.latitude
        CommonModel.shared.long = center.longitude
    }
    
    public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            return nil
        }
        
        var pinView : MKAnnotationView? = nil
        let identifer = "pin"
        
        pinView = mapView .dequeueReusableAnnotationView(withIdentifier: identifer)
        if pinView == nil {
            pinView = MKAnnotationView.init(annotation: annotation, reuseIdentifier: identifer)
        }
        
        let tag = (annotation as! CustomAnnotation).tag
        
        if (tag == 1) {
            
            let pinImage = UIImage(named: "Pointer Branch", in: Bundle(for: type(of: self)), compatibleWith: nil)
            let size = CGSize(width: 25, height: 32)
            UIGraphicsBeginImageContext(size)
            pinImage!.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            pinView?.image = resizedImage
        }
        else if (tag == 2) {
            let pinImage = UIImage(named: "Pointer Customer", in: Bundle(for: type(of: self)), compatibleWith: nil)
            let size = CGSize(width: 25, height: 32)
            UIGraphicsBeginImageContext(size)
            pinImage!.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            pinView?.image = resizedImage
        }
        else {
            let pinImage = UIImage(named: "Pointer Rider", in: Bundle(for: type(of: self)), compatibleWith: nil)
            let size = CGSize(width: 25, height: 32)
            UIGraphicsBeginImageContext(size)
            pinImage!.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
            pinView?.image = resizedImage
        }
        return pinView
    }
}

extension MapVC : UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.staff?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ExecutiveCell()
        cell.selectionStyle = .none
        cell.setupData(data: (staff?[indexPath.row])!)
        
        if indexPath.row == ((self.staff?.count ?? 0) - 1) {
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: screenWidth)
        }
        return cell
    }
}


class CustomAnnotation: NSObject, MKAnnotation {
    
    let coordinate: CLLocationCoordinate2D
    let tag : Int
    
    init(coordinate: CLLocationCoordinate2D, tag : Int) {
        self.coordinate = coordinate
        self.tag = tag
        super.init()
    }
}

extension MKMapView {
    
    func fitAll() {
        var zoomRect            = MKMapRect.null;
        for annotation in annotations {
            let annotationPoint = MKMapPoint(annotation.coordinate)
            let pointRect       = MKMapRect(x: annotationPoint.x, y: annotationPoint.y, width: 0.01, height: 0.01);
            zoomRect            = zoomRect.union(pointRect);
        }
        setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100), animated: true)
    }
}
