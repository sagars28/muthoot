//
//  WebViewPDFVC.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/31/21.
//

import UIKit
import WebKit

public class WebViewPDFVC:  BaseController, WKUIDelegate, WKNavigationDelegate {
    
    var webView: WKWebView!
    var urlString = ""
    var navTitle = "Agreement"
    
    var dict : NSDictionary?
    var objSummary : LoanSummaryModel?
    
    var status = ""
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Colors.viewBackground
        setupUI()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleSubmitAgreementResponce(notification:)), name: Notification.Name("SuccessSubmitOwnershipAgreement"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePlegdetResponce(notification:)), name: Notification.Name("SuccessSubmitAgreement"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.SuccessConfirmGoldHandover(notification:)), name: Notification.Name("SuccessConfirmGoldHandover"), object: nil)
        
        
        createNavigationBarWithBack(navTitle)
        setupData()
    }
    
    fileprivate func setupUI() {
        
        let btnLogOut = UIButton()
        if #available(iOS 13.0, *) {
            btnLogOut.setImage(UIImage(named: "logOut", in: Bundle(for: type(of: self)), with: nil), for: .normal)
        } else {
            btnLogOut.setImage(UIImage(named: "logOut", in: Bundle(for: type(of: self)), compatibleWith : nil), for: .normal)
        }
        
        btnLogOut.setTitleColor(.white, for: .normal)
        btnLogOut.backgroundColor = Colors.theme
        btnLogOut.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle)
        btnLogOut.addTarget(self, action: #selector(handleLogOutBtn(_:)), for: .touchUpInside)
        viewNavigationBarBase.addSubview(btnLogOut)
        btnLogOut.enableAutolayout()
        btnLogOut.trailingMargin(16)
        btnLogOut.fixHeight(40)
        btnLogOut.fixWidth(40)
        btnLogOut.centerY()
        
        let btnNotification = UIButton()
        
        if #available(iOS 13.0, *) {
            btnNotification.setImage(UIImage(named: "notification", in: Bundle(for: type(of: self)), with: nil), for: .normal)
        } else {
            btnNotification.setImage(UIImage(named: "notification", in: Bundle(for: type(of: self)), compatibleWith : nil), for: .normal)
        }
        
        btnNotification.setTitleColor(.white, for: .normal)
        btnNotification.backgroundColor = Colors.theme
        btnNotification.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle)
        btnNotification.addTarget(self, action: #selector(handleNotificationBtn(_:)), for: .touchUpInside)
        viewNavigationBarBase.addSubview(btnNotification)
        btnNotification.enableAutolayout()
        btnNotification.add(toLeft: 4, of: btnLogOut)
        btnNotification.fixHeight(35)
        btnNotification.fixWidth(35)
        btnNotification.centerY()
        
        
        btnNotification.isHidden = true
        btnLogOut.isHidden = true
        
        
        let bottomView = UIView()
        bottomView.clipsToBounds = true
        bottomView.backgroundColor = Colors.theme
        view.addSubview(bottomView)
        bottomView.enableAutolayout()
        bottomView.centerX()
        bottomView.fixWidth(screenWidth)
        bottomView.bottomMargin(Constraints.bottom)
        bottomView.fixHeight(50)
        
        let btnDisagree = UIButton()
        btnDisagree.setTitle("DISAGREE", for: .normal)
        btnDisagree.setTitleColor(.white, for: .normal)
        btnDisagree.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        btnDisagree.addTarget(self, action: #selector(handleDisagreeBtn(_:)), for: .touchUpInside)
        bottomView.addSubview(btnDisagree)
        btnDisagree.enableAutolayout()
        btnDisagree.trailingMargin(20)
        btnDisagree.fixHeight(40)
        btnDisagree.centerY()
        
        let btnAgree = UIButton()
        btnAgree.setTitle("AGREE", for: .normal)
        btnAgree.setTitleColor(.white, for: .normal)
        btnAgree.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        btnAgree.addTarget(self, action: #selector(handleAgreeBtn(_:)), for: .touchUpInside)
        bottomView.addSubview(btnAgree)
        btnAgree.enableAutolayout()
        btnAgree.add(toLeft: 20, of: btnDisagree)
        btnAgree.fixHeight(40)
        btnAgree.centerY()
        
        
        webView = WKWebView(frame: CGRect(x: 0,y: Constraints.top+self.topHeight, width: screenWidth - 0, height:screenHeight-(Constraints.top+self.topHeight+Constraints.bottom + 50)))
        webView.scrollView.showsVerticalScrollIndicator = false
        // webView.shadow(shadowColor: .lightGray, shadowOffset: CGSize(width: 0, height: 2), shadowOpacity: 0.5, shadowRadius: 4)
        webView.layer.cornerRadius = 10
        webView.clipsToBounds = true
        webView.scrollView.bounces = false
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view.addSubview(webView)
    }
    
    func setupData() {
        if let data = self.dict?["data"] as? [String:Any] {
            if let str = data["ownershipdeclarationformpdf"] as? String {
                if let data = Data(base64Encoded: str, options: .ignoreUnknownCharacters) {
                    webView.load(data, mimeType: "application/pdf", characterEncodingName: "utf-8", baseURL: URL(fileURLWithPath: ""))
                }
            }
            if let str = data["pledgeform"] as? String {
                if let data = Data(base64Encoded: str, options: .ignoreUnknownCharacters) {
                    webView.load(data, mimeType: "application/pdf", characterEncodingName: "utf-8", baseURL: URL(fileURLWithPath: ""))
                }
            }
        }
    }
    
    @objc func handleAgreeBtn(_ sender : UIButton) {
        let popup = SignaturePopup()
        popup.delegate = self
        popup.frame = view.bounds
        popup.createUI()
        view.window?.addSubview(popup)
    }
    
    @objc func handleDisagreeBtn(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleNotificationBtn(_ sender : UIButton) {
        
    }
    
    @objc func handleLogOutBtn(_ sender : UIButton) {
        
    }
    
    func saveAgreement(sign : String) {
        
        Loader.shared.StartActivityIndicator(obj: self)
        let mobile = self.objSummary?.data?[0].mobileNo ?? 0
        let LRID = self.objSummary?.data?[0].lrid ?? ""
        let enquiryId = self.objSummary?.data?[0].datumID ?? ""
        
        let locationDict = ["mobileNo": mobile,
                            "lrid": LRID,
                            "enquiryId": enquiryId,
                            "signature": "data:image/png;base64,"+sign] as [String : Any]
        
        if (status == "agreement") {
            let mainDict = ["eventName": "SUBMITOWNERSHIPAGREEMENT", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
        }
        
        else if (status == "handover") {
            let locationDict = ["mobileNo": mobile,
                                "LRID": LRID,
                                "enquiryId": enquiryId,
                                "action": "confirm",
                                "signature": "data:image/png;base64,"+sign] as [String : Any]
            let mainDict = ["eventName": "CONFIRMGOLDHANDOVER", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
        }
        
        else {
            let mainDict = ["eventName": "SUBMITAGREEMENT", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
        }
    }
    
    
    //MARK:- Handle Responce
    
    @objc func handleSubmitAgreementResponce(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        
        if let dict = notification.userInfo as NSDictionary? {
            if let data = dict["data"]  as? [String:Any] {
                if let str = data["ownershipdeclarationform"] as? String {
                    let vc = ViewAgreementVC()
                    vc.agreementStr = str
                    vc.status = "agreement"
                    vc.navTitle = "Ownership Agreement"
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        }
    }
    
    @objc func handlePlegdetResponce(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        
        if let dict = notification.userInfo as NSDictionary? {
            if let data = dict["data"]  as? [String:Any] {
                if let str = data["pledgeform"] as? String {
                    DispatchQueue.main.async {
                        let vc = ViewAgreementVC()
                        vc.agreementStr = str
                        vc.status = "pledge"
                        vc.navTitle = "Pledge Agreement"
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                }
            }
        }
    }
    
    @objc func SuccessConfirmGoldHandover(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        
        if let dict = notification.userInfo as NSDictionary? {
            if let data = dict["data"]  as? [String:Any] {
                if let str = data["releaseagreement"] as? String {
                    DispatchQueue.main.async {
                        let vc = ViewAgreementVC()
                        vc.agreementStr = str
                        vc.status = "pledge"
                        vc.navTitle = "Pledge Agreement"
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                }
            }
        }
    }
}

extension WebViewPDFVC : SignatureDelegate {
    func SignatureText(sign: String) {
        DispatchQueue.main.async {
            self.saveAgreement(sign: sign)
        }
    }
}
