//
//  CustomerPhotoVC.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/28/21.
//

import UIKit

public class CustomerPhotoVC: BaseController {
    
    let imagePicker = UIImagePickerController()
    let ivProfole = UIImageView()
    let btnCross = UIButton()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.viewBackground
        imagePicker.delegate = self
        setupUI()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        createNavigationBar("Select Customer Photo")
    }
    
    
    fileprivate func setupUI() {
        
        // Bottom View
        let bottomView = UIView()
        bottomView.backgroundColor = .white
        view.addSubview(bottomView)
        bottomView.enableAutolayout()
        bottomView.centerX()
        bottomView.fixWidth(screenWidth)
        bottomView.bottomMargin(Constraints.bottom)
        bottomView.fixHeight(50)
        
        let btnCancel = UIButton()
        btnCancel.setTitle("CANCEL", for: .normal)
        btnCancel.setTitleColor(Colors.red, for: .normal)
        btnCancel.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title - 2)
        btnCancel.addTarget(self, action: #selector(handleCancelBtn(_:)), for: .touchUpInside)
        bottomView.addSubview(btnCancel)
        btnCancel.enableAutolayout()
        btnCancel.trailingMargin(20)
        btnCancel.bottomMargin(0)
        btnCancel.topMargin(0)
        
        let btnUploadPhoto = UIButton()
        btnUploadPhoto.setTitle("UPLOAD CUSTOMER PHOTO", for: .normal)
        btnUploadPhoto.setTitleColor(Colors.theme, for: .normal)
        btnUploadPhoto.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title - 2)
        btnUploadPhoto.addTarget(self, action: #selector(pickerBrowsePicker(_:)), for: .touchUpInside)
        bottomView.addSubview(btnUploadPhoto)
        btnUploadPhoto.enableAutolayout()
        btnUploadPhoto.add(toLeft: 16, of: btnCancel)
        btnUploadPhoto.bottomMargin(0)
        btnUploadPhoto.topMargin(0)
        
        
        //MARK:- ScrollView
        let scrollViewMain = UIScrollView()
        scrollViewMain.contentInsetAdjustmentBehavior = .never
        scrollViewMain.backgroundColor = .clear
        scrollViewMain.bounces = false
        scrollViewMain.isScrollEnabled = true
        scrollViewMain.showsVerticalScrollIndicator = true
        view.addSubview(scrollViewMain)
        scrollViewMain.enableAutolayout()
        scrollViewMain.leadingMargin(0)
        scrollViewMain.fixWidth(screenWidth)
        scrollViewMain.topMargin(Constraints.top + self.topHeight)
        scrollViewMain.aboveView(0, to: bottomView)
        
        // Main View Under Scroll View
        let viewBack = UIView()
        viewBack.backgroundColor = .clear
        scrollViewMain.addSubview(viewBack)
        viewBack.enableAutolayout()
        viewBack.centerX()
        viewBack.fixWidth(screenWidth)
        viewBack.topMargin(0)
        viewBack.bottomMargin(0)
        viewBack.flexibleHeightGreater(10)
        
        let lblCapture = UILabel()
        lblCapture.text = "Please Select Customer Photo"
        lblCapture.textAlignment = .left
        lblCapture.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        lblCapture.textColor = Colors.textGray
        viewBack.addSubview(lblCapture)
        lblCapture.enableAutolayout()
        lblCapture.topMargin(10)
        lblCapture.leadingMargin(30)
        lblCapture.trailingMargin(20)
        lblCapture.fixHeight(25)
        
        
        let btnCapturePhoto = UIButton()
        btnCapturePhoto.backgroundColor = Colors.viewBackground
        btnCapturePhoto.setTitle("  Capture Photo", for: .normal)
        
        if #available(iOS 13.0, *) {
            btnCapturePhoto.setImage(UIImage(named: "camera", in: Bundle(for: type(of: self)), with: nil), for: .normal)
            btnCapturePhoto.setBackgroundImage(UIImage(named: "capture", in: Bundle(for: type(of: self)), with: nil), for: .normal)
        } else {
            btnCapturePhoto.setImage(UIImage(named: "camera", in: Bundle(for: type(of: self)), compatibleWith : nil), for: .normal)
            btnCapturePhoto.setBackgroundImage(UIImage(named: "capture", in: Bundle(for: type(of: self)), compatibleWith : nil), for: .normal)
        }
        
        btnCapturePhoto.setTitleColor(Colors.gray, for: .normal)
        btnCapturePhoto.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 1)
        btnCapturePhoto.addTarget(self, action: #selector(handleCapture(_:)), for: .touchUpInside)
        viewBack.addSubview(btnCapturePhoto)
        btnCapturePhoto.enableAutolayout()
        btnCapturePhoto.leadingMargin(30)
        btnCapturePhoto.trailingMargin(30)
        btnCapturePhoto.fixHeight(50)
        btnCapturePhoto.belowView(16, to: lblCapture)
        
        
        ivProfole.isHidden = true
        ivProfole.contentMode = .scaleAspectFill
        ivProfole.clipsToBounds = true
        ivProfole.backgroundColor = .white
        viewBack.addSubview(ivProfole)
        ivProfole.enableAutolayout()
        ivProfole.centerX()
        ivProfole.fixWidth(screenWidth - 140)
        ivProfole.belowView(25, to: btnCapturePhoto)
        ivProfole.fixHeight(150)
        ivProfole.bottomMargin(20)
        
        
        btnCross.isHidden = true
        btnCross.backgroundColor = Colors.viewBackground
        if #available(iOS 13.0, *) {
            btnCross.setImage(UIImage(named: "cross", in: Bundle(for: type(of: self)), with: nil), for: .normal)
        } else {
            btnCross.setImage(UIImage(named: "cross", in: Bundle(for: type(of: self)), compatibleWith : nil), for: .normal)
        }
        btnCross.setTitleColor(Colors.gray, for: .normal)
        btnCross.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 1)
        btnCross.addTarget(self, action: #selector(handleCrossBtn(_:)), for : .touchUpInside)
        viewBack.addSubview(btnCross)
        btnCross.enableAutolayout()
        btnCross.centerY(to: ivProfole)
        btnCross.add(toRight: 20, of: ivProfole)
        btnCross.fixHeight(50)
        btnCross.fixWidth(50)
        
    }
    
    @objc fileprivate func handleCancelBtn(_ sender : UIButton) {
        
    }
    
    @objc fileprivate func handleCrossBtn(_ sender : UIButton) {
        self.ivProfole.isHidden = true
        btnCross.isHidden = true
    }
    
    @objc fileprivate func handleCapture(_ sender : UIButton) {
        let vc = RescheduleVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    @objc func pickerBrowsePicker(_ sender: UIButton) {
        self.view.endEditing(true)
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.imagePicker.sourceType = .photoLibrary
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        if Device.isIpad {
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = self.view.bounds
            }
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera() {
        if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        } else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary() {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
}


extension CustomerPhotoVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let chosenImage = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        ivProfole.isHidden = false
        btnCross.isHidden = false
        ivProfole.image = chosenImage
        dismiss(animated:true, completion: nil)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.imagePicker.dismiss(animated: true, completion: nil)
        
    }
}


