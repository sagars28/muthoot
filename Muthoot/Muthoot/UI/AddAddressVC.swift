//
//  AddAddressVC.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/21/21.
//

import UIKit
import SocketIO
import MapKit
import CoreLocation

public class AddAddressVC: BaseController, UIGestureRecognizerDelegate {
    
    let scrollViewMain = UIScrollView()
    let viewBack = UIView()
    
    var mapView: MKMapView!
    var locationManager = CLLocationManager()
    let newPin = MKPointAnnotation()
    
    let tfName = UITextField()
    let txtAddress = UITextView()
    let tfCity = UITextField()
    let tfState = UITextField()
    let tfPincode = UITextField()
    var lastTag = Int()
    var country = ""
    var enquiryId = ""
    
    var objSlot : AvailableSlotModel?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        setupUI()
        getLocation()
        
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .lightContent
        } else {
            return .default
        }
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        //  createNavigationBar("Add address")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoNext(notification:)), name: Notification.Name("SendLoanAddressResponse"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    func setupUI() {
        
        let headerView = TopPgrogess()
        headerView.backgroundColor = .white
        headerView.setView()
        headerView.lblOne.isHidden = true
        headerView.lblTwo.isHidden = true
        headerView.ivFirst.backgroundColor = .white
        headerView.ivSecond.backgroundColor = .white
        
        if #available(iOS 13.0, *) {
            headerView.ivFirst.image = UIImage(named: "rightWhite", in: Bundle(for: type(of: self)), with: nil)
            headerView.ivSecond.image = UIImage(named: "rightWhite", in: Bundle(for: type(of: self)), with: nil)
        } else {
            headerView.ivFirst.image = UIImage(named: "rightWhite", in: Bundle(for: type(of: self)), compatibleWith: nil)
            headerView.ivSecond.image = UIImage(named: "rightWhite", in: Bundle(for: type(of: self)), compatibleWith: nil)
        }
        
        headerView.ivThird.backgroundColor = Colors.theme
        headerView.btnFour.addTarget(self, action: #selector(nextBtnTapped(_:)), for: .touchUpInside)
        headerView.btnTwo.addTarget(self, action: #selector(backBtnTapped(_:)), for: .touchUpInside)
        headerView.btnOne.addTarget(self, action: #selector(backOneTapped(_:)), for: .touchUpInside)
        
        view.addSubview(headerView)
        headerView.enableAutolayout()
        headerView.fixHeight(80)
        headerView.leadingMargin(0)
        headerView.trailingMargin(0)
        headerView.topMargin(self.topbarHeight)
        
        let bottomView = BottomBtnBar()
        bottomView.btnBack.addTarget(self, action: #selector(backBtnTapped(_:)), for: .touchUpInside)
        bottomView.btnNext.addTarget(self, action: #selector(nextBtnTapped(_:)), for: .touchUpInside)
        bottomView.setView()
        view.addSubview(bottomView)
        bottomView.enableAutolayout()
        bottomView.centerX()
        bottomView.fixWidth(screenWidth)
        bottomView.bottomMargin(Constraints.bottom)
        bottomView.fixHeight(50)
        
        
        scrollViewMain.contentInsetAdjustmentBehavior = .never
        scrollViewMain.backgroundColor = UIColor.white
        scrollViewMain.bounces = false
        scrollViewMain.isScrollEnabled = true
        scrollViewMain.showsVerticalScrollIndicator = true
        view.addSubview(scrollViewMain)
        scrollViewMain.enableAutolayout()
        scrollViewMain.leadingMargin(0)
        scrollViewMain.fixWidth(screenWidth)
        scrollViewMain.belowView(0, to: headerView)
        scrollViewMain.aboveView(0, to: bottomView)
        
        // Main View Under Scroll View
        
        viewBack.backgroundColor = .white
        scrollViewMain.addSubview(viewBack)
        viewBack.enableAutolayout()
        viewBack.centerX()
        viewBack.fixWidth(screenWidth)
        viewBack.topMargin(0)
        viewBack.bottomMargin(0)
        viewBack.flexibleHeightGreater(10)
        
        
        let lblService = UILabel()
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1
        lblService.attributedText = NSMutableAttributedString(string: "Help us find you", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        lblService.textAlignment = .center
        lblService.font = UIFont(name: Fonts.latoRegular, size: TextSize.title + 6)
        viewBack.addSubview(lblService)
        lblService.enableAutolayout()
        lblService.topMargin(8)
        lblService.leadingMargin(8)
        lblService.trailingMargin(8)
        lblService.fixHeight(30)
        
        //  let mapView = MKMapView()
        mapView = MKMapView()
        // let gestureRecognizer = UITapGestureRecognizer(target:self,action:#selector(handleTap))
        // gestureRecognizer.delegate = self
        // mapView.addGestureRecognizer(gestureRecognizer)
        mapView.showsUserLocation = true
        mapView.delegate = self
        mapView.mapType = MKMapType.standard
        mapView.isZoomEnabled = true
        mapView.isScrollEnabled = true
        mapView.showsCompass = true
        mapView.showsScale = true
        mapView.showsUserLocation = true
        viewBack.addSubview(mapView)
        mapView.enableAutolayout()
        mapView.leadingMargin(0)
        mapView.trailingMargin(0)
        mapView.fixHeight(258)
        mapView.belowView(20, to: lblService)
        
        
        let btnLocate = UIButton()
        btnLocate.clipsToBounds = true
        btnLocate.backgroundColor = .white
        btnLocate.setImage( UIImage(named: "locate", in: Bundle(for: type(of: self
        )), compatibleWith: nil), for: .normal)
        btnLocate.addTarget(self, action: #selector(goToCurrentLocation(_:)), for: .touchUpInside)
        mapView.addSubview(btnLocate)
        btnLocate.enableAutolayout()
        btnLocate.trailingMargin(8)
        btnLocate.topMargin(8)
        btnLocate.fixWidth(30)
        btnLocate.fixHeight(30)
        
        
        let addView = UIView()
        addView.clipsToBounds = true
        addView.backgroundColor = .white
        addView.layer.cornerRadius = 25
        addView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        viewBack.addSubview(addView)
        addView.enableAutolayout()
        addView.leadingMargin(0)
        addView.trailingMargin(0)
        addView.fixHeight(400)
        addView.belowView(-16, to: mapView)
        addView.bottomMargin(0)
        
        let lbllocation = UILabel()
        lbllocation.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 2)
        lbllocation.text = "Set marker to your location"
        lbllocation.textAlignment = .center
        lbllocation.textColor = Colors.textGray
        addView.addSubview(lbllocation)
        lbllocation.enableAutolayout()
        lbllocation.topMargin(25)
        lbllocation.leadingMargin(0)
        lbllocation.trailingMargin(0)
        lbllocation.fixHeight(25)
        
        let toolbar = UIToolbar().ToolbarPiker(mySelect: #selector(handToolBar), title: "Done")
        tfName.inputAccessoryView = toolbar
        tfState.inputAccessoryView = toolbar
        tfCity.inputAccessoryView = toolbar
        txtAddress.inputAccessoryView = toolbar
        tfPincode.inputAccessoryView = toolbar
        
        tfName.autocorrectionType = .no
        tfName.autocapitalizationType = .none
        tfName.tag = 100
        tfName.delegate = self
        tfName.setLeftPaddingPoints(8)
        tfName.setRightPaddingPoints(8)
        tfName.textColor = .black
        tfName.placeholder = "Name"
        tfName.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 1)
        tfName.layer.cornerRadius = 5
        tfName.layer.borderWidth = 0.8
        tfName.layer.borderColor = Colors.gray.cgColor
        addView.addSubview(tfName)
        tfName.enableAutolayout()
        tfName.belowView(30, to: lbllocation)
        tfName.leadingMargin(16)
        tfName.trailingMargin(16)
        tfName.fixHeight(45)
        
        txtAddress.autocorrectionType = .no
        txtAddress.autocapitalizationType = .none
        txtAddress.delegate = self
        txtAddress.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        txtAddress.textColor = Colors.gray
        txtAddress.text = "Address"
        txtAddress.textAlignment = .left
        txtAddress.layer.cornerRadius = 5
        txtAddress.layer.borderWidth = 0.8
        txtAddress.layer.borderColor = Colors.gray.cgColor
        txtAddress.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 1)
        addView.addSubview(txtAddress)
        txtAddress.enableAutolayout()
        txtAddress.belowView(25, to: tfName)
        txtAddress.leadingMargin(16)
        txtAddress.trailingMargin(16)
        txtAddress.fixHeight(100)
        
        let width = (screenWidth - 48) / 2
        tfCity.autocorrectionType = .no
        tfCity.autocapitalizationType = .none
        tfCity.isUserInteractionEnabled = false
        tfCity.delegate = self
        tfCity.tag = 101
        tfCity.setLeftPaddingPoints(8)
        tfCity.setRightPaddingPoints(8)
        tfCity.textColor = .black
        tfCity.placeholder = "City"
        tfCity.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 1)
        tfCity.layer.cornerRadius = 5
        tfCity.layer.borderWidth = 0.8
        tfCity.layer.borderColor = Colors.gray.cgColor
        addView.addSubview(tfCity)
        tfCity.enableAutolayout()
        tfCity.belowView(25, to: txtAddress)
        tfCity.leadingMargin(16)
        tfCity.fixWidth(width)
        tfCity.fixHeight(45)
        
        tfState.isUserInteractionEnabled = false
        tfState.delegate = self
        tfState.tag = 102
        tfState.setLeftPaddingPoints(8)
        tfState.setRightPaddingPoints(8)
        tfState.textColor = .black
        tfState.placeholder = "State"
        tfState.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 1)
        tfState.layer.cornerRadius = 5
        tfState.layer.borderWidth = 0.8
        tfState.layer.borderColor = Colors.gray.cgColor
        addView.addSubview(tfState)
        tfState.enableAutolayout()
        tfState.belowView(25, to: txtAddress)
        tfState.add(toRight: 16, of: tfCity)
        tfState.fixWidth(width)
        tfState.fixHeight(45)
        
        tfPincode.isUserInteractionEnabled = false
        tfPincode.delegate = self
        tfPincode.tag = 103
        tfPincode.setLeftPaddingPoints(8)
        tfPincode.setRightPaddingPoints(8)
        tfPincode.textColor = .black
        tfPincode.placeholder = "Pincode"
        tfPincode.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 1)
        tfPincode.layer.cornerRadius = 5
        tfPincode.layer.borderWidth = 0.8
        tfPincode.layer.borderColor = Colors.gray.cgColor
        addView.addSubview(tfPincode)
        tfPincode.enableAutolayout()
        tfPincode.belowView(25, to: tfCity)
        tfPincode.leadingMargin(16)
        tfPincode.trailingMargin(16)
        tfPincode.fixHeight(45)
    }
    
    fileprivate func getLocation() {
        locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    @objc func handleTap(gestureRecognizer: UITapGestureRecognizer) {
        let location = gestureRecognizer.location(in: mapView)
        let coordinate = mapView.convert(location, toCoordinateFrom: mapView)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        mapView.addAnnotation(annotation)
    }
    
    @objc func goToCurrentLocation(_ sender : UIButton) {
        
        if !hasLocationPermission() {
            let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
                UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
            alertController.addAction(cancelAction)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else {
            getLocation()
        }
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            @unknown default:
                break
            }
        } else {
            hasPermission = false
        }
        return hasPermission
    }
    
    @objc func backBtnTapped(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func backOneTapped(_ sender : UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: PincodeVC.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
    }
    
    @objc func gotoNext(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(AvailableSlotModel.self, from: jsonData!)
            self.objSlot = data
            if (data.status ?? false) {
                self.showToast(message: data.message ?? "")
                let vc = LoanDateTimeVC()
                vc.objSlot = self.objSlot
                vc.enquiryId = self.enquiryId
                self.navigationController?.pushViewController(vc, animated: false)
            }
            else{
                self.showToast(message: data.message ?? "")
            }
        }
        catch let error {
           // print(error)
        }
    }
    
    @objc func nextBtnTapped(_ sender : UIButton) {
        self.view.endEditing(true)
        if (tfName.text == "") {
            self.showToast(message:"SELECT NAME")
            return
        }
        
        if (txtAddress.text == "" || txtAddress.text == "Address") {
            self.showToast(message:"SELECT ADDRESS")
            return
        }
        
        if (tfCity.text == "") {
            self.showToast(message:"SELECT ADDRESS")
            return
        }
        
        if (tfState.text == "") {
            self.showToast(message:"SELECT STATE")
            return
        }
        
        Loader.shared.StartActivityIndicator(obj: self)
        
        let locationDict = ["name": tfName.text ?? "",
                            "enquiryId": enquiryId,
                            "address": txtAddress.text ?? "",
                            "city" : tfCity.text ?? "",
                            "state": tfState.text ?? "",
                            "country" : country,
                            "pincode": tfPincode.text ?? "",
                            "selectedLatitude": CommonModel.shared.lat,
                            "selectedLongitude": CommonModel.shared.long,
                            "addressid": 0] as [String : Any]
        
        let mainDict = ["eventName": "sendLoanAddress", "encryption": false,"data":locationDict ] as [String : Any]
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
    
    @objc func keyboardWillShow(notification:NSNotification) {
        
        guard let userInfo = notification.userInfo else {return}
        guard let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {return}
        let keyboardFrame = keyboardSize.cgRectValue
        
        if self.viewBack.bounds.origin.y == 0{
            self.viewBack.bounds.origin.y += keyboardFrame.height
        }
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        if self.viewBack.bounds.origin.y != 0 {
            self.viewBack.bounds.origin.y = 0
        }
    }
}

extension AddAddressVC : UITextFieldDelegate {
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        lastTag = textField.tag
        return true
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        if let nextResponder :UIResponder  = textField.superview?.viewWithTag(nextTag) as UIResponder? {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
}

extension AddAddressVC : UITextViewDelegate {
    
    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if textView.text == "Address" {
            textView.text = ""
            textView.textColor = .black
            textView.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        }
        return true
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        if (textView.text == "") {
            textView.text = "Address"
            textView.textColor = Colors.gray
            textView.contentInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        }
    }
}

extension AddAddressVC : CLLocationManagerDelegate, MKMapViewDelegate {
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let location = locations.last else {//print("Location Not Found")
            return
        }
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        locationManager.stopUpdatingLocation()
        mapView.setRegion(region, animated: true)
        
        newPin.coordinate = location.coordinate
    
        CommonModel.shared.lat = center.latitude
        CommonModel.shared.long = center.longitude
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
            if (error != nil) {
                
            }
            
            let placemark = placemarks! as [CLPlacemark]
            if placemark.count > 0 {
                let placemark = placemarks![0]
                
                self.tfCity.text = placemark.locality ?? ""
                self.tfState.text = placemark.administrativeArea ?? ""
                self.tfPincode.text = placemark.postalCode ?? ""
                self.country = placemark.country ?? ""
            }
        }
    }
    
    public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else {
            let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "userLocation")
            annotationView.image = UIImage(named: "pin", in: Bundle(for: type(of: self)), compatibleWith: nil)
            annotationView.isDraggable = true
            return annotationView
        }
        return nil
    }
}

class pinAnnotation:NSObject,MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    init(coordinate:CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}
