//
//  AddressListVC.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/10/21.
//

import UIKit

public class AddressListVC: BaseController {
    
    var refreshControl:UIRefreshControl!
    
    let separator = UIView()
    let lblLongPress = UILabel()
    let tblView = UITableView()
    var tblViewHeightConstraint : NSLayoutConstraint?
    
    let lblService = UILabel()
    let btnDelete = UIButton()
    let lblUsername = UILabel()
    let lblAddress = UILabel()
    
    var defaultAddId = ""
    var objDefault : Address?
    
    let viewBack = UIView()
    var objAddress : AddressListModel?
    var index = Int()
    var enquiryId = ""
    
    var objSlot : AvailableSlotModel?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        setupUI()
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(sender:)))
        tblView.addGestureRecognizer(longPress)
    }
    
    
    public override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        // createNavigationBar("Add address")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchAddress(notification:)), name: Notification.Name("SendLoanAmountResponse"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoNext(notification:)), name: Notification.Name("SendLoanAddressResponse"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDeleteAddress(notification:)), name: Notification.Name("DeleteAddressResponse"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleDefaultAddress(notification:)), name: Notification.Name("UpdateDefaultAddressReponse"), object: nil)
        
        setupData()
    }
    
    
    public override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .lightContent
        } else {
            return .lightContent
        }
    }
    
    func setupUI() {
        
        let headerView = TopPgrogess()
        headerView.backgroundColor = .white
        headerView.setView()
        headerView.lblOne.isHidden = true
        headerView.lblTwo.isHidden = true
        headerView.ivFirst.backgroundColor = .white
        headerView.ivSecond.backgroundColor = .white
        
        
        if #available(iOS 13.0, *) {
            headerView.ivFirst.image = UIImage(named: "rightWhite", in: Bundle(for: type(of: self)), with: nil)
            headerView.ivSecond.image = UIImage(named: "rightWhite", in: Bundle(for: type(of: self)), with: nil)
        } else {
            headerView.ivFirst.image = UIImage(named: "rightWhite", in: Bundle(for: type(of: self)), compatibleWith : nil)
            headerView.ivSecond.image = UIImage(named: "rightWhite", in: Bundle(for: type(of: self)), compatibleWith: nil)
        }
        
        
        headerView.ivThird.backgroundColor = Colors.theme
        headerView.btnFour.addTarget(self, action: #selector(nextBtnTapped(_:)), for: .touchUpInside)
        headerView.btnTwo.addTarget(self, action: #selector(backBtnTapped(_:)), for: .touchUpInside)
        headerView.btnOne.addTarget(self, action: #selector(backOneTapped(_:)), for: .touchUpInside)
        
        view.addSubview(headerView)
        headerView.enableAutolayout()
        headerView.fixHeight(80)
        headerView.leadingMargin(0)
        headerView.trailingMargin(0)
        headerView.topMargin(self.topbarHeight)
        
        let bottomView = BottomBtnBar()
        bottomView.btnBack.addTarget(self, action: #selector(backBtnTapped(_:)), for: .touchUpInside)
        bottomView.btnNext.addTarget(self, action: #selector(nextBtnTapped(_:)), for: .touchUpInside)
        bottomView.setView()
        view.addSubview(bottomView)
        bottomView.enableAutolayout()
        bottomView.centerX()
        bottomView.fixWidth(screenWidth)
        bottomView.bottomMargin(Constraints.bottom)
        bottomView.fixHeight(50)
        
        let btnAddAddress = UIButton()
        btnAddAddress.backgroundColor = Colors.theme
        btnAddAddress.clipsToBounds = true
        btnAddAddress.setTitle("Add", for: .normal)
        btnAddAddress.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        btnAddAddress.setTitleColor(.white, for: .normal)
        btnAddAddress.layer.cornerRadius = 6
        btnAddAddress.addShadow()
        btnAddAddress.addTarget(self, action: #selector(handleAddressTapped(_:)), for: .touchUpInside)
        view.addSubview(btnAddAddress)
        btnAddAddress.enableAutolayout()
        btnAddAddress.aboveView(-20, to: bottomView)
        btnAddAddress.fixWidth(100)
        btnAddAddress.trailingMargin(20)
        btnAddAddress.fixHeight(40)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        
        let scrollViewMain = UIScrollView()
        scrollViewMain.contentInsetAdjustmentBehavior = .never
        scrollViewMain.backgroundColor = UIColor.white
        scrollViewMain.bounces = true
        scrollViewMain.isScrollEnabled = true
        scrollViewMain.showsVerticalScrollIndicator = true
        scrollViewMain.refreshControl = refreshControl
        view.addSubview(scrollViewMain)
        scrollViewMain.enableAutolayout()
        scrollViewMain.leadingMargin(0)
        scrollViewMain.fixWidth(screenWidth)
        scrollViewMain.belowView(0, to: headerView)
        scrollViewMain.aboveView(-20, to: btnAddAddress)
        
        // Main View Under Scroll View
        viewBack.backgroundColor = .white
        scrollViewMain.addSubview(viewBack)
        viewBack.enableAutolayout()
        viewBack.centerX()
        viewBack.fixWidth(screenWidth)
        viewBack.topMargin(0)
        viewBack.bottomMargin(0)
        viewBack.flexibleHeightGreater(10)
        
        lblService.font = UIFont(name: Fonts.HelveticaNeueMedium, size: 18)
        viewBack.addSubview(lblService)
        lblService.enableAutolayout()
        lblService.topMargin(8)
        lblService.leadingMargin(0)
        lblService.trailingMargin(0)
        lblService.fixHeight(30)
        
        let lblDefaultAdd = UILabel()
        lblDefaultAdd.text = "Default Address"
        lblDefaultAdd.textAlignment = .left
        lblDefaultAdd.font = UIFont(name: Fonts.HelveticaRegular, size: 16)
        lblDefaultAdd.textColor = Colors.gray
        viewBack.addSubview(lblDefaultAdd)
        lblDefaultAdd.enableAutolayout()
        lblDefaultAdd.belowView(20, to: lblService)
        lblDefaultAdd.leadingMargin(16)
        lblDefaultAdd.trailingMargin(0)
        lblDefaultAdd.fixHeight(30)
        
        let viewDefaultAdd = UIView()
        viewDefaultAdd.clipsToBounds = true
        viewDefaultAdd.layer.cornerRadius = 12
        viewBack.addSubview(viewDefaultAdd)
        viewDefaultAdd.enableAutolayout()
        viewDefaultAdd.belowView(8, to: lblDefaultAdd)
        viewDefaultAdd.leadingMargin(16)
        viewDefaultAdd.trailingMargin(16)
        //viewDefaultAdd.fixHeight(120)
        
        let ivGradient = UIImageView()
        ivGradient.contentMode = .scaleAspectFill
        ivGradient.image = UIImage(named: "gradientBack", in: Bundle(for: type(of: self
        )), compatibleWith: nil)
        ivGradient.clipsToBounds = true
        ivGradient.layer.cornerRadius = 8
        viewDefaultAdd.addSubview(ivGradient)
        ivGradient.enableAutolayout()
        ivGradient.topMargin(4)
        ivGradient.leadingMargin(0)
        ivGradient.trailingMargin(0)
        ivGradient.bottomMargin(4)
        
        let ivUser = UIImageView()
        ivUser.clipsToBounds = true
        ivUser.contentMode = .scaleAspectFit
        if #available(iOS 13.0, *) {
            ivUser.image = UIImage(named: "profile", in: Bundle(for: type(of: self)), with: nil)?.withRenderingMode(.alwaysTemplate)
        } else {
            ivUser.image = UIImage(named: "profile", in: Bundle(for: type(of: self)), compatibleWith : nil)?.withRenderingMode(.alwaysTemplate)
        }
        ivUser.tintColor = .white
        viewDefaultAdd.addSubview(ivUser)
        ivUser.enableAutolayout()
        ivUser.leadingMargin(20)
        ivUser.topMargin(20)
        ivUser.fixHeight(20)
        ivUser.fixWidth(20)
        
        
        lblUsername.text = ""
        lblUsername.textColor = .white
        lblUsername.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 1)
        viewDefaultAdd.addSubview(lblUsername)
        lblUsername.enableAutolayout()
        lblUsername.centerY(to: ivUser)
        lblUsername.add(toRight: 25, of: ivUser)
        lblUsername.trailingMargin(20)
        lblUsername.fixHeight(30)
        
        let ivAddress = UIImageView()
        ivAddress.clipsToBounds = true
        ivAddress.contentMode = .scaleAspectFit
        if #available(iOS 13.0, *) {
            ivAddress.image = UIImage(named: "home", in: Bundle(for: type(of: self)), with: nil)?.withRenderingMode(.alwaysTemplate)
        } else {
            ivAddress.image = UIImage(named: "home", in: Bundle(for: type(of: self)), compatibleWith : nil)?.withRenderingMode(.alwaysTemplate)
        }
        ivAddress.tintColor = .white
        viewDefaultAdd.addSubview(ivAddress)
        ivAddress.enableAutolayout()
        ivAddress.leadingMargin(20)
        ivAddress.belowView(16, to: ivUser)
        ivAddress.fixHeight(20)
        ivAddress.fixWidth(20)
        
        lblAddress.numberOfLines = 0
        lblAddress.text = ""
        lblAddress.textColor = .white
        lblAddress.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 1)
        viewDefaultAdd.addSubview(lblAddress)
        lblAddress.enableAutolayout()
        lblAddress.belowView(16, to: ivUser)
        lblAddress.add(toRight: 25, of: ivUser)
        lblAddress.trailingMargin(20)
        lblAddress.flexibleHeightGreater(30)
        lblAddress.bottomMargin(20)
        
        
        separator.clipsToBounds = true
        separator.backgroundColor = Colors.gray
        viewBack.addSubview(separator)
        separator.enableAutolayout()
        separator.belowView(20, to: viewDefaultAdd)
        separator.leadingMargin(16)
        separator.trailingMargin(16)
        separator.fixHeight(0.6)
        
        lblLongPress.numberOfLines = 0
        lblLongPress.text = "Long press on address to make it default"
        lblLongPress.textAlignment = .center
        lblLongPress.font = UIFont(name: Fonts.HelveticaRegular, size: 16)
        lblLongPress.textColor = Colors.gray
        viewBack.addSubview(lblLongPress)
        lblLongPress.enableAutolayout()
        lblLongPress.belowView(16, to: separator)
        lblLongPress.leadingMargin(16)
        lblLongPress.trailingMargin(0)
        lblLongPress.flexibleHeightGreater(25)
        
        tblView.delegate = self
        tblView.dataSource = self
        tblView.estimatedRowHeight = UITableView.automaticDimension
        tblView.separatorStyle = .none
        tblView.backgroundColor = .clear
        tblView.showsVerticalScrollIndicator = false
        tblView.isScrollEnabled = false
        tblView.bounces = true
        viewBack.addSubview(tblView)
        tblView.enableAutolayout()
        tblView.trailingMargin(0)
        tblView.belowView(8, to: lblLongPress)
        tblViewHeightConstraint = tblView.heightAnchor.constraint(greaterThanOrEqualToConstant: 100)
        tblViewHeightConstraint?.isActive = true
        tblView.leadingMargin(0)
        tblView.bottomMargin(Constraints.bottom)
    }
    
    fileprivate func fetchAddress() {
        let locationDict = ["enquiryId": self.enquiryId, "loanAmount":"50000"]
        let mainDict = ["eventName": "sendLoanAmount", "encryption": false,"data":locationDict ] as [String : Any]
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
    
    @objc func handleDeleteAddress(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            
            let data = try JSONDecoder().decode(DeleteAddModel.self, from: jsonData!)
            if (data.status ?? false) == true {
                showToast(message: data.message ?? "")
                self.objAddress?.data?.addresses?.remove(at: index)
                self.tblView.reloadData()
                self.perform(#selector(handleHeight), with: self, afterDelay: 0.2)
                self.perform(#selector(handleHeight), with: self, afterDelay: 0.2)
            }
            else {
                showToast(message: data.message ?? "")
            }
        }
        catch let error {
            //print(error)
        }
    }
    
    @objc func handleDefaultAddress(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            
            let data = try JSONDecoder().decode(DeleteAddModel.self, from: jsonData!)
            if (data.status ?? false) == true {
                showToast(message: data.message ?? "")
                self.fetchAddress()
            }
            else {
                showToast(message: data.message ?? "")
            }
        }
        catch let error {
             //print(error)
        }
    }
    
    
    fileprivate func setupData() {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1
        lblService.attributedText = NSMutableAttributedString(string: self.objAddress?.data?.header ?? "", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle,.font : UIFont(name: Fonts.acuminProRegular, size: TextSize.title + 6)!])
        lblService.textAlignment = .center
        
        for idx in 0..<(self.objAddress?.data?.addresses?.count ?? 0) {
            if ((self.objAddress?.data?.addresses?[idx].isDefault ?? false) == true) {
                lblUsername.text =  self.objAddress?.data?.addresses?[idx].name ?? ""
                lblAddress.text =  (self.objAddress?.data?.addresses?[idx].address ?? "") + " - " + (self.objAddress?.data?.addresses?[idx].pincode ?? "")
                defaultAddId = self.objAddress?.data?.addresses?[idx].addressid ?? ""
                objDefault = self.objAddress?.data?.addresses?[idx]
            }
        }
        
        if ((self.objAddress?.data?.addresses?.count ?? 0) > 1) {
            self.tblView.isHidden = false
            lblLongPress.isHidden = false
            separator.isHidden = false
        }
        else {
            lblLongPress.isHidden = true
            self.tblView.isHidden = true
            separator.isHidden = true
        }
        
        self.view.layoutIfNeeded()
        
        self.tblView.reloadData()
        self.perform(#selector(handleHeight), with: self, afterDelay: 0.2)
        
        self.tblView.reloadData()
        self.perform(#selector(handleHeight), with: self, afterDelay: 0.2)
    }
    
    @objc fileprivate func handleHeight() {
        self.view.layoutIfNeeded()
        self.tblView.layoutIfNeeded()
        self.tblViewHeightConstraint?.constant = self.tblView.contentSize.height
        self.tblViewHeightConstraint?.constant = self.tblView.contentSize.height
    }
    
    
    @objc private func handleLongPress(sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began {
            let touchPoint = sender.location(in: tblView)
            if let indexPath = tblView.indexPathForRow(at: touchPoint) {
                
                let locationDict = [ "mobileNo": CommonModel.shared.mobileNumber,
                                     "enquiryId": self.enquiryId,
                                     "addressId": self.objAddress?.data?.addresses?[indexPath.row].addressid ?? ""] as [String : Any]
                
                let mainDict = ["eventName": "UPDATEDEFAULTADDRESS", "encryption": false,"data":locationDict ] as [String : Any]
                Loader.shared.StartActivityIndicator(obj: self)
                SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
            }
        }
    }
    
    @objc func handleAddressTapped(_ sender : UIButton) {
        
        let vc = AddAddressVC()
        vc.enquiryId = enquiryId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func refresh(sender:AnyObject) {
        fetchAddress()
    }
    
    @objc func fetchAddress(notification: Notification) {
        refreshControl.endRefreshing()
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(AddressListModel.self, from: jsonData!)
            if (data.status ?? false) {
                self.objAddress = data
                DispatchQueue.main.async {
                    self.setupData()
                }
            }
            else {
                self.showToast(message: (data.message ?? ""))
            }
        }
        catch let error {
            // print(error)
        }
    }
    
    @objc func gotoNext(notification: Notification) {
        
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(AvailableSlotModel.self, from: jsonData!)
            self.objSlot = data
            if (data.status ?? false) {
                self.showToast(message: data.message ?? "")
                DispatchQueue.main.async {
                    let vc = LoanDateTimeVC()
                    vc.objSlot = self.objSlot
                    vc.enquiryId = self.enquiryId
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
            else {
                self.showToast(message: data.message ?? "")
            }
        }
        catch let error {
             //print(error)
        }
    }
    
    @objc func backBtnTapped(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func backOneTapped(_ sender : UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: PincodeVC.self) {
                self.navigationController!.popToViewController(controller, animated: false)
                break
            }
        }
    }
    
    
    @objc func nextBtnTapped(_ sender : UIButton) {
        
        Loader.shared.StartActivityIndicator(obj: self)
        let locationDict = ["name": objDefault?.name ?? "",
                            "enquiryId": enquiryId,
                            "address": objDefault?.address ?? "",
                            "pincode": objDefault?.pincode ?? "",//tfPincode.text ?? "",
                            "selectedLatitude": objDefault?.latitude ?? "",//CommonModel.shared.lat,
                            "selectedLongitude": objDefault?.longitude ?? "",//CommonModel.shared.long,
                            "addressid": self.defaultAddId] as [String : Any]
        let mainDict = ["eventName": "sendLoanAddress", "encryption": false,"data":locationDict ] as [String : Any]
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
    
    @objc func handleDelete(_ sender : UIButton) {
        
        index = sender.tag
        let locationDict = [ "mobileNo": CommonModel.shared.mobileNumber,
                             "enquiryId": self.enquiryId,
                             "addressId": self.objAddress?.data?.addresses?[sender.tag].addressid ?? ""] as [String : Any]
        
        let mainDict = ["eventName": "DELETEADDRESS", "encryption": false,"data":locationDict ] as [String : Any]
        Loader.shared.StartActivityIndicator(obj: self)
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
}


extension AddressListVC : UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.objAddress?.data?.addresses?.count ?? 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = AddressCell()
        cell.selectionStyle = .none
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(handleDelete(_:)), for: .touchUpInside)
        cell.lblUsername.text = self.objAddress?.data?.addresses?[indexPath.row].name ?? ""
        cell.lblAddress.text = (self.objAddress?.data?.addresses?[indexPath.row].address ?? "") + " - " + (self.objAddress?.data?.addresses?[indexPath.row].pincode ?? "")
        return cell
    }
}
