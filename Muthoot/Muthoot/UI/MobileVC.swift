//
//  MobileVC.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/8/21.
//

import UIKit
import CoreLocation

public class MobileVC: BaseController {
    
    let scrollViewMain = UIScrollView()
    let viewBack = UIView()
    
    let tfMobile = UITextField()
    let tfMobileTwo = UITextField()
    let tfLoanId = UITextField()
    let tfTMobileThree = UITextField()
    let tfRequestId = UITextField()
    var lastTag = Int()
    
    var locationManager: CLLocationManager!
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        setupUI()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoStartLoan(notification:)), name: Notification.Name("StartProcess"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoStartLoan(notification:)), name: Notification.Name("GetPincode"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoLoanSummary(notification:)), name: Notification.Name("MoveToLoanSummary"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        
        createNavigationBar("MuthootTrack")
    }
    
    
    public override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate func  setupUI() {
        
        scrollViewMain.contentInsetAdjustmentBehavior = .never
        scrollViewMain.backgroundColor = UIColor.white
        scrollViewMain.bounces = false
        scrollViewMain.isScrollEnabled = true
        scrollViewMain.showsVerticalScrollIndicator = true
        view.addSubview(scrollViewMain)
        scrollViewMain.enableAutolayout()
        scrollViewMain.leadingMargin(0)
        scrollViewMain.fixWidth(screenWidth)
        scrollViewMain.topMargin(Constraints.top + self.topHeight)
        scrollViewMain.bottomMargin(Constraints.bottom)
        
        // Main View Under Scroll View
        
        viewBack.backgroundColor = .white
        scrollViewMain.addSubview(viewBack)
        viewBack.enableAutolayout()
        viewBack.centerX()
        viewBack.fixWidth(screenWidth)
        viewBack.topMargin(0)
        viewBack.bottomMargin(0)
        viewBack.flexibleHeightGreater(10)
        
        let toolbar = UIToolbar().ToolbarPiker(mySelect: #selector(handToolBar), title: "Done")
        tfMobile.inputAccessoryView = toolbar
        let placeholder = NSAttributedString(string: "Mobile Number",
                                             attributes: [NSAttributedString.Key.foregroundColor: Colors.gray])
        tfMobile.delegate = self
        tfMobile.tag = 100
        tfMobile.textAlignment = .center
        tfMobile.textColor = UIColor.black
        tfMobile.attributedPlaceholder = placeholder
        tfMobile.font = UIFont(name: Fonts.HelveticaRegular, size: 16)
        viewBack.addSubview(tfMobile)
        tfMobile.enableAutolayout()
        tfMobile.topMargin(100)
        tfMobile.fixWidth(150)
        tfMobile.fixHeight(35)
        tfMobile.centerX()
        
        
        let separator1 = UIView()
        separator1.backgroundColor = .gray
        viewBack.addSubview(separator1)
        separator1.enableAutolayout()
        separator1.centerX()
        separator1.fixWidth(150)
        separator1.belowView(0, to: tfMobile)
        separator1.fixHeight(1)
        
        let btnNext = UIButton()
        btnNext.backgroundColor = Colors.theme
        btnNext.titleLabel?.font = UIFont(name: Fonts.HelveticaNeueMedium, size: TextSize.title)
        btnNext.setTitle("START LOAN", for: .normal)
        btnNext.setTitleColor(.white, for: .normal)
        btnNext.addTarget(self,action:#selector(handleStartLoan(_:)),for:.touchUpInside)
        viewBack.addSubview(btnNext)
        btnNext.enableAutolayout()
        btnNext.belowView(16, to: separator1)
        btnNext.fixWidth(120)
        btnNext.fixHeight(40)
        btnNext.centerX()
        
        let width = (screenWidth - 80) / 2
        
        tfMobileTwo.inputAccessoryView = toolbar
        tfMobileTwo.delegate = self
        tfMobileTwo.tag = 101
        tfMobileTwo.textAlignment = .center
        tfMobileTwo.textColor = UIColor.black
        tfMobileTwo.attributedPlaceholder = placeholder
        tfMobileTwo.font = UIFont(name: Fonts.HelveticaRegular, size: 16)
        viewBack.addSubview(tfMobileTwo)
        tfMobileTwo.enableAutolayout()
        tfMobileTwo.belowView(20, to: btnNext)
        tfMobileTwo.fixWidth(width)
        tfMobileTwo.fixHeight(35)
        tfMobileTwo.leadingMargin(30)
        
        
        let separator2 = UIView()
        separator2.backgroundColor = .gray
        viewBack.addSubview(separator2)
        separator2.enableAutolayout()
        separator2.leadingMargin(30)
        separator2.fixWidth(width)
        separator2.belowView(0, to: tfMobileTwo)
        separator2.fixHeight(1)
        
        let loanIDdplaceholder = NSAttributedString(string: "LoanID",
                                                    attributes: [NSAttributedString.Key.foregroundColor: Colors.gray])
        tfLoanId.inputAccessoryView = toolbar
        tfLoanId.delegate = self
        tfLoanId.tag = 102
        tfLoanId.textAlignment = .center
        tfLoanId.textColor = UIColor.black
        tfLoanId.attributedPlaceholder = loanIDdplaceholder
        tfLoanId.font = UIFont(name: Fonts.HelveticaRegular, size: 16)
        viewBack.addSubview(tfLoanId)
        tfLoanId.enableAutolayout()
        tfLoanId.belowView(20, to: btnNext)
        tfLoanId.fixWidth(width)
        tfLoanId.fixHeight(35)
        tfLoanId.add(toRight: 20, of: tfMobileTwo)
        
        let separator3 = UIView()
        separator3.backgroundColor = .gray
        viewBack.addSubview(separator3)
        separator3.enableAutolayout()
        separator3.add(toRight: 20, of: tfMobileTwo)
        separator3.fixWidth(150)
        separator3.belowView(0, to: tfLoanId)
        separator3.fixHeight(1)
        
        let btnRepledge = UIButton()
        btnRepledge.backgroundColor = Colors.theme
        btnRepledge.titleLabel?.font = UIFont(name: Fonts.HelveticaNeueMedium, size: TextSize.title)
        btnRepledge.setTitle("SUBMIT REPLEDGE", for: .normal)
        btnRepledge.setTitleColor(.white, for: .normal)
        btnRepledge.addTarget(self,action:#selector(handleRepladge(_:)),for:.touchUpInside)
        viewBack.addSubview(btnRepledge)
        btnRepledge.enableAutolayout()
        btnRepledge.belowView(16, to: separator2)
        btnRepledge.fixWidth(160)
        btnRepledge.fixHeight(40)
        btnRepledge.centerX()
        
        tfTMobileThree.inputAccessoryView = toolbar
        tfTMobileThree.delegate = self
        tfTMobileThree.tag = 103
        tfTMobileThree.textAlignment = .center
        tfTMobileThree.textColor = UIColor.black
        tfTMobileThree.attributedPlaceholder = placeholder
        tfTMobileThree.font = UIFont(name: Fonts.HelveticaRegular, size: 16)
        viewBack.addSubview(tfTMobileThree)
        tfTMobileThree.enableAutolayout()
        tfTMobileThree.belowView(20, to: btnRepledge)
        tfTMobileThree.fixWidth(width)
        tfTMobileThree.fixHeight(35)
        tfTMobileThree.leadingMargin(30)
        
        let separator4 = UIView()
        separator4.backgroundColor = .gray
        viewBack.addSubview(separator4)
        separator4.enableAutolayout()
        separator4.leadingMargin(30)
        separator4.fixWidth(width)
        separator4.belowView(0, to: tfTMobileThree)
        separator4.fixHeight(1)
        
        let RequestIdplaceholder = NSAttributedString(string: "RequestID",
                                                      attributes: [NSAttributedString.Key.foregroundColor: Colors.gray])
        tfRequestId.inputAccessoryView = toolbar
        tfRequestId.delegate = self
        tfRequestId.tag = 104
        tfRequestId.textAlignment = .center
        tfRequestId.textColor = UIColor.black
        tfRequestId.attributedPlaceholder = RequestIdplaceholder
        tfRequestId.font = UIFont(name: Fonts.HelveticaRegular, size: 16)
        viewBack.addSubview(tfRequestId)
        tfRequestId.enableAutolayout()
        tfRequestId.belowView(20, to: btnRepledge)
        tfRequestId.fixWidth(width)
        tfRequestId.fixHeight(35)
        tfRequestId.add(toRight: 20, of: tfMobileTwo)
        
        let separator5 = UIView()
        separator5.backgroundColor = .gray
        viewBack.addSubview(separator5)
        separator5.enableAutolayout()
        separator5.add(toRight: 20, of: tfMobileTwo)
        separator5.fixWidth(width)
        separator5.belowView(0, to: tfRequestId)
        separator5.fixHeight(1)
        
        let btnRelease = UIButton()
        btnRelease.backgroundColor = Colors.theme
        btnRelease.titleLabel?.font = UIFont(name: Fonts.HelveticaNeueMedium, size: TextSize.title)
        btnRelease.setTitle("SUBMIT RELEASE", for: .normal)
        btnRelease.setTitleColor(.white, for: .normal)
        btnRelease.addTarget(self,action:#selector(handleRelease(_:)),for:.touchUpInside)
        viewBack.addSubview(btnRelease)
        
        btnRelease.enableAutolayout()
        btnRelease.belowView(16, to: separator4)
        btnRelease.fixWidth(160)
        btnRelease.fixHeight(40)
        btnRelease.centerX()
        btnRelease.bottomMargin(0)
    }
    
    fileprivate func getLocation() {
        
        locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    
    @objc func gotoStartLoan(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(PincodeModel.self, from: jsonData!)
            if (data.status ?? false) {
                let vc = PincodeVC()
                vc.objPerson = data
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                self.showToast(message: data.message ?? "")
            }
        }
        catch let error {
           // print(error)
        }
    }
    
    @objc func gotoLoanSummary(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(LoanSummaryModel.self, from: jsonData!)
            if (data.status ?? false) {
                let vc = LoanSummaryVC()
                vc.objLoanSummary = data
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                self.showToast(message: data.message ?? "")
            }
        }
        catch let error {
           // print(error)
        }
    }
    
    @objc func keyboardWillShow(notification:NSNotification) {
        if (lastTag > 102) {
            
            self.scrollViewMain.isScrollEnabled = true
            let info = notification.userInfo!
            let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
            let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize!.height + 50, right: 0.0)
            
            self.scrollViewMain.contentInset = contentInsets
            self.scrollViewMain.scrollIndicatorInsets = contentInsets
            
            var aRect : CGRect = self.view.frame
            aRect.size.height -= keyboardSize!.height
            let bottomOffset = CGPoint(x: 0, y: scrollViewMain.contentSize.height - scrollViewMain.bounds.height + scrollViewMain.contentInset.bottom)
            scrollViewMain.setContentOffset(bottomOffset, animated: true)
        }
    }
    
    @objc func keyboardWillHide(notification:NSNotification) {
        if (lastTag > 102) {
            let info = notification.userInfo!
            let keyboardSize = (info[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
            let contentInsets : UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: -keyboardSize!.height, right: 0.0)
            self.scrollViewMain.contentInset = contentInsets
            self.scrollViewMain.scrollIndicatorInsets = contentInsets
            self.view.endEditing(true)
        }
    }
    
    
    //MARK:- Handle Button
    @objc fileprivate func handleStartLoan(_ sender : UIButton) {
        self.view.endEditing(true)
        if ((self.tfMobile.text ?? "") == "") {
            self.showToast(message: "Enter mobile number")
            return
        }
        
        let vc = PincodeVC()
        vc.mobileNo = self.tfMobile.text ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc fileprivate func handleRepladge(_ sender : UIButton) {
        
        self.view.endEditing(true)
        if ((self.tfMobileTwo.text ?? "") == "") {
            self.showToast(message: "Enter mobile number")
            return
        }
        
        if ((self.tfLoanId.text ?? "") == "") {
            self.showToast(message: "Enter Loan number")
            return
        }
        
        let vc = RepledgeRequestVC()
        vc.mobileNo = self.tfMobileTwo.text ?? ""
        vc.loanNo = tfLoanId.text ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @objc fileprivate func handleRelease(_ sender : UIButton) {
        self.view.endEditing(true)
        if ((self.tfTMobileThree.text ?? "") == "") {
            self.showToast(message: "Enter mobile number")
            return
        }
        
        if ((self.tfRequestId.text ?? "") == "") {
            self.showToast(message: "Enter Loan number")
            return
        }
        
        let vc = ReleaseLoanVC()
        vc.mobileNo = self.tfTMobileThree.text ?? ""
        vc.loanNo = self.tfRequestId.text ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MobileVC : UITextFieldDelegate {
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        lastTag = textField.tag
        return true
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        if let nextResponder :UIResponder  = textField.superview?.viewWithTag(nextTag) as UIResponder? {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
}

extension MobileVC : CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let location = locations.last else {//print("Location Not Found")
            return
        }
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        CommonModel.shared.lat = center.latitude
        CommonModel.shared.long = center.longitude
    }
}
