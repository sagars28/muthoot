//
//  KYCDetailsVC.swift
//  Muthoot Track
//
//  Created by Sagar on 21/06/21.
//

import UIKit

public class KYCDetailsVC: BaseController {
    
    let stackView = UIStackView()
    let lblName = UILabel()
    let lblMobileNumber = UILabel()
    let lblGender = UILabel()
    let lblCareOfRelation = UILabel()
    let lblCareOfName = UILabel()
    let lblDOB = UILabel()
    let lblEmailId = UILabel()
    let lblMotherName = UILabel()
    let lblMaritalStatus = UILabel()
    let lblResidentialStatus = UILabel()
    let lblNationality = UILabel()
    let lblOccupation = UILabel()
    let lblIncome = UILabel()
    let lblPresentAdd = UILabel()
    let lblRiskCat = UILabel()
    let lblPermanentAdd = UILabel()
    let cvBackView = UIView()
    
    var cvDoccuments : UICollectionView!
    
    var objKycDetails : KycApprovalModel?
    var objLoanSummary : LoanSummaryModel?
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.viewBackground
        setupUI()
        setupData()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        createNavigationBarWithBack("KYC Details")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.HandleKYCApprove(notification:)), name: Notification.Name("SuccessApproveKyc"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.HandleKYCRejectSuccess(notification:)), name: Notification.Name("successRejectKyc"), object: nil)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    fileprivate func setupUI() {
        
        let bottomView = UIView()
        bottomView.clipsToBounds = true
        bottomView.backgroundColor = .white
        view.addSubview(bottomView)
        bottomView.enableAutolayout()
        bottomView.centerX()
        bottomView.fixWidth(screenWidth)
        bottomView.bottomMargin(Constraints.bottom)
        bottomView.fixHeight(50)
        
        let btnDisagree = UIButton()
        btnDisagree.setTitle("DISAGREE", for: .normal)
        btnDisagree.setTitleColor(Colors.red, for: .normal)
        btnDisagree.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        btnDisagree.addTarget(self, action: #selector(handleDisagreeBtn(_:)), for: .touchUpInside)
        bottomView.addSubview(btnDisagree)
        btnDisagree.enableAutolayout()
        btnDisagree.trailingMargin(20)
        btnDisagree.fixHeight(50)
        btnDisagree.centerY()
        
        let btnAgree = UIButton()
        btnAgree.setTitle("AGREE", for: .normal)
        btnAgree.setTitleColor(Colors.theme, for: .normal)
        btnAgree.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        btnAgree.addTarget(self, action: #selector(handleAgreeBtn(_:)), for: .touchUpInside)
        bottomView.addSubview(btnAgree)
        btnAgree.enableAutolayout()
        btnAgree.add(toLeft: 20, of: btnDisagree)
        btnAgree.fixHeight(50)
        btnAgree.centerY()
        
        
        //MARK:- ScrollView
        let scrollViewMain = UIScrollView()
        scrollViewMain.contentInsetAdjustmentBehavior = .never
        scrollViewMain.backgroundColor = Colors.viewBackground
        scrollViewMain.bounces = false
        scrollViewMain.isScrollEnabled = true
        scrollViewMain.showsVerticalScrollIndicator = true
        view.addSubview(scrollViewMain)
        scrollViewMain.enableAutolayout()
        scrollViewMain.leadingMargin(0)
        scrollViewMain.fixWidth(screenWidth)
        scrollViewMain.topMargin(Constraints.top + self.topHeight)
        scrollViewMain.aboveView(-8, to: bottomView)
        
        // Main View Under Scroll View
        let viewBack = UIView()
        viewBack.backgroundColor = .white
        viewBack.clipsToBounds = true
        viewBack.layer.cornerRadius = 8
        viewBack.layer.borderWidth = 0.4
        viewBack.layer.borderColor = Colors.textGray.withAlphaComponent(0.7).cgColor
        viewBack.addShadow()
        scrollViewMain.addSubview(viewBack)
        viewBack.enableAutolayout()
        viewBack.centerX()
        viewBack.fixWidth(screenWidth - 32)
        viewBack.topMargin(16)
        viewBack.bottomMargin(16)
        viewBack.flexibleHeightGreater(10)
        
        lblName.numberOfLines = 0
        lblName.textAlignment = .left
        viewBack.addSubview(lblName)
        lblName.enableAutolayout()
        lblName.topMargin(16)
        lblName.leadingMargin(20)
        lblName.trailingMargin(16)
        lblName.flexibleHeightGreater(20)
        
        lblMobileNumber.numberOfLines = 0
        lblMobileNumber.textAlignment = .left
        viewBack.addSubview(lblMobileNumber)
        lblMobileNumber.enableAutolayout()
        lblMobileNumber.belowView(16, to: lblName)
        lblMobileNumber.leadingMargin(20)
        lblMobileNumber.trailingMargin(16)
        lblMobileNumber.flexibleHeightGreater(20)
        
        lblGender.numberOfLines = 0
        lblGender.textAlignment = .left
        viewBack.addSubview(lblGender)
        lblGender.enableAutolayout()
        lblGender.belowView(16, to: lblMobileNumber)
        lblGender.leadingMargin(20)
        lblGender.trailingMargin(16)
        lblGender.flexibleHeightGreater(20)
        
        lblCareOfRelation.numberOfLines = 0
        lblCareOfRelation.textAlignment = .left
        viewBack.addSubview(lblCareOfRelation)
        lblCareOfRelation.enableAutolayout()
        lblCareOfRelation.belowView(16, to: lblGender)
        lblCareOfRelation.leadingMargin(20)
        lblCareOfRelation.trailingMargin(16)
        lblCareOfRelation.flexibleHeightGreater(20)
        
        lblCareOfName.numberOfLines = 0
        lblCareOfName.textAlignment = .left
        viewBack.addSubview(lblCareOfName)
        lblCareOfName.enableAutolayout()
        lblCareOfName.belowView(16, to: lblCareOfRelation)
        lblCareOfName.leadingMargin(20)
        lblCareOfName.trailingMargin(16)
        lblCareOfName.flexibleHeightGreater(20)
        
        lblDOB.numberOfLines = 0
        lblDOB.textAlignment = .left
        viewBack.addSubview(lblDOB)
        lblDOB.enableAutolayout()
        lblDOB.belowView(16, to: lblCareOfName)
        lblDOB.leadingMargin(20)
        lblDOB.trailingMargin(16)
        lblDOB.flexibleHeightGreater(20)
        
        lblEmailId.numberOfLines = 0
        lblEmailId.textAlignment = .left
        viewBack.addSubview(lblEmailId)
        lblEmailId.enableAutolayout()
        lblEmailId.belowView(16, to: lblDOB)
        lblEmailId.leadingMargin(20)
        lblEmailId.trailingMargin(16)
        lblEmailId.flexibleHeightGreater(20)
        
        lblMotherName.numberOfLines = 0
        lblMotherName.textAlignment = .left
        viewBack.addSubview(lblMotherName)
        lblMotherName.enableAutolayout()
        lblMotherName.belowView(16, to: lblEmailId)
        lblMotherName.leadingMargin(20)
        lblMotherName.trailingMargin(16)
        lblMotherName.flexibleHeightGreater(20)
        
        lblMaritalStatus.numberOfLines = 0
        lblMaritalStatus.textAlignment = .left
        viewBack.addSubview(lblMaritalStatus)
        lblMaritalStatus.enableAutolayout()
        lblMaritalStatus.belowView(16, to: lblMotherName)
        lblMaritalStatus.leadingMargin(20)
        lblMaritalStatus.trailingMargin(16)
        lblMaritalStatus.flexibleHeightGreater(20)
        
        lblResidentialStatus.numberOfLines = 0
        lblResidentialStatus.textAlignment = .left
        viewBack.addSubview(lblResidentialStatus)
        lblResidentialStatus.enableAutolayout()
        lblResidentialStatus.belowView(16, to: lblMaritalStatus)
        lblResidentialStatus.leadingMargin(20)
        lblResidentialStatus.trailingMargin(16)
        lblResidentialStatus.flexibleHeightGreater(20)
        
        lblNationality.numberOfLines = 0
        lblNationality.textAlignment = .left
        viewBack.addSubview(lblNationality)
        lblNationality.enableAutolayout()
        lblNationality.belowView(16, to: lblResidentialStatus)
        lblNationality.leadingMargin(20)
        lblNationality.trailingMargin(16)
        lblNationality.flexibleHeightGreater(20)
        
        lblOccupation.numberOfLines = 0
        lblOccupation.textAlignment = .left
        viewBack.addSubview(lblOccupation)
        lblOccupation.enableAutolayout()
        lblOccupation.belowView(16, to: lblNationality)
        lblOccupation.leadingMargin(20)
        lblOccupation.trailingMargin(16)
        lblOccupation.flexibleHeightGreater(20)
        
        lblIncome.numberOfLines = 0
        lblIncome.textAlignment = .left
        viewBack.addSubview(lblIncome)
        lblIncome.enableAutolayout()
        lblIncome.belowView(16, to: lblOccupation)
        lblIncome.leadingMargin(20)
        lblIncome.trailingMargin(16)
        lblIncome.flexibleHeightGreater(20)
        
        lblPresentAdd.numberOfLines = 0
        lblPresentAdd.textAlignment = .left
        viewBack.addSubview(lblPresentAdd)
        lblPresentAdd.enableAutolayout()
        lblPresentAdd.belowView(16, to: lblIncome)
        lblPresentAdd.leadingMargin(20)
        lblPresentAdd.trailingMargin(16)
        lblPresentAdd.flexibleHeightGreater(20)
        
        lblRiskCat.numberOfLines = 0
        lblRiskCat.textAlignment = .left
        viewBack.addSubview(lblRiskCat)
        lblRiskCat.enableAutolayout()
        lblRiskCat.belowView(16, to: lblPresentAdd)
        lblRiskCat.leadingMargin(20)
        lblRiskCat.trailingMargin(16)
        lblRiskCat.flexibleHeightGreater(20)
        
        lblPermanentAdd.numberOfLines = 0
        lblPermanentAdd.textAlignment = .left
        viewBack.addSubview(lblPermanentAdd)
        lblPermanentAdd.enableAutolayout()
        lblPermanentAdd.belowView(16, to: lblRiskCat)
        lblPermanentAdd.leadingMargin(20)
        lblPermanentAdd.trailingMargin(16)
        lblPermanentAdd.flexibleHeightGreater(20)
        
        cvBackView.clipsToBounds = true
        cvBackView.addShadow()
        viewBack.addSubview(cvBackView)
        cvBackView.enableAutolayout()
        cvBackView.centerX()
        cvBackView.belowView(20, to: lblPermanentAdd)
        cvBackView.fixWidth(screenWidth - 32)
        cvBackView.flexibleHeightGreater(280)
        cvBackView.bottomMargin(20)
        
        let lblDoccument = UILabel()
        lblDoccument.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle)
        lblDoccument.numberOfLines = 0
        lblDoccument.textAlignment = .left
        lblDoccument.textColor = Colors.textGray
        lblDoccument.text = "Documents"
        cvBackView.addSubview(lblDoccument)
        lblDoccument.enableAutolayout()
        lblDoccument.topMargin(8)
        lblDoccument.leadingMargin(20)
        lblDoccument.trailingMargin(16)
        lblDoccument.fixHeight(25)
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: screenWidth/2 + 20, height: 240)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 30
        layout.minimumLineSpacing = 16
        
        cvDoccuments = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cvDoccuments.isUserInteractionEnabled = true
        cvDoccuments.clipsToBounds = true
        cvDoccuments.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 30)
        cvDoccuments.tag = 500
        cvDoccuments.showsHorizontalScrollIndicator = false
        cvDoccuments.delegate = self
        cvDoccuments.dataSource = self
        cvDoccuments.backgroundColor = .white
        cvDoccuments.showsVerticalScrollIndicator = false
        cvDoccuments.isScrollEnabled = true
        cvDoccuments.bounces = false
        cvDoccuments.register(DoccumetsCell.self, forCellWithReuseIdentifier: "cell")
        cvBackView.addSubview(cvDoccuments)
        cvDoccuments.enableAutolayout()
        cvDoccuments.belowView(8, to: lblDoccument)
        cvDoccuments.leadingMargin(20)
        cvDoccuments.trailingMargin(20)
        cvDoccuments.bottomMargin(0)
    }
    
    func setupData() {
        let custPrefix = objKycDetails?.data?.custPrefix ?? ""
        let firstName = objKycDetails?.data?.firstName ?? ""
        let lastName = objKycDetails?.data?.lastName ?? ""
        
        lblName.attributedText = getAttrString2("Name\n", custPrefix + ". " + firstName + " " + lastName)
        lblMobileNumber.attributedText = getAttrString2("Mobile Number\n", objKycDetails?.data?.mobileNumber ?? "")
        lblGender.attributedText = getAttrString2("Gender\n", objKycDetails?.data?.genderID ?? "")
        lblCareOfRelation.attributedText = getAttrString2("Care Of Relation\n", objKycDetails?.data?.careOfRelation ?? "")
        
        let carePrifix = objKycDetails?.data?.careOfPrefix ?? ""
        let careName = objKycDetails?.data?.careOfName ?? ""
        
        lblCareOfName.attributedText = getAttrString2("Care Of Name\n", carePrifix + ". " + careName)
        lblDOB.attributedText = getAttrString2("DOB\n", objKycDetails?.data?.dob ?? "")
        lblEmailId.attributedText = getAttrString2("Email Id\n", objKycDetails?.data?.emailID ?? "")
        
        let motherPrifix = objKycDetails?.data?.careOfPrefix ?? ""
        let motherName = objKycDetails?.data?.careOfName ?? ""
        
        lblMotherName.attributedText = getAttrString2("Mother's Name\n", motherPrifix + ". " + motherName)
        lblMaritalStatus.attributedText = getAttrString2("Maritial Status\n", objKycDetails?.data?.maritialStatus ?? "")
        
        lblResidentialStatus.attributedText = getAttrString2("Residential Status\n", objKycDetails?.data?.residentialStatus ?? "")
        
        lblNationality.attributedText = getAttrString2("Nationality\n", objKycDetails?.data?.nationality ?? "")
        lblOccupation.attributedText = getAttrString2("Occupation\n", objKycDetails?.data?.occupationID ?? "")
        
        lblIncome.attributedText = getAttrString2("Income\n", objKycDetails?.data?.incomeID ?? "")
        
        
        let presentAdd1 = objKycDetails?.data?.presentAddressLine1 ?? ""
        let presentAdd2 = objKycDetails?.data?.presentAddressLine2 ?? ""
        let town = objKycDetails?.data?.presentTown ?? ""
        let state = objKycDetails?.data?.presentStateID ?? ""
        let country = objKycDetails?.data?.presentCountry ?? ""
        let pincode = objKycDetails?.data?.presentPinCode ?? ""
        
        lblPresentAdd.attributedText = getAttrString2("Present Address\n", "\(presentAdd1), \(presentAdd2), \(town), \(state), \(country) - \(pincode)")
        lblRiskCat.attributedText = getAttrString2("Risk Category\n", objKycDetails?.data?.riskCategory ?? "")
        
        let  permentAdd1 = objKycDetails?.data?.presentAddressLine1 ?? ""
        let  permentAdd2 = objKycDetails?.data?.presentAddressLine2 ?? ""
        let  permentTown = objKycDetails?.data?.presentTown ?? ""
        let  permentState = objKycDetails?.data?.presentStateID ?? ""
        let  permentCountry = objKycDetails?.data?.presentCountry ?? ""
        let  permentPincode = objKycDetails?.data?.presentPinCode ?? ""
        
        lblPermanentAdd.attributedText = getAttrString2("Present Address\n", "\(permentAdd1), \(permentAdd2), \(permentTown), \(permentState), \(permentCountry) - \(permentPincode)")
        
        if ((objKycDetails?.data?.uploadedDocuments?.count ?? 0) == 0) {
            self.cvBackView.isHidden = true
        }
        else {
            self.cvBackView.isHidden = false
            cvDoccuments.reloadData()
        }
    }
    
    @objc func handleAgreeBtn(_ sender : UIButton) {
        let popup = SignaturePopup()
        popup.delegate = self
        popup.frame = view.bounds
        popup.createUI()
        view.window?.addSubview(popup)
    }
    
    
    @objc func handleDisagreeBtn(_ sender : UIButton) {
        
        Loader.shared.StartActivityIndicator(obj: self)
        
        let mobileNo = self.objLoanSummary?.data?[0].mobileNo ?? 0
        let LRID = self.objLoanSummary?.data?[0].lrid ?? ""
        let enquiryId = self.objLoanSummary?.data?[0].datumID ?? ""
        let _ = self.objLoanSummary?.data?[0].id ?? ""
        
        let locationDict = ["mobileNo": mobileNo,
                            "LRID": LRID,
                            "enquiryId": enquiryId,
                            "id": objKycDetails?.data?.id ?? "",
                            "action": "reject",
                            "signature": ""] as [String : Any]
        
        let mainDict = ["eventName": "REJECTKYC", "encryption": false,"data":locationDict ] as [String : Any]
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
    
    func submitKYCDetails(sign : String) {
        Loader.shared.StartActivityIndicator(obj: self)
        
        let mobileNo = self.objLoanSummary?.data?[0].mobileNo ?? 0
        let LRID = self.objLoanSummary?.data?[0].lrid ?? ""
        let enquiryId = self.objLoanSummary?.data?[0].datumID ?? ""
        let _ = self.objLoanSummary?.data?[0].id ?? ""
        
        let locationDict = ["mobileNo": mobileNo,
                            "LRID": LRID,
                            "enquiryId": enquiryId,
                            "id": objKycDetails?.data?.id ?? "",
                            "action": "approve",
                            "signature": sign] as [String : Any]
        
        let mainDict = ["eventName": "APPROVEKYC", "encryption": false,"data":locationDict ] as [String : Any]
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
    
    @objc func HandleKYCApprove(notification : Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(KYCApproveModel.self, from: jsonData!)
            if (data.status ?? false) {
                self.showToast(message: data.message ?? "")
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            else {
                self.showToast(message: data.message ?? "")
            }
        }
        catch let error {
            //print(error)
        }
    }
    
    
    @objc func HandleKYCRejectSuccess(notification : Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        
        do {
            let data = try JSONDecoder().decode(DeleteAddModel.self, from: jsonData!)
            if (data.status ?? false) {
                self.showToast(message: data.message ?? "")
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            }
            else {
                self.showToast(message: data.message ?? "")
            }
        }
        catch let err {
            print(err)
        }
    }
    
    
    @objc func HandleImage(_ sender : UIButton) {
        let vc = ImageViewVC()
        vc.imageUrl = objKycDetails?.data?.uploadedDocuments?[sender.tag].base64Image ?? ""
        self.navigationController?.pushViewController(vc, animated: false)
    }
}

extension KYCDetailsVC : UICollectionViewDelegate, UICollectionViewDataSource {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objKycDetails?.data?.uploadedDocuments?.count ?? 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DoccumetsCell
        
        cell.btnImage.tag = indexPath.row
        cell.btnImage.addTarget(self, action: #selector(HandleImage(_:)), for: .touchUpInside)
        cell.setupData(data: (objKycDetails?.data?.uploadedDocuments?[indexPath.row]) ?? nil)
        return cell
    }
    
}

extension KYCDetailsVC : SignatureDelegate {
    func SignatureText(sign: String) {
        DispatchQueue.main.async {
            self.submitKYCDetails(sign: sign)
        }
    }
}
