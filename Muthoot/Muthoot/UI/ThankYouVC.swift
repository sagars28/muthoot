//
//  ThankYouVC.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/25/21.
//

import UIKit

public class ThankYouVC: BaseController {
    
    let scrollViewMain = UIScrollView()
    let viewBack = UIView()
    
    let lblAmount = UILabel()
    let lblLoanId = UILabel()
    let lblDateTime = UILabel()
    let lblAddress = UILabel()
    
    var objLoanSummary : ThankYouModel?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor =  .white
        setupUI()
        setupData()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoLoanSummary(notification:)), name: Notification.Name("MoveToLoanSummary"), object: nil)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate func setupUI() {
        
        let ivBackground = UIImageView()
        ivBackground.clipsToBounds = true
        ivBackground.contentMode = .scaleAspectFill
        if #available(iOS 13.0, *) {
            ivBackground.image = UIImage(named: "ThankYou", in: Bundle(for: type(of: self)), with: nil)
        } else {
            ivBackground.image = UIImage(named: "ThankYou", in: Bundle(for: type(of: self)), compatibleWith: nil)
        }
        view.addSubview(ivBackground)
        ivBackground.enableAutolayout()
        ivBackground.leadingMargin(0)
        ivBackground.trailingMargin(0)
        ivBackground.topMargin(0)
        ivBackground.bottomMargin(0)
        
        scrollViewMain.contentInsetAdjustmentBehavior = .never
        scrollViewMain.backgroundColor = .clear
        scrollViewMain.bounces = false
        scrollViewMain.isScrollEnabled = true
        scrollViewMain.showsVerticalScrollIndicator = true
        view.addSubview(scrollViewMain)
        scrollViewMain.enableAutolayout()
        scrollViewMain.leadingMargin(0)
        scrollViewMain.fixWidth(screenWidth)
        scrollViewMain.topMargin(Constraints.top)
        scrollViewMain.bottomMargin(Constraints.bottom)
        
        // Main View Under Scroll View
        
        viewBack.backgroundColor = .clear
        scrollViewMain.addSubview(viewBack)
        viewBack.enableAutolayout()
        viewBack.centerX()
        viewBack.fixWidth(screenWidth)
        viewBack.topMargin(0)
        viewBack.bottomMargin(0)
        viewBack.flexibleHeightGreater(10)
        
        let ivLogo = UIImageView()
        ivLogo.clipsToBounds = true
        ivLogo.contentMode = .scaleAspectFill
        if #available(iOS 13.0, *) {
            ivLogo.image = UIImage(named: "ThankyouLogo", in: Bundle(for: type(of: self)), with: nil)
        } else {
            ivLogo.image = UIImage(named: "ThankyouLogo", in: Bundle(for: type(of: self)), compatibleWith: nil)
        }
        viewBack.addSubview(ivLogo)
        ivLogo.enableAutolayout()
        ivLogo.centerX()
        ivLogo.fixHeight(170)
        ivLogo.topMargin(100)
        ivLogo.fixWidth(170)
        
        let lblThankYou = UILabel()
        lblThankYou.text = "THANK YOU !"
        lblThankYou.textAlignment = .center
        lblThankYou.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 6)
        lblThankYou.textColor = .white
        viewBack.addSubview(lblThankYou)
        lblThankYou.enableAutolayout()
        lblThankYou.belowView(20, to: ivLogo)
        lblThankYou.leadingMargin(16)
        lblThankYou.trailingMargin(16)
        lblThankYou.fixHeight(30)
        
        let lblDesc = UILabel()
        lblDesc.text = "for choosing door step gold loan"
        lblDesc.textAlignment = .center
        lblDesc.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 5)
        lblDesc.textColor = .white
        viewBack.addSubview(lblDesc)
        lblDesc.enableAutolayout()
        lblDesc.belowView(20, to: lblThankYou)
        lblDesc.leadingMargin(16)
        lblDesc.trailingMargin(16)
        lblDesc.fixHeight(30)
        
        let viewDetails = UIView()
        viewDetails.clipsToBounds = true
        viewDetails.backgroundColor = .white
        viewDetails.layer.cornerRadius = 8
        viewBack.addSubview(viewDetails)
        viewDetails.enableAutolayout()
        viewDetails.belowView(20, to: lblDesc)
        viewDetails.leadingMargin(20)
        viewDetails.trailingMargin(20)
        viewDetails.flexibleHeightGreater(20)
        
        let lblSummary = UILabel()
        lblSummary.text = "Loan Summary"
        lblSummary.textAlignment = .left
        lblSummary.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 2)
        lblSummary.textColor = Colors.gray
        viewDetails.addSubview(lblSummary)
        lblSummary.enableAutolayout()
        lblSummary.topMargin(20)
        lblSummary.leadingMargin(20)
        lblSummary.trailingMargin(20)
        lblSummary.fixHeight(30)
        
        let separator = UIView()
        separator.clipsToBounds = true
        separator.backgroundColor = Colors.gray
        viewDetails.addSubview(separator)
        separator.enableAutolayout()
        separator.belowView(16, to: lblSummary)
        separator.leadingMargin(0)
        separator.trailingMargin(0)
        separator.fixHeight(0.5)
        
        let width = (screenWidth - 80) / 2
        
        let lblTempLoan = UILabel()
        lblTempLoan.text = "Loan Summary"
        lblTempLoan.textAlignment = .left
        lblTempLoan.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblTempLoan.textColor = Colors.gray
        viewDetails.addSubview(lblTempLoan)
        lblTempLoan.enableAutolayout()
        lblTempLoan.belowView(20, to: separator)
        lblTempLoan.leadingMargin(20)
        lblTempLoan.fixWidth(width)
        lblTempLoan.fixHeight(25)
        
        lblLoanId.numberOfLines = 0
        lblLoanId.textAlignment = .right
        lblLoanId.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblLoanId.textColor = Colors.darkGray
        viewDetails.addSubview(lblLoanId)
        lblLoanId.enableAutolayout()
        lblLoanId.belowView(20, to: separator)
        lblLoanId.add(toRight: 8, of: lblTempLoan)
        lblLoanId.trailingMargin(16)
        lblLoanId.flexibleHeightGreater(25)
        
        let lblTempAmount = UILabel()
        lblTempAmount.text = "Loan Amount"
        lblTempAmount.textAlignment = .left
        lblTempAmount.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblTempAmount.textColor = Colors.gray
        viewDetails.addSubview(lblTempAmount)
        lblTempAmount.enableAutolayout()
        lblTempAmount.belowView(8, to: lblTempLoan)
        lblTempAmount.leadingMargin(20)
        lblTempAmount.fixWidth(width)
        lblTempAmount.fixHeight(25)
        
        
        lblAmount.text = ""
        lblAmount.textAlignment = .right
        lblAmount.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblAmount.textColor = Colors.darkGray
        viewDetails.addSubview(lblAmount)
        lblAmount.enableAutolayout()
        lblAmount.belowView(8, to: lblTempLoan)
        lblAmount.add(toRight: 8, of: lblTempAmount)
        lblAmount.trailingMargin(16)
        lblAmount.fixHeight(25)
        
        let lblTempDateTime = UILabel()
        lblTempDateTime.text = "Date & Time"
        lblTempDateTime.textAlignment = .left
        lblTempDateTime.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblTempDateTime.textColor = Colors.gray
        viewDetails.addSubview(lblTempDateTime)
        lblTempDateTime.enableAutolayout()
        lblTempDateTime.belowView(8, to: lblTempAmount)
        lblTempDateTime.leadingMargin(20)
        lblTempDateTime.fixWidth(width)
        lblTempDateTime.fixHeight(25)
        
        
        lblDateTime.text = ""
        lblDateTime.textAlignment = .right
        lblDateTime.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblDateTime.textColor = Colors.darkGray
        viewDetails.addSubview(lblDateTime)
        lblDateTime.enableAutolayout()
        lblDateTime.belowView(8, to: lblTempAmount)
        lblDateTime.add(toRight: 8, of: lblTempDateTime)
        lblDateTime.trailingMargin(16)
        lblDateTime.fixHeight(25)
        
        let lblTempAddress = UILabel()
        lblTempAddress.text = "Address"
        lblTempAddress.textAlignment = .left
        lblTempAddress.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblTempAddress.textColor = Colors.gray
        viewDetails.addSubview(lblTempAddress)
        lblTempAddress.enableAutolayout()
        lblTempAddress.belowView(8, to: lblTempDateTime)
        lblTempAddress.leadingMargin(20)
        lblTempAddress.fixWidth(70)
        lblTempAddress.fixHeight(25)
        
        lblAddress.text = ""
        lblAddress.numberOfLines = 0
        lblAddress.textAlignment = .right
        lblAddress.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblAddress.textColor = Colors.darkGray
        viewDetails.addSubview(lblAddress)
        lblAddress.enableAutolayout()
        lblAddress.belowView(12, to: lblTempDateTime)
        lblAddress.add(toRight: 12, of: lblTempAddress)
        lblAddress.trailingMargin(16)
        lblAddress.flexibleHeightGreater(25)
        lblAddress.bottomMargin(25)
        
        let btnDetails = UIButton()
        btnDetails.clipsToBounds = true
        btnDetails.setTitle("Request Details", for: .normal)
        btnDetails.setTitleColor(Colors.theme, for: .normal)
        btnDetails.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        btnDetails.layer.cornerRadius = 4
        btnDetails.backgroundColor = .white
        btnDetails.addTarget(self, action: #selector(handleDetails(_:)), for: .touchUpInside)
        viewBack.addSubview(btnDetails)
        btnDetails.enableAutolayout()
        btnDetails.belowView(30, to: viewDetails)
        btnDetails.centerX()
        btnDetails.fixWidth(150)
        btnDetails.fixHeight(40)
        btnDetails.bottomMargin(20)
    }
    
    func setupData () {
        lblLoanId.text = self.objLoanSummary?.data?.loanData?.lrid ?? ""
        lblAmount.text =  "₹" + (Int(self.objLoanSummary?.data?.loanData?.loanAmount ?? "") ?? 0).delimiter
        let date = (self.objLoanSummary?.data?.loanData?.selectedDate ?? "").convertToMonth(in: "dd/MM/yyyy", out: "dd MMM yyyy")
        lblDateTime.text = date + " " + (self.objLoanSummary?.data?.loanData?.selectedTime ?? "")
        
        let name = self.objLoanSummary?.data?.loanData?.name ?? ""
        let address = self.objLoanSummary?.data?.loanData?.address ?? ""
        let _ = self.objLoanSummary?.data?.loanData?.loanstate ?? ""
        let _ = self.objLoanSummary?.data?.loanData?.selectedPincode ?? ""
        
        lblAddress.text = name + ", " + address
    }
    
    
    @objc func gotoLoanSummary(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(LoanSummaryModel.self, from: jsonData!)
            if (data.status ?? false) {
                let vc = LoanSummaryVC()
                vc.objLoanSummary = data
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                self.showToast(message: data.message ?? "")
            }
        }
        catch let error {
            // print(error)
        }
    }
    
    
    @objc func handleDetails(_ sender : UIButton) {
        
        //  Loader.shared.StartActivityIndicator(obj: self)
        
        
        //        let locationDict = ["mobileNo": objLoanSummary?.data?.loanData?.mobileNo ?? 0 ,"latitude": CommonModel.shared.lat, "longitude": CommonModel.shared.long] as [String : Any]
        //
        //        let mainDict = ["eventName": "startProcess", "encryption": false,"data":locationDict ] as [String : Any]
        //        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
        
        if ((objLoanSummary?.data?.loanData?.mobileNo ?? 0) != 0) {
            let vc = LoanSummaryVC()
            CommonModel.shared.mobileNumber = "\(objLoanSummary?.data?.loanData?.mobileNo ?? 0)"
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func backBtnTapped(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func nextBtnTapped(_ sender : UIButton) {
        
    }
}
