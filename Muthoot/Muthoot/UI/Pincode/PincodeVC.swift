//
//  PincodeVC.swift
//  Muthoot Track
//
//  Created by SmartConnect Technologies on 01/03/21.
//

import UIKit
import CoreLocation

public class PincodeVC: BaseController {
    
    var locationManager: CLLocationManager!
    
    let btnPincode = UITextField()
    var objPerson : PincodeModel?
    
    let lblService = UILabel()
    
    var pincode = ""
    public var mobileNo = ""
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        getLocation()
        SocketHelper.shared.closeConnection()
        VersionControlSocketHelper.shared.establishConnection()
        setupUI()
        CommonModel.shared.mobileNumber = mobileNo
        Loader.shared.StartActivityIndicator(obj: self)
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .lightContent
        } else {
            return .lightContent
        }
    }
    
    
    public override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        // For Version Control
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleConfig(notification:)), name: Notification.Name("Config"), object: nil)
        
        // For Main Socket
        // Get new url of this socket in version control
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleConfigMainSocket(notification:)), name: Notification.Name("ConfigMainSocket"), object: nil)
        
        // Others Main Starting Event
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoStartLoan(notification:)), name: Notification.Name("StartProcess"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoStartLoan(notification:)), name: Notification.Name("GetPincode"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.pincodeSearchResponse(notification:)), name: Notification.Name("PincodeSearchResponse"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("SendPinCodeResponse"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoLoanSummary(notification:)), name: Notification.Name("MoveToLoanSummary"), object: nil)
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate func getLocation() {
        
        locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    fileprivate func setupUI() {
        
        let headerView = TopPgrogess()
        headerView.backgroundColor = .white
        headerView.setView()
        headerView.ivFirst.backgroundColor = Colors.theme
        headerView.btnTwo.addTarget(self, action: #selector(handleNext(_:)), for: .touchUpInside)
        headerView.btnThree.addTarget(self, action: #selector(handleNext(_:)), for: .touchUpInside)
        headerView.btnFour.addTarget(self, action: #selector(handleNext(_:)), for: .touchUpInside)
        
        view.addSubview(headerView)
        headerView.enableAutolayout()
        headerView.fixHeight(80)
        headerView.leadingMargin(0)
        headerView.trailingMargin(0)
        headerView.topMargin(self.topbarHeight)
        // headerView.topMargin(Constraints.top + self.topbarHeight)
        
        lblService.textAlignment = .center
        view.addSubview(lblService)
        lblService.enableAutolayout()
        lblService.belowView(20, to: headerView)
        lblService.leadingMargin(0)
        lblService.trailingMargin(0)
        lblService.fixHeight(40)
        
        let lblPincode = UILabel()
        lblPincode.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title + 1)
        lblPincode.text = "Select Pincode"
        lblPincode.textColor = Colors.textGray
        view.addSubview(lblPincode)
        lblPincode.enableAutolayout()
        lblPincode.belowView(75, to: lblService)
        lblPincode.leadingMargin(30)
        lblPincode.trailingMargin(20)
        
        btnPincode.setLeftPaddingPoints(16)
        btnPincode.setRightPaddingPoints(16)
        btnPincode.backgroundColor = .white
        btnPincode.placeholder = "Enter Pincode"
        btnPincode.contentHorizontalAlignment = .left
        btnPincode.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title + 2)
        btnPincode.textColor = UIColor.black
        btnPincode.keyboardType = .numberPad
        btnPincode.layer.cornerRadius = 5
        btnPincode.layer.borderWidth = 1
        btnPincode.layer.borderColor = Colors.textGray.cgColor
        btnPincode.addTarget(self,action:#selector(textDidChange(textField:)),
                             for:.editingChanged)
        view.addSubview(btnPincode)
        btnPincode.enableAutolayout()
        btnPincode.belowView(16, to: lblPincode)
        btnPincode.fixHeight(50)
        btnPincode.leadingMargin(28)
        btnPincode.trailingMargin(28)
        
       /* let downArrow = UIImageView()
        downArrow.clipsToBounds = true
        downArrow.contentMode = .scaleAspectFit
        downArrow.image = UIImage(named: "shape", in: Bundle(for: type(of: self)), compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
        
        downArrow.tintColor = .gray
        btnPincode.addSubview(downArrow)
        downArrow.enableAutolayout()
        downArrow.fixHeight(16)
        downArrow.fixWidth(16)
        downArrow.trailingMargin(16)
        downArrow.centerY()*/
        
        let bottomView = UIView()
        bottomView.clipsToBounds = true
        bottomView.backgroundColor = Colors.theme
        view.addSubview(bottomView)
        bottomView.enableAutolayout()
        bottomView.fixHeight(50)
        bottomView.leadingMargin(0)
        bottomView.trailingMargin(0)
        bottomView.bottomMargin(Constraints.bottom)
        
        let btnNext = UIButton()
        btnNext.contentHorizontalAlignment = .right
        btnNext.semanticContentAttribute = .forceRightToLeft
        btnNext.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 2)
        btnNext.setImage(UIImage(named: "arrowRight", in: Bundle(for: type(of: self
        )), compatibleWith: nil), for: .normal)
        
        btnNext.setTitle("NEXT", for: .normal)
        btnNext.setTitleColor(.white, for: .normal)
        btnNext.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 14)
        btnNext.addTarget(self,action:#selector(handleNext(_:)),for:.touchUpInside)
        bottomView.addSubview(btnNext)
        btnNext.enableAutolayout()
        btnNext.centerY()
        btnNext.fixHeight(50)
        btnNext.trailingMargin(20)
        btnNext.fixWidth(120)
    }
    
    //MARK:- Handle Button Action
    
    @objc func handleNext(_ sender : UIButton) {
        if (self.pincode == "") {
            self.showToast(message:"SELECT PINCODE")
        }
        else {
            Loader.shared.StartActivityIndicator(obj: self)
            let locationDict = ["enquiryId": self.objPerson?.data?.enquiryID ?? "","pinCode": self.pincode]
            let mainDict = ["eventName": "sendPinCode", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
        }
    }
    
    @objc final private func textDidChange(textField: UITextField) {
        if ((textField.text?.count ?? 0) > 1) {
            let locationDict = ["pinCode": (self.btnPincode.text ?? "")]
            let mainDict = ["eventName": "pincodeSearch", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
        }
    }
    
    func showPincode(arr : [String]) {
        
        for v in (self.view.window!.subviews) {
            if v.isKind(of: SliderPincode.self) {
                v.removeFromSuperview()
            }
        }
        let popup = SliderPincode()
         popup.delegate = self
         popup.frame = view.bounds
         guard let frame = btnPincode.viewFrame else {
             return
         }
         popup.createUI(title: "SELECT PINCODE", arrOption: arr, y: (frame.origin.y + 50.0))
         view.window?.addSubview(popup)
    }
    
    @objc func handlePincode(_ sender : UIButton) {
        if (self.objPerson != nil) {
            let popup = SliderPopup()
            popup.delegate = self
            popup.frame = view.bounds
            let arr = self.objPerson?.data?.pincodes ?? []
            popup.createUI(title: "SELECT PINCODE", arrOption: arr)
            view.window?.addSubview(popup)
        }
        else {
            Loader.shared.StartActivityIndicator(obj: self)
            let locationDict = ["mobileNo": self.mobileNo ,"latitude": CommonModel.shared.lat, "longitude": CommonModel.shared.long] as [String : Any]
            
            let mainDict = ["eventName": "startProcess", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
        }
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(GoldRateModel.self, from: jsonData!)
            DispatchQueue.main.async {
                let vc = CalculateLoanVC()
                vc.objGoldRate = data
                vc.enquiryId = self.objPerson?.data?.enquiryID ?? ""
                self.navigationController?.pushViewController(vc, animated: false)
            }
        }
        catch let error {
            //print(error)
        }
    }
    
    //MARK:- Handle Events Response
    @objc func gotoStartLoan(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(PincodeModel.self, from: jsonData!)
            if (data.status ?? false) {
                self.objPerson = data
                let text = data.data?.header ?? ""
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineHeightMultiple = 1
                lblService.attributedText = NSMutableAttributedString(string: text, attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle, .font : UIFont(name: Fonts.latoRegular, size: TextSize.title + 6)!, .foregroundColor : UIColor.black])
                lblService.textAlignment  = .center
            }
            else {
                self.showToast(message: data.message ?? "")
            }
        }
        catch let _ {
           // print(error)
        }
    }
    
    @objc func pincodeSearchResponse(notification: Notification) {
        if let dict = notification.userInfo as? [String:Any] {
            if let data = dict["data"] as? [String:Any] {
                if let arrPincode = data["pincodes"] as? [String] {
                    showPincode(arr: arrPincode)
                }
            }
        }
    }
    
    
    @objc func gotoLoanSummary(notification: Notification) {
        
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(LoanSummaryModel.self, from: jsonData!)
            if (data.status ?? false) {
                let vc = LoanSummaryVC()
                vc.objLoanSummary = data
                DispatchQueue.main.async {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            else {
                self.showToast(message: data.message ?? "")
            }
        }
        catch let error {
           // print(error)
        }
    }
}

extension PincodeVC : SelectionDelegate {
    func SelectedText(tag: Int, text: String, index: Int) {
        self.pincode = text
        btnPincode.text = text
        self.view.endEditing(true)
    }
}


extension PincodeVC : CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let location = locations.last else {//print("Location Not Found")
            return
        }
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        // let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        CommonModel.shared.lat = center.latitude
        CommonModel.shared.long = center.longitude
    }
}


//MARK:- API Calling OR Socket Events
extension PincodeVC {
    
    @objc func handleConfig(notification: Notification) {
        VersionControlSocketHelper.shared.firstEmits()
    }
    
    @objc func handleConfigMainSocket(notification: Notification) {
        let locationDict = ["mobileNo": self.mobileNo ,"latitude": CommonModel.shared.lat, "longitude": CommonModel.shared.long] as [String : Any]
        
        let mainDict = ["eventName": "startProcess", "encryption": false,"data":locationDict ] as [String : Any]
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
}
