//
//  OrnamentCell.swift
//  Muthoot Track
//
//  Created by Sagar on 22/06/21.
//

import UIKit

class OrnamentCell: UITableViewCell {
    
    let lblItem = PaddingLabel()
    let lblKarat = PaddingLabel()
    let lblNet = PaddingLabel()
    let lblGrs = PaddingLabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupCell() {
        self.backgroundColor = .clear
        
        let backView = UIView()
        backView.clipsToBounds = true
        backView.backgroundColor = UIColor.white
        self.contentView.addSubview(backView)
        backView.enableAutolayout()
        backView.trailingMargin(0)
        backView.topMargin(0)
        backView.bottomMargin(8)
        backView.leadingMargin(0)
        backView.flexibleHeightGreater(10)
        
        let width = (screenWidth - 40) / 4
        
        
        lblItem.text = " "
        lblItem.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle + 1)
        lblItem.textColor = .black
        lblItem.textAlignment = .center
        backView.addSubview(lblItem)
        lblItem.enableAutolayout()
        lblItem.topMargin(0)
        lblItem.leadingMargin(0)
        lblItem.fixWidth(width - 10)
        lblItem.fixHeight(40)
        
        lblKarat.text = " "
        lblKarat.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle + 1)
        lblKarat.textColor = .black
        lblKarat.textAlignment = .center
        backView.addSubview(lblKarat)
        lblKarat.enableAutolayout()
        lblKarat.topMargin(0)
        lblKarat.add(toRight: 0, of: lblItem)
        lblKarat.fixWidth(width - 10)
        lblKarat.fixHeight(40)
        
        
        lblNet.text = " "
        lblNet.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle + 1)
        lblNet.textColor = .black
        lblNet.textAlignment = .center
        backView.addSubview(lblNet)
        lblNet.enableAutolayout()
        lblNet.topMargin(0)
        lblNet.add(toRight: 0, of: lblKarat)
        lblNet.fixWidth(width)
        lblNet.fixHeight(40)
        
        lblGrs.text = " "
        lblGrs.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle + 1)
        lblGrs.textColor = .black
        lblGrs.textAlignment = .center
        backView.addSubview(lblGrs)
        lblGrs.enableAutolayout()
        lblGrs.topMargin(0)
        lblGrs.add(toRight: 0, of: lblNet)
        lblGrs.trailingMargin(0)
        lblGrs.fixHeight(40)
        lblGrs.bottomMargin(0)
    }

}
