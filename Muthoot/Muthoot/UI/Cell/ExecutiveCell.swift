//
//  ExecutiveCell.swift
//  Muthoot Track
//
//  Created by Sagar on 19/07/21.
//

import UIKit

class ExecutiveCell:  UITableViewCell {
    
    let separator1 = UIView()
    let lblOccupation = UILabel()
    let lblUsername = UILabel()
    let ivLogo = UIImageView()
    let btnCall = UIButton()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupCell() {
        self.backgroundColor = .clear
        
        let backView = UIView()
        backView.clipsToBounds = true
        backView.backgroundColor = UIColor.white
        self.contentView.addSubview(backView)
        backView.enableAutolayout()
        backView.trailingMargin(0)
        backView.topMargin(8)
        backView.bottomMargin(0)
        backView.leadingMargin(0)
        
        
        ivLogo.clipsToBounds = true
        ivLogo.contentMode = .scaleAspectFill
        //ivLogo.backgroundColor = .gray
        //ivLogo.image = UIImage(named: "ThankyouLogo", in: Bundle(for: type(of: self)), with: nil)
        ivLogo.layer.cornerRadius = 5
        backView.addSubview(ivLogo)
        ivLogo.enableAutolayout()
        ivLogo.topMargin(8)
        ivLogo.fixHeight(60)
        ivLogo.leadingMargin(16)
        ivLogo.fixWidth(60)
        ivLogo.bottomMargin(16)
    
        lblUsername.text = " "
        lblUsername.textColor = UIColor.black.withAlphaComponent(0.8)
        lblUsername.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 3)
        backView.addSubview(lblUsername)
        lblUsername.enableAutolayout()
        lblUsername.topMargin(12)
        lblUsername.add(toRight: 16, of: ivLogo)
        lblUsername.trailingMargin(70)
        
       
        lblOccupation.text = " "
        lblOccupation.textColor = Colors.textGray
        lblOccupation.font = UIFont(name: Fonts.latoRegular, size: TextSize.title - 1)
        backView.addSubview(lblOccupation)
        lblOccupation.enableAutolayout()
        lblOccupation.belowView(4, to: lblUsername)
        lblOccupation.add(toRight: 16, of: ivLogo)
        lblOccupation.trailingMargin(70)
        
        
       /* separator1.clipsToBounds = true
        separator1.backgroundColor = Colors.gray
        backView.addSubview(separator1)
        separator1.enableAutolayout()
        separator1.bottomMargin(0)
        separator1.leadingMargin(8)
        separator1.trailingMargin(8)
        separator1.fixHeight(0.5)*/
    }
    
    func setupData(data : Staff) {

        lblUsername.text = data.name ?? ""
        
        if (data.designation ?? "" == "bm") {
            lblOccupation.text = "Branch Manager" + " | " + (data.phoneNo ?? "")
        }
        else if (data.designation ?? "" == "rider") {
            lblOccupation.text = "Rider" + " | " + (data.phoneNo ?? "")
        }
        else {
            lblOccupation.text = data.designation ?? ""
        }
        
        let str = (data.profilephoto ?? "") .replacingOccurrences(of: "data:image/png;base64,", with: "",options: .caseInsensitive)
        
        let newImageData = Data(base64Encoded: str)
        if let img = newImageData {
            self.ivLogo.image = UIImage(data: img)
        }
    }
}
