//
//  DayCell.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/25/21.
//

import UIKit

class DayCell: UICollectionViewCell {
    
    let lblDate = UILabel()
    let lblDay = UILabel()
    let viewBack = UIView()
    let separator = UIView()
    
    var objSlot : AvailableSlotModel?
    var objSlot2 : RescheduleModel? // Coming from Reschedule Controller
    var objSlot3 : RepledgeModel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                lblDate.textColor = .white
                lblDay.textColor = .white
                viewBack.backgroundColor = Colors.theme
                separator.backgroundColor = .white
            }
            else {
                lblDate.textColor = .black
                lblDay.textColor = .black
                viewBack.backgroundColor = .white
                separator.backgroundColor = Colors.gray
            }
        }
    }
    
    
    fileprivate func setupCell() {
        
        viewBack.backgroundColor = .white
        viewBack.layer.cornerRadius = 6
        viewBack.layer.shadowOffset = CGSize(width: 2, height: 4)
        viewBack.layer.shadowColor = UIColor.lightGray.cgColor
        viewBack.layer.shadowOpacity = 0.8
        viewBack.layer.shadowRadius = 4
        viewBack.layer.masksToBounds = false
        clipsToBounds = false
        self.contentView.addSubview(viewBack)
        viewBack.enableAutolayout()
        viewBack.leadingMargin(0)
        viewBack.topMargin(0)
        viewBack.trailingMargin(0)
        viewBack.bottomMargin(0)
        
        
        lblDay.textAlignment = .center
        lblDay.text = "Mon"
        lblDay.textColor = .black
        lblDay.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 4)
        viewBack.addSubview(lblDay)
        lblDay.enableAutolayout()
        lblDay.leadingMargin(2)
        lblDay.topMargin(4)
        lblDay.trailingMargin(2)
        lblDay.fixHeight(30)
        
        
        separator.backgroundColor = Colors.gray
        separator.clipsToBounds = false
        viewBack.addSubview(separator)
        separator.enableAutolayout()
        separator.leadingMargin(0)
        separator.belowView(2, to: lblDay)
        separator.trailingMargin(0)
        separator.fixHeight(0.8)
        
        
        lblDate.textAlignment = .center
        lblDate.text = "25"
        lblDate.textColor = .black
        lblDate.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 7)
        viewBack.addSubview(lblDate)
        lblDate.enableAutolayout()
        lblDate.leadingMargin(0)
        lblDate.belowView(4, to: separator)
        lblDate.trailingMargin(0)
        lblDate.bottomMargin(0)
    }
    
    func setupData(index : Int) {
        lblDay.text = self.objSlot?.data?.availableDates?[index].convertToMonth(in: "dd/MM/yyyy", out: "EEE")
        lblDate.text = self.objSlot?.data?.availableDates?[index].convertToMonth(in: "dd/MM/yyyy", out: "dd")
    }
    
    func setupData2(index : Int) { // Coming from Resc
        lblDay.text = self.objSlot2?.data?.availabledates?[index].convertToMonth(in: "dd/MM/yyyy", out: "EEE")
        lblDate.text = self.objSlot2?.data?.availabledates?[index].convertToMonth(in: "dd/MM/yyyy", out: "dd")
    }
    
    func setupData3(index : Int) { // Coming from Resc
        lblDay.text = self.objSlot3?.data?.availableDates?[index].convertToMonth(in: "dd/MM/yyyy", out: "EEE")
        lblDate.text = self.objSlot3?.data?.availableDates?[index].convertToMonth(in: "dd/MM/yyyy", out: "dd")
    }
}
