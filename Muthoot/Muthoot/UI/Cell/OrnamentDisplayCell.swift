//
//  OrnamentDisplayCell.swift
//  Muthoot Track
//
//  Created by Sagar on 02/07/21.
//

import UIKit

class OrnamentDisplayCell: UITableViewCell {

    let ivLogo = UIImageView()
    let lblTitle = UILabel()
    let lblSubTitle = UILabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupCell() {
        self.backgroundColor = .clear
        
        let backView = UIView()
        backView.clipsToBounds = true
        backView.backgroundColor = UIColor.white
        self.contentView.addSubview(backView)
        backView.enableAutolayout()
        backView.trailingMargin(0)
        backView.topMargin(0)
        backView.bottomMargin(0)
        backView.leadingMargin(0)
        backView.flexibleHeightGreater(10)
        
        ivLogo.clipsToBounds = true
        ivLogo.contentMode = .scaleAspectFill
        ivLogo.backgroundColor = .gray
        ivLogo.layer.cornerRadius = 12
        backView.addSubview(ivLogo)
        ivLogo.enableAutolayout()
        ivLogo.topMargin(16)
        ivLogo.fixHeight(80)
        ivLogo.leadingMargin(20)
        ivLogo.fixWidth(80)
        ivLogo.bottomMargin(16)
        
        lblTitle.text = "Sagar"
        lblTitle.textAlignment = .left
        lblTitle.font = UIFont(name: Fonts.latoRegular, size: TextSize.title + 3)
        lblTitle.textColor = .black
        backView.addSubview(lblTitle)
        lblTitle.enableAutolayout()
        lblTitle.topMargin(12)
        lblTitle.add(toRight: 20, of: ivLogo)
        lblTitle.fixHeight(22)
        lblTitle.trailingMargin(20)
        
        lblSubTitle.numberOfLines = 0
        lblSubTitle.text = "Carot : 20 |"
        lblSubTitle.textAlignment = .left
        lblSubTitle.font = UIFont(name: Fonts.latoRegular, size: TextSize.title - 1)
        lblSubTitle.textColor = Colors.gray
        backView.addSubview(lblSubTitle)
        lblSubTitle.enableAutolayout()
        lblSubTitle.belowView(0, to: lblTitle)
        lblSubTitle.add(toRight: 20, of: ivLogo)
        lblSubTitle.flexibleHeightGreater(20)
        lblSubTitle.trailingMargin(20)
    }
}
