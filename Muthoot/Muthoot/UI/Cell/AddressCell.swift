//
//  AddressCellTableViewCell.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/20/21.
//

import UIKit

class AddressCell: UITableViewCell {
    
    let btnDelete = UIButton()
    let lblUsername = UILabel()
    let lblAddress = UILabel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupCell() {
        self.backgroundColor = .clear
        
        let backView = UIView()
        backView.clipsToBounds = true
        backView.backgroundColor = UIColor.white
        backView.layer.cornerRadius = 8
        backView.layer.borderWidth = 0.6
        backView.layer.borderColor = Colors.gray.cgColor
        self.contentView.addSubview(backView)
        backView.enableAutolayout()
        backView.trailingMargin(20)
        backView.topMargin(8)
       // backView.fixHeight(100)
        backView.bottomMargin(8)
        backView.leadingMargin(20)
        
        let ivUser = UIImageView()
        ivUser.clipsToBounds = true
        ivUser.contentMode = .scaleAspectFit
        if #available(iOS 13.0, *) {
            ivUser.image = UIImage(named: "profile", in: Bundle(for: type(of: self)), with: nil)?.withRenderingMode(.alwaysTemplate)
        } else {
            ivUser.image = UIImage(named: "profile", in: Bundle(for: type(of: self)), compatibleWith: nil)?.withRenderingMode(.alwaysTemplate)
        }
        ivUser.tintColor = Colors.gray
        backView.addSubview(ivUser)
        ivUser.enableAutolayout()
        ivUser.leadingMargin(20)
        ivUser.topMargin(20)
        ivUser.fixHeight(20)
        ivUser.fixWidth(20)
        
        btnDelete.clipsToBounds = true
        btnDelete.setImage(UIImage(named: "delete", in: Bundle(for: type(of: self
        )), compatibleWith: nil), for: .normal)
        backView.addSubview(btnDelete)
        btnDelete.enableAutolayout()
        btnDelete.trailingMargin(0)
        btnDelete.topMargin(0)
        btnDelete.fixHeight(50)
        btnDelete.fixWidth(50)
        
        lblUsername.text = " "
        lblUsername.textColor = Colors.darkGray
        lblUsername.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 1)
        backView.addSubview(lblUsername)
        lblUsername.enableAutolayout()
        lblUsername.centerY(to: ivUser)
        lblUsername.add(toRight: 25, of: ivUser)
        lblUsername.add(toLeft: 8, of: btnDelete)
        lblUsername.fixHeight(30)
        
        let ivAddress = UIImageView()
        ivAddress.clipsToBounds = true
        ivAddress.contentMode = .scaleAspectFit
        if #available(iOS 13.0, *) {
            ivAddress.image = UIImage(named: "home", in: Bundle(for: type(of: self)), with: nil)
        } else {
            ivAddress.image = UIImage(named: "home", in: Bundle(for: type(of: self)), compatibleWith: nil)
        }
        backView.addSubview(ivAddress)
        ivAddress.enableAutolayout()
        ivAddress.leadingMargin(20)
        ivAddress.belowView(16, to: ivUser)
        ivAddress.fixHeight(20)
        ivAddress.fixWidth(20)
        
        lblAddress.numberOfLines = 0
        lblAddress.text = " "
        lblAddress.textColor = Colors.darkGray
        lblAddress.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 1)
        backView.addSubview(lblAddress)
        lblAddress.enableAutolayout()
        lblAddress.belowView(16, to: ivUser)
        lblAddress.add(toRight: 25, of: ivUser)
        lblAddress.trailingMargin(20)
        lblAddress.flexibleHeightGreater(0)
        lblAddress.bottomMargin(20)
        
    }
}
