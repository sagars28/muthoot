//
//  TimeCell.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/25/21.
//

import UIKit

class TimeCell: UICollectionViewCell {
    
    let lblTime = PaddingLabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupCell()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                lblTime.textColor = .white
                lblTime.backgroundColor = Colors.theme
                lblTime.layer.cornerRadius = 4
                lblTime.layer.borderWidth = 1
                lblTime.layer.borderColor = Colors.theme.cgColor
            }
            else {
                lblTime.textColor = .black
                lblTime.backgroundColor = .white
                lblTime.layer.cornerRadius = 4
                lblTime.layer.borderWidth = 1
                lblTime.layer.borderColor = Colors.theme.cgColor
            }
        }
    }
    
    fileprivate func setupCell() {
        
        lblTime.adjustsFontSizeToFitWidth = true
        lblTime.textAlignment = .center
        lblTime.text = ""
        lblTime.textColor = .black
        lblTime.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title + 1)
        lblTime.layer.cornerRadius = 4
        lblTime.layer.borderWidth = 1
        lblTime.backgroundColor = .white
        lblTime.layer.borderColor = Colors.theme.cgColor
        self.contentView.addSubview(lblTime)
        lblTime.enableAutolayout()
        lblTime.leadingMargin(2)
        lblTime.topMargin(2)
        lblTime.trailingMargin(2)
        lblTime.bottomMargin(2)
    
    }
}
