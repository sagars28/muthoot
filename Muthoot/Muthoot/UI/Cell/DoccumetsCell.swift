//
//  DoccumetsCell.swift
//  Muthoot Track
//
//  Created by Sagar on 21/06/21.
//

import UIKit

class DoccumetsCell: UICollectionViewCell {
    
    let ivDoccument = UIImageView()
    let lblIDType = UILabel()
    let lblId = UILabel()
    let lblExpiryDate = UILabel()
    let btnImage = UIButton()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI() {
        
        let viewBack = UIView()
        viewBack.backgroundColor = .white
        viewBack.layer.cornerRadius = 6
        viewBack.layer.masksToBounds = false
        viewBack.clipsToBounds = true
        self.contentView.addSubview(viewBack)
        viewBack.enableAutolayout()
        viewBack.leadingMargin(0)
        viewBack.topMargin(0)
        viewBack.trailingMargin(0)
        viewBack.bottomMargin(0)
        
        ivDoccument.isUserInteractionEnabled = false
        ivDoccument.clipsToBounds = true
        ivDoccument.contentMode = .scaleAspectFill
        ivDoccument.backgroundColor = .gray
        ivDoccument.layer.cornerRadius = 8
        viewBack.addSubview(ivDoccument)
        ivDoccument.enableAutolayout()
        ivDoccument.topMargin(8)
        ivDoccument.leadingMargin(0)
        ivDoccument.trailingMargin(0)
        
        btnImage.isUserInteractionEnabled = true
        btnImage.clipsToBounds = true
        btnImage.backgroundColor = .clear
        viewBack.addSubview(btnImage)
        btnImage.enableAutolayout()
        btnImage.topMargin(0)
        btnImage.leadingMargin(0)
        btnImage.trailingMargin(0)
        btnImage.bottomMargin(100)
   

        
        lblIDType.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle)
        lblIDType.numberOfLines = 0
        lblIDType.textAlignment = .left
        lblIDType.textColor = Colors.textGray
        lblIDType.text = ""
        viewBack.addSubview(lblIDType)
        lblIDType.enableAutolayout()
        lblIDType.belowView(8, to: ivDoccument)
        lblIDType.leadingMargin(0)
        lblIDType.trailingMargin(0)
        lblIDType.flexibleHeightGreater(25)
        
        lblId.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle)
        lblId.numberOfLines = 0
        lblId.textAlignment = .left
        lblId.textColor = .black//Colors.gray
        lblId.text = " "
        viewBack.addSubview(lblId)
        lblId.enableAutolayout()
        lblId.belowView(0, to: lblIDType)
        lblId.leadingMargin(0)
        lblId.trailingMargin(0)
        lblId.fixHeight(20)
        
        lblExpiryDate.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle)
        lblExpiryDate.numberOfLines = 0
        lblExpiryDate.textAlignment = .left
        lblExpiryDate.textColor = .black//Colors.gray
        lblExpiryDate.text = " "
        viewBack.addSubview(lblExpiryDate)
        lblExpiryDate.enableAutolayout()
        lblExpiryDate.belowView(2, to: lblId)
        lblExpiryDate.leadingMargin(0)
        lblExpiryDate.trailingMargin(0)
        lblExpiryDate.fixHeight(20)
        lblExpiryDate.bottomMargin(0)
    }
    
    func setupData(data : UploadedDocument?) {
        
        if (data?.idNo ?? "" != "0") {
            lblId.text = "ID : \(data?.idNo ?? "")"
        }
        lblIDType.text = data?.documenttypeid ?? ""
        if ((data?.expiryDate ?? "") != "") {
            lblExpiryDate.text = "ExpiryDate : \(data?.expiryDate ?? "")"
        }
        
        let image = data?.base64Image ?? ""
        let str = image.replacingOccurrences(of: "data:image/png;base64,", with: "",options: .caseInsensitive)
        
        let newImageData = Data(base64Encoded: str)
        if let img = newImageData {
            self.ivDoccument.image = UIImage(data: img)
        }
    }
}
