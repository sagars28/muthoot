//
//  LoanSummaryVC.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/26/21.
//

import UIKit

public class LoanSummaryVC: BaseController {
    
    //Cancel Button
    let btnCancel = UIButton()
    let btnLoan = UIButton() // OR REPLEDGE
    
    
    let stackView   = UIStackView()
    let summaryDetailsView = UIView()
    let executiveDetailsView = UIView()
    let ornamentView = UIView()
    let loanDetailsView = UIView()
    let bankDetailsView = UIView()
    let finalDisbursmentView = UIView()
    let ownershipAgreementView = UIView()
    let pledgeFormView = UIView()
    let releaseAgreementView = UIView()
    
    let approveLoanView = UIView()
    let kycDetailsView = UIView()
    let nomineeDetailsView = UIView()
    
    
    let lblDateTime = UILabel()
    let lblAmount = UILabel()
    let lblLoanId = UILabel()
    let lblAddress = UILabel()
    
    let tblView = UITableView()
    var tblViewHeightConstraint : NSLayoutConstraint?
    
    let ornamentTblView = UITableView()
    var ornamentTblViewHeightConstraint : NSLayoutConstraint?
    
    let lblCustomerLoanNumber = UILabel()
    let lblProduct = UILabel()
    let lblProductCat = UILabel()
    let lblSchemeName = UILabel()
    let lblFinalLoanAmount = UILabel()
    let lblPaymnetFrequency = UILabel()
    let lblTenure = UILabel()
    let lblInterestRate = UILabel()
    let lblMonthlyPayment = UILabel()
    
    //Start Loan Process
    let btnRejectLoanProcess = UIButton()
    let btnApproveLoanProcess = UIButton()
    
    
    //Bank Details
    let lblBeneficiary = UILabel()
    let lblBankName = UILabel()
    let lblIFSCCode = UILabel()
    let lblAccountNumber = UILabel()
    let lblBranchName = UILabel()
    let lblTempBranchName = UILabel()
    
    //Nominee Details
    
    let lblNomineeName = UILabel()
    let lblNomineeRelation = UILabel()
    let lblNomineeDOB = UILabel()
    let lblNomineeAddress = UILabel()
    
    
    //Final Loan Diserment
    let lblLoanAmount = UILabel()
    let lblFinalInterestRate = UILabel()
    let lblEMI = UILabel()
    let lblTotalInterestAmount = UILabel()
    let lblTotalPayableAmount = UILabel()
    
    
    //Confirmation TExt
    let lblStartProcess = UILabel()
    let lblKyc = UILabel()
    let btnKycDetails = UIButton()
    
    //Refresh Control
    var refreshControl:UIRefreshControl!
    
    var objLoanSummary : LoanSummaryModel?
    var objKycApproval : KycApprovalModel?
    var objLoanSummary2 : RepledgeSummaryModel?
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = Colors.viewBackground
        setupUI()
        setupData()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        createNavigationBarWithBack("Loan Summary")
        btnNavBack.removeTarget(self, action: #selector(handleBack(_:)), for: .allEvents)
        btnNavBack.addTarget(self, action: #selector(backBtnTapped(_:)), for: .touchUpInside)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoStartLoan(notification:)), name: Notification.Name("StartProcess"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoLoanSummary(notification:)), name: Notification.Name("MoveToLoanSummary"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoStartLoan(notification:)), name: Notification.Name("GetPincode"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoReschedule(notification:)), name: Notification.Name("SuccessStartLoanCancel"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getKYCApprove(notification:)), name: Notification.Name("SuccessGetKycForApproval"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleCONFIRMSTARTPROCESS(notification:)), name: Notification.Name("SuccessCONFIRMSTARTPROCESS"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleCONFIRMLOANDETAILSCUSTOMER(notification:)), name: Notification.Name("SuccessCONFIRMLOANDETAILSCUSTOMER"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.HandleSTARTAGREEMENTCREATION(notification:)), name: Notification.Name("SuccessStartAgreementCreation"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.HandleSTARTAGREEMENT(notification:)), name: Notification.Name("SuccessGETSIGNEDAGREEMENT"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.HandleGetPledgeForm(notification:)), name: Notification.Name("SuccessStartPledgeForm"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.CONFIRMGOLDHANDOVER(notification:)), name: Notification.Name("CONFIRMGOLDHANDOVER"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.startHandOver(notification:)), name: Notification.Name("SuccessSTARTGOLDHANDOVER"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleCONFIRMLOANDETAILSCUSTOMER(notification:)), name: Notification.Name("SucessConfirmedRepledgeProcess"), object: nil)
        
        setupCancelButton()
        refresh(sender: self.refreshControl)
        //fetchKYCDetials()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    public override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    public override func viewDidLayoutSubviews() {
        self.tblView.layoutIfNeeded()
        self.view.layoutIfNeeded()
        self.tblViewHeightConstraint?.constant = self.tblView.contentSize.height
        self.ornamentTblViewHeightConstraint?.constant = self.ornamentTblView.contentSize.height
    }
    
    fileprivate func setupUI() {
        
        //MARK:- ScrollView
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        
        
        let scrollViewMain = UIScrollView()
        scrollViewMain.contentInsetAdjustmentBehavior = .never
        scrollViewMain.backgroundColor = .clear
        scrollViewMain.bounces = true
        scrollViewMain.isScrollEnabled = true
        scrollViewMain.showsVerticalScrollIndicator = true
        scrollViewMain.refreshControl = refreshControl
        view.addSubview(scrollViewMain)
        scrollViewMain.enableAutolayout()
        scrollViewMain.leadingMargin(0)
        scrollViewMain.fixWidth(screenWidth)
        scrollViewMain.topMargin(Constraints.top + self.topHeight)
        scrollViewMain.bottomMargin(Constraints.bottom)
        
        // Main View Under Scroll View
        let viewBack = UIView()
        viewBack.backgroundColor = .clear
        scrollViewMain.addSubview(viewBack)
        viewBack.enableAutolayout()
        viewBack.centerX()
        viewBack.fixWidth(screenWidth)
        viewBack.topMargin(0)
        viewBack.bottomMargin(8)
        viewBack.flexibleHeightGreater(10)
        
        stackView.axis  = NSLayoutConstraint.Axis.vertical
        stackView.distribution  = .fill
        stackView.spacing   = 16.0
        stackView.alignment = .center
        viewBack.addSubview(stackView)
        stackView.enableAutolayout()
        stackView.leadingMargin(0)
        stackView.trailingMargin(0)
        stackView.topMargin(0)
        stackView.bottomMargin(0)
        
        viewBack.clipsToBounds = true
        viewBack.backgroundColor = .clear
        
        stackView.addArrangedSubview(summaryDetailsView)
        stackView.addArrangedSubview(executiveDetailsView)
        stackView.addArrangedSubview(ornamentView)
        stackView.addArrangedSubview(loanDetailsView)
        stackView.addArrangedSubview(bankDetailsView)
        stackView.addArrangedSubview(finalDisbursmentView)
        stackView.addArrangedSubview(nomineeDetailsView)
        stackView.addArrangedSubview(ownershipAgreementView)
        stackView.addArrangedSubview(pledgeFormView)
        stackView.addArrangedSubview(releaseAgreementView)
        stackView.addArrangedSubview(approveLoanView)
        stackView.addArrangedSubview(kycDetailsView)
        
        releaseAgreementView.isHidden = true
        bankDetailsView.isHidden = true
        finalDisbursmentView.isHidden = true
        kycDetailsView.isHidden = true
        
        //MARK:- summaryDetailsView
        summaryDetailsView.clipsToBounds = true
        summaryDetailsView.backgroundColor = .white
        summaryDetailsView.layer.cornerRadius = 8
        summaryDetailsView.layer.borderWidth = 1.5
        summaryDetailsView.layer.borderColor = Colors.separator.cgColor
        summaryDetailsView.addShadow()
        stackView.addSubview(summaryDetailsView)
        summaryDetailsView.enableAutolayout()
        summaryDetailsView.topMargin(20)
        summaryDetailsView.leadingMargin(20)
        summaryDetailsView.trailingMargin(20)
        summaryDetailsView.flexibleHeightGreater(20)
        
        let lblSummary = UILabel()
        lblSummary.text = "Summary Details"
        lblSummary.textAlignment = .left
        lblSummary.font = UIFont(name: Fonts.latoRegular, size: TextSize.title + 4)
        lblSummary.textColor = Colors.darkGray
        summaryDetailsView.addSubview(lblSummary)
        lblSummary.enableAutolayout()
        lblSummary.topMargin(16)
        lblSummary.leadingMargin(20)
        lblSummary.trailingMargin(20)
        lblSummary.fixHeight(25)
        
        let separator = UIView()
        separator.clipsToBounds = true
        separator.backgroundColor = Colors.separator
        summaryDetailsView.addSubview(separator)
        separator.enableAutolayout()
        separator.belowView(16, to: lblSummary)
        separator.leadingMargin(0)
        separator.trailingMargin(0)
        separator.fixHeight(1.5)
        
        
        btnLoan.clipsToBounds = true
        btnLoan.setTitle("New Loan", for: .normal)
        btnLoan.setTitleColor(.white, for: .normal)
        btnLoan.backgroundColor = Colors.green
        btnLoan.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        btnLoan.layer.cornerRadius = 4
        btnLoan.addTarget(self, action: #selector(handleNewLoan(_:)), for: .touchUpInside)
        summaryDetailsView.addSubview(btnLoan)
        btnLoan.enableAutolayout()
        btnLoan.trailingMargin(16)
        btnLoan.centerY(to: lblSummary)
        btnLoan.fixWidth(100)
        btnLoan.fixHeight(35)
        
        let width = (screenWidth - 80) / 2
        
        let lblTempLoan = UILabel()
        lblTempLoan.text = "Request ID"
        lblTempLoan.textAlignment = .left
        lblTempLoan.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblTempLoan.textColor = Colors.gray
        summaryDetailsView.addSubview(lblTempLoan)
        lblTempLoan.enableAutolayout()
        lblTempLoan.belowView(10, to: separator)
        lblTempLoan.leadingMargin(20)
        lblTempLoan.fixWidth(width)
        lblTempLoan.fixHeight(28)
        
        lblLoanId.numberOfLines = 0
        lblLoanId.text = ""
        lblLoanId.textAlignment = .right
        lblLoanId.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblLoanId.textColor = Colors.darkGray
        summaryDetailsView.addSubview(lblLoanId)
        lblLoanId.enableAutolayout()
        lblLoanId.belowView(10, to: separator)
        lblLoanId.add(toRight: 8, of: lblTempLoan)
        lblLoanId.trailingMargin(16)
        lblLoanId.flexibleHeightGreater(28)
        
        let lblTempAmount = UILabel()
        lblTempAmount.text = "Loan Amount"
        lblTempAmount.textAlignment = .left
        lblTempAmount.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblTempAmount.textColor = Colors.gray
        summaryDetailsView.addSubview(lblTempAmount)
        lblTempAmount.enableAutolayout()
        lblTempAmount.belowView(4, to: lblTempLoan)
        lblTempAmount.leadingMargin(20)
        lblTempAmount.fixWidth(width)
        lblTempAmount.fixHeight(28)
        
        lblAmount.text = " "
        lblAmount.textAlignment = .right
        lblAmount.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblAmount.textColor = Colors.darkGray
        summaryDetailsView.addSubview(lblAmount)
        lblAmount.enableAutolayout()
        lblAmount.belowView(4, to: lblTempLoan)
        lblAmount.add(toRight: 8, of: lblTempAmount)
        lblAmount.trailingMargin(16)
        lblAmount.fixHeight(28)
        
        let lblTempDateTime = UILabel()
        lblTempDateTime.text = "Date & Time"
        lblTempDateTime.textAlignment = .left
        lblTempDateTime.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblTempDateTime.textColor = Colors.gray
        summaryDetailsView.addSubview(lblTempDateTime)
        lblTempDateTime.enableAutolayout()
        lblTempDateTime.belowView(4, to: lblTempAmount)
        lblTempDateTime.leadingMargin(20)
        lblTempDateTime.fixWidth(width)
        lblTempDateTime.fixHeight(28)
        
        
        lblDateTime.text = " "
        lblDateTime.textAlignment = .right
        lblDateTime.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblDateTime.textColor = Colors.darkGray
        summaryDetailsView.addSubview(lblDateTime)
        lblDateTime.enableAutolayout()
        lblDateTime.belowView(4, to: lblTempAmount)
        lblDateTime.add(toRight: 8, of: lblTempDateTime)
        lblDateTime.trailingMargin(16)
        lblDateTime.fixHeight(28)
        
        let lblTempAddress = UILabel()
        lblTempAddress.text = "Address"
        lblTempAddress.textAlignment = .left
        lblTempAddress.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblTempAddress.textColor = Colors.gray
        summaryDetailsView.addSubview(lblTempAddress)
        lblTempAddress.enableAutolayout()
        lblTempAddress.belowView(4, to: lblTempDateTime)
        lblTempAddress.fixWidth(65)
        lblTempAddress.leadingMargin(20)
        lblTempAddress.fixHeight(28)
        
        lblAddress.text = " "
        lblAddress.numberOfLines = 0
        lblAddress.textAlignment = .right
        lblAddress.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblAddress.textColor = Colors.darkGray
        summaryDetailsView.addSubview(lblAddress)
        lblAddress.enableAutolayout()
        lblAddress.belowView(4, to: lblTempDateTime)
        lblAddress.add(toRight: 20, of: lblTempAddress)
        lblAddress.trailingMargin(16)
        lblAddress.flexibleHeightGreater(28)
        lblAddress.bottomMargin(25)
        
        //MARK:- Executive DetailsView
        executiveDetailsView.clipsToBounds = true
        executiveDetailsView.backgroundColor = .white
        executiveDetailsView.layer.cornerRadius = 8
        executiveDetailsView.layer.borderWidth = 1.5
        executiveDetailsView.layer.borderColor = Colors.separator.cgColor
        executiveDetailsView.addShadow()
        stackView.addSubview(executiveDetailsView)
        executiveDetailsView.enableAutolayout()
        executiveDetailsView.belowView(20, to: summaryDetailsView)
        executiveDetailsView.leadingMargin(20)
        executiveDetailsView.trailingMargin(20)
        // executiveDetailsView.flexibleHeightGreater(0)
        
        let lblExecutive = UILabel()
        lblExecutive.text = "Executive Details"
        lblExecutive.textAlignment = .left
        lblExecutive.font = UIFont(name: Fonts.latoRegular, size: TextSize.title + 4)
        lblExecutive.textColor = Colors.darkGray
        executiveDetailsView.addSubview(lblExecutive)
        lblExecutive.enableAutolayout()
        lblExecutive.topMargin(16)
        lblExecutive.leadingMargin(20)
        lblExecutive.trailingMargin(20)
        lblExecutive.fixHeight(25)
        
        let separator1 = UIView()
        separator1.clipsToBounds = true
        separator1.backgroundColor = Colors.separator
        executiveDetailsView.addSubview(separator1)
        separator1.enableAutolayout()
        separator1.belowView(16, to: lblExecutive)
        separator1.leadingMargin(0)
        separator1.trailingMargin(0)
        separator1.fixHeight(1.5)
        
        if #available(iOS 15.0, *) {
            tblView.sectionHeaderTopPadding = 0
        }
        tblView.tag = 100
        //tblView.tableFooterView = UIView()
        tblView.delegate = self
        tblView.dataSource = self
        tblView.estimatedRowHeight = UITableView.automaticDimension
        tblView.separatorStyle = .singleLine
        tblView.backgroundColor = .clear
        tblView.showsVerticalScrollIndicator = false
        tblView.isScrollEnabled = false
        tblView.bounces = true
        executiveDetailsView.addSubview(tblView)
        tblView.enableAutolayout()
        tblView.trailingMargin(0)
        tblView.belowView(8, to: separator1)
        tblViewHeightConstraint = tblView.heightAnchor.constraint(greaterThanOrEqualToConstant: 0)
        tblViewHeightConstraint?.isActive = true
        tblView.leadingMargin(0)
        tblView.bottomMargin(8)
        
        //MARK:- ornamentSummaryView
        ornamentView.clipsToBounds = true
        ornamentView.backgroundColor = .white
        ornamentView.layer.cornerRadius = 8
        ornamentView.layer.borderWidth = 1.5
        ornamentView.layer.borderColor = Colors.separator.cgColor
        ornamentView.addShadow()
        stackView.addSubview(ornamentView)
        ornamentView.enableAutolayout()
        ornamentView.belowView(20, to: executiveDetailsView)
        ornamentView.leadingMargin(20)
        ornamentView.trailingMargin(20)
        ornamentView.flexibleHeightGreater(20)
        
        let lblOrnament = UILabel()
        lblOrnament.text = "Ornament Summary"
        lblOrnament.textAlignment = .left
        lblOrnament.font = UIFont(name: Fonts.latoRegular, size: TextSize.title + 4)
        lblOrnament.textColor = Colors.darkGray
        ornamentView.addSubview(lblOrnament)
        lblOrnament.enableAutolayout()
        lblOrnament.topMargin(16)
        lblOrnament.leadingMargin(20)
        lblOrnament.trailingMargin(20)
        lblOrnament.fixHeight(25)
        
        if #available(iOS 15.0, *) {
            ornamentTblView.sectionHeaderTopPadding = 0
        }
        ornamentTblView.tag = 101
        ornamentTblView.tableFooterView = UIView()
        ornamentTblView.delegate = self
        ornamentTblView.dataSource = self
        ornamentTblView.estimatedRowHeight = UITableView.automaticDimension
        ornamentTblView.separatorStyle = .singleLine
        ornamentTblView.backgroundColor = .clear
        ornamentTblView.showsVerticalScrollIndicator = false
        ornamentTblView.isScrollEnabled = false
        ornamentTblView.bounces = true
        ornamentView.addSubview(ornamentTblView)
        ornamentTblView.enableAutolayout()
        ornamentTblView.trailingMargin(0)
        ornamentTblView.belowView(16, to: lblOrnament)
        ornamentTblViewHeightConstraint = ornamentTblView.heightAnchor.constraint(greaterThanOrEqualToConstant: 0)
        ornamentTblViewHeightConstraint?.isActive = true
        ornamentTblView.leadingMargin(0)
        ornamentTblView.bottomMargin(8)
        
        
        //MARK:- LoanDetailsView
        loanDetailsView.clipsToBounds = true
        loanDetailsView.backgroundColor = .white
        loanDetailsView.layer.cornerRadius = 8
        loanDetailsView.layer.borderWidth = 1.5
        loanDetailsView.layer.borderColor = Colors.separator.cgColor
        loanDetailsView.addShadow()
        stackView.addSubview(loanDetailsView)
        loanDetailsView.enableAutolayout()
        loanDetailsView.belowView(20, to: ornamentView)
        loanDetailsView.leadingMargin(20)
        loanDetailsView.trailingMargin(20)
        loanDetailsView.flexibleHeightGreater(0)
        
        
        let lblLoanDetails = UILabel()
        lblLoanDetails.text = "Loan Details"
        lblLoanDetails.textAlignment = .left
        lblLoanDetails.font = UIFont(name: Fonts.latoRegular, size: TextSize.title + 4)
        lblLoanDetails.textColor = Colors.darkGray
        loanDetailsView.addSubview(lblLoanDetails)
        lblLoanDetails.enableAutolayout()
        lblLoanDetails.topMargin(16)
        lblLoanDetails.leadingMargin(20)
        lblLoanDetails.trailingMargin(20)
        lblLoanDetails.fixHeight(25)
        
        let separator3 = UIView()
        separator3.clipsToBounds = true
        separator3.backgroundColor = Colors.separator
        loanDetailsView.addSubview(separator3)
        separator3.enableAutolayout()
        separator3.belowView(16, to: lblLoanDetails)
        separator3.leadingMargin(0)
        separator3.trailingMargin(0)
        separator3.fixHeight(1.5)
        
        
        let width2 = (screenWidth - 60) / 2
        
        lblCustomerLoanNumber.numberOfLines = 0
        lblCustomerLoanNumber.textAlignment = .left
        loanDetailsView.addSubview(lblCustomerLoanNumber)
        lblCustomerLoanNumber.enableAutolayout()
        lblCustomerLoanNumber.belowView(16, to: separator3)
        lblCustomerLoanNumber.leadingMargin(20)
        lblCustomerLoanNumber.trailingMargin(8)
        lblCustomerLoanNumber.flexibleHeightGreater(20)
        
        lblProduct.numberOfLines = 0
        lblProduct.textAlignment = .left
        loanDetailsView.addSubview(lblProduct)
        lblProduct.enableAutolayout()
        lblProduct.belowView(20, to: lblCustomerLoanNumber)
        lblProduct.leadingMargin(20)
        lblProduct.fixWidth(width2)
        lblProduct.flexibleHeightGreater(20)
        
        
        lblProductCat.numberOfLines = 0
        lblProductCat.textAlignment = .left
        loanDetailsView.addSubview(lblProductCat)
        lblProductCat.enableAutolayout()
        lblProductCat.centerY(to: lblProduct)
        lblProductCat.add(toRight: 8, of: lblProduct)
        lblProductCat.fixWidth(width2)
        lblProductCat.flexibleHeightGreater(20)
        
        lblSchemeName.numberOfLines = 0
        lblSchemeName.textAlignment = .left
        loanDetailsView.addSubview(lblSchemeName)
        lblSchemeName.enableAutolayout()
        lblSchemeName.belowView(16, to: lblProduct)
        lblSchemeName.leadingMargin(20)
        lblSchemeName.fixWidth(width2)
        lblSchemeName.flexibleHeightGreater(20)
        
        lblInterestRate.numberOfLines = 0
        lblInterestRate.textAlignment = .left
        loanDetailsView.addSubview(lblInterestRate)
        lblInterestRate.enableAutolayout()
        lblInterestRate.belowView(20, to: lblProduct)
        lblInterestRate.add(toRight: 8, of: lblSchemeName)
        lblInterestRate.fixWidth(width2)
        lblInterestRate.flexibleHeightGreater(20)
        
        lblMonthlyPayment.numberOfLines = 0
        lblMonthlyPayment.textAlignment = .left
        loanDetailsView.addSubview(lblMonthlyPayment)
        lblMonthlyPayment.enableAutolayout()
        lblMonthlyPayment.belowView(20, to: lblSchemeName)
        lblMonthlyPayment.leadingMargin(20)
        lblMonthlyPayment.fixWidth(width2)
        lblMonthlyPayment.flexibleHeightGreater(20)
        
        
        lblTenure.numberOfLines = 0
        lblTenure.textAlignment = .left
        loanDetailsView.addSubview(lblTenure)
        lblTenure.enableAutolayout()
        lblTenure.belowView(20, to: lblSchemeName)
        lblTenure.add(toRight: 8, of: lblMonthlyPayment)
        lblTenure.fixWidth(width2)
        lblTenure.flexibleHeightGreater(20)
        
        lblFinalLoanAmount.numberOfLines = 0
        lblFinalLoanAmount.textAlignment = .left
        loanDetailsView.addSubview(lblFinalLoanAmount)
        lblFinalLoanAmount.enableAutolayout()
        lblFinalLoanAmount.belowView(20, to: lblMonthlyPayment)
        lblFinalLoanAmount.leadingMargin(20)
        lblFinalLoanAmount.fixWidth(width2)
        lblFinalLoanAmount.flexibleHeightGreater(20)
        
        lblPaymnetFrequency.numberOfLines = 0
        lblPaymnetFrequency.textAlignment = .left
        loanDetailsView.addSubview(lblPaymnetFrequency)
        lblPaymnetFrequency.enableAutolayout()
        lblPaymnetFrequency.belowView(20, to: lblFinalLoanAmount)
        lblPaymnetFrequency.leadingMargin(20)
        lblPaymnetFrequency.fixWidth(width2)
        lblPaymnetFrequency.flexibleHeightGreater(20)
        lblPaymnetFrequency.bottomMargin(20)
        

        //MARK:- Bank Details View
        bankDetailsView.clipsToBounds = true
        bankDetailsView.backgroundColor = .white
        bankDetailsView.layer.cornerRadius = 8
        bankDetailsView.layer.borderWidth = 1.5
        bankDetailsView.layer.borderColor = Colors.separator.cgColor
        bankDetailsView.addShadow()
        stackView.addSubview(bankDetailsView)
        bankDetailsView.enableAutolayout()
        bankDetailsView.belowView(20, to: loanDetailsView)
        bankDetailsView.leadingMargin(20)
        bankDetailsView.trailingMargin(20)
        bankDetailsView.flexibleHeightGreater(0)
        
        
        let lblBankDetails = UILabel()
        lblBankDetails.text = "Bank Details"
        lblBankDetails.textAlignment = .left
        lblBankDetails.font = UIFont(name: Fonts.latoRegular, size: TextSize.title + 4)
        lblBankDetails.textColor = Colors.darkGray
        bankDetailsView.addSubview(lblBankDetails)
        lblBankDetails.enableAutolayout()
        lblBankDetails.topMargin(16)
        lblBankDetails.leadingMargin(20)
        lblBankDetails.trailingMargin(20)
        lblBankDetails.fixHeight(25)
        
        let separator4 = UIView()
        separator4.clipsToBounds = true
        separator4.backgroundColor = Colors.separator
        bankDetailsView.addSubview(separator4)
        separator4.enableAutolayout()
        separator4.belowView(16, to: lblBankDetails)
        separator4.leadingMargin(0)
        separator4.trailingMargin(0)
        separator4.fixHeight(1.5)
        
        
        let lblTempBeneficiary = UILabel()
        lblTempBeneficiary.text = "Beneficiary"
        lblTempBeneficiary.textAlignment = .left
        lblTempBeneficiary.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblTempBeneficiary.textColor = Colors.gray
        bankDetailsView.addSubview(lblTempBeneficiary)
        lblTempBeneficiary.enableAutolayout()
        lblTempBeneficiary.belowView(10, to: separator4)
        lblTempBeneficiary.leadingMargin(20)
        lblTempBeneficiary.fixWidth(100)
        lblTempBeneficiary.fixHeight(28)
        
        
        lblBeneficiary.text = " "
        lblBeneficiary.textAlignment = .right
        lblBeneficiary.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblBeneficiary.textColor = Colors.darkGray
        bankDetailsView.addSubview(lblBeneficiary)
        lblBeneficiary.enableAutolayout()
        lblBeneficiary.belowView(10, to: separator4)
        lblBeneficiary.trailingMargin(16)
        lblBeneficiary.fixWidth(width2)
        lblBeneficiary.fixHeight(28)
        lblBeneficiary.add(toRight: 8, of: lblTempBeneficiary)
        
        
        let lblTempBankName = UILabel()
        lblTempBankName.text = "Bank Name"
        lblTempBankName.textAlignment = .left
        lblTempBankName.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblTempBankName.textColor = Colors.gray
        bankDetailsView.addSubview(lblTempBankName)
        lblTempBankName.enableAutolayout()
        lblTempBankName.belowView(4, to: lblTempBeneficiary)
        lblTempBankName.leadingMargin(20)
        lblTempBankName.fixWidth(width2)
        lblTempBankName.fixHeight(28)
        
        
        lblBankName.text = " "
        lblBankName.textAlignment = .right
        lblBankName.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblBankName.textColor = Colors.darkGray
        bankDetailsView.addSubview(lblBankName)
        lblBankName.enableAutolayout()
        lblBankName.belowView(4, to: lblTempBeneficiary)
        lblBankName.trailingMargin(16)
        lblBankName.fixWidth(width2)
        lblBankName.fixHeight(28)
        lblBankName.add(toRight: 8, of: lblTempBankName)
        
        
        let lblTempIFSCCode = UILabel()
        lblTempIFSCCode.text = "IFSC Code"
        lblTempIFSCCode.textAlignment = .left
        lblTempIFSCCode.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblTempIFSCCode.textColor = Colors.gray
        bankDetailsView.addSubview(lblTempIFSCCode)
        lblTempIFSCCode.enableAutolayout()
        lblTempIFSCCode.belowView(4, to: lblBankName)
        lblTempIFSCCode.leadingMargin(20)
        lblTempIFSCCode.fixWidth(width2)
        lblTempIFSCCode.fixHeight(28)
        
        
        lblIFSCCode.text = ""
        lblIFSCCode.textAlignment = .right
        lblIFSCCode.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblIFSCCode.textColor = Colors.darkGray
        bankDetailsView.addSubview(lblIFSCCode)
        lblIFSCCode.enableAutolayout()
        lblIFSCCode.belowView(4, to: lblBankName)
        lblIFSCCode.trailingMargin(16)
        lblIFSCCode.fixWidth(width2)
        lblIFSCCode.fixHeight(28)
        lblIFSCCode.add(toRight: 8, of: lblTempIFSCCode)
        
        let lblTempAccountNumber = UILabel()
        lblTempAccountNumber.text = "Account Number"
        lblTempAccountNumber.textAlignment = .left
        lblTempAccountNumber.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblTempAccountNumber.textColor = Colors.gray
        bankDetailsView.addSubview(lblTempAccountNumber)
        lblTempAccountNumber.enableAutolayout()
        lblTempAccountNumber.belowView(4, to: lblTempIFSCCode)
        lblTempAccountNumber.leadingMargin(20)
        lblTempAccountNumber.fixWidth(width2)
        lblTempAccountNumber.fixHeight(28)
        //lblTempAccountNumber.bottomMargin(20)
        
        lblAccountNumber.text = " "
        lblAccountNumber.textAlignment = .right
        lblAccountNumber.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblAccountNumber.textColor = Colors.darkGray
        bankDetailsView.addSubview(lblAccountNumber)
        lblAccountNumber.enableAutolayout()
        lblAccountNumber.belowView(4, to: lblTempIFSCCode)
        lblAccountNumber.trailingMargin(16)
        lblAccountNumber.fixWidth(width2)
        lblAccountNumber.fixHeight(28)
        lblAccountNumber.add(toRight: 8, of: lblTempAccountNumber)
        
         lblTempBranchName.text = "Branch Name"
         lblTempBranchName.textAlignment = .left
         lblTempBranchName.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
         lblTempBranchName.textColor = Colors.gray
         bankDetailsView.addSubview(lblTempBranchName)
         lblTempBranchName.enableAutolayout()
         lblTempBranchName.belowView(4, to: lblTempAccountNumber)
         lblTempBranchName.leadingMargin(20)
         lblTempBranchName.fixWidth(width2)
         lblTempBranchName.flexibleWidthGreater(28)
         lblTempBranchName.bottomMargin(20)
         
         lblBranchName.text = " "
         lblBranchName.textAlignment = .right
         lblBranchName.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
         lblBranchName.textColor = Colors.darkGray
         bankDetailsView.addSubview(lblBranchName)
         lblBranchName.enableAutolayout()
         lblBranchName.belowView(4, to: lblTempAccountNumber)
         lblBranchName.trailingMargin(16)
         lblBranchName.fixWidth(width2)
         lblBranchName.flexibleWidthGreater(28)
         lblBranchName.add(toRight: 8, of: lblTempBranchName)
        
        
        //MARK:- Final Disbursment Amount View
        finalDisbursmentView.clipsToBounds = true
        finalDisbursmentView.backgroundColor = .white
        finalDisbursmentView.layer.cornerRadius = 8
        finalDisbursmentView.layer.borderWidth = 0.4
        finalDisbursmentView.layer.borderColor = Colors.textGray.withAlphaComponent(0.7).cgColor
        bankDetailsView.addShadow()
        stackView.addSubview(finalDisbursmentView)
        finalDisbursmentView.enableAutolayout()
        finalDisbursmentView.belowView(20, to: bankDetailsView)
        finalDisbursmentView.leadingMargin(20)
        finalDisbursmentView.trailingMargin(20)
        finalDisbursmentView.flexibleHeightGreater(40)
        
        let lblfinalDisbursme = UILabel()
        lblfinalDisbursme.text = "Final Disbursment Amount"
        lblfinalDisbursme.textAlignment = .left
        lblfinalDisbursme.font = UIFont(name: Fonts.latoRegular, size: TextSize.title + 4)
        lblfinalDisbursme.textColor = Colors.darkGray
        finalDisbursmentView.addSubview(lblfinalDisbursme)
        lblfinalDisbursme.enableAutolayout()
        lblfinalDisbursme.topMargin(16)
        lblfinalDisbursme.leadingMargin(20)
        lblfinalDisbursme.trailingMargin(20)
        lblfinalDisbursme.fixHeight(25)
        
        
        let separator5 = UIView()
        separator5.clipsToBounds = true
        separator5.backgroundColor = Colors.separator
        finalDisbursmentView.addSubview(separator5)
        separator5.enableAutolayout()
        separator5.belowView(16, to: lblfinalDisbursme)
        separator5.leadingMargin(0)
        separator5.trailingMargin(0)
        separator5.fixHeight(1.5)
        
        
        let lbltempLoanAmount = UILabel()
        lbltempLoanAmount.text = "Loan Amount"
        lbltempLoanAmount.textAlignment = .left
        lbltempLoanAmount.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lbltempLoanAmount.textColor = Colors.gray
        finalDisbursmentView.addSubview(lbltempLoanAmount)
        lbltempLoanAmount.enableAutolayout()
        lbltempLoanAmount.belowView(10, to: separator5)
        lbltempLoanAmount.leadingMargin(20)
        lbltempLoanAmount.fixWidth(120)
        lbltempLoanAmount.fixHeight(28)
        
        lblLoanAmount.text = " "
        lblLoanAmount.textAlignment = .right
        lblLoanAmount.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblLoanAmount.textColor = Colors.darkGray
        finalDisbursmentView.addSubview(lblLoanAmount)
        lblLoanAmount.enableAutolayout()
        lblLoanAmount.belowView(10, to: separator5)
        lblLoanAmount.trailingMargin(16)
        lblLoanAmount.fixWidth(width2)
        lblLoanAmount.fixHeight(28)
        lblLoanAmount.add(toRight: 8, of: lbltempLoanAmount)
        
        
        let lblTempInterestRate = UILabel()
        lblTempInterestRate.text = "Interest Rate"
        lblTempInterestRate.textAlignment = .left
        lblTempInterestRate.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblTempInterestRate.textColor = Colors.gray
        finalDisbursmentView.addSubview(lblTempInterestRate)
        lblTempInterestRate.enableAutolayout()
        lblTempInterestRate.belowView(4, to: lbltempLoanAmount)
        lblTempInterestRate.leadingMargin(20)
        lblTempInterestRate.fixWidth(width2)
        lblTempInterestRate.fixHeight(28)
        
        
        lblFinalInterestRate.text = ""
        lblFinalInterestRate.textAlignment = .right
        lblFinalInterestRate.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblFinalInterestRate.textColor = Colors.darkGray
        finalDisbursmentView.addSubview(lblFinalInterestRate)
        lblFinalInterestRate.enableAutolayout()
        lblFinalInterestRate.belowView(4, to: lbltempLoanAmount)
        lblFinalInterestRate.trailingMargin(16)
        lblFinalInterestRate.fixWidth(width2)
        lblFinalInterestRate.fixHeight(28)
        lblFinalInterestRate.add(toRight: 8, of: lblTempInterestRate)
        
        let lblTempEMI = UILabel()
        lblTempEMI.text = "EMI/Month"
        lblTempEMI.textAlignment = .left
        lblTempEMI.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblTempEMI.textColor = Colors.gray
        finalDisbursmentView.addSubview(lblTempEMI)
        lblTempEMI.enableAutolayout()
        lblTempEMI.belowView(4, to: lblTempInterestRate)
        lblTempEMI.leadingMargin(20)
        lblTempEMI.fixWidth(width2)
        lblTempEMI.fixHeight(28)
        
        
        lblEMI.text = " "
        lblEMI.textAlignment = .right
        lblEMI.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblEMI.textColor = Colors.darkGray
        finalDisbursmentView.addSubview(lblEMI)
        lblEMI.enableAutolayout()
        lblEMI.belowView(4, to: lblTempInterestRate)
        lblEMI.trailingMargin(16)
        lblEMI.fixWidth(width2)
        lblEMI.fixHeight(28)
        lblEMI.add(toRight: 8, of: lblTempEMI)
        
        let lblTempTotalInterestAmount = UILabel()
        lblTempTotalInterestAmount.text = "Total Interest Amount"
        lblTempTotalInterestAmount.textAlignment = .left
        lblTempTotalInterestAmount.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblTempTotalInterestAmount.textColor = Colors.gray
        finalDisbursmentView.addSubview(lblTempTotalInterestAmount)
        lblTempTotalInterestAmount.enableAutolayout()
        lblTempTotalInterestAmount.belowView(4, to: lblTempEMI)
        lblTempTotalInterestAmount.leadingMargin(20)
        lblTempTotalInterestAmount.fixWidth(width2)
        lblTempTotalInterestAmount.fixHeight(28)
        
        
        lblTotalInterestAmount.text = ""
        lblTotalInterestAmount.textAlignment = .right
        lblTotalInterestAmount.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblTotalInterestAmount.textColor = Colors.darkGray
        finalDisbursmentView.addSubview(lblTotalInterestAmount)
        lblTotalInterestAmount.enableAutolayout()
        lblTotalInterestAmount.belowView(4, to: lblTempEMI)
        lblTotalInterestAmount.trailingMargin(16)
        lblTotalInterestAmount.fixWidth(width2)
        lblTotalInterestAmount.fixHeight(28)
        lblTotalInterestAmount.add(toRight: 8, of: lblTempTotalInterestAmount)
        
        let lblTempTotalPayableAmount = UILabel()
        lblTempTotalPayableAmount.text = "Total Payable Amount"
        lblTempTotalPayableAmount.textAlignment = .left
        lblTempTotalPayableAmount.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblTempTotalPayableAmount.textColor = Colors.gray
        finalDisbursmentView.addSubview(lblTempTotalPayableAmount)
        lblTempTotalPayableAmount.enableAutolayout()
        lblTempTotalPayableAmount.belowView(4, to: lblTempTotalInterestAmount)
        lblTempTotalPayableAmount.leadingMargin(20)
        lblTempTotalPayableAmount.fixWidth(width2)
        lblTempTotalPayableAmount.fixHeight(28)
        lblTempTotalPayableAmount.bottomMargin(20)
        
        lblTotalPayableAmount.text = " "
        lblTotalPayableAmount.textAlignment = .right
        lblTotalPayableAmount.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblTotalPayableAmount.textColor = Colors.darkGray
        finalDisbursmentView.addSubview(lblTotalPayableAmount)
        lblTotalPayableAmount.enableAutolayout()
        lblTotalPayableAmount.belowView(4, to: lblTempTotalInterestAmount)
        lblTotalPayableAmount.trailingMargin(16)
        lblTotalPayableAmount.fixWidth(width2)
        lblTotalPayableAmount.fixHeight(28)
        lblTotalPayableAmount.add(toRight: 8, of: lblTempTotalPayableAmount)
        
        
        //MARK:- Nominee View
        
        nomineeDetailsView.clipsToBounds = true
        nomineeDetailsView.backgroundColor = .white
        nomineeDetailsView.layer.cornerRadius = 8
        nomineeDetailsView.layer.borderWidth = 0.4
        nomineeDetailsView.layer.borderColor = Colors.textGray.withAlphaComponent(0.7).cgColor
        nomineeDetailsView.addShadow()
        stackView.addSubview(nomineeDetailsView)
        nomineeDetailsView.enableAutolayout()
        nomineeDetailsView.belowView(20, to: finalDisbursmentView)
        nomineeDetailsView.leadingMargin(20)
        nomineeDetailsView.trailingMargin(20)
        nomineeDetailsView.flexibleHeightGreater(0)
        
        let lblNomineeDetails = UILabel()
        lblNomineeDetails.text = "Nominee Details"
        lblNomineeDetails.textAlignment = .left
        lblNomineeDetails.font = UIFont(name: Fonts.latoRegular, size: TextSize.title + 4)
        lblNomineeDetails.textColor = Colors.darkGray
        nomineeDetailsView.addSubview(lblNomineeDetails)
        lblNomineeDetails.enableAutolayout()
        lblNomineeDetails.topMargin(16)
        lblNomineeDetails.leadingMargin(20)
        lblNomineeDetails.trailingMargin(20)
        lblNomineeDetails.fixHeight(25)
        
        let separator8 = UIView()
        separator8.clipsToBounds = true
        separator8.backgroundColor = Colors.separator
        nomineeDetailsView.addSubview(separator8)
        separator8.enableAutolayout()
        separator8.belowView(16, to: lblNomineeDetails)
        separator8.leadingMargin(0)
        separator8.trailingMargin(0)
        separator8.fixHeight(1.5)
        
        
        let lblTempNomineeName = UILabel()
        lblTempNomineeName.text = "Nominee Name"
        lblTempNomineeName.textAlignment = .left
        lblTempNomineeName.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblTempNomineeName.textColor = Colors.gray
        nomineeDetailsView.addSubview(lblTempNomineeName)
        lblTempNomineeName.enableAutolayout()
        lblTempNomineeName.belowView(10, to: separator8)
        lblTempNomineeName.leadingMargin(20)
        lblTempNomineeName.fixWidth(120)
        lblTempNomineeName.fixHeight(28)
        
        lblNomineeName.text = " "
        lblNomineeName.textAlignment = .right
        lblNomineeName.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblNomineeName.textColor = Colors.darkGray
        nomineeDetailsView.addSubview(lblNomineeName)
        lblNomineeName.enableAutolayout()
        lblNomineeName.belowView(10, to: separator8)
        lblNomineeName.trailingMargin(16)
        lblNomineeName.fixWidth(width2)
        lblNomineeName.fixHeight(28)
        lblNomineeName.add(toRight: 8, of: lblTempNomineeName)
        
        
        let lblTempNomineeRelation = UILabel()
        lblTempNomineeRelation.text = "Nominee Relation"
        lblTempNomineeRelation.textAlignment = .left
        lblTempNomineeRelation.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        lblTempNomineeRelation.textColor = Colors.gray
        nomineeDetailsView.addSubview(lblTempNomineeRelation)
        lblTempNomineeRelation.enableAutolayout()
        lblTempNomineeRelation.belowView(4, to: lblTempNomineeName)
        lblTempNomineeRelation.leadingMargin(20)
        lblTempNomineeRelation.fixWidth(width2)
        lblTempNomineeRelation.fixHeight(28)
        
        
        lblNomineeRelation.text = " "
        lblNomineeRelation.textAlignment = .right
        lblNomineeRelation.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblNomineeRelation.textColor = Colors.darkGray
        nomineeDetailsView.addSubview(lblNomineeRelation)
        lblNomineeRelation.enableAutolayout()
        lblNomineeRelation.belowView(4, to: lblTempNomineeName)
        lblNomineeRelation.trailingMargin(16)
        lblNomineeRelation.fixWidth(width2)
        lblNomineeRelation.fixHeight(28)
        lblNomineeRelation.add(toRight: 8, of: lblTempNomineeRelation)
        
        
        let lblTempNomineeDOB = UILabel()
        lblTempNomineeDOB.text = "Nominee DOB"
        lblTempNomineeDOB.textAlignment = .left
        lblTempNomineeDOB.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblTempNomineeDOB.textColor = Colors.gray
        nomineeDetailsView.addSubview(lblTempNomineeDOB)
        lblTempNomineeDOB.enableAutolayout()
        lblTempNomineeDOB.belowView(4, to: lblTempNomineeRelation)
        lblTempNomineeDOB.leadingMargin(20)
        lblTempNomineeDOB.fixWidth(width2)
        lblTempNomineeDOB.fixHeight(28)
        
        
        lblNomineeDOB.text = ""
        lblNomineeDOB.textAlignment = .right
        lblNomineeDOB.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblNomineeDOB.textColor = Colors.darkGray
        nomineeDetailsView.addSubview(lblNomineeDOB)
        lblNomineeDOB.enableAutolayout()
        lblNomineeDOB.belowView(4, to: lblTempNomineeRelation)
        lblNomineeDOB.trailingMargin(16)
        lblNomineeDOB.fixWidth(width2)
        lblNomineeDOB.fixHeight(28)
        lblNomineeDOB.add(toRight: 8, of: lblTempNomineeDOB)
        
        let lblTempNomineeAddress = UILabel()
        lblTempNomineeAddress.text = "Nominee Address"
        lblTempNomineeAddress.textAlignment = .left
        lblTempNomineeAddress.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title)
        lblTempNomineeAddress.textColor = Colors.gray
        nomineeDetailsView.addSubview(lblTempNomineeAddress)
        lblTempNomineeAddress.enableAutolayout()
        lblTempNomineeAddress.belowView(4, to: lblTempNomineeDOB)
        lblTempNomineeAddress.leadingMargin(20)
        lblTempNomineeAddress.fixWidth(width2)
        lblTempNomineeAddress.fixHeight(28)
        
        lblNomineeAddress.numberOfLines = 0
        lblNomineeAddress.text = " "
        lblNomineeAddress.textAlignment = .right
        lblNomineeAddress.font = UIFont(name: Fonts.latoRegular, size: TextSize.title)
        lblNomineeAddress.textColor = Colors.darkGray
        nomineeDetailsView.addSubview(lblNomineeAddress)
        lblNomineeAddress.enableAutolayout()
        lblNomineeAddress.belowView(4, to: lblTempNomineeDOB)
        lblNomineeAddress.trailingMargin(16)
        lblNomineeAddress.fixWidth(width2)
        lblNomineeAddress.flexibleHeightGreater(28)
        lblNomineeAddress.add(toRight: 8, of: lblTempNomineeAddress)
        lblNomineeAddress.bottomMargin(20)
        
        // End Nominee Vikew
        
        //MARK:- Final OwnershipAgreementView View
        ownershipAgreementView.clipsToBounds = true
        ownershipAgreementView.backgroundColor = .white
        ownershipAgreementView.layer.cornerRadius = 8
        ownershipAgreementView.layer.borderWidth = 1.5
        ownershipAgreementView.layer.borderColor = Colors.separator.cgColor
        ownershipAgreementView.addShadow()
        stackView.addSubview(ownershipAgreementView)
        ownershipAgreementView.enableAutolayout()
        ownershipAgreementView.belowView(20, to: nomineeDetailsView)
        ownershipAgreementView.leadingMargin(20)
        ownershipAgreementView.trailingMargin(20)
        ownershipAgreementView.flexibleHeightGreater(40)
        
        
        let lblownershipAgreement = UILabel()
        lblownershipAgreement.text = "Ownership - Agreement Pdf"
        lblownershipAgreement.textAlignment = .left
        lblownershipAgreement.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        lblownershipAgreement.textColor = Colors.darkGray
        ownershipAgreementView.addSubview(lblownershipAgreement)
        lblownershipAgreement.enableAutolayout()
        lblownershipAgreement.topMargin(16)
        lblownershipAgreement.leadingMargin(20)
        lblownershipAgreement.trailingMargin(20)
        lblownershipAgreement.fixHeight(25)
        
        let separator11 = UIView()
        separator11.clipsToBounds = true
        separator11.backgroundColor = Colors.separator
        ownershipAgreementView.addSubview(separator11)
        separator11.enableAutolayout()
        separator11.belowView(16, to: lblownershipAgreement)
        separator11.leadingMargin(0)
        separator11.trailingMargin(0)
        separator11.fixHeight(1.5)
        
        
        let btnOwnershipAgreement = UIButton()
        btnOwnershipAgreement.setTitle("View Details", for: .normal)
        btnOwnershipAgreement.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle)
        btnOwnershipAgreement.setTitleColor(Colors.theme, for: .normal)
        btnOwnershipAgreement.addTarget(self, action: #selector(handlebtnOwnershipAgreementBtn(_:)), for: .touchUpInside)
        ownershipAgreementView.addSubview(btnOwnershipAgreement)
        btnOwnershipAgreement.enableAutolayout()
        btnOwnershipAgreement.fixWidth(120)
        btnOwnershipAgreement.trailingMargin(0)
        btnOwnershipAgreement.belowView(8, to: separator11)
        btnOwnershipAgreement.bottomMargin(8)
        
        //MARK:- PledgeForm View
        pledgeFormView.clipsToBounds = true
        pledgeFormView.backgroundColor = .white
        pledgeFormView.layer.cornerRadius = 8
        pledgeFormView.layer.borderWidth = 1.5
        pledgeFormView.layer.borderColor = Colors.separator.cgColor
        pledgeFormView.addShadow()
        stackView.addSubview(pledgeFormView)
        pledgeFormView.enableAutolayout()
        pledgeFormView.belowView(20, to: ownershipAgreementView)
        pledgeFormView.leadingMargin(20)
        pledgeFormView.trailingMargin(20)
        pledgeFormView.flexibleHeightGreater(40)
        
        
        let lblpledgeForm = UILabel()
        lblpledgeForm.text = "Pledge Form PDF"
        lblpledgeForm.textAlignment = .left
        lblpledgeForm.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        lblpledgeForm.textColor = Colors.darkGray
        pledgeFormView.addSubview(lblpledgeForm)
        lblpledgeForm.enableAutolayout()
        lblpledgeForm.topMargin(16)
        lblpledgeForm.leadingMargin(20)
        lblpledgeForm.trailingMargin(20)
        lblpledgeForm.fixHeight(25)
        
        let separator10 = UIView()
        separator10.clipsToBounds = true
        separator10.backgroundColor = Colors.separator
        pledgeFormView.addSubview(separator10)
        separator10.enableAutolayout()
        separator10.belowView(16, to: lblpledgeForm)
        separator10.leadingMargin(0)
        separator10.trailingMargin(0)
        separator10.fixHeight(1.5)
        
        let btnpledgeForm = UIButton()
        btnpledgeForm.setTitle("View Details", for: .normal)
        btnpledgeForm.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle)
        btnpledgeForm.setTitleColor(Colors.theme, for: .normal)
        btnpledgeForm.addTarget(self, action: #selector(handlebtnPledgeForm(_:)), for: .touchUpInside)
        pledgeFormView.addSubview(btnpledgeForm)
        btnpledgeForm.enableAutolayout()
        btnpledgeForm.fixWidth(120)
        btnpledgeForm.trailingMargin(0)
        btnpledgeForm.belowView(8, to: separator10)
        btnpledgeForm.bottomMargin(8)
        
        //MARK:- Release Agreement
        
        releaseAgreementView.clipsToBounds = true
        releaseAgreementView.backgroundColor = .white
        releaseAgreementView.layer.cornerRadius = 8
        releaseAgreementView.layer.borderWidth = 1.5
        releaseAgreementView.layer.borderColor = Colors.separator.cgColor
        releaseAgreementView.addShadow()
        stackView.addSubview(releaseAgreementView)
        releaseAgreementView.enableAutolayout()
        releaseAgreementView.belowView(20, to: pledgeFormView)
        releaseAgreementView.leadingMargin(20)
        releaseAgreementView.trailingMargin(20)
        releaseAgreementView.flexibleHeightGreater(40)
        
        
        let lblRelease = UILabel()
        lblRelease.text = "Release - Agreement Pdf"
        lblRelease.textAlignment = .left
        lblRelease.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
        lblRelease.textColor = Colors.darkGray
        releaseAgreementView.addSubview(lblRelease)
        lblRelease.enableAutolayout()
        lblRelease.topMargin(10)
        lblRelease.leadingMargin(20)
        lblRelease.trailingMargin(20)
        lblRelease.fixHeight(25)
        
        let separator12 = UIView()
        separator12.clipsToBounds = true
        separator12.backgroundColor = Colors.separator
        releaseAgreementView.addSubview(separator12)
        separator12.enableAutolayout()
        separator12.belowView(10, to: lblRelease)
        separator12.leadingMargin(0)
        separator12.trailingMargin(0)
        separator12.fixHeight(1.5)
        
        let btnReleaseAgreement = UIButton()
        btnReleaseAgreement.setTitle("View Details", for: .normal)
        btnReleaseAgreement.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle)
        btnReleaseAgreement.setTitleColor(Colors.theme, for: .normal)
        btnReleaseAgreement.addTarget(self, action: #selector(handleReleaseAgreementBtn(_:)), for: .touchUpInside)
        releaseAgreementView.addSubview(btnReleaseAgreement)
        btnReleaseAgreement.enableAutolayout()
        btnReleaseAgreement.fixWidth(120)
        btnReleaseAgreement.trailingMargin(0)
        btnReleaseAgreement.belowView(8, to: separator12)
        btnReleaseAgreement.bottomMargin(8)
        
        
        //MARK:-  Approve Loan Details Scree
        approveLoanView.clipsToBounds = true
        approveLoanView.backgroundColor = .white
        approveLoanView.layer.cornerRadius = 8
        approveLoanView.layer.borderWidth = 1.5
        approveLoanView.layer.borderColor = Colors.separator.cgColor
        approveLoanView.addShadow()
        stackView.addSubview(approveLoanView)
        approveLoanView.enableAutolayout()
        approveLoanView.belowView(25, to: releaseAgreementView)
        approveLoanView.leadingMargin(20)
        approveLoanView.trailingMargin(20)
        approveLoanView.flexibleHeightGreater(40)
        
        
        lblStartProcess.text = "Please approve Start loan process"
        lblStartProcess.textAlignment = .left
        lblStartProcess.font = UIFont(name: Fonts.HelveticaNeueMedium, size: TextSize.title)
        lblStartProcess.textColor = Colors.darkGray
        approveLoanView.addSubview(lblStartProcess)
        lblStartProcess.enableAutolayout()
        lblStartProcess.topMargin(16)
        lblStartProcess.leadingMargin(20)
        lblStartProcess.trailingMargin(20)
        lblStartProcess.fixHeight(25)
        
        let separator7 = UIView()
        separator7.clipsToBounds = true
        separator7.backgroundColor = Colors.separator
        approveLoanView.addSubview(separator7)
        separator7.enableAutolayout()
        separator7.belowView(16, to: lblStartProcess)
        separator7.leadingMargin(0)
        separator7.trailingMargin(0)
        separator7.fixHeight(1.5)
        
        
        btnRejectLoanProcess.setTitle("Reject", for: .normal)
        btnRejectLoanProcess.setTitleColor(Colors.red, for: .normal)
        btnRejectLoanProcess.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle)
        btnRejectLoanProcess.addTarget(self, action: #selector(handleRejectLoanProcess(_:)), for: .touchUpInside)
        approveLoanView.addSubview(btnRejectLoanProcess)
        btnRejectLoanProcess.enableAutolayout()
        btnRejectLoanProcess.trailingMargin(20)
        btnRejectLoanProcess.belowView(8, to: separator7)
        btnRejectLoanProcess.bottomMargin(8)
        
        btnApproveLoanProcess.setTitle("Approve", for: .normal)
        btnApproveLoanProcess.setTitleColor(Colors.theme, for: .normal)
        btnApproveLoanProcess.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle)
        btnApproveLoanProcess.addTarget(self, action: #selector(handleApproveLoanProcess(_:)), for: .touchUpInside)
        approveLoanView.addSubview(btnApproveLoanProcess)
        btnApproveLoanProcess.enableAutolayout()
        btnApproveLoanProcess.add(toLeft: 20, of: btnRejectLoanProcess)
        btnApproveLoanProcess.centerY(to: btnRejectLoanProcess)
        
        kycDetailsView.clipsToBounds = true
        kycDetailsView.backgroundColor = .white
        kycDetailsView.layer.cornerRadius = 8
        kycDetailsView.layer.borderWidth = 1.5
        kycDetailsView.layer.borderColor = Colors.separator.cgColor
        kycDetailsView.addShadow()
        stackView.addSubview(kycDetailsView)
        kycDetailsView.enableAutolayout()
        kycDetailsView.belowView(25, to: approveLoanView)
        kycDetailsView.leadingMargin(20)
        kycDetailsView.trailingMargin(20)
        kycDetailsView.flexibleHeightGreater(40)
        kycDetailsView.bottomMargin(20)
        
        
        lblKyc.text = "Kyc Details"
        lblKyc.textAlignment = .left
        lblKyc.font = UIFont(name: Fonts.HelveticaNeueMedium, size: TextSize.title)
        lblKyc.textColor = Colors.darkGray
        kycDetailsView.addSubview(lblKyc)
        lblKyc.enableAutolayout()
        lblKyc.topMargin(10)
        lblKyc.leadingMargin(20)
        lblKyc.trailingMargin(20)
        lblKyc.fixHeight(25)
        
        let separator9 = UIView()
        separator9.clipsToBounds = true
        separator9.backgroundColor = Colors.separator
        kycDetailsView.addSubview(separator9)
        separator9.enableAutolayout()
        separator9.belowView(16, to: lblKyc)
        separator9.leadingMargin(0)
        separator9.trailingMargin(0)
        separator9.fixHeight(1.5)
        
        
        btnKycDetails.setTitle("View Details", for: .normal)
        btnKycDetails.setTitleColor(Colors.theme, for: .normal)
        btnKycDetails.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle)
        btnKycDetails.addTarget(self, action: #selector(handleKYCDetaikls(_:)), for: .touchUpInside)
        kycDetailsView.addSubview(btnKycDetails)
        btnKycDetails.enableAutolayout()
        btnKycDetails.trailingMargin(20)
        btnKycDetails.belowView(8, to: separator9)
        btnKycDetails.bottomMargin(8)
    }
    
    
    //MARK:- Refresh Control
    @objc func refresh(sender:AnyObject) {
        let locationDict = ["mobileNo": CommonModel.shared.mobileNumber,"latitude": CommonModel.shared.lat,"longitude": CommonModel.shared.long] as [String : Any]
        
        let mainDict = ["eventName": "startProcess", "encryption": false,"data":locationDict ] as [String : Any]
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
    
    func fetchKYCDetials() {
        Loader.shared.StartActivityIndicator(obj: self)
        
        let locationDict = ["mobileNo": self.objLoanSummary?.data?[0].mobileNo ?? "",
                            "LRID": self.objLoanSummary?.data?[0].lrid ?? "",
                            "enquiryId": self.objLoanSummary?.data?[0].datumID ?? ""] as [String : Any]
        
        let mainDict = ["eventName": "GETKYCFORAPPROVAL", "encryption": false,"data":locationDict ] as [String : Any]
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
    
    func setupCancelButton() {
        
        btnCancel.setTitle("Cancel", for: .normal)
        btnCancel.setTitleColor(.white, for: .normal)
        btnCancel.titleLabel?.font =  UIFont(name: Fonts.acuminProRegular, size: TextSize.title + 6)
        btnCancel.addTarget(self, action: #selector(handleCancelAppointnment(_:)), for: .touchUpInside)
        viewNavigationBarBase.addSubview(btnCancel)
        btnCancel.enableAutolayout()
        btnCancel.trailingMargin(16)
        btnCancel.centerY(to: lblNavTitleMiddle)
    }
    
    
    @objc func handleHeight() {
        stackView.sizeToFit()
        self.tblView.layoutIfNeeded()
        self.ornamentTblView.layoutIfNeeded()
        
        self.view.layoutIfNeeded()
        self.tblViewHeightConstraint?.constant = self.tblView.contentSize.height
        stackView.sizeToFit()
        
        self.tblViewHeightConstraint?.constant = self.tblView.contentSize.height
        self.ornamentTblViewHeightConstraint?.constant = self.ornamentTblView.contentSize.height
        stackView.sizeToFit()
        
        self.ornamentTblViewHeightConstraint?.constant = self.ornamentTblView.contentSize.height
        
    }
    
    fileprivate func setupData() {
        
        
        if ((self.objLoanSummary?.data?.count ?? 0) > 0) {
            
            //  self.objLoanSummary?.data?[0].staff?.reverse()
            
            btnLoan.setTitle(self.objLoanSummary?.data?[0].enquiryType ?? "", for: .normal)
            
            lblLoanId.text = self.objLoanSummary?.data?[0].lrid ?? ""
            let amount = Float(self.objLoanSummary?.data?[0].loanAmount ?? "") ?? 0
            let name = self.objLoanSummary?.data?[0].name ?? ""
            let pincode = self.objLoanSummary?.data?[0].pincode ?? ""
            let address = self.objLoanSummary?.data?[0].address ?? ""
            
            let date = (self.objLoanSummary?.data?[0].selectedDate ?? "").convertToMonth(in: "dd/MM/yyyy", out: "dd MMM yyyy")
            
            lblAmount.text =  "₹" + amount.delimiter
            lblDateTime.text = date + " " + (self.objLoanSummary?.data?[0].selectedTime ?? "")
            lblAddress.text = name + " " + address + " " + pincode
            
            // Loan Details
            self.loanDetailsView.isHidden = false
            let customerLoanNo = self.objLoanSummary?.data?[0].finalLoanDetails?.customerloanno ?? ""
            
            if (customerLoanNo == "") {
                self.btnCancel.isHidden = false
            }
            else {
                self.btnCancel.isHidden = true
            }
            
            if (self.objLoanSummary?.data?[0].scheme?.isEmpty ?? true) {
                self.loanDetailsView.isHidden = true
            }
            
            else {
                self.loanDetailsView.isHidden = false
                
                lblCustomerLoanNumber.attributedText = getAttrString("Customer Loan Number\t", self.objLoanSummary?.data?[0].finalLoanDetails?.customerloanno ?? "")
                
                lblProduct.attributedText = getAttrString("Product\n", self.objLoanSummary?.data?[0].finalLoanDetails?.productid ?? "")
                lblProductCat.attributedText = getAttrString("Product Category\n", self.objLoanSummary?.data?[0].finalLoanDetails?.productcategory ?? "")
                lblSchemeName.attributedText = getAttrString("Scheme Name\n", self.objLoanSummary?.data?[0].finalLoanDetails?.scheme ?? "")
                
                let finalloanamount = Int(self.objLoanSummary?.data?[0].finalLoanDetails?.finalloanamount ?? "0") ?? 0
                lblFinalLoanAmount.attributedText = getAttrString("Final Loan Amount\n", "₹ " + finalloanamount.delimiter)
                
                lblPaymnetFrequency.attributedText = getAttrString("Payment Frequency\n", self.objLoanSummary?.data?[0].finalLoanDetails?.repaytype ?? "")
                
                lblTenure.attributedText = getAttrString("Tenure\n", self.objLoanSummary?.data?[0].finalLoanDetails?.selectedTenure ?? "")
                lblInterestRate.attributedText = getAttrString("Interest Rate\n", "\((self.objLoanSummary?.data?[0].finalLoanDetails?.rate ?? ""))%")
                
                let emi = Int(self.objLoanSummary?.data?[0].finalLoanDetails?.emi ?? "") ?? 0
                lblMonthlyPayment.attributedText = getAttrString("Monthly Payment\n","₹ " + emi.delimiter)
            }
            
            if (self.objLoanSummary?.data?[0].selectedbankDetails?.bankName ?? "") == "" {
                self.bankDetailsView.isHidden = true
            }  
            
            //Bank Details
            
            if (self.objLoanSummary?.data?[0].ornaments?.isEmpty ?? true) {
                self.ornamentView.isHidden = true
            }
            else {
                self.ornamentView.isHidden = false
                self.ornamentTblView.reloadData()
            }
            
            if ((self.objLoanSummary?.data?[0].pledgeform ?? false) == false) {
                self.pledgeFormView.isHidden = true
            }
            else {
                self.pledgeFormView.isHidden = false
            }
            
            if ((self.objLoanSummary?.data?[0].ownershipagreement ?? false) == false) {
                self.ownershipAgreementView.isHidden = true
            }
            else {
                self.ownershipAgreementView.isHidden = false
            }
            
            if ((self.objLoanSummary?.data?[0].releaseagreement ?? false) == false) {
                self.releaseAgreementView.isHidden = true
            }
            else {
                self.releaseAgreementView.isHidden = false
            }
            
            // Approve Loan Process Popup
            self.approveLoanView.isHidden = true
            let status = self.objLoanSummary?.data?[0].status ?? ""
            
            switch status {
            
            case "AWAITINGKYCCONFIRMATION" :
                self.kycDetailsView.isHidden = false
                lblKyc.text = self.objLoanSummary?.confirmationText ?? ""
                break
                
                
            case "STARTLOANPROCESS":
                lblStartProcess.text = self.objLoanSummary?.confirmationText ?? ""
                self.approveLoanView.isHidden = false
                break
                
            case "SENDTOCUSTOMER":
                lblStartProcess.text = self.objLoanSummary?.confirmationText ?? ""
                self.approveLoanView.isHidden = false
                break
                
            case "AGREEMENTCREATION" :
                self.kycDetailsView.isHidden = false
                lblKyc.text = self.objLoanSummary?.confirmationText ?? ""
                break
                
            case "SUBMITOWNERSHIPAGREEMENT" :
                self.kycDetailsView.isHidden = false
                lblKyc.text = self.objLoanSummary?.confirmationText ?? ""
                btnKycDetails.setTitle("START PLEDGE FORM", for: .normal)
                break
                
            case "STARTPLEDGEFORM" :
                self.kycDetailsView.isHidden = false
                lblKyc.text = self.objLoanSummary?.confirmationText ?? ""
                //  btnKycDetails.setTitle("START PLEDGE FORM", for: .normal)
                break
                
            case "HANDEDOVERGOLDTOCUSTOMER":
                lblStartProcess.text = self.objLoanSummary?.confirmationText ?? ""
                self.approveLoanView.isHidden = false
                btnRejectLoanProcess.setTitle("DECLINE", for: .normal)
                btnApproveLoanProcess.setTitle("CONFIRM", for: .normal)
                break
                
            case "STARTREPLEDGEPROCESS":
                lblStartProcess.text = self.objLoanSummary?.confirmationText ?? ""
                self.approveLoanView.isHidden = false
                btnRejectLoanProcess.setTitle("REJECT", for: .normal)
                btnApproveLoanProcess.setTitle("APPROVE", for: .normal)
                break
                
            default:
                break
            }
            
            // finalDisbursmentView
            if (self.objLoanSummary?.data?[0].loandetails?.count ?? 0 > 0) {
                self.finalDisbursmentView.isHidden = false
                
                if (self.objLoanSummary?.data?[0].selectedbankDetails?.bankName ?? "") == "" {
                    self.bankDetailsView.isHidden = true
                }
                else {
                    self.bankDetailsView.isHidden = false
                }
                
                lblLoanAmount.text =   "₹ " + (Int(self.objLoanSummary?.data?[0].loandetails?[0].loanamount ?? "0") ?? 0).delimiter
                lblFinalInterestRate.text = (self.objLoanSummary?.data?[0].loandetails?[0].roi ?? "") + "%"
                lblEMI.text = "₹ " + (Int(self.objLoanSummary?.data?[0].loandetails?[0].emi ?? "0") ?? 0).delimiter
                lblTotalInterestAmount.text = "₹ " + (Int(self.objLoanSummary?.data?[0].loandetails?[0].totalinterest ?? "0") ?? 0).delimiter
                lblTotalPayableAmount.text = "₹ " + (Int(self.objLoanSummary?.data?[0].loandetails?[0].totalpayable ?? "0") ?? 0).delimiter
                
                lblBeneficiary.text = self.objLoanSummary?.data?[0].selectedbankDetails?.beneficaryName ?? ""
                lblBankName.text = self.objLoanSummary?.data?[0].selectedbankDetails?.bankName ?? ""
                lblIFSCCode.text = self.objLoanSummary?.data?[0].selectedbankDetails?.ifsc ?? ""
                lblAccountNumber.text = self.objLoanSummary?.data?[0].selectedbankDetails?.accountNo ?? ""
                let bankName = self.objLoanSummary?.data?[0].selectedbankDetails?.branchname ?? ""
                
                if (bankName == "") {
                    lblBranchName.text = ""
                    lblTempBranchName.text = ""
                }
                else {
                    lblBranchName.text = bankName
                    lblTempBranchName.text = "Branch Name"
                }
            }
            else {
                self.finalDisbursmentView.isHidden = true
                self.bankDetailsView.isHidden = true
            }
            
            
            // Nominee View
            let nomineeName = self.objLoanSummary?.data?[0].selectedbankDetails?.nomineeName ?? ""
            let nomineeRelation = self.objLoanSummary?.data?[0].selectedbankDetails?.nomineeRelation ?? ""
            let nomineeDOB = self.objLoanSummary?.data?[0].selectedbankDetails?.nomineeDOB ?? ""
            let nomineeAddress1 = self.objLoanSummary?.data?[0].selectedbankDetails?.nomineeAddress1 ?? ""
            let nomineeAddress2 = self.objLoanSummary?.data?[0].selectedbankDetails?.nomineeAddress2 ?? ""
            let nomineeAddressCity = self.objLoanSummary?.data?[0].selectedbankDetails?.nomineeCity ?? ""
            let nomineeAddressState = self.objLoanSummary?.data?[0].selectedbankDetails?.nomineeState ?? ""
            let nomineeAddressPincode = self.objLoanSummary?.data?[0].selectedbankDetails?.nomineePincode ?? ""
            
            if ((nomineeName == "") && (nomineeRelation == "")  && (nomineeDOB == "") && (nomineeAddress1 == "")) {
                self.nomineeDetailsView.isHidden = true
            }
            else {
                self.nomineeDetailsView.isHidden = false
                lblNomineeName.text = nomineeName
                lblNomineeRelation.text = nomineeRelation
                lblNomineeDOB.text = nomineeDOB
                lblNomineeAddress.text = nomineeAddress1 + "," + nomineeAddress2 + "," + nomineeAddressCity + "," + nomineeAddressState + "-" + nomineeAddressPincode
            }
            
            self.tblView.reloadData()
            self.ornamentTblView.reloadData()
            
            self.stackView.sizeToFit()
            self.perform(#selector(handleHeight), with: self, with: 0.3)
            self.stackView.sizeToFit()
        }
    }
    
    
    //MARK:- Handle Button Tapped
    @objc func handleKYCDetaikls(_ sender : UIButton) {
        
        if ((self.objLoanSummary?.data?[0].status ?? "") == "AGREEMENTCREATION") {
            Loader.shared.StartActivityIndicator(obj: self)
            let locationDict = ["mobileNo": self.objLoanSummary?.data?[0].mobileNo ?? "",
                                "lrid": self.objLoanSummary?.data?[0].lrid ?? "",
                                "enquiryId": self.objLoanSummary?.data?[0].datumID ?? ""] as [String : Any]
            let mainDict = ["eventName": "STARTAGREEMENTCREATION", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
        }
        
        else if ((self.objLoanSummary?.data?[0].status ?? "") == "SUBMITOWNERSHIPAGREEMENT") || ((self.objLoanSummary?.data?[0].status ?? "") == "STARTPLEDGEFORM"){
            Loader.shared.StartActivityIndicator(obj: self)
            let locationDict = ["mobileNo": self.objLoanSummary?.data?[0].mobileNo ?? "",
                                "lrid": self.objLoanSummary?.data?[0].lrid ?? "",
                                "enquiryId": self.objLoanSummary?.data?[0].datumID ?? ""] as [String : Any]
            let mainDict = ["eventName": "STARTPLEDGEFORM", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
        }
        
        else {
            fetchKYCDetials()
        }
    }
    
    @objc func handleNewLoan(_ sender : UIButton) {
        
    }
    
    
    @objc func handleCallBtn(_ sender : UIButton) {
        let mobile = self.objLoanSummary?.data?[0].staff?[sender.tag].phoneNo ?? ""
        if let url = NSURL(string: "tel://\(mobile)"), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url as URL)
        }
    }
    
    @objc func backBtnTapped(_ sender : UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
        removeFromParent()
    }
    
    @objc func handlebtnOwnershipAgreementBtn(_ sender : UIButton) {
        Loader.shared.StartActivityIndicator(obj: self)
        let locationDict = ["mobileNo": self.objLoanSummary?.data?[0].mobileNo ?? "",
                            "lrid": self.objLoanSummary?.data?[0].lrid ?? "",
                            "enquiryId": self.objLoanSummary?.data?[0].datumID ?? "",
                            "docType": "ownership"] as [String : Any]
        
        let mainDict = ["eventName": "GETSIGNEDAGREEMENT", "encryption": false,"data":locationDict ] as [String : Any]
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
    
    
    @objc func handlebtnPledgeForm(_ sender : UIButton) {
        Loader.shared.StartActivityIndicator(obj: self)
        let locationDict = ["mobileNo": self.objLoanSummary?.data?[0].mobileNo ?? "",
                            "lrid": self.objLoanSummary?.data?[0].lrid ?? "",
                            "enquiryId": self.objLoanSummary?.data?[0].datumID ?? "",
                            "docType": "pledge"] as [String : Any]
        
        let mainDict = ["eventName": "GETSIGNEDAGREEMENT", "encryption": false,"data":locationDict ] as [String : Any]
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
    
    
    @objc func handleReleaseAgreementBtn(_ sender : UIButton) {
        Loader.shared.StartActivityIndicator(obj: self)
        let locationDict = ["mobileNo": self.objLoanSummary?.data?[0].mobileNo ?? "",
                            "lrid": self.objLoanSummary?.data?[0].lrid ?? "",
                            "enquiryId": self.objLoanSummary?.data?[0].datumID ?? "",
                            "docType": "release"] as [String : Any]
        
        let mainDict = ["eventName": "GETSIGNEDAGREEMENT", "encryption": false,"data":locationDict ] as [String : Any]
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
    
    
    
    //MARK:- Locate Button Clicked
    
    @objc func HandlelocateBtnClicked(_ sender : UIButton) {
        let vc = MapVC()
        vc.branchLat = self.objLoanSummary?.data?[0].staff?[0].branch?.latitude ?? ""
        vc.branchLong = self.objLoanSummary?.data?[0].staff?[0].branch?.longitude ?? ""
        
        vc.customerLat = self.objLoanSummary?.data?[0].selectedLatitude ?? ""
        vc.customerLong = self.objLoanSummary?.data?[0].selectedLongitude ?? ""
        
        vc.staff = self.objLoanSummary?.data?[0].staff
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    //MARK:- API Calling
    @objc func handleApproveLoanProcess(_ sender : UIButton) {
        
        let status = self.objLoanSummary?.data?[0].status ?? ""
        
        switch status {
        
        case "STARTLOANPROCESS":
            Loader.shared.StartActivityIndicator(obj: self)
            let locationDict = ["mobileNo": self.objLoanSummary?.data?[0].mobileNo ?? "",
                                "LRID": self.objLoanSummary?.data?[0].lrid ?? "",
                                "enquiryId": self.objLoanSummary?.data?[0].datumID ?? "",
                                "action": "approve"] as [String : Any]
            let mainDict = ["eventName": "CONFIRMSTARTPROCESS", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
            break
            
        case "SENDTOCUSTOMER":
            Loader.shared.StartActivityIndicator(obj: self)
            let locationDict = ["mobileNo": self.objLoanSummary?.data?[0].mobileNo ?? "",
                                "LRID": self.objLoanSummary?.data?[0].lrid ?? "",
                                "enquiryId": self.objLoanSummary?.data?[0].datumID ?? "",
                                "action": "approve"] as [String : Any]
            let mainDict = ["eventName": "CONFIRMLOANDETAILSCUSTOMER", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
            break
            
        case "HANDEDOVERGOLDTOCUSTOMER":
            Loader.shared.StartActivityIndicator(obj: self)
            let locationDict = ["mobileNo": self.objLoanSummary?.data?[0].mobileNo ?? "",
                                "LRID": self.objLoanSummary?.data?[0].lrid ?? "",
            ] as [String : Any]
            let mainDict = ["eventName": "STARTGOLDHANDOVER", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
            break
            
            
        case "STARTREPLEDGEPROCESS":
            Loader.shared.StartActivityIndicator(obj: self)
            let locationDict = ["mobileNo": self.objLoanSummary?.data?[0].mobileNo ?? "",
                                "LRID": self.objLoanSummary?.data?[0].lrid ?? "",
                                "enquiryId": self.objLoanSummary?.data?[0].datumID ?? "",
                                "action": "approve"] as [String : Any]
            let mainDict = ["eventName": "CONFIRMEDREPLEDGEPROCESS", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
            break
            
            
        default:
            break
        }
    }
    
    @objc func handleCancelAppointnment(_ sender : UIButton) {
        Loader.shared.StartActivityIndicator(obj: self)
        
        let locationDict = ["mobileNo": self.objLoanSummary?.data?[0].mobileNo ?? "",
                            "LRID": self.objLoanSummary?.data?[0].lrid ?? "",
                            "enquiryId": self.objLoanSummary?.data?[0].datumID ?? ""] as [String : Any]
        
        let mainDict = ["eventName": "STARTCANCEL", "encryption": false,"data":locationDict ] as [String : Any]
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
    
    
    @objc func handleRejectLoanProcess(_ sender : UIButton) {
        
        let status = self.objLoanSummary?.data?[0].status ?? ""
        switch status {
        
        case "STARTLOANPROCESS":
            Loader.shared.StartActivityIndicator(obj: self)
            let locationDict = ["mobileNo": self.objLoanSummary?.data?[0].mobileNo ?? "",
                                "LRID": self.objLoanSummary?.data?[0].lrid ?? "",
                                "enquiryId": self.objLoanSummary?.data?[0].datumID ?? "",
                                "action": "reject"] as [String : Any]
            let mainDict = ["eventName": "CONFIRMSTARTPROCESS", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
            
            break
            
        case "SENDTOCUSTOMER":
            Loader.shared.StartActivityIndicator(obj: self)
            let locationDict = ["mobileNo": self.objLoanSummary?.data?[0].mobileNo ?? "",
                                "LRID": self.objLoanSummary?.data?[0].lrid ?? "",
                                "enquiryId": self.objLoanSummary?.data?[0].datumID ?? "",
                                "action": "reject"] as [String : Any]
            let mainDict = ["eventName": "CONFIRMLOANDETAILSCUSTOMER", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
            break
            
            
        case "HANDEDOVERGOLDTOCUSTOMER":
            Loader.shared.StartActivityIndicator(obj: self)
            let locationDict = ["mobileNo": self.objLoanSummary?.data?[0].mobileNo ?? "",
                                "LRID": self.objLoanSummary?.data?[0].lrid ?? "",
                                "enquiryId": self.objLoanSummary?.data?[0].datumID ?? "",
                                "signature" : "",
                                "action": "decline"] as [String : Any]
            let mainDict = ["eventName": "CONFIRMGOLDHANDOVER", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
            break
            
        case "STARTREPLEDGEPROCESS":
            Loader.shared.StartActivityIndicator(obj: self)
            let locationDict = ["mobileNo": self.objLoanSummary?.data?[0].mobileNo ?? "",
                                "LRID": self.objLoanSummary?.data?[0].lrid ?? "",
                                "enquiryId": self.objLoanSummary?.data?[0].datumID ?? "",
                                "action": "reject"] as [String : Any]
            let mainDict = ["eventName": "CONFIRMEDREPLEDGEPROCESS", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
            break
            
        default :
            break
        }
    }
    
    //MARK:- Handle Responce
    @objc func gotoReschedule(notification: Notification) {
        
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(RescheduleModel.self, from: jsonData!)
            if (data.status ?? false) {
                let vc = RescheduleVC()
                vc.objReschedule = data
                vc.objLoanSummary = self.objLoanSummary
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                self.showToast(message: data.message ?? "")
            }
        }
        catch let error {
            //print(error)
        }
    }
    
    @objc func getKYCApprove(notification: Notification) {
        self.kycDetailsView.isHidden = true
        
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(KycApprovalModel.self, from: jsonData!)
            self.objKycApproval = data
            if (data.status ?? false) {
                if data.data != nil {
                    let vc =  KYCDetailsVC()
                    vc.objKycDetails = data
                    vc.objLoanSummary = self.objLoanSummary
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else {
                    self.showToast(message: data.message ?? "")
                }
            }
            else {
                
            }
        }
        catch let error {
           // print(error)
        }
    }
    
    @objc func handleCONFIRMSTARTPROCESS(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        
        if let dict = notification.userInfo as NSDictionary? {
            if let data = dict["data"]  as? [String:Any] {
                if let msg = data["message"] as? String {
                    showToast(message: msg )
                    self.approveLoanView.isHidden = true
                }
            }
        }
    }
    
    @objc func handleCONFIRMLOANDETAILSCUSTOMER(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        
        if let dict = notification.userInfo as NSDictionary? {
            if let data = dict["data"]  as? [String:Any] {
                if let msg = data["message"] as? String {
                    showToast(message: msg )
                    self.approveLoanView.isHidden = true
                }
            }
        }
    }
    
    
    @objc func HandleSTARTAGREEMENTCREATION(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        if let dict = notification.userInfo as NSDictionary? {
            if let _ = dict["data"]  as? [String:Any] {
                self.kycDetailsView.isHidden = true
                let vc = WebViewPDFVC()
                vc.objSummary = self.objLoanSummary
                vc.status = "agreement"
                vc.dict = dict
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    @objc func HandleSTARTAGREEMENT(notification: Notification) {
        // goto webView
        Loader.shared.StopActivityIndicator(obj: self)
        if let dict = notification.userInfo as NSDictionary? {
            if let data = dict["data"]  as? [String:Any] {
                self.kycDetailsView.isHidden = true
                if let str = data["agreement"] as? String {
                    let vc = ViewAgreementVC()
                    vc.agreementStr = str
                    vc.status = "agreement"
                    vc.navTitle = data["docType"] as? String ?? ""
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    @objc func HandleGetPledgeForm(notification: Notification) {
        /* if let dict = notification.userInfo as NSDictionary? {
         if let data = dict["data"]  as? [String:Any] {
         self.kycDetailsView.isHidden = true
         if let str = data["pledgeform"] as? String {
         let vc = ViewAgreementVC()
         vc.status = "pledge"
         vc.agreementStr = str
         vc.navTitle = data["docType"] as? String ?? ""
         self.navigationController?.pushViewController(vc, animated: true)
         }
         }
         }*/
        Loader.shared.StopActivityIndicator(obj: self)
        if let dict = notification.userInfo as NSDictionary? {
            if let _ = dict["data"]  as? [String:Any] {
                self.kycDetailsView.isHidden = true
                let vc = WebViewPDFVC()
                vc.objSummary = self.objLoanSummary
                vc.dict = dict
                vc.status = "plegde"
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    
    @objc func gotoStartLoan(notification: Notification) {
        refreshControl?.endRefreshing()
        Loader.shared.StopActivityIndicator(obj: self)
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(PincodeModel.self, from: jsonData!)
            if (data.status ?? false) {
                self.navigationController?.popToRootViewController(animated: true)
            }
            else {
                self.showToast(message: data.message ?? "")
            }
        }
        catch let error {
            // print(error)
        }
    }
    
    @objc func gotoLoanSummary(notification: Notification) {
        
        refreshControl?.endRefreshing()
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(LoanSummaryModel.self, from: jsonData!)
            if (data.status ?? false) {
                self.objLoanSummary = nil
                self.objLoanSummary = data
                setupData()
            }
            else {
                self.showToast(message: data.message ?? "")
            }
        }
        catch let error {
            // print(error)
        }
    }
    
    
    @objc func CONFIRMGOLDHANDOVER(notification : Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        if let dict = notification.userInfo as NSDictionary? {
            if let msg = dict["message"] as? String {
                showToast(message: msg )
                self.approveLoanView.isHidden = true
            }
        }
    }
    
    @objc func startHandOver(notification: Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        if let dict = notification.userInfo as NSDictionary? {
            if let _ = dict["data"]  as? [String:Any] {
                self.kycDetailsView.isHidden = true
                let vc = WebViewPDFVC()
                vc.objSummary = self.objLoanSummary
                vc.status = "handover"
                vc.dict = dict
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}


extension LoanSummaryVC : UITableViewDelegate, UITableViewDataSource {
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (tableView.tag == 100) {
            return 0
        }
        else {
            return UITableView.automaticDimension
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if (tableView.tag == 100) {
            if (self.objLoanSummary?.data?[0].status ?? "") == "STARTEDTOCUSTOMER" {
                return UITableView.automaticDimension
            }
            else {
                return 0
            }
        }
        else {
            return 0
        }
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if (tableView.tag == 100) {
            let hView = UIView()
            hView.clipsToBounds = true
            
            let separator1 = UIView()
            separator1.clipsToBounds = true
            separator1.backgroundColor = Colors.separator
            hView.addSubview(separator1)
            separator1.enableAutolayout()
            separator1.topMargin(0)
            separator1.leadingMargin(0)
            separator1.trailingMargin(0)
            separator1.fixHeight(1.5)
            
            let btnLocate = UIButton()
            btnLocate.setTitle("LOCATE", for: .normal)
            btnLocate.setTitleColor(Colors.theme, for: .normal)
            btnLocate.titleLabel?.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.title)
            btnLocate.addTarget(self, action: #selector(HandlelocateBtnClicked(_:)), for: .touchUpInside)
            hView.addSubview(btnLocate)
            btnLocate.enableAutolayout()
            btnLocate.belowView(8, to: separator1)
            btnLocate.trailingMargin(16)
            btnLocate.fixHeight(40)
            btnLocate.bottomMargin(0)
            return hView
        }
        else {
            return UIView()
        }
    }
    
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        if (tableView.tag == 100) {
            return 1
        }
        else {
            return 1
        }
    }
    
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if (tableView.tag == 100) {
            return UIView()
        }
        else {
            
            let hView = UIView()
            hView.clipsToBounds = true
            
            let width = (screenWidth - 40) / 4
            let lblTempItem = PaddingLabel()
            lblTempItem.backgroundColor = Colors.viewBackground
            lblTempItem.text = "Items"
            lblTempItem.textAlignment = .center
            lblTempItem.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle - 1)
            lblTempItem.textColor = Colors.gray
            hView.addSubview(lblTempItem)
            lblTempItem.enableAutolayout()
            lblTempItem.topMargin(0)
            lblTempItem.leadingMargin(0)
            lblTempItem.fixWidth(width - 10)
            lblTempItem.fixHeight(40)
            
            let lblTempKarat = PaddingLabel()
            lblTempKarat.backgroundColor = Colors.viewBackground
            lblTempKarat.text = "Karat"
            lblTempKarat.textAlignment = .center
            lblTempKarat.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle - 1)
            lblTempKarat.textColor = Colors.gray
            hView.addSubview(lblTempKarat)
            lblTempKarat.enableAutolayout()
            lblTempKarat.topMargin(0)
            lblTempKarat.add(toRight: 0, of: lblTempItem)
            lblTempKarat.fixWidth(width - 10)
            lblTempKarat.fixHeight(40)
            
            let lblTempNet = PaddingLabel()
            lblTempNet.backgroundColor = Colors.viewBackground
            lblTempNet.text = "Net wt(grms)"
            lblTempNet.adjustsFontSizeToFitWidth = true
            lblTempNet.textAlignment = .center
            lblTempNet.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle - 1)
            lblTempNet.textColor = Colors.gray
            hView.addSubview(lblTempNet)
            lblTempNet.enableAutolayout()
            lblTempNet.topMargin(0)
            lblTempNet.add(toRight: 0, of: lblTempKarat)
            lblTempNet.fixWidth(width)
            lblTempNet.fixHeight(40)
            
            let lblTempGrs = PaddingLabel()
            lblTempGrs.backgroundColor = Colors.viewBackground
            lblTempGrs.text = "GRS wt(grms)"
            lblTempGrs.textAlignment = .center
            lblTempGrs.font = UIFont(name: Fonts.HelveticaRegular, size: TextSize.subTitle - 1)
            lblTempGrs.textColor = Colors.gray
            hView.addSubview(lblTempGrs)
            lblTempGrs.enableAutolayout()
            lblTempGrs.topMargin(0)
            lblTempGrs.add(toRight: 0, of: lblTempNet)
            lblTempGrs.trailingMargin(0)
            lblTempGrs.fixHeight(40)
            lblTempGrs.bottomMargin(0)
            return hView
        }
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView.tag == 100) {
            return self.objLoanSummary?.data?[0].staff?.count ?? 0
        }
        else {
            return self.objLoanSummary?.data?[0].ornamentsummary?.count ?? 0
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (tableView.tag == 100) {
            let cell  = UserCell()
            cell.selectionStyle = .none
            cell.setupData(data: (self.objLoanSummary?.data?[0].staff?[indexPath.row])!)
            cell.btnCall.tag = indexPath.row
            cell.btnCall.addTarget(self, action: #selector(handleCallBtn(_:)), for: .touchUpInside)
            
            if indexPath.row == ((self.objLoanSummary?.data?[0].staff?.count ?? 0) - 1) {
                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: screenWidth)
            }
            
            self.view.layoutIfNeeded()
            return cell
        }
        
        else {
            
            let cell  = OrnamentCell()
            cell.selectionStyle = .none
            cell.lblItem.text = "\(objLoanSummary?.data?[0].ornamentsummary?[indexPath.row].items ?? 0)"
            cell.lblKarat.text = objLoanSummary?.data?[0].ornamentsummary?[indexPath.row].karats ?? ""
            cell.lblNet.text = "\(objLoanSummary?.data?[0].ornamentsummary?[indexPath.row].weight ?? 0)"
            cell.lblGrs.text = "\(objLoanSummary?.data?[0].ornamentsummary?[indexPath.row].grossweight ?? 0)"
            
            if indexPath.row == ((self.objLoanSummary?.data?[0].ornamentsummary?.count ?? 0) - 1) {
                cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: screenWidth)
            }
            return cell
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if (tableView.tag == 101) {
            if ((objLoanSummary?.data?[0].ornaments?.count ?? 0) > 0 ){
                let popup = OrnamentPopup()
                popup.frame = view.bounds
                popup.arrData = objLoanSummary?.data?[0].ornaments
                popup.createUI()
                view.window?.addSubview(popup)
            }
        }
    }
}
