//
//  ImageViewVC.swift
//  Muthoot Track
//
//  Created by Sagar on 03/07/21.
//

import UIKit

class ImageViewVC: BaseController, UIWebViewDelegate, UIScrollViewDelegate {
    
    var imageUrl = ""
    var navTitle = ""
    let iVGallary = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        let scrollViewMain = UIScrollView()
        scrollViewMain.maximumZoomScale = 6
        scrollViewMain.minimumZoomScale = 1
        scrollViewMain.zoomScale = 0.5
        scrollViewMain.delegate = self
        view.addSubview(scrollViewMain)
        
        scrollViewMain.enableAutolayout()
        scrollViewMain.leadingMargin(0)
        scrollViewMain.fixWidth(screenWidth)
        scrollViewMain.bottomMargin(0)
        scrollViewMain.topMargin(0)
        
        iVGallary.frame = .zero
        iVGallary.contentMode = .scaleAspectFit
        scrollViewMain.addSubview(iVGallary)
        iVGallary.enableAutolayout()
        iVGallary.fixWidth(screenWidth-20)
        iVGallary.leadingMargin(10)
        iVGallary.centerY()
        iVGallary.topMargin(10)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
        createNavigationBarWithBack("")
        setupData()
    }
    
    func setupData() {
        let image = imageUrl
        let str = image.replacingOccurrences(of: "data:image/png;base64,", with: "",options: .caseInsensitive)
        
        let newImageData = Data(base64Encoded: str)
        if let img = newImageData {
            self.iVGallary.image = UIImage(data: img)
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return iVGallary
    }
}
