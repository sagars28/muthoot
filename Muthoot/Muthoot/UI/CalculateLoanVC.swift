//
//  CalculateLoanVC.swift
//  Muthoot Track
//
//  Created by Suraj Singh on 5/6/21.
//

import UIKit
import SocketIO

class CalculateLoanVC : BaseController {
    
    let lblService = UILabel()
    let scrollViewMain = CustomeScrollView()
    let lblDesc = UILabel()
    let txtAmount = UITextField()
    let txtAmount2 = UITextField()
    let txtCaret = UITextField()
    let txtGold = UITextField()
    let txtGold2 = UITextField()
    let lblSliderMaxValue = UILabel()
    let lblSliderMinValue = UILabel()
    let slider = CustomSlider()
    var sliderLableP = UILabel()
    let arrowImageView = UIImageView()
    var lastIndex = Int()
    let viewBack = UIView()
    
    let lblAmount2 = UILabel()
    
    var objGoldRate : GoldRateModel?
    var objSlot : AvailableSlotModel?
    
    var enquiryId = ""
    
    var finalAmount : Float = 0.0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        setupUI()
    }
    
    override public var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13.0, *) {
            return .lightContent
        } else {
            return .lightContent
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
        //createNavigationBar("Calculate Loan Amount")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoAddressList(notification:)), name: Notification.Name("SendLoanAmountResponse"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleError(notification:)), name: Notification.Name("SendLoanAmountError"), object: nil)
        
        setupData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
    
    fileprivate func setupUI() {
        
        // Bottom View for back and next
        let bottomView = BottomBtnBar()
        bottomView.btnBack.addTarget(self, action: #selector(backBtnTapped(_:)), for: .touchUpInside)
        bottomView.btnNext.addTarget(self, action: #selector(nextBtnTapped(_:)), for: .touchUpInside)
        bottomView.setView()
        view.addSubview(bottomView)
        bottomView.enableAutolayout()
        bottomView.centerX()
        bottomView.fixWidth(screenWidth)
        bottomView.bottomMargin(Constraints.bottom)
        bottomView.fixHeight(50)
        
        let headerView = TopPgrogess()
        headerView.backgroundColor = .white
        headerView.setView()
        headerView.lblOne.isHidden = true
        headerView.ivFirst.backgroundColor = .white
        if #available(iOS 13.0, *) {
            headerView.ivFirst.image = UIImage(named: "rightWhite", in: Bundle(for: type(of: self)), with: nil)
        } else {
            headerView.ivFirst.image = UIImage(named: "rightWhite", in: Bundle(for: type(of: self)), compatibleWith : nil)
        }
        headerView.ivSecond.backgroundColor = Colors.theme
        
        headerView.btnOne.addTarget(self, action: #selector(handleBack(_:)), for: .touchUpInside)
        headerView.btnThree.addTarget(self, action: #selector(nextBtnTapped(_:)), for: .touchUpInside)
        headerView.btnFour.addTarget(self, action: #selector(nextBtnTapped(_:)), for: .touchUpInside)
        
        view.addSubview(headerView)
        headerView.enableAutolayout()
        headerView.fixHeight(80)
        headerView.leadingMargin(0)
        headerView.trailingMargin(0)
        headerView.topMargin(self.topbarHeight)
        
        
        scrollViewMain.backgroundColor = UIColor.white
        scrollViewMain.bounces = false
        scrollViewMain.isScrollEnabled = true
        scrollViewMain.showsVerticalScrollIndicator = true
        view.addSubview(scrollViewMain)
        scrollViewMain.enableAutolayout()
        scrollViewMain.leadingMargin(0)
        scrollViewMain.fixWidth(screenWidth)
        scrollViewMain.belowView(0, to: headerView)
        scrollViewMain.aboveView(0, to: bottomView)
        
        // Main View Under Scroll View
        viewBack.backgroundColor = .white
        scrollViewMain.addSubview(viewBack)
        viewBack.enableAutolayout()
        viewBack.centerX()
        viewBack.fixWidth(screenWidth)
        viewBack.topMargin(0)
        viewBack.bottomMargin(0)
        viewBack.flexibleHeightGreater(10)
        
        lblService.text = "Approximate"
        lblService.numberOfLines = 0
        lblService.textAlignment = .center
        viewBack.addSubview(lblService)
        lblService.enableAutolayout()
        lblService.topMargin(30)
        lblService.leadingMargin(50)
        lblService.trailingMargin(50)
        lblService.flexibleHeightGreater(20)
        
        
        let boxView = UIView()
        boxView.backgroundColor = .white
        boxView.layer.cornerRadius = 16
        boxView.layer.borderWidth = 1
        boxView.layer.borderColor = UIColor.black.cgColor
        viewBack.addSubview(boxView)
        boxView.enableAutolayout()
        boxView.belowView(30, to: lblService)
        boxView.leadingMargin(30)
        boxView.trailingMargin(30)
        boxView.flexibleHeightGreater(10)
        
        
        sliderLableP.text = "₹ 0"
        sliderLableP.clipsToBounds = true
        sliderLableP.textAlignment = NSTextAlignment.center
        sliderLableP.textColor = UIColor.black
        sliderLableP.font = UIFont(name: Fonts.HelveticaRegular, size: 20.0)
        boxView.addSubview(sliderLableP)
        sliderLableP.enableAutolayout()
        sliderLableP.topMargin(20)
        sliderLableP.centerX()
        sliderLableP.fixHeight(20)
        
        slider.tintColor = Colors.theme
        slider.thumbTintColor =  Colors.gray
        slider.maximumTrackTintColor = Colors.separator
        slider.minimumValue = 50000.0
        slider.maximumValue = 100000.0
        slider.isContinuous = true
        slider.value = 4.0
        slider.addTarget(self, action: #selector(sliderValueDidChange(sender:)), for: .valueChanged)
        boxView.addSubview(slider)
        slider.enableAutolayout()
        slider.belowView(10, to: sliderLableP)
        slider.leadingMargin(30)
        slider.trailingMargin(30)
        slider.flexibleHeightGreater(20)
        
        
        lblSliderMinValue.text = "₹ " + ""
        lblSliderMinValue.textAlignment = .left
        lblSliderMinValue.font = UIFont(name: Fonts.HelveticaRegular, size: 17)
        boxView.addSubview(lblSliderMinValue)
        lblSliderMinValue.enableAutolayout()
        lblSliderMinValue.belowView(12, to: slider)
        lblSliderMinValue.leadingMargin(30)
        lblSliderMinValue.fixWidth((screenWidth - 40) / 2)
        
        lblSliderMaxValue.text = "₹ " + ""
        lblSliderMaxValue.textAlignment = .right
        lblSliderMaxValue.font = UIFont(name: Fonts.HelveticaRegular, size: 17)
        boxView.addSubview(lblSliderMaxValue)
        lblSliderMaxValue.enableAutolayout()
        lblSliderMaxValue.belowView(12, to: slider)
        lblSliderMaxValue.trailingMargin(30)
        lblSliderMaxValue.fixWidth((screenWidth - 40) / 2)
        

        txtGold.keyboardType = .decimalPad
        txtGold.delegate = self
        txtGold.tag = 100
        txtGold.text = ""
        txtGold.tintColor = Colors.theme
        txtGold.setLeftPaddingPoints(8)
        txtGold.textColor = UIColor.black
        txtGold.backgroundColor = .white
        txtGold.layer.borderColor = UIColor.black.cgColor
        txtGold.layer.borderWidth = 1
        txtGold.layer.cornerRadius = 8
        txtGold.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title + 2)
        txtGold.textAlignment = .center
        txtGold.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        boxView.addSubview(txtGold)
        txtGold.enableAutolayout()
        txtGold.trailingMargin(20)
        txtGold.belowView(20, to: lblSliderMaxValue)
        txtGold.fixWidth(120)
        txtGold.fixHeight(40)
        let toolbar = UIToolbar().ToolbarPiker(mySelect: #selector(handToolBar), title: "Done")
        txtGold.inputAccessoryView = toolbar
        txtGold.bottomMargin(20)
        
        let lblGoldReq = UILabel()
        lblGoldReq.adjustsFontSizeToFitWidth = true
        lblGoldReq.text = "Gold required in grams:"
        lblGoldReq.textAlignment = .right
        lblGoldReq.font = UIFont(name: Fonts.HelveticaRegular, size: 17)
        boxView.addSubview(lblGoldReq)
        lblGoldReq.enableAutolayout()
        lblGoldReq.add(toLeft: 8, of: txtGold)
        lblGoldReq.centerY(to: txtGold)
        lblGoldReq.leadingMargin(30)
        
        let lblOR = UILabel()
        lblOR.text = "---------- OR ----------"
        lblOR.textAlignment = .center
        lblOR.font = UIFont(name: Fonts.latoRegular, size: 22)
        lblOR.textColor = UIColor.darkGray
        viewBack.addSubview(lblOR)
        lblOR.enableAutolayout()
        lblOR.belowView(20, to: boxView)
        lblOR.centerX()
        lblOR.flexibleHeightGreater(20)
        
        
        let amountBackView = UIView()
        amountBackView.clipsToBounds = true
        amountBackView.layer.cornerRadius = 20
        amountBackView.backgroundColor = .white
        amountBackView.addShadow()
        viewBack.addSubview(amountBackView)
        amountBackView.enableAutolayout()
        amountBackView.leadingMargin(25)
        amountBackView.trailingMargin(25)
        amountBackView.belowView(20, to: lblOR)
        amountBackView.flexibleHeightGreater(20)
        
        let iVamount = UIImageView()
        iVamount.clipsToBounds = true
        iVamount.contentMode = .scaleToFill
        if #available(iOS 13.0, *) {
            iVamount.image = UIImage(named: "gradientBack2", in: Bundle(for: type(of: self)), with: nil) ?? UIImage()
        }
        else {
            iVamount.image = UIImage(named: "gradientBack2", in: Bundle(for: type(of: self)), compatibleWith : nil)
        }
        
        iVamount.layer.cornerRadius = 8
        iVamount.addShadow()
        amountBackView.addSubview(iVamount)
        iVamount.enableAutolayout()
        iVamount.topMargin(0)
        iVamount.bottomMargin(0)
        iVamount.leadingMargin(-8)
        iVamount.trailingMargin(-8)
        
        
        let lblWeight = UILabel()
        lblWeight.numberOfLines = 0
        lblWeight.text = "Provide the total weight of your gold to know the max loan amount you are eligible for :-"
        lblWeight.textAlignment = .left
        lblWeight.font = UIFont(name: Fonts.latoRegular, size: 17)
        lblWeight.textColor = UIColor.white
        amountBackView.addSubview(lblWeight)
        lblWeight.enableAutolayout()
        lblWeight.topMargin(25)
        lblWeight.leadingMargin(25)
        lblWeight.trailingMargin(25)
        
        txtGold2.keyboardType = .decimalPad
        txtGold2.delegate = self
        txtGold2.tag = 100
        txtGold2.text = ""
        txtGold2.tintColor = Colors.theme
        txtGold2.setLeftPaddingPoints(8)
        txtGold2.textColor = UIColor.black
        txtGold2.backgroundColor = .white
        txtGold2.layer.borderColor = UIColor.black.cgColor
        txtGold2.layer.borderWidth = 1
        txtGold2.layer.cornerRadius = 8
        txtGold2.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title + 2)
        txtGold2.textAlignment = .center
        txtGold2.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        amountBackView.addSubview(txtGold2)
        txtGold2.enableAutolayout()
        txtGold2.trailingMargin(20)
        txtGold2.belowView(20, to: lblWeight)
        txtGold2.trailingMargin(30)
        txtGold2.fixWidth(120)
        txtGold2.fixHeight(40)
        let toolbar2 = UIToolbar().ToolbarPiker(mySelect: #selector(handToolBar), title: "Done")
        txtGold2.inputAccessoryView = toolbar2
        
        let lblWeightInGram = UILabel()
        lblWeightInGram.adjustsFontSizeToFitWidth = true
        lblWeightInGram.numberOfLines = 0
        lblWeightInGram.text = "Weight of gold (in gm)"
        lblWeightInGram.textAlignment = .left
        lblWeightInGram.font = UIFont(name: Fonts.latoRegular, size: 17)
        lblWeightInGram.textColor = UIColor.white
        amountBackView.addSubview(lblWeightInGram)
        lblWeightInGram.enableAutolayout()
        lblWeightInGram.add(toLeft: 8, of: txtGold2)
        lblWeightInGram.centerY(to: txtGold2)
        lblWeightInGram.leadingMargin(25)
        
        
        lblAmount2.text = "₹ 0"
        lblAmount2.textAlignment = .right
        lblAmount2.textColor = .black
        lblAmount2.font = UIFont(name: Fonts.latoRegular, size: 22)
        amountBackView.addSubview(lblAmount2)
        lblAmount2.enableAutolayout()
        lblAmount2.belowView(20, to: txtGold2)
        lblAmount2.trailingMargin(30)
        lblAmount2.fixHeight(30)
        lblAmount2.bottomMargin(50)
        
        let lblMaxAmount = UILabel()
        lblMaxAmount.text = "Max loan Amount "
        lblMaxAmount.adjustsFontSizeToFitWidth = true
        lblMaxAmount.textAlignment = .left
        lblMaxAmount.textColor = .white
        lblMaxAmount.font = UIFont(name: Fonts.latoRegular, size: 18)
        amountBackView.addSubview(lblMaxAmount)
        lblMaxAmount.enableAutolayout()
        lblMaxAmount.leadingMargin(25)
        lblMaxAmount.centerY(to: lblAmount2)
        lblMaxAmount.fixHeight(25)
        lblMaxAmount.add(toLeft: 8, of: lblAmount2)
        
        let lblMsg = UILabel()
        lblMsg.numberOfLines = 0
        lblMsg.text = "Muthoot Gold Loan from home accepts only gold of 22KT purity.\nLoan value will depend on weight of gold and current market rate."
        lblMsg.textAlignment = .left
        lblMsg.font = UIFont(name: Fonts.latoRegular, size: 15)
        lblMsg.textColor = UIColor.black
        viewBack.addSubview(lblMsg)
        lblMsg.enableAutolayout()
        lblMsg.belowView(20, to: amountBackView)
        lblMsg.leadingMargin(30)
        lblMsg.trailingMargin(30)
        
        lblDesc.numberOfLines = 0
        lblDesc.text = ""
        lblDesc.textAlignment = .left
        lblDesc.font = UIFont(name: Fonts.latoRegular, size: 15)
        lblDesc.textColor = Colors.textGray
        viewBack.addSubview(lblDesc)
        lblDesc.enableAutolayout()
        lblDesc.belowView(30, to: lblMsg)
        lblDesc.leadingMargin(30)
        lblDesc.trailingMargin(30)
        lblDesc.flexibleHeightGreater(25)
        lblDesc.bottomMargin(20)
        
        
        /*sliderLableP = UILabel()
        sliderLableP.clipsToBounds = true
        sliderLableP.isHidden = true
        sliderLableP.backgroundColor = Colors.theme
        sliderLableP.textAlignment = NSTextAlignment.center
        sliderLableP.textColor = UIColor.white
        sliderLableP.font = UIFont(name: Fonts.HelveticaRegular, size: 12)
        sliderLableP.layer.cornerRadius = 4
        viewBack.addSubview(sliderLableP)
        sliderLableP.enableAutolayout()
        sliderLableP.aboveView(-7, to: slider)
        sliderLableP.centerX()
        sliderLableP.fixWidth(80)
        sliderLableP.fixHeight(25)
        
        
        arrowImageView.isHidden = true
        arrowImageView.clipsToBounds = true
        arrowImageView.contentMode = .scaleToFill
        if #available(iOS 13.0, *) {
            arrowImageView.image = UIImage(named: "Polygon", in: Bundle(for: type(of: self)), with: nil)
        } else {
            arrowImageView.image = UIImage(named: "Polygon", in: Bundle(for: type(of: self)), compatibleWith : nil)
        }
        viewBack.addSubview(arrowImageView)
        arrowImageView.enableAutolayout()
        arrowImageView.fixHeight(8)
        arrowImageView.fixWidth(10)
        arrowImageView.belowView(-1, to: sliderLableP)
        arrowImageView.centerX(to: sliderLableP)
        
        
        let lblgm = UILabel()
        lblgm.text = "gm"
        lblgm.textAlignment = .left
        lblgm.textColor = .white
        lblgm.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title + 2)
        amountBackView.addSubview(lblgm)
        lblgm.enableAutolayout()
        lblgm.centerY(to: txtGold)
        lblgm.add(toRight: 16, of: txtGold)
        lblgm.fixWidth(50)
        lblgm.fixHeight(40)
        
        txtAmount.isUserInteractionEnabled = false
        txtAmount.setRightPaddingPoints(8)
        txtAmount.setLeftPaddingPoints(8)
        txtAmount.keyboardType = .decimalPad
        txtAmount.textAlignment = .left
        txtAmount.inputAccessoryView = toolbar
        txtAmount.text = ""
        txtAmount.tintColor = Colors.theme
        txtAmount.textColor = UIColor.black
        txtAmount.backgroundColor = .white
        txtAmount.layer.borderColor = UIColor.black.cgColor
        txtAmount.layer.borderWidth = 1
        txtAmount.layer.cornerRadius = 8
        txtAmount.font = UIFont(name: Fonts.latoRegular, size: TextSize.title + 2)
        txtAmount.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        amountBackView.addSubview(txtAmount)
        txtAmount.enableAutolayout()
        txtAmount.add(toRight: 16, of: lblgm)
        txtAmount.centerY(to: txtGold)
        txtAmount.fixWidth(120)
        txtAmount.fixHeight(40)
        
        let lblPrice = UILabel()
        lblPrice.text = "₹"
        lblPrice.textAlignment = .left
        lblPrice.textColor = .white
        lblPrice.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title + 2)
        amountBackView.addSubview(lblPrice)
        lblPrice.enableAutolayout()
        lblPrice.centerY(to: txtGold)
        lblPrice.add(toRight: 16, of: txtAmount)
        lblPrice.fixWidth(20)
        lblPrice.fixHeight(40)
         
        
        txtAmount2.setRightPaddingPoints(8)
        txtAmount2.setLeftPaddingPoints(8)
        txtAmount2.keyboardType = .decimalPad
        txtAmount2.textAlignment = .left
        txtAmount2.inputAccessoryView = toolbar
        txtAmount2.text = ""
        txtAmount2.tintColor = Colors.theme
        txtAmount2.textColor = UIColor.black
        txtAmount2.backgroundColor = .white
        txtAmount2.layer.borderColor = UIColor.black.cgColor
        txtAmount2.layer.borderWidth = 1
        txtAmount2.layer.cornerRadius = 8
        txtAmount2.font = UIFont(name: Fonts.latoRegular, size: TextSize.title + 2)
        txtAmount2.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        amountBackView.addSubview(txtAmount2)
        txtAmount2.enableAutolayout()
        txtAmount2.add(toRight: 16, of: lblgm2)
        txtAmount2.centerY(to: txtGold2)
        txtAmount2.fixWidth(120)
        txtAmount2.fixHeight(40)
        
        let lblPrice2 = UILabel()
        lblPrice2.text = "₹"
        lblPrice2.textAlignment = .left
        lblPrice2.textColor = .white
        lblPrice2.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title + 2)
        amountBackView.addSubview(lblPrice2)
        lblPrice2.enableAutolayout()
        lblPrice2.centerY(to: txtAmount2)
        lblPrice2.add(toRight: 16, of: txtAmount2)
        lblPrice2.fixWidth(20)
        lblPrice2.fixHeight(40) */
        
        
        
        
        
        
    }
    
    //MARK:- Fetch Response
    fileprivate func setupData() {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1
        lblService.attributedText = NSMutableAttributedString(string: objGoldRate?.data?.header ?? "", attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        lblService.textAlignment = .center
        lblService.font = UIFont(name: Fonts.acuminProRegular, size: TextSize.title + 6)
        
        lblDesc.text = objGoldRate?.data?.disclaimer ?? ""
        lblSliderMinValue.text = "₹ " + (objGoldRate?.data?.loanRequest?.minAmount ?? 0).delimiter
        lblSliderMaxValue.text = "₹ " + (objGoldRate?.data?.loanRequest?.maxAmount ?? 0).delimiter
        
        sliderLableP.text = "₹ " + (objGoldRate?.data?.loanRequest?.minAmount ?? 0).delimiter
        
        slider.minimumValue = Float(objGoldRate?.data?.loanRequest?.minAmount ?? 0)
        slider.maximumValue = Float(objGoldRate?.data?.loanRequest?.maxAmount ?? 0)
        
        txtCaret.text = (objGoldRate?.data?.goldRate?.purity ?? "") + " ct"
        txtAmount.text = slider.minimumValue.delimiter
        
        let amount = Float(Double(objGoldRate?.data?.goldRate?.nonSolidRate ?? "0.0") ?? 0.0)
        txtGold.text = (slider.value / amount).delimiter
        
        finalAmount = Float(objGoldRate?.data?.loanRequest?.minAmount ?? 0)
    }
    
    //MARK:- Handle Buttons
    @objc func backBtnTapped(_ sender : UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func nextBtnTapped(_ sender : UIButton) {
        

        
        let minAmt = (objGoldRate?.data?.loanRequest?.minAmount ?? 0)
        let maxAmt = (objGoldRate?.data?.loanRequest?.maxAmount ?? 0)
        
        if (finalAmount < Float(minAmt)) {
            self.showToast(message: "Loan amount should be greater than \(minAmt)")
        }
        
        else if (finalAmount > Float(maxAmt)) {
            let popup = MaxAmountPopup()
            popup.delegate = self
            popup.frame = view.bounds
            popup.createUI(maxAmnt: "\(maxAmt)")
            self.view.addSubview(popup)
        }
        
        else {
            Loader.shared.StartActivityIndicator(obj: self)
            let locationDict = ["enquiryId": self.enquiryId, "loanAmount": finalAmount] as [String : Any]
            let mainDict = ["eventName": "sendLoanAmount", "encryption": false,"data":locationDict ] as [String : Any]
            SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
        }
    }
    
    //MARK:- Go To Address List View Controller
    @objc func gotoAddressList(notification: Notification) {
        
        Loader.shared.StopActivityIndicator(obj: self)
        
        let dict = notification.userInfo as NSDictionary?
        let jsonData = try? JSONSerialization.data(withJSONObject: dict!, options: [])
        do {
            let data = try JSONDecoder().decode(AddressListModel.self, from: jsonData!)
            if (data.status ?? false) {
                if ((data.data?.addresses?.count ?? 0) == 0) {
                    DispatchQueue.main.async {
                        let vc = AddAddressVC()
                        vc.enquiryId = self.enquiryId
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                }
                else {
                    let vc = AddressListVC()
                    vc.objAddress = data
                    vc.enquiryId = enquiryId
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
            else {
                self.showToast(message: (data.message ?? ""))
            }
        }
        catch let error {
           // print(error)
        }
    }
    
    @objc func sliderValueDidChange(sender:UISlider!)
    {
        view.endEditing(true)
        let step: Float = 10000
        let roundedValue = round(sender.value / step) * step
        sender.value = roundedValue
        
        
        let trackRect = sender.trackRect(forBounds: sender.frame)
        let thumbRect = sender.thumbRect(forBounds: sender.bounds, trackRect: trackRect, value: sender.value)
        sliderLableP.isHidden = false
        arrowImageView.isHidden = false
        
        sliderLableP.text =  "₹ " + slider.value.delimiter
       // sliderLableP.center = CGPoint(x: thumbRect.midX, y: sliderLableP.center.y)
        arrowImageView.center = CGPoint(x: thumbRect.midX, y: (sliderLableP.center.y + sliderLableP.frame.size.height) - 9)
        
        txtAmount.text = slider.value.delimiter
        let amount = Float(Double(objGoldRate?.data?.goldRate?.nonSolidRate ?? "0.0") ?? 0.0)
        txtGold.text = (sender.value / amount).delimiter
        
        finalAmount = slider.value
        
        txtAmount2.text = ""
        lblAmount2.text = "₹ 0"
        txtGold2.text = ""
    }
    
    @objc func handleError(notification : Notification) {
        Loader.shared.StopActivityIndicator(obj: self)
        
        if let dict = notification.userInfo as NSDictionary? {
            if let msg = dict["message"]  as? String {
                showToast(message: msg )
            }
        }
    }
}

extension CalculateLoanVC : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        lastIndex = textField.tag
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        if let nextResponder :UIResponder  = textField.superview?.viewWithTag(nextTag) as UIResponder? {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return false
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if (textField.text != "") {
            if (textField == txtGold2) { // Amount value did change //calculate gram
                
                txtGold.text = ""
                txtAmount.text = ""
                
                let amt = Float(textField.text ?? "0.0")! * Float((self.objGoldRate?.data?.goldRate?.nonSolidRate ?? "0.0"))!
                txtAmount.text = amt.delimiter
                slider.setValue(Float(amt), animated: true)
                
                let trackRect = slider.trackRect(forBounds: slider.frame)
                let thumbRect = slider.thumbRect(forBounds: slider.bounds, trackRect: trackRect, value: slider.value)
                sliderLableP.isHidden = false
                arrowImageView.isHidden = false
                
                sliderLableP.text =  "₹ " + "\(amt)"
                lblAmount2.text =  "₹ " + "\(amt)"
                
                finalAmount = amt
                
                //"₹ \(sender.value)" //Polygon
               // sliderLableP.center = CGPoint(x: thumbRect.midX, y: sliderLableP.center.y)
                //arrowImageView.center = CGPoint(x: thumbRect.midX, y: (sliderLableP.center.y + sliderLableP.frame.size.height) - 9)
                self.view.layoutIfNeeded()
            }
            else if (textField == txtGold) { // Gram value did change // Calculate Amount
                
                txtGold2.text = ""
                txtAmount2.text = ""
                
                let amt = Float(textField.text ?? "0.0")! * Float((self.objGoldRate?.data?.goldRate?.nonSolidRate ?? "0.0"))!
                txtAmount.text = amt.delimiter
                slider.setValue(Float(amt), animated: true)
                
                let trackRect = slider.trackRect(forBounds: slider.frame)
                let thumbRect = slider.thumbRect(forBounds: slider.bounds, trackRect: trackRect, value: slider.value)
                sliderLableP.isHidden = false
                arrowImageView.isHidden = false
                lblAmount2.text =  "₹ 0"
                
                sliderLableP.text =  "₹ " + "\(amt)"
                finalAmount = amt
                
                //"₹ \(sender.value)" //Polygon
               // sliderLableP.center = CGPoint(x: thumbRect.midX, y: sliderLableP.center.y)
                //arrowImageView.center = CGPoint(x: thumbRect.midX, y: (sliderLableP.center.y + sliderLableP.frame.size.height) - 9)
                
                self.view.layoutIfNeeded()
            }
        }
    }
}

extension CalculateLoanVC : MaxAmountPr {
    func value() {
        let maxAmt = (objGoldRate?.data?.loanRequest?.maxAmount ?? 0)
        Loader.shared.StartActivityIndicator(obj: self)
        let locationDict = ["enquiryId": self.enquiryId, "loanAmount": maxAmt] as [String : Any]
        let mainDict = ["eventName": "sendLoanAmount", "encryption": false,"data":locationDict ] as [String : Any]
        SocketHelper.shared.emitForSocket(eventName: "req", arg: mainDict)
    }
}


extension Bundle {
    public static let myFramework = Bundle(identifier: "com.unfyd.Muthoot-Track")
}


