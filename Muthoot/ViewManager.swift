//
//  ViewManager.swift
//  MuthootUser
//
//  Created by SmartConnect Technologies on 04/05/21.
//  Copyright © 2021 SmartConnect Technologies. All rights reserved.
//

import UIKit

let screenWidth = UIScreen.main.bounds.width
let screenHeight = UIScreen.main.bounds.height
let base = BaseController()
let toastFailure = "toastFailure"
let toastSuccess = "toastSucces"


struct Colors {
    
    static let theme = UIColor.rgb(red: 0, green: 149, blue: 217) 
    static let gray = UIColor.rgb(red: 175, green: 175, blue: 175)
    static let textGray = UIColor.rgb(red: 146, green: 146, blue: 146)
    static let darkGray = UIColor.black.withAlphaComponent(0.7)
    static let viewBackground = UIColor.rgb(red: 244, green: 244, blue: 244)//229.5
    static let green = UIColor.rgb(red: 1, green: 160, blue: 36)
    static let red = UIColor.rgb(red: 232, green: 69, blue: 60)
    static let separator = UIColor(hexString: "#EAEAEA")!
}

struct Fonts {
    
    static let HelveticaNeueMedium = "AcuminPro-Regular"
    static let HelveticaRegular = "Lato-Regular"
    static let acuminProRegular = "AcuminPro-Regular"
    static let latoRegular = "Lato-Regular"
    
    
  //  static let latoMedium = "Lato-Medium"
  //  static let latoBold = "Lato-Bold"
  //  static let latoBlack = "Lato-Black"
    
    
  //  static let acuminProMedium = "AcuminPro-Medium"
  //  static let acuminProBold = "AcuminPro-Bold"
}
    

struct TextSize {
    static let title:CGFloat = Device.isIpad ? 18 : 16
    static let subTitle:CGFloat = Device.isIpad ? 16 : 14
    static let header:CGFloat = Device.isIpad ? 21 : 17
    static let mainHeader:CGFloat = Device.isIpad ? 29 : 25
}

 struct Constraints {
    static let bottom:CGFloat = isIphoneX() ? 35 : 0
    // static let top:CGFloat = isIphoneX() ? 85 : Device.isIpad ? 85 : 65
    static let lead:CGFloat = Device.isIpad ? 20 : 10
    static var top: CGFloat {
        var statusBarHeight: CGFloat = 0
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
            statusBarHeight = (window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0) + (base.navigationController?.navigationBar.frame.height ?? 0)
        } else {
            statusBarHeight = UIApplication.shared.statusBarFrame.height + (base.navigationController?.navigationBar.frame.height ?? 0)
        }
        return statusBarHeight + 10
    }
}

public class Device {
    public class var isIpad:Bool {
        if #available(iOS 8.0, * ) {
            return UIScreen.main.traitCollection.userInterfaceIdiom == .pad
        } else {
            return UIDevice.current.userInterfaceIdiom == .pad
        }
    }
    public class var isIphone:Bool {
        if #available(iOS 8.0, *) {
            return UIScreen.main.traitCollection.userInterfaceIdiom == .phone
        } else {
            return UIDevice.current.userInterfaceIdiom == .phone
        }
    }
}

extension UIDevice {
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    
    var isShort: Bool {
        if UIDevice.current.screenType.rawValue == UIDevice.ScreenType.iPhones_5_5s_5c_SE.rawValue || UIDevice.current.screenType.rawValue == UIDevice.ScreenType.iPhone4_4S.rawValue {
            return true
        }
        return false
    }
    
    enum ScreenType: String {
        case iPhone4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhoneX = "iPhone X"
        case unknown
    }
    
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhoneX
        default:
            return .unknown
        }
    }
}
